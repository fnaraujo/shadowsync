create table shadowsync_info
(
	id_shadowsync_info bigint,
	ult_alt_forma_pgto timestamp,
	ult_alt_funcionario timestamp,
	ult_alt_estoque timestamp,
	ult_alt_saldo timestamp,
	constraint shadowsync_info_pk primary key (id_shadowsync_info)
);

alter table shadowsync_info add column ult_envio_estoque timestamp without time zone;
alter table shadowsync_info add column ult_envio_saldo timestamp without time zone;

insert into shadowsync_info values (1,null,null,null,null,null,null);

update shadowsync_info set ult_alt_forma_pgto = (select data_ult_alteracao as ult_atualizacao from loja where id_loja = 1)
	where ult_alt_forma_pgto is null;
	
update shadowsync_info set ult_alt_funcionario = (select data_ult_alteracao as ult_atualizacao from loja where id_loja = 1)
	where ult_alt_funcionario is null;
	
update shadowsync_info set ult_alt_estoque = (select data_ult_alteracao as ult_atualizacao from loja where id_loja = 1)
	where ult_alt_estoque is null;

update shadowsync_info set ult_alt_saldo = (select data_ult_alteracao as ult_atualizacao from loja where id_loja = 1)
	where ult_alt_saldo is null;

update shadowsync_info set ult_envio_estoque = (select data_ult_alteracao as ult_atualizacao from loja where id_loja = 1)
	where ult_envio_estoque is null;
	
update shadowsync_info set ult_envio_saldo = (select data_ult_alteracao as ult_atualizacao from loja where id_loja = 1)
	where ult_envio_saldo is null;

update loja set metodo_classificacao = 0 where metodo_classificacao is null;

update loja set nivel_classificacao = 0 where nivel_classificacao is null;

alter table loja add column id_shadowsync_info bigint;

alter table loja add constraint loja_shadowsyncinfo_fk foreign key(id_shadowsync_info) 
				references shadowsync_info (id_shadowsync_info) on update no action 
										on delete no action;

update loja set id_shadowsync_info = 1 where id_loja = 1;

alter table estoque_produto add constraint produto_loja_unq unique(id_loja, id_produto); 

CREATE UNIQUE INDEX only_one_preco_padrao_true
  ON estoque_preco
  USING btree
  (preco_padrao, id_estoque)
  WHERE preco_padrao;

alter table shadowsync_info add column ult_alt_preco timestamp without time zone;
alter table shadowsync_info add column ult_envio_preco timestamp without time zone;

update shadowsync_info set ult_alt_preco = (select data_ult_alteracao as ult_atualizacao from loja where id_loja = 1)
	where ult_alt_preco is null;
	
update shadowsync_info set ult_envio_preco = (select data_ult_alteracao as ult_atualizacao from loja where id_loja = 1)
	where ult_envio_preco is null;