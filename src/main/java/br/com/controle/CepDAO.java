package br.com.controle;

import br.com.controle.exception.InfraEstruturaException;
import br.com.controle.exception.ObjetoNaoEncontradoException;
import br.com.modelo.Cep;
import br.com.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.StatelessSession;

/**
 *
 * @author Sisbras
 */
public class CepDAO {

    public long inclui(Cep cep, String session) {
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }
            sessao.save(cep);

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return cep.getIdCep();
    }
    

    public long saveOrUpdate(Cep cep, String session) {
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }
            sessao.saveOrUpdate(cep);

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return cep.getIdCep();
    }

    public Cep retornaUmCep(String numero, String session) throws ObjetoNaoEncontradoException {

        Cep obj = null;
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }

            String hql = "SELECT obj FROM Cep as obj "
                    + " where obj.numero = :NUMERO";

            Query qry = sessao.createQuery(hql);

            qry.setParameter("NUMERO", numero);

            obj = (Cep) qry.uniqueResult();

            if (obj == null) {
                throw new ObjetoNaoEncontradoException();
            }

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return obj;
    }
    
    public Cep retornaUmCep(String numero, StatelessSession sessao) throws ObjetoNaoEncontradoException {

        Cep obj = null;
        try {

            String hql = "SELECT obj FROM Cep as obj "
                    + " where obj.numero = :NUMERO";

            Query qry = sessao.createQuery(hql);

            qry.setParameter("NUMERO", numero);

            obj = (Cep) qry.uniqueResult();

            if (obj == null) {
                throw new ObjetoNaoEncontradoException();
            }

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return obj;
    }

    public Cep retornaUmCepLazy(String numero, String session) throws ObjetoNaoEncontradoException {

        Cep obj = null;
        try {
            StatelessSession sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSessionFactory().openStatelessSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionFactoryRemote().openStatelessSession();
                    break;
                default:
                    sessao = HibernateUtil.getSessionFactory().openStatelessSession();

            }

            String hql = "SELECT obj.idCep"
                    + "         ,obj.numero"
                    + "         ,obj.logradouro"
                    + "         ,obj.logradouroAbrev"
                    + "         ,obj.tipo"
                    + "   FROM "
                    + "      Cep as obj "
                    + " where "
                    + "      obj.numero = :NUMERO";

            Query qry = sessao.createQuery(hql);

            qry.setParameter("NUMERO", numero);

            Object[] objs = (Object[]) qry.uniqueResult();

            if (objs != null
                    && objs.length > 0) {
                obj = new Cep();
                obj.setIdCep((long) objs[0]);
                obj.setNumero((String) objs[1]);
                obj.setLogradouro((String) objs[2]);
                obj.setLogradouroAbrev((String) objs[3]);
                obj.setTipo((String) objs[4]);
            }

            if (obj == null) {
                throw new ObjetoNaoEncontradoException();
            }

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return obj;
    }
}
