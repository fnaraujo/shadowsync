package br.com.controle;

import br.com.controle.exception.InfraEstruturaException;
import br.com.modelo.EstoquePreco;
import br.com.modelo.EstoqueProduto;
import br.com.modelo.Loja;
import br.com.util.HibernateUtil;
import java.util.Date;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Sisbras
 */
public class EstoquePrecoDAO {

    public long inclui(EstoquePreco estoquePreco, String session) {
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }
            sessao.save(estoquePreco);

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return estoquePreco.getIdEstoquePreco();
    }

    public long altera(EstoquePreco estoquePreco, String session) {
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }
            sessao.update(estoquePreco);

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return estoquePreco.getIdEstoquePreco();
    }

    public long saveOrUpdate(EstoquePreco estoquePreco, String session) {
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }
            sessao.saveOrUpdate(estoquePreco);

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return estoquePreco.getIdEstoquePreco();
    }

    public EstoquePreco merge(EstoquePreco estoquePreco, String session) {
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }
            return (EstoquePreco) sessao.merge(estoquePreco);

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }
    }

    public void deletePrecos(EstoqueProduto ep, String session) {
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }

            String sql = "delete from estoque_preco where id_estoque = :ID_ESTOQUE";

            Query qry = sessao.createSQLQuery(sql);
            qry.setParameter("ID_ESTOQUE", ep.getIdEstoque());

            qry.executeUpdate();

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }
    }

    public List<EstoquePreco> listaPrecos(Date ultimaAtualizacao, Loja loja, String session) {
        List<EstoquePreco> lista;
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();
            }

            String sql = "From EstoquePreco prec "
                    + "     JOIN FETCH prec.estoqueProduto.loja "
                    + "     WHERE "
                    + "         prec.atualizacaoPreco > :ULTIMAALTERACAO "
                    + "     AND "
                    + "         prec.estoqueProduto.loja.empresa.cnpj = :CNPJ "
                    + "     ORDER BY "
                    + "         prec.estoqueProduto.idEstoque, prec.numOrdem ";

            Query qry = sessao.createQuery(sql);
            qry.setParameter("ULTIMAALTERACAO", ultimaAtualizacao);
            qry.setParameter("CNPJ", loja.getEmpresa().getCnpj());

            lista = qry.list();

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }
        return lista;
    }

    public List<EstoquePreco> listaPrecos(Date ultimaAtualizacao, String session) {
        List<EstoquePreco> lista;
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();
            }

            String sql = "From EstoquePreco prec "
                    + "     JOIN FETCH prec.estoqueProduto.loja "
                    + "     WHERE "
                    + "         prec.atualizacaoPreco > :ULTIMAALTERACAO "
                    + "     ORDER BY "
                    + "         prec.estoqueProduto.idEstoque, prec.numOrdem ";

            Query qry = sessao.createQuery(sql);
            qry.setParameter("ULTIMAALTERACAO", ultimaAtualizacao);

            lista = qry.list();

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }
        return lista;
    }

    public EstoquePreco retornaUmPreco(String codigo, int numOrdem, Loja loja, String session) {
        EstoquePreco preco = null;
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();
            }

            String sql = ""
                    + " select "
                    + "     * "
                    + "  from "
                    + "     estoque_preco prec "
                    + " inner join "
                    + "     estoque_produto ep using (id_estoque) "
                    + " inner join "
                    + "     produto p using (id_produto) "
                    + " where "
                    + "     p.codigo = '" + codigo + "' "
                    + " and "
                    + "     prec.num_ordem = " + numOrdem
                    + " and "
                    + "     ep.id_loja = " + loja.getIdLoja();

            Query qry = sessao.createSQLQuery(sql).addEntity(EstoquePreco.class);

            preco = (EstoquePreco) qry.uniqueResult();

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }
        return preco;
    }

    public void registraLog(EstoquePreco preco, String session) {
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();
            }

            if (preco == null) {
                return;
            }

            String sql = "insert into log_estoque_preco "
                    + " select "
                    + "     nextval('sq_log_estoque_preco') "
                    + "    ,id_estoque "
                    + "    ,id_tabela_preco "
                    + "    ,valor_venda "
                    + "    ,valor_desconto "
                    + "    ,data_limite_desconto "
                    + "    ,now() "
                    + "    , null"
                    + "    ,preco_padrao "
                    + "    ,num_ordem "
                    + "    , 'shadowsync'"
                    + "   from "
                    + "         estoque_preco"
                    + "   where"
                    + "         id_estoque_preco = " + preco.getIdEstoquePreco();

            sessao.createSQLQuery(sql).executeUpdate();

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }
    }
}
