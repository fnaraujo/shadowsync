package br.com.controle;

import br.com.controle.exception.InfraEstruturaException;
import br.com.controle.exception.ObjetoNaoEncontradoException;
import br.com.modelo.CstIPI;
import br.com.util.HibernateUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author flavio.nunes
 */
public class CstIpiDAO {

    public long inclui(CstIPI cstIpi, String session) {
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }            
            sessao.save(cstIpi);

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return cstIpi.getIdCstIpi();
    }

    public CstIPI retornaUmCstIpi(String codigo, String session) throws ObjetoNaoEncontradoException {
        CstIPI obj = null;
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }

            String hql = " SELECT obj from CstIPI as obj"                    
                    + " WHERE 1=1"
                    + "AND obj.codigo = '" + codigo + "'";

            obj = (CstIPI) sessao.createQuery(hql).uniqueResult();

            if (obj == null) {
                throw new ObjetoNaoEncontradoException();
            }
        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return obj;
    }
    
    @SuppressWarnings("unchecked")
    public List<CstIPI> listaCstIPI(String session) {
        List<CstIPI> lista = null;
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }

            String hql = " SELECT obj from CstIPI as obj"                    
                    + " WHERE 1=1";

            Query qry = sessao.createQuery(hql);

            lista = qry.list();


        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return lista;
    }
}
