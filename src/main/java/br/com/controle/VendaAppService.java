package br.com.controle;

import br.com.controle.exception.InfraEstruturaException;
import br.com.controle.exception.ObjetoNaoEncontradoException;
import br.com.modelo.FormaPagamento;
import br.com.modelo.ItemVenda;
import br.com.modelo.LogStatusVenda;
import br.com.modelo.Loja;
import br.com.modelo.LojaDepartamento;
import br.com.modelo.PagamentoVenda;
import br.com.modelo.StatusVenda;
import br.com.modelo.Venda;
import br.com.util.HibernateUtil;
import br.com.visao.FrmGerenciaConexao;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Administrador
 */
public class VendaAppService {

    private VendaDAO vendaDAO = new VendaDAO();
    private FormaPagamentoDAO formaPgtoDAO = new FormaPagamentoDAO();
    private ClienteDAO clienteDAO = new ClienteDAO();
    private ProdutoDAO produtoDAO = new ProdutoDAO();

    private void inicializaTransacao(String session) throws InfraEstruturaException {
        switch (session.toLowerCase()) {
            case "local":
                HibernateUtil.beginTransaction();
                break;
            case "remote":
                HibernateUtil.beginTransactionRemote();
                break;
            default:
                HibernateUtil.beginTransaction();

        }
    }

    private void confirmaTransacao(String session) throws InfraEstruturaException {
        switch (session.toLowerCase()) {
            case "local":
                HibernateUtil.commitTransaction();
                break;
            case "remote":
                HibernateUtil.commitTransactionRemote();
                break;
            default:
                HibernateUtil.commitTransaction();

        }

    }

    private void retornaTransacao(String session) throws InfraEstruturaException {
        switch (session.toLowerCase()) {
            case "local":
                HibernateUtil.rollbackTransaction();
                break;
            case "remote":
                HibernateUtil.rollbackTransactionRemote();
                break;
            default:
                HibernateUtil.rollbackTransaction();

        }

    }

    public long inclui(Venda venda, String session) {
        try {

            inicializaTransacao(session);

            long numero = vendaDAO.inclui(venda, session);

            confirmaTransacao(session);

            return numero;
        } catch (InfraEstruturaException e) {
            try {
                retornaTransacao(session);
            } catch (InfraEstruturaException ie) {
            }

            throw e;
        }
    }

    public void altera(Venda venda, String session) {
        try {
            inicializaTransacao(session);

            vendaDAO.altera(venda, session);

            confirmaTransacao(session);

        } catch (InfraEstruturaException e) {
            try {
                retornaTransacao(session);
            } catch (InfraEstruturaException ie) {
            }

            throw e;
        }
    }

    public Venda retornaUmaFormaPagamento(Long id, String session)
            throws Exception {
        try {
            inicializaTransacao(session);

            Venda venda = vendaDAO.retornaUmaVenda(id, session);

            confirmaTransacao(session);

            return venda;

        } catch (ObjetoNaoEncontradoException e) {
            try {
                retornaTransacao(session);
            } catch (InfraEstruturaException ie) {
                throw ie;
            }

            throw e;
        } catch (InfraEstruturaException e) {
            try {
                HibernateUtil.rollbackTransaction();
            } catch (InfraEstruturaException ie) {
            }

            throw e;
        }
    }

    public List<Venda> listaVendasWeb(String session) {

        List<Venda> lista = null;

        try {
            inicializaTransacao(session);

            lista = vendaDAO.listaVendasWeb();

            confirmaTransacao(session);
        } catch (InfraEstruturaException e) {
            try {
                retornaTransacao(session);
            } catch (InfraEstruturaException ie) {
            }

            throw e;
        }

        return lista;
    }

    public void atualizaVendaRemoto(Date ultimaAlteracao) {
        try {
            
            inicializaTransacao(FrmGerenciaConexao.BASE_LOCAL);
            inicializaTransacao(FrmGerenciaConexao.BASE_REMOTA);
            
            List<Venda> statusVendasCanc = vendaDAO.listaVendasWebCanceladasLocal(ultimaAlteracao);
            
            List<Venda> statusVendasFinal = vendaDAO.listaVendasWebFinalizadasLocal(ultimaAlteracao);
            
            vendaDAO.atualizaVendaWeb(statusVendasCanc, statusVendasFinal);
            
            confirmaTransacao(FrmGerenciaConexao.BASE_REMOTA);
            confirmaTransacao(FrmGerenciaConexao.BASE_LOCAL);
            
        } catch (InfraEstruturaException e) {
            
            try {
                retornaTransacao(FrmGerenciaConexao.BASE_REMOTA);
                retornaTransacao(FrmGerenciaConexao.BASE_LOCAL);
            } catch (InfraEstruturaException ie) {
            }
            throw e;
            
        }
    }

    public void atualizaLocal(List<Venda> vendas)
            throws Exception {
        try {
            inicializaTransacao(FrmGerenciaConexao.BASE_LOCAL);

            inicializaTransacao(FrmGerenciaConexao.BASE_REMOTA);

            for (Venda venda : vendas) {

                Venda vendaAtualizacao = new Venda();

                vendaAtualizacao.setBoletoEmCarne(false);
                vendaAtualizacao.setCliente(clienteDAO.retornaUmCliente(venda.getCliente().getCpfCnpj(), FrmGerenciaConexao.BASE_LOCAL));
                vendaAtualizacao.setCustoDevolucao(BigDecimal.ZERO);
                vendaAtualizacao.setCustoReposicao(BigDecimal.ZERO);
                vendaAtualizacao.setDataEntrega(null);
                vendaAtualizacao.setDataVenda(venda.getDataVenda());
                vendaAtualizacao.setDepartamento(new LojaDepartamento(1, ""));
                vendaAtualizacao.setEnderecoIp(null);
                vendaAtualizacao.setFromOrcamento(false);
                vendaAtualizacao.setGerarBoleto(false);
                vendaAtualizacao.setGerouComissao(false);
                vendaAtualizacao.setHash(null);
                vendaAtualizacao.setLoja(new Loja(1, ""));
                vendaAtualizacao.setLucroLiquido(BigDecimal.ZERO);
                vendaAtualizacao.setLucroVenda(BigDecimal.ZERO);
                vendaAtualizacao.setNumeroCcf(null);
                vendaAtualizacao.setObservacao(venda.getObservacao());
                vendaAtualizacao.setOpcaoEntrega(venda.getOpcaoEntrega());
                vendaAtualizacao.setStatusVenda(1);
                vendaAtualizacao.setTotalAcrescimo(venda.getTotalAcrescimo());
                vendaAtualizacao.setTotalComissao(venda.getTotalComissao());
                vendaAtualizacao.setTotalDesconto(venda.getTotalDesconto());
                vendaAtualizacao.setTotalDevolucao(venda.getTotalDevolucao());
                vendaAtualizacao.setTotalVenda(venda.getTotalVenda());
                vendaAtualizacao.setTotalPagar(venda.getTotalPagar());
                vendaAtualizacao.setFuncionario(venda.getFuncionario());

                vendaAtualizacao.setIdPedidoWeb(venda.getIdVenda());

                for (int i = 0; i < venda.getPagamentos().size(); i++) {

                    FormaPagamento forma = formaPgtoDAO.retornaUmaFormaDePagamento(venda.getPagamentos().get(i).getDescricao(), FrmGerenciaConexao.BASE_LOCAL);

                    PagamentoVenda pgto = new PagamentoVenda();
                    pgto.setDataPagamento(venda.getPagamentos().get(i).getDataPagamento());
                    pgto.setDescricao(venda.getPagamentos().get(i).getDescricao());
                    pgto.setEstornado(false);
                    pgto.setGeraContasReceber(venda.getPagamentos().get(i).getGeraContasReceber());
                    pgto.setPreFaturamento(venda.getPagamentos().get(i).getPreFaturamento());
                    pgto.setTotalPago(venda.getPagamentos().get(i).getTotalPago());
                    pgto.setVenda(vendaAtualizacao);
                    pgto.setFormaPagamento(forma);

                    vendaAtualizacao.getPagamentos().add(pgto);
                }

                for (int i = 0; i < venda.getItemVendas().size(); i++) {
                    ItemVenda itemVenda = new ItemVenda();
                    itemVenda.setVenda(vendaAtualizacao);
                    itemVenda.setNumOrdem(venda.getItemVendas().get(i).getNumOrdem());
                    itemVenda.setQtdVenda(venda.getItemVendas().get(i).getQtdVenda());
                    itemVenda.setPrecoVenda(venda.getItemVendas().get(i).getPrecoVenda());
                    itemVenda.setPrecoTotal(venda.getItemVendas().get(i).getPrecoTotal());
                    itemVenda.setDataInclusao(venda.getItemVendas().get(i).getDataInclusao());
                    itemVenda.setQtdEmbalagem(venda.getItemVendas().get(i).getQtdEmbalagem());
                    itemVenda.setStatusItem(venda.getItemVendas().get(i).getStatusItem());
                    itemVenda.setCodigoFiscalOperacao(1);
                    itemVenda.setDescricaoProduto(venda.getItemVendas().get(i).getDescricaoProduto());
                    itemVenda.setProduto(produtoDAO.retornaUmProduto(venda.getItemVendas().get(i).getProduto().getCodigo(), FrmGerenciaConexao.BASE_LOCAL));

                    vendaAtualizacao.getItemVendas().add(itemVenda);
                }

                vendaDAO.inclui(vendaAtualizacao, FrmGerenciaConexao.BASE_LOCAL);

                LogStatusVenda log = new LogStatusVenda();
                log.setDataCadastro(venda.getDataVenda());
                log.setFuncionario(venda.getFuncionario());
                log.setStatusVenda(new StatusVenda(1, "EM ABERTO"));
                log.setVenda(vendaAtualizacao);
                HibernateUtil.getSession().save(log);

                venda.setStatusVenda(3); // move status remoto para AGUARDANDO_CONCLUSAO
                vendaDAO.altera(venda, FrmGerenciaConexao.BASE_REMOTA);

            }
            confirmaTransacao(FrmGerenciaConexao.BASE_REMOTA);

            confirmaTransacao(FrmGerenciaConexao.BASE_LOCAL);

        } catch (Exception e) {
            try {
                HibernateUtil.rollbackTransaction();

                HibernateUtil.rollbackTransactionRemote();

            } catch (InfraEstruturaException ie) {
            }

            throw e;
        }
    }
}
