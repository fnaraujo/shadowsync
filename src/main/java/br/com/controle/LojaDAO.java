package br.com.controle;

import br.com.controle.exception.InfraEstruturaException;
import br.com.controle.exception.ObjetoNaoEncontradoException;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import br.com.util.HibernateUtil;
import br.com.modelo.Loja;
import br.com.modelo.LojaDepartamento;
import java.util.Date;
import org.hibernate.Query;

/**
 *
 * @author flavio.nunes
 */
public class LojaDAO {

    public long inclui(Loja loja, String session) {
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }
            sessao.save(loja.getEmpresa());
            sessao.save(loja);

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return loja.getIdLoja();
    }

    public void altera(Loja loja, String session) {
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }

            sessao.update(loja);

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

    }

    public Loja retornaUmaLoja(String cnpj, String session) throws ObjetoNaoEncontradoException {
        Loja loja = null;
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }

            String hql = " SELECT l from Loja as l"
                    + " LEFT JOIN FETCH l.empresa"
                    + " WHERE 1=1"
                    + "AND l.empresa.cnpj = '" + cnpj + "'";

            loja = (Loja) sessao.createQuery(hql).uniqueResult();

            if (loja == null) {
                throw new ObjetoNaoEncontradoException();
            }
        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return loja;
    }

    public Loja retornaLojaMaster(String session) throws ObjetoNaoEncontradoException {
        Loja loja = null;
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }

            String hql = " SELECT l from Loja as l"
                    + " LEFT JOIN FETCH l.empresa"
                    + " WHERE 1=1"
                    + "AND l.lojaMaster = TRUE";

            loja = (Loja) sessao.createQuery(hql).uniqueResult();

            if (loja == null) {
                throw new ObjetoNaoEncontradoException();
            }
        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return loja;
    }

    @SuppressWarnings("unchecked")
    public List<Loja> listaLojas(Date ultimaAtualizacao, String session) {
        List<Loja> lista = null;
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }

            String hql = " SELECT l from Loja as l"
                    + " LEFT JOIN FETCH l.empresa"
                    + " where 1=1"
                    + " and l.dataUltAlteracao > :ULTIMAALTERACAO order by l.empresa.codigoid";

            Query qry = sessao.createQuery(hql);
            qry.setParameter("ULTIMAALTERACAO", ultimaAtualizacao);

            lista = qry.list();

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return lista;
    }

    public Loja retornaLojaMasterRomotaNaoImplatada() {
        Loja loja = null;
        try {
            Session sessao = HibernateUtil.getSessionRemote();

            String hql = " SELECT l from Loja as l"
                    + " LEFT JOIN FETCH l.empresa"
                    + " where 1=1"
                    + " and l.dataImplantacaoRemota is null "
                    + " and l.lojaMaster = true ";

            Query qry = sessao.createQuery(hql);

            loja = (Loja) qry.uniqueResult();

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return loja;
    }

    public List<Loja> listaLojas(String session) {
        List<Loja> lista = null;
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }

            String hql = " SELECT l from Loja as l"
                    + " LEFT JOIN FETCH l.empresa"
                    + " where 1=1"
                    + " order by l.empresa.codigoid";

            Query qry = sessao.createQuery(hql);

            lista = qry.list();

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return lista;
    }
    
    public List<Loja> listaOutrasLojas(String session, Loja loja) {
        List<Loja> lista = null;
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }

            String hql = " SELECT l from Loja as l"
                    + " LEFT JOIN FETCH l.empresa"
                    + " where 1=1 "
                    + " and l.empresa.cnpj <> :CNPJ"
                    + " order by l.empresa.codigoid";

            Query qry = sessao.createQuery(hql);
            
            qry.setParameter("CNPJ", loja.getEmpresa().getCnpj());

            lista = qry.list();

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return lista;
    }

    public long incluiDepartamento(LojaDepartamento depto, String session) {
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }
            sessao.save(depto);

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return depto.getIdLojaDepartamento();
    }

    @SuppressWarnings("unchecked")
    public List<LojaDepartamento> listaDepartamentos(Loja loja, String session) {
        List<LojaDepartamento> lista = null;
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }

            String hql = " SELECT l from LojaDepartamento as l"
                    + " where 1=1"
                    + " and l.loja.idLoja = " + loja.getIdLoja();

            Query qry = sessao.createQuery(hql);

            lista = qry.list();

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return lista;
    }
}
