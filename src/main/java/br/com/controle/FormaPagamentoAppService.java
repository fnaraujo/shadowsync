package br.com.controle;

import br.com.controle.exception.InfraEstruturaException;
import br.com.controle.exception.ObjetoNaoEncontradoException;
import br.com.modelo.FormaPagamento;
import br.com.modelo.FormaPagamentoPrazo;
import br.com.modelo.Funcionario;
import br.com.util.HibernateUtil;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Administrador
 */
public class FormaPagamentoAppService {

    private FormaPagamentoDAO formaPgtoDAO = new FormaPagamentoDAO();
    private FuncionarioDAO funcionarioDAO = new FuncionarioDAO();

    private void inicializaTransacao(String session) throws InfraEstruturaException {
        switch (session.toLowerCase()) {
            case "local":
                HibernateUtil.beginTransaction();
                break;
            case "remote":
                HibernateUtil.beginTransactionRemote();
                break;
            default:
                HibernateUtil.beginTransaction();

        }
    }

    private void confirmaTransacao(String session) throws InfraEstruturaException {
        switch (session.toLowerCase()) {
            case "local":
                HibernateUtil.commitTransaction();
                break;
            case "remote":
                HibernateUtil.commitTransactionRemote();
                break;
            default:
                HibernateUtil.commitTransaction();

        }

    }

    private void retornaTransacao(String session) throws InfraEstruturaException {
        switch (session.toLowerCase()) {
            case "local":
                HibernateUtil.rollbackTransaction();
                break;
            case "remote":
                HibernateUtil.rollbackTransactionRemote();
                break;
            default:
                HibernateUtil.rollbackTransaction();

        }

    }

    public long inclui(FormaPagamento formaPgto, String session) {
        try {

            inicializaTransacao(session);

            long numero = formaPgtoDAO.inclui(formaPgto, session);

            confirmaTransacao(session);

            return numero;
        } catch (InfraEstruturaException e) {
            try {
                retornaTransacao(session);
            } catch (InfraEstruturaException ie) {
            }

            throw e;
        }
    }

    public void altera(FormaPagamento formaPgto, String session) {
        try {
            inicializaTransacao(session);

            formaPgtoDAO.altera(formaPgto, session);

            confirmaTransacao(session);

        } catch (InfraEstruturaException e) {
            try {
                retornaTransacao(session);
            } catch (InfraEstruturaException ie) {
            }

            throw e;
        }
    }

    public FormaPagamento retornaUmaFormaPagamento(String descricao, String session)
            throws Exception {
        try {
            inicializaTransacao(session);

            FormaPagamento formaPgto = formaPgtoDAO.retornaUmaFormaDePagamento(descricao, session);

            confirmaTransacao(session);

            return formaPgto;
        } catch (ObjetoNaoEncontradoException e) {
            try {
                retornaTransacao(session);
            } catch (InfraEstruturaException ie) {
                throw ie;
            }

            throw e;
        } catch (InfraEstruturaException e) {
            try {
                HibernateUtil.rollbackTransaction();
            } catch (InfraEstruturaException ie) {
            }

            throw e;
        }
    }

    public List<FormaPagamento> listaFormaPagamentos(Date ultimaAtualizacao, String session) {
        List<FormaPagamento> lista = null;

        try {
            inicializaTransacao(session);

            lista = formaPgtoDAO.listaFormasDePagamento(ultimaAtualizacao, session);

            confirmaTransacao(session);
        } catch (InfraEstruturaException e) {
            try {
                retornaTransacao(session);
            } catch (InfraEstruturaException ie) {
            }

            throw e;
        }

        return lista;
    }

    public void atualizaCadastros(List<FormaPagamento> formas, String session)
            throws Exception {
        try {            
            inicializaTransacao(session);

            Funcionario funcionario = null;
            for (FormaPagamento forma : formas) {

                if(funcionario == null){
                    funcionario = funcionarioDAO.retornaUmFuncionario("1", session);
                }
                
                FormaPagamento formaAtualizacao = null;

                try {
                    formaAtualizacao = formaPgtoDAO.retornaUmaFormaDePagamento(forma.getDescricao(), session);
                } catch (ObjetoNaoEncontradoException e) {
                    formaAtualizacao = new FormaPagamento();                    
                    formaAtualizacao.setDescricao(forma.getDescricao());
                }
                
                formaAtualizacao.setAtivo(forma.getAtivo());
                formaAtualizacao.setLimiteMinimo(forma.getLimiteMinimo());
                formaAtualizacao.setDataCadastro(forma.getDataCadastro());
                formaAtualizacao.setUltimaAlteracao(forma.getUltimaAlteracao());
                formaAtualizacao.setPrecedencia(forma.getPrecedencia());
                formaAtualizacao.setPreFaturamento(forma.getPreFaturamento());
                formaAtualizacao.setgeraContasReceber(forma.getgeraContasReceber());        
                formaAtualizacao.setLimiteMinimo(forma.getLimiteMinimo());        
                formaAtualizacao.setCreditoDevolucao(forma.isCreditoDevolucao());        
                formaAtualizacao.setTipoPagamento(forma.getTipoPagamento());        
                formaAtualizacao.setMaximoDesconto(forma.getMaximoDesconto());
                formaAtualizacao.setFuncionario(funcionario);
                formaAtualizacao.setPermiteDesconto(forma.isPermiteDesconto());
                
                if (forma.getgeraContasReceber()) {
                    for (int i = 0; i < forma.getPrazosPagamento().size(); i++) {
                        try {
                            formaAtualizacao.getPrazosPagamento().get(i).setQtdDias(forma.getPrazosPagamento().get(i).getQtdDias());
                            formaAtualizacao.getPrazosPagamento().get(i).setPercentual(forma.getPrazosPagamento().get(i).getPercentual());
                        } catch (IndexOutOfBoundsException e) {
                            formaAtualizacao.getPrazosPagamento().add(new FormaPagamentoPrazo(0, formaAtualizacao, forma.getPrazosPagamento().get(i).getQtdDias(), forma.getPrazosPagamento().get(i).getPercentual()));
                        }
                    }
                }
                if(formaAtualizacao.getIdFormaPagamento() > 0){
                    formaPgtoDAO.altera(formaAtualizacao, session);
                }else{
                    formaPgtoDAO.inclui(formaAtualizacao, session);
                }
            }
            
            confirmaTransacao(session);

        } 
        catch (Exception e) {
            try {
                HibernateUtil.rollbackTransaction();
            } catch (InfraEstruturaException ie) {
            }

            throw e;
        }
    }
}
