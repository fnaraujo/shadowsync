package br.com.controle;

import br.com.controle.exception.InfraEstruturaException;
import br.com.util.Config;
import br.com.util.HibernateUtil;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.ServerSocket;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import org.hibernate.HibernateException;
import org.hibernate.Session;

/**
 *
 * @author hugo
 */
public class Sistema {

    private static Sistema instance = null;
    public static final String NOME = "ShadowSync";
    public static final String VERSAO = "v1.0";
    public static String PATH;
    public static Config configuracao;
    public static URL HIBERNATE_STREAM;
    public static String DB_URL = null;
    public static String DB_URL_REMOTE = null;
    private static final Locale BRASIL = new Locale("pt", "BR");
    private static final DecimalFormatSymbols REAL = new DecimalFormatSymbols(BRASIL);
    private static DecimalFormat dinheiro;
    private static DecimalFormat decimalTresCasas;
    private static DecimalFormat decimalUmaCasa;
    private static DecimalFormat numeroInteiro;
    private static SimpleDateFormat dataToBD;
    private static SimpleDateFormat parserToBD;
    private static SimpleDateFormat dataFormatada;
    private static SimpleDateFormat dataFormatadaComHora;
    private static int pos = 0;
    private String[] semana = {"Domingo", "Segunda-feira", "Terça-feira", "Quarta-feira", "Quinta-feira", "Sexta-feira", "Sábado"};
    public static final String SENHA = "sisbras@13579";
    public static ServerSocket SOCKET;

    public static final String PATH_CONFIG = "./config.xml";

    public static Sistema getInstance()  {
        if (instance == null) {
            dinheiro = new DecimalFormat("###,###,##0.00", REAL);
            decimalUmaCasa = new DecimalFormat("###,###,##0.0");
            decimalTresCasas = new DecimalFormat("###,###,##0.000");
            numeroInteiro = new DecimalFormat("###,###,##0");
            HIBERNATE_STREAM = Sistema.class.getResource("/hibernate.cfg.xml");

            try {
                //URL config = Sistema.class.getResource("/config.xml");
                //configuracao = new Config(config.getPath());
                configuracao = new Config(PATH_CONFIG);
            } catch (Exception ex) {
                Logger.getLogger(Sistema.class.getName()).log(Level.SEVERE, null, ex);
            }
            configuracao.imprime();

            PATH = Sistema.class.getResource("").getPath();
            System.out.println(PATH);
            PATH = PATH.replaceAll("%20", " ");
            if (PATH.indexOf(".jar") > 0) {
                PATH = PATH.substring(4).replaceFirst("/[^/]+\\.jar!.*$", "/");
                if (PATH.charAt(0) == ':') {
                    PATH = PATH.substring(1, PATH.length());
                } else if (PATH.charAt(0) == '/') {
                    PATH = PATH.substring(1, PATH.length());
                }
            } else {
                PATH = Sistema.class.getResource("/").getPath();
            }
            System.out.println(PATH);
            dataToBD = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            parserToBD = new SimpleDateFormat("yyyy-MM-dd");
            dataFormatada = new SimpleDateFormat("dd/MM/yyyy");
            dataFormatadaComHora = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

            instance = new Sistema();
        }
        return instance;
    }

    public Date retornaDataBanco(String tipoSessao) {
        Date data = null;
        try {
            inicializaTransacao(tipoSessao);

            Session sessao;
            switch (tipoSessao.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }

            String sql = "SELECT CAST(date_trunc('SECOND',now()) AS TIMESTAMP WITHOUT TIME ZONE)";

            Object obj = sessao.createSQLQuery(sql).uniqueResult();

            data = new Date(((java.sql.Timestamp) obj).getTime());

            confirmaTransacao(tipoSessao);

        } catch (HibernateException e) {
            retornaTransacao(tipoSessao);
            e.printStackTrace();
        }
        return data;
    }

    public Date retornaDataBancoSemBlocoTransacao(String tipoSessao) {
        Date data = null;
        try {

            Session sessao;
            switch (tipoSessao.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }

            String sql = "SELECT CAST(date_trunc('SECOND',now()) AS TIMESTAMP WITHOUT TIME ZONE)";

            Object obj = sessao.createSQLQuery(sql).uniqueResult();

            data = new Date(((java.sql.Timestamp) obj).getTime());

        } catch (HibernateException e) {
            retornaTransacao(tipoSessao);
            e.printStackTrace();
        }
        return data;
    }

    public DecimalFormat getDecimalFormat() {
        return dinheiro;
    }

    public DecimalFormat getDecimalTresCasas() {
        return decimalTresCasas;
    }

    public DecimalFormat getNumeroInteiro() {
        return numeroInteiro;
    }

    public String formatFloat(float valor) {
        return dinheiro.format(valor);
    }

    public String formatDouble(double valor) {
        return dinheiro.format(valor);
    }

    public float formatFloat(String valor) throws ParseException {
        float valorFormatado = 0;

        if ((valor != null) && (!valor.isEmpty())) {
            valor = valor.replace(".", "");
            valorFormatado = dinheiro.parse(valor).floatValue();
        }

        return valorFormatado;
    }

    public float formatFloatTresCasas(String valor) throws ParseException {
        float valorFormatado = 0;

        if ((valor != null) && (!valor.isEmpty())) {
            valor = valor.replace(".", "");
            valorFormatado = decimalTresCasas.parse(valor).floatValue();
        }

        return valorFormatado;
    }

    public String diaDaSemana(Date date) {
        return semana[date.getDay()];
    }

    public BigDecimal formatBigDecimal(String valor) {
        if (valor.isEmpty()) {
            valor = "0";
        } else {
            valor = valor.replace(".", "").replace(",", ".");
        }
        return BigDecimal.valueOf(Double.valueOf(valor));
    }

    public BigDecimal formatBigDecimal(float valor) {
        return BigDecimal.valueOf(Double.valueOf(valor));
    }

    public String formatBigDecimal(BigDecimal valor) {
        if (valor == null) {
            return this.formatFloat(0);
        } else {
            return this.formatFloat(valor.floatValue());
        }

    }

    public String formatBigDecimalUmaCasa(BigDecimal valor) {
        if (valor == null) {
            return this.formatFloatUmaCasa(0);
        } else {
            return this.formatFloatUmaCasa(valor.floatValue());
        }

    }

    public String formatBigDecimalTresCasas(BigDecimal valor) {
        if (valor == null) {
            return this.formatFloat(0, true);
        } else {
            return this.formatFloat(valor.floatValue(), true);
        }

    }

    public String formatFloatUmaCasa(float valor) {
        return decimalUmaCasa.format(valor);
    }

    public String formatFloat(float valor, boolean tresCasas) {
        return decimalTresCasas.format(valor);
    }

    public float formatFloat(String valor, boolean tresCasas) throws ParseException {
        return decimalTresCasas.parse(valor).floatValue();
    }

    public String formatInteger(long valor) {
        return numeroInteiro.format(valor);
    }

    public String formatInteger(int valor) {
        return numeroInteiro.format((long) valor);
    }

    public int formatInteger(String valor) {
        if (valor.isEmpty()) {
            return 0;
        } else {
            valor = valor.replace(".", "");
            return Integer.valueOf(valor);
        }

    }

    public String formataInteiro(javax.swing.JTextField field) {
        DecimalFormat df = instance.getNumeroInteiro();
        String valor = field.getText().replace(".", "");

        try {
            valor = df.format(Long.valueOf(valor));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return valor;
    }

    public String formataValor(javax.swing.JTextField field) {
        DecimalFormat df = instance.getDecimalFormat();
        String valor = null;

        try {
            valor = df.format(instance.formatFloat(field.getText()));
        } catch (ParseException e) {
            valor = df.format(instance.formatFloat(0));
            e.printStackTrace();
        }

        return valor;
    }

    public String dataToString(Date data) {
        return dataToBD.format(data);
    }

    public String dataFormatada(Date data) {
        return dataFormatada.format(data);
    }

    public String dataFormatadaComHora(Date data) {
        return dataFormatadaComHora.format(data);
    }

    public Date parserToData(String data) {
        if (data == null) {
            return null;
        }
        if (data.toLowerCase().equals("null")) {
            return null;
        }
        try {
            return parserToBD.parse(data);
        } catch (ParseException ex) {
            return null;
        }
    }

    public String dataToParser(Date data) {
        if (data == null) {
            return null;
        }
        try {
            return parserToBD.format(data);
        } catch (Exception ex) {
            return null;
        }
    }

    public Date stringToData(String data) {
        if (data == null) {
            return null;
        }
        if (data.toLowerCase().equals("null")) {
            return null;
        }
        try {
            return dataToBD.parse(data);
        } catch (ParseException ex) {
            return null;
        }
    }

    public Date strToData(String data) {
        if (data == null) {
            return null;
        }
        if (data.toLowerCase().equals("null")) {
            return null;
        }
        try {
            return dataFormatada.parse(data);
        } catch (ParseException ex) {
            return null;
        }
    }

    public String montaStringConsulta(String str) {
        if (str == null) {
            return null;
        }

        return str.trim().replaceAll(" ", "%");

    }

    private void inicializaTransacao(String session) throws InfraEstruturaException {
        switch (session.toLowerCase()) {
            case "local":
                HibernateUtil.beginTransaction();
                break;
            case "remote":
                HibernateUtil.beginTransactionRemote();
                break;
            default:
                HibernateUtil.beginTransaction();

        }
    }

    private void confirmaTransacao(String session) throws InfraEstruturaException {
        switch (session.toLowerCase()) {
            case "local":
                HibernateUtil.commitTransaction();
                break;
            case "remote":
                HibernateUtil.commitTransactionRemote();
                break;
            default:
                HibernateUtil.commitTransaction();

        }

    }

    private void retornaTransacao(String session) throws InfraEstruturaException {
        switch (session.toLowerCase()) {
            case "local":
                HibernateUtil.rollbackTransaction();
                break;
            case "remote":
                HibernateUtil.rollbackTransactionRemote();
                break;
            default:
                HibernateUtil.rollbackTransaction();

        }

    }

    public String getSenha() {
        JPanel panel = new JPanel();
        JLabel label = new JLabel("Digite a senha do ADMIN");
        JPasswordField pass = new JPasswordField(20);
        panel.add(label);
        panel.add(pass);

        String[] options = new String[]{"Confirmar", "Cancelar"};
        int option = JOptionPane.showOptionDialog(null, panel, "Shadowsync - Autenticação de administrador",
                JOptionPane.NO_OPTION, JOptionPane.PLAIN_MESSAGE,
                null, options, options[0]);
        if (option == 0) // pressing OK button
        {
            return String.valueOf(pass.getPassword());
        } else {
            return null;
        }
    }

    public void lockInstancia() {
        try {
            SOCKET = new ServerSocket(4243);
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Já existe uma instancia do aplicativo em execução"
                    + "", "Mensagem", JOptionPane.WARNING_MESSAGE);
            System.exit(0);
        }
    }
}
