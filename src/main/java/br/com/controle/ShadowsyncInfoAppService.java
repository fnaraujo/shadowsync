package br.com.controle;

import br.com.controle.exception.InfraEstruturaException;
import br.com.modelo.ShadowsyncInfo;
import br.com.util.HibernateUtil;

/**
 *
 * @author SAMUEL
 */
public class ShadowsyncInfoAppService {

    ShadowsyncInfoDAO shadowsyncInfoDAO = new ShadowsyncInfoDAO();
    
    private void inicializaTransacao(String session) throws InfraEstruturaException {
        switch (session.toLowerCase()) {
            case "local":
                HibernateUtil.beginTransaction();
                break;
            case "remote":
                HibernateUtil.beginTransactionRemote();
                break;
            default:
                HibernateUtil.beginTransaction();

        }
    }

    private void confirmaTransacao(String session) throws InfraEstruturaException {
        switch (session.toLowerCase()) {
            case "local":
                HibernateUtil.commitTransaction();
                break;
            case "remote":
                HibernateUtil.commitTransactionRemote();
                break;
            default:
                HibernateUtil.commitTransaction();

        }

    }

    private void retornaTransacao(String session) throws InfraEstruturaException {
        switch (session.toLowerCase()) {
            case "local":
                HibernateUtil.rollbackTransaction();
                break;
            case "remote":
                HibernateUtil.rollbackTransactionRemote();
                break;
            default:
                HibernateUtil.rollbackTransaction();

        }

    }

    public void altera(ShadowsyncInfo shadowsyncInfo, String session) {
        try {
            inicializaTransacao(session);

            shadowsyncInfoDAO.altera(shadowsyncInfo, session);

            confirmaTransacao(session);

        } catch (InfraEstruturaException e) {
            try {
                retornaTransacao(session);
            } catch (InfraEstruturaException ie) {
            }

            throw e;
        }
    }
}
