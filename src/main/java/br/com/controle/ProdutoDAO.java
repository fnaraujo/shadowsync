package br.com.controle;

import br.com.controle.exception.InfraEstruturaException;
import br.com.controle.exception.ObjetoNaoEncontradoException;
import br.com.modelo.Funcionario;
import br.com.modelo.Produto;
import br.com.util.HibernateUtil;
import java.util.Date;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author flavio.nunes
 */
public class ProdutoDAO {

    public long inclui(Produto produto, String session) {
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }            
            sessao.save(produto);

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return produto.getIdProduto();
    }

    public void altera(Produto produto, String session) {
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }
                       
            sessao.update(produto);            

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }
        
    }    
    
    public Produto retornaUmProduto(String codigo, String session) throws ObjetoNaoEncontradoException {
        Produto obj = null;
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }

            String hql = " SELECT obj from Produto as obj"                    
                    + " WHERE 1=1"
                    + "AND obj.codigo = '" + codigo + "'";

            obj = (Produto) sessao.createQuery(hql).uniqueResult();

            if (obj == null) {
                throw new ObjetoNaoEncontradoException();
            }
        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return obj;
    }

    @SuppressWarnings("unchecked")
    public List<Produto> listaProdutos(Date ultimaAtualizacao, String session) {
        List<Produto> lista = null;
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }

            String hql = " SELECT obj from Produto as obj"
                    + " where 1=1"
                    + " and (obj.ultimaAtualizacao > :ULTIMAALTERACAO OR (obj.ultimaAtualizacao is null and obj.dataCadastro > :ULTIMAALTERACAO))"
                    + "order by obj.idFuncionario";
            
            Query qry = sessao.createQuery(hql);
            qry.setParameter("ULTIMAALTERACAO", ultimaAtualizacao);

            lista = qry.list();


        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return lista;
    }
    
    
}
