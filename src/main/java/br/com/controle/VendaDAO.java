package br.com.controle;

import br.com.controle.exception.InfraEstruturaException;
import br.com.controle.exception.ObjetoNaoEncontradoException;
import br.com.modelo.LogStatusVenda;
import br.com.modelo.Venda;
import br.com.util.HibernateUtil;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author flavio.nunes
 */
public class VendaDAO {

    public long inclui(Venda venda, String session) {
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }
            sessao.save(venda);

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return venda.getIdVenda();
    }

    public void altera(Venda venda, String session) {
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }

            sessao.update(venda);

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

    }

    public Venda retornaUmaVenda(Long id, String session) throws ObjetoNaoEncontradoException {
        Venda objVenda = null;
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }

            String hql = " SELECT obj from Venda as obj"
                    + " WHERE 1=1"
                    + "AND obj.idVenda = " + id;

            objVenda = (Venda) sessao.createQuery(hql).uniqueResult();

            if (objVenda == null) {
                throw new ObjetoNaoEncontradoException();
            }
        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return objVenda;
    }

    public List<Venda> listaVendasWeb() {

        List<Venda> lista = null;
        try {
            Session sessao = HibernateUtil.getSessionRemote();

            String hql = " SELECT obj from Venda as obj"
                    + " LEFT JOIN FETCH obj.pagamentos pgto"
                    + " where 1=1"
                    + " and obj.statusVenda = 2"
                    + "order by obj.idVenda";

            Query qry = sessao.createQuery(hql);

            lista = qry.list();

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return lista;
    }

    public List<Venda> listaVendasWebCanceladasLocal(Date dataAtualizacao) {

        List<Venda> lista = new ArrayList<>();
        try {
            Session sessao = HibernateUtil.getSession();

            String hql = " SELECT obj from LogStatusVenda as obj "
                    + " join fetch obj.venda"
                    + " where 1=1 "
                    + " and obj.statusVenda.idStatusVenda = 0 "
                    + " and obj.venda.idPedidoWeb is not null "
                    + " and obj.dataCadastro > :DATA_ULT_ATUALIZACAO "
                    + " order by obj.venda.idVenda ";

            Query qry = sessao.createQuery(hql);

            qry.setParameter("DATA_ULT_ATUALIZACAO", dataAtualizacao);

            List<LogStatusVenda> listaLog = qry.list();

            for (LogStatusVenda log : listaLog) {
                lista.add(log.getVenda());
            }

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return lista;
    }

    public List<Venda> listaVendasWebFinalizadasLocal(Date dataAtualizacao) {

        List<Venda> lista = new ArrayList<>();
        try {
            Session sessao = HibernateUtil.getSession();

            String hql = " SELECT obj from LogStatusVenda as obj "
                    + " join fetch obj.venda"
                    + " where 1=1 "
                    + " and obj.statusVenda.idStatusVenda = 3 "
                    + " and obj.venda.idPedidoWeb is not null "
                    + " and obj.dataCadastro > :DATA_ULT_ATUALIZACAO "
                    + " and obj.venda.idVenda not in "
                    + "                             (select "
                    + "                                    venda.idVenda "
                    + "                                from "
                    + "                                    LogStatusVenda"
                    + "                               where "
                    + "                                    statusVenda.idStatusVenda = 0 "
                    + "                                 and "
                    + "                                    venda.idVenda = obj.venda.idVenda "
                    + "                              ) "
                    + " order by obj.venda.idVenda ";

            Query qry = sessao.createQuery(hql);

            qry.setParameter("DATA_ULT_ATUALIZACAO", dataAtualizacao);

            List<LogStatusVenda> listaLog = qry.list();

            for (LogStatusVenda log : listaLog) {
                lista.add(log.getVenda());
            }

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }
        return lista;
    }

    public void atualizaVendaWeb(List<Venda> vendasCanceladas, List<Venda> vendasFinalizadas) {

        try {
            Session sessao = HibernateUtil.getSessionRemote();

            String sql = "update venda  set status_venda = 0 "
                    + " where id_venda in ( 0 ";

            for (Venda vc : vendasCanceladas) {
                sql += ", " + vc.getIdPedidoWeb();
            }
            
            sql += " )";
            
            sessao.createSQLQuery(sql).executeUpdate();
            
            sql = "update venda  set status_venda = 4 "
                    + " where id_venda in ( 0 ";

            for (Venda vf : vendasFinalizadas) {
                sql += ", " + vf.getIdPedidoWeb();
            }
            
            sql += " )";
            
            sessao.createSQLQuery(sql).executeUpdate();

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }
    }
}
