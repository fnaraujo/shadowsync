package br.com.controle;

import br.com.controle.exception.InfraEstruturaException;
import br.com.controle.exception.ObjetoNaoEncontradoException;
import br.com.modelo.CodigoTributario;
import br.com.modelo.EstoquePreco;
import br.com.modelo.EstoqueProduto;
import br.com.modelo.Fabricante;
import br.com.modelo.Grupo;
import br.com.modelo.Loja;
import br.com.modelo.Ncm;
import br.com.modelo.Produto;
import br.com.modelo.TabelaPreco;
import br.com.modelo.UnidadeMedida;
import br.com.util.HibernateUtil;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author flavio.nunes
 */
public class EstoqueProdutoDAO {

    public long inclui(EstoqueProduto estoque, List<EstoquePreco> precos, List<Loja> lojas, Loja loja, String session) {
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }
            for (EstoquePreco preco : estoque.getEstoquePrecos()) {
                if (preco.getTabelaPreco().getIdTabelaPreco() == 0) {
                    sessao.save(preco.getTabelaPreco());
                }
            }

            if (estoque.getIdEstoque() > 0) {
                // se o produto ja estiver instancia na loja local
            } else {
                sessao.save(estoque.getProduto());
                sessao.save(estoque);
            }

            try {
                EstoquePreco prec = new EstoquePrecoDAO()
                        .retornaUmPreco(estoque.getProduto().getCodigo(), precos.get(0).getNumOrdem(), loja, session);
                if (prec == null) {
                    // grava preco da loja que esta enviando caso nao possua cadastro
                    gravaPreco(estoque, precos, loja, session);
                }
            } catch (Exception e) {
            }

            // instancia o produto nas outras lojas que não possuem instancia
            if (lojas != null) {
                for (int i = 0; i < lojas.size(); i++) {

                    try {
                        new EstoqueProdutoDAO().retornaUmEstoque(estoque.getProduto().getCodigo(), session, lojas.get(i));
                    } catch (ObjetoNaoEncontradoException e) {

                        EstoqueProduto estoqueProduto = new EstoqueProduto();
                        estoqueProduto.setProduto(estoque.getProduto());
                        estoqueProduto.setLoja(lojas.get(i));
                        estoqueProduto.setClasseAbc('N');
                        estoqueProduto.setQtdMinimo(BigDecimal.ZERO);
                        estoqueProduto.setQtdMaximo(BigDecimal.ZERO);
                        estoqueProduto.setQtdDisponivel(BigDecimal.ZERO);
                        estoqueProduto.setCustoMedio(estoque.getCustoMedio());
                        estoqueProduto.setCustoReposicao(estoque.getCustoReposicao());
                        estoqueProduto.setDemandaMes(BigDecimal.ZERO);
                        estoqueProduto.setLocacao("");
                        estoqueProduto.setPagaComissao(estoque.getPagaComissao());
                        estoqueProduto.setEsgotamentoDiario(BigDecimal.ZERO);
                        estoqueProduto.setLiberadoCompra(true);
                        estoqueProduto.setLiberadoContagem(true);

                        sessao.save(estoqueProduto);

                        gravaPreco(estoqueProduto, precos, loja, session);

                    }
                }
            }

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return estoque.getIdEstoque();
    }

    public long inclui(EstoqueProduto estoque, List<Loja> lojas, String session) {
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }
            for (EstoquePreco preco : estoque.getEstoquePrecos()) {
                if (preco.getTabelaPreco().getIdTabelaPreco() == 0) {
                    sessao.save(preco.getTabelaPreco());
                }
            }

            if (estoque.getIdEstoque() > 0) {
                // se o produto ja estiver instancia na loja local
            } else {
                sessao.save(estoque.getProduto());
                sessao.save(estoque);
            }

            // instancia o produto nas outras lojas que não possuem instancia
            if (lojas != null) {
                for (int i = 0; i < lojas.size(); i++) {

                    try {
                        new EstoqueProdutoDAO().retornaUmEstoque(estoque.getProduto().getCodigo(), session, lojas.get(i));
                    } catch (ObjetoNaoEncontradoException e) {

                        EstoqueProduto estoqueProduto = new EstoqueProduto();
                        estoqueProduto.setProduto(estoque.getProduto());
                        estoqueProduto.setLoja(lojas.get(i));
                        estoqueProduto.setClasseAbc('N');
                        estoqueProduto.setQtdMinimo(BigDecimal.ZERO);
                        estoqueProduto.setQtdMaximo(BigDecimal.ZERO);
                        estoqueProduto.setQtdDisponivel(BigDecimal.ZERO);
                        estoqueProduto.setCustoMedio(estoque.getCustoMedio());
                        estoqueProduto.setCustoReposicao(estoque.getCustoReposicao());
                        estoqueProduto.setDemandaMes(BigDecimal.ZERO);
                        estoqueProduto.setLocacao("");
                        estoqueProduto.setPagaComissao(estoque.getPagaComissao());
                        estoqueProduto.setEsgotamentoDiario(BigDecimal.ZERO);
                        estoqueProduto.setLiberadoCompra(true);
                        estoqueProduto.setLiberadoContagem(true);

                        sessao.save(estoqueProduto);
                    }
                }
            }

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return estoque.getIdEstoque();
    }

    private void gravaPreco(EstoqueProduto estoque, List<EstoquePreco> precos, Loja loja, String session) {

        TabelaPrecoDAO tabelaPrecoDAO = new TabelaPrecoDAO();

        for (EstoquePreco precoBase : precos) {

            EstoquePreco precoAtualizacao = null;

            TabelaPreco tab;
            try {
                tab = tabelaPrecoDAO.retornaUmProduto(precoBase.getTabelaPreco().getIndice(), session);
            } catch (ObjetoNaoEncontradoException e) {
                TabelaPreco tabBase = precoBase.getTabelaPreco();

                tab = new TabelaPreco(0, loja, ("TAB INDICE=" + tabBase.getIndice()).replaceAll("\\.", ","), tabBase.getIndice(), true, true);
                tabelaPrecoDAO.inclui(tab, session);
            }

            if (precoAtualizacao == null) {
                precoAtualizacao = new EstoquePreco(estoque);
            }

            precoAtualizacao.setValorVenda(precoBase.getValorVenda());
            precoAtualizacao.setValorDesconto(precoBase.getValorDesconto());
            precoAtualizacao.setAtualizacaoPreco(precoBase.getAtualizacaoPreco() != null
                    ? precoBase.getAtualizacaoPreco()
                    : Sistema.getInstance().retornaDataBancoSemBlocoTransacao(session));
            precoAtualizacao.setDataLimiteDesconto(precoBase.getDataLimiteDesconto());
            precoAtualizacao.setFormasPgtoPromocao(precoBase.getFormasPgtoPromocao());
            precoAtualizacao.setPercentualPromocao(precoBase.getPercentualPromocao());
            precoAtualizacao.setPrecoPadrao(precoBase.getPrecoPadrao());
            precoAtualizacao.setTabelaPreco(tab);
            precoAtualizacao.setNumOrdem(precoBase.getNumOrdem());

            new EstoquePrecoDAO().inclui(precoAtualizacao, session);
        }
    }

    public void incluiParaOutrasLojas(EstoqueProduto estoque, String session) {
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }

//            String sql = "insert into estoque_produto "
//                    + "(id_produto,id_loja,classe_abc, demanda_mes, custo_reposicao,custo_medio "
//                    + ",qtd_disponivel,qtd_maximo,qtd_minimo,locacao,ultima_compra,ultima_venda "
//                    + ",ultima_atualizacao,data_inventario,qtd_contada,hash,paga_comissao,liberado_contagem "
//                    + ",indice_comissao_ger,indice_comissao_mec,esgotamento_diario,id_loja_departamento,qtd_alocada "
//                    + ",liberado_compra) "
//                    + "select  "
//                    + "	p.id_produto "
//                    + "	,l.id_loja "
//                    + "	,'N' "
//                    + "	,0 "
//                    + "	,0 "
//                    + "	,0 "
//                    + " ,0 "
//                    + " ,0 "
//                    + " ,0 "
//                    + " ,'' "
//                    + " ,null "
//                    + " ,null "
//                    + "	,null "
//                    + "	,null "
//                    + "	,0 "
//                    + "	,null "
//                    + "	,false "
//                    + "	,false "
//                    + "	,0,0,0,1,0 "
//                    + "	,false "
//                    + "from produto p "
//                    + "inner join estoque_produto ep on p.id_produto = ep.id_produto "
//                    + "INNER JOIN LOJA L ON L.ID_LOJA <> :IDLOJA";
//            sql += " WHERE p.codigo = :CODIGO";
//            
//            Query qry = sessao.createSQLQuery(sql);
//            qry.setParameter("IDLOJA", estoque.getLoja().getIdLoja());
//            qry.setParameter("CODIGO", estoque.getProduto().getCodigo());
//            
//            qry.executeUpdate();           
            sessao.save(estoque);

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        } catch (Exception e) {
            throw new InfraEstruturaException(e);
        }
    }

    public void incluiParaOutrasLojas(Loja lojaLocal, Date dataCadastro, String session) {
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();
            }

            String sql = "insert into estoque_produto "
                    + "(id_produto,id_loja,classe_abc, demanda_mes, custo_reposicao,custo_medio "
                    + ",qtd_disponivel,qtd_maximo,qtd_minimo,locacao,ultima_compra,ultima_venda "
                    + ",ultima_atualizacao,data_inventario,qtd_contada,hash,paga_comissao,liberado_contagem "
                    + ",indice_comissao_ger,indice_comissao_mec,esgotamento_diario,id_loja_departamento,qtd_alocada "
                    + ",liberado_compra) "
                    + "select  "
                    + "	p.id_produto "
                    + "	,l.id_loja "
                    + "	,'N' "
                    + "	,0 "
                    + "	,0 "
                    + "	,0 "
                    + " ,0 "
                    + " ,0 "
                    + " ,0 "
                    + " ,'' "
                    + " ,null "
                    + " ,null "
                    + "	,null "
                    + "	,null "
                    + "	,0 "
                    + "	,null "
                    + "	,false "
                    + "	,false "
                    + "	,0,0,0,1,0 "
                    + "	,false "
                    + "from produto p "
                    + "inner join estoque_produto ep on p.id_produto = ep.id_produto "
                    + "INNER JOIN LOJA L ON L.ID_LOJA <> :IDLOJA";
            if (dataCadastro != null) {
                sql += " WHERE p.data_cadastro >= :DATACADASTRO";
            }

            Query qry = sessao.createSQLQuery(sql);
            qry.setParameter("IDLOJA", lojaLocal.getIdLoja());
            if (dataCadastro != null) {
                qry.setParameter("DATACADASTRO", dataCadastro);
            }

            qry.executeUpdate();

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        } catch (Exception e) {
            throw new InfraEstruturaException(e);
        }
    }

    public void altera(EstoqueProduto estoque, String session) {
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }

            sessao.update(estoque);

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

    }

    public void merge(EstoqueProduto estoque, List<Loja> lojas, String session) {
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }

            if (estoque.getIdEstoque() > 0) {

            } else {

                for (EstoquePreco preco : estoque.getEstoquePrecos()) {
                    if (preco.getTabelaPreco().getIdTabelaPreco() == 0) {
                        sessao.merge(preco.getTabelaPreco());
                    }
                }

                for (int i = 0; i < lojas.size(); i++) {
                    if (lojas.get(i).getNome().equals(estoque.getLoja().getNome())) {
                        continue;
                    } else {
                        EstoqueProduto estoqueProduto = new EstoqueProduto();
                        estoqueProduto.setProduto(estoque.getProduto());
                        estoqueProduto.setLoja(lojas.get(i));
                        estoqueProduto.setClasseAbc('N');
                        estoqueProduto.setQtdMinimo(BigDecimal.ZERO);
                        estoqueProduto.setQtdMaximo(BigDecimal.ZERO);
                        estoqueProduto.setQtdDisponivel(BigDecimal.ZERO);
                        estoqueProduto.setCustoMedio(estoque.getCustoMedio());
                        estoqueProduto.setCustoReposicao(estoque.getCustoReposicao());
                        estoqueProduto.setDemandaMes(BigDecimal.ZERO);
                        estoqueProduto.setLocacao("");
                        estoqueProduto.setPagaComissao(estoque.getPagaComissao());
                        estoqueProduto.setEsgotamentoDiario(BigDecimal.ZERO);
                        estoqueProduto.setLiberadoCompra(true);
                        estoqueProduto.setLiberadoContagem(true);

                        sessao.merge(estoqueProduto);
                    }
                }
            }

            sessao.merge(estoque);

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

    }

    public EstoqueProduto retornaUmEstoque(String codigo, String session, Loja loja) throws ObjetoNaoEncontradoException {
        EstoqueProduto obj = null;
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }

            String hql = " SELECT obj from EstoqueProduto as obj"
                    + " INNER JOIN FETCH obj.produto"
                    + " WHERE 1=1"
                    + " AND obj.produto.codigo = '" + codigo + "'"
                    + " AND obj.loja.empresa.cnpj = '" + loja.getEmpresa().getCnpj() + "'";

            Query qry = sessao.createQuery(hql);

            obj = (EstoqueProduto) qry.uniqueResult();

            if (obj == null) {
                throw new ObjetoNaoEncontradoException();
            }
        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return obj;
    }

    public EstoqueProduto retornaUmEstoque(EstoqueProduto ep, String session) throws ObjetoNaoEncontradoException {
        EstoqueProduto obj = null;
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();
            }

            String hql = " SELECT obj from EstoqueProduto as obj"
                    + " INNER JOIN FETCH obj.produto"
                    + " WHERE 1=1"
                    + " AND obj.produto.codigo = '" + ep.getProduto().getCodigo() + "'"
                    + " AND obj.loja.empresa.cnpj = '" + ep.getLoja().getEmpresa().getCnpj() + "'";

            Query qry = sessao.createQuery(hql);

            obj = (EstoqueProduto) qry.uniqueResult();

            if (obj == null) {
                throw new ObjetoNaoEncontradoException();
            }
        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return obj;
    }

    public EstoqueProduto retornaUmEstoque(EstoqueProduto ep, Loja loja, String session) throws ObjetoNaoEncontradoException {
        EstoqueProduto obj = null;
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();
            }

            String hql = " SELECT obj from EstoqueProduto as obj"
                    + " INNER JOIN FETCH obj.produto"
                    + " WHERE 1=1"
                    + " AND obj.produto.codigo = '" + ep.getProduto().getCodigo() + "'"
                    + " AND obj.loja.empresa.cnpj = '" + loja.getEmpresa().getCnpj() + "'";

            Query qry = sessao.createQuery(hql);

            obj = (EstoqueProduto) qry.uniqueResult();

            if (obj == null) {
                throw new ObjetoNaoEncontradoException();
            }
        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return obj;
    }

    public EstoqueProduto retornaUmEstoqueJDBC(String codigo, String session, Loja loja) throws ObjetoNaoEncontradoException {
        EstoqueProduto obj = null;
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }

            String hql = " select "
                    + "     ep.id_produto," //0
                    + "     ep.id_loja," //1
                    + "     id_estoque," //2
                    + "     esgotamento_diario," //3
                    + "     demanda_mes," // 4
                    + "     data_inventario," // 5
                    + "     custo_reposicao," // 6
                    + "     custo_medio," // 7
                    + "     classe_abc," // 8
                    + "     ultimo_mvto_estoque," // 9
                    + "     ultima_venda," // 10
                    + "     ultima_compra," // 11
                    + "     ultima_atualizacao," // 12
                    + "     qtd_minimo," // 13
                    + "     qtd_maximo," // 14
                    + "     qtd_disponivel," // 15
                    + "     qtd_contada," // 16
                    + "     qtd_alocada," // 17
                    + "     paga_comissao," // 18
                    + "     locacao," // 19
                    + "     liberado_contagem," // 20
                    + "     liberado_compra," // 21
                    + "     indice_comissao_mec," // 22
                    + "     indice_comissao_ger," // 23
                    // campos produto
                    + "     p.codigo_similaridade," // 24
                    + "     p.origem as p_origem," // 25
                    + "     p.data_alterado," // 26
                    + "     p.data_cadastro," // 27
                    + "     p.id_codigo_tributario," // 28
                    + "     p.id_fabricante," // 29
                    + "     p.id_ncm," // 30
                    + "     p.id_unidade_medida," // 31
                    + "     p.id_grupo," // 32
                    + "     p.ean," // 33
                    + "     p.ativo," // 34
                    + "     p.emissao_etiqueta," // 35
                    + "     p.peso," // 36
                    + "     p.qtd_embalagem," // 37
                    + "     p.aplicacao," // 38
                    + "     p.descricao as p_descricao," // 39
                    + "     p.codigo as p_codigo," //40
                    // campos codigo_tributario
                    + "     ct.perc_aprov_sn," // 41
                    + "     ct.codigo_ecf," // 42
                    + "     ct.origem as ct_origem," // 43
                    + "     ct.situacao_tributaria," // 44
                    + "     ct.base_icm_compra," // 45
                    + "     ct.base_icm_venda," // 46
                    + "     ct.descricao as ct_descricao," // 47
                    + "     ct.codigo as ct_codigo," // 48
                    // campos grupo
                    + "     g.lucro_minimo_item," // 49
                    + "     g.descricao as g_descricao," // 50
                    // campos ncm
                    + "     n.protocolo," // 51
                    + "     n.mva_op_ext," // 52
                    + "     n.mva_op_int," // 53
                    + "     n.descricao as n_descricao," // 54
                    + "     n.codigo as n_codigo," // 55
                    // campos fabricante
                    + "     f.indice as f_indice," // 56
                    + "     f.descricao as f_descricao," // 57
                    // unidade medida
                    + "     un.fracionada," // 58
                    + "     un.mnemonico," // 59
                    + "     un.descricao as un_descricao" // 60
                    + "   from "
                    + "     estoque_produto ep"
                    + "   inner join"
                    + "     produto p on p.id_produto = ep.id_produto"
                    + "   inner join"
                    + "     loja l on l.id_loja = ep.id_loja"
                    + "   inner join"
                    + "     empresa e on e.codigo_id = l.id_empresa"
                    + "   left outer join"
                    + "     codigo_tributario ct on ct.id_codigo_tributario = p.id_codigo_tributario"
                    + "   left outer join"
                    + "     grupo g on g.id_grupo = p.id_grupo"
                    + "   left outer join"
                    + "     ncm n on n.id_ncm = p.id_ncm"
                    + "   left outer join"
                    + "     fabricante f on f.id_fabricante = p.id_fabricante"
                    + "   left outer join"
                    + "     unidade_medida un on un.id_unidade_medida = p.id_unidade_medida"
                    + "   where"
                    + "     p.codigo = :CODIGO"
                    + "   and"
                    + "     e.cnpj = :CNPJ";

            Query qry = sessao.createSQLQuery(hql);

            qry.setParameter("CODIGO", codigo);
            qry.setParameter("CNPJ", loja.getEmpresa().getCnpj());

            Object[] o = (Object[]) qry.uniqueResult();

            if (o != null
                    && o.length > 0) {

                Produto p = new Produto();
                p.setIdProduto(((BigInteger) o[0]).longValue());

                Loja l = new Loja();
                l.setIdLoja((int) o[1]);

                EstoqueProduto ep = new EstoqueProduto();
                ep.setIdEstoque(((BigInteger) o[2]).longValue());
                ep.setEsgotamentoDiario((BigDecimal) o[3]);
                ep.setDemandaMes((BigDecimal) o[4]);
                ep.setDataInventario((Date) o[5]);
                ep.setCustoReposicao((BigDecimal) o[6]);
                ep.setCustoMedio((BigDecimal) o[7]);
                ep.setClasseAbc((char) o[8]);
                ep.setUltimoMvtoEstoque((Date) o[9]);
                ep.setUltimaVenda((Date) o[10]);
                ep.setUltimaCompra((Date) o[11]);
                ep.setUltimaAtualizacao((Date) o[12]);
                ep.setQtdMinimo((BigDecimal) o[13]);
                ep.setQtdMaximo((BigDecimal) o[14]);
                ep.setQtdDisponivel(new BigDecimal((float) o[15]));
                ep.setQtdContada((BigDecimal) o[16]);
                ep.setQtdAlocada((BigDecimal) o[17]);
                ep.setPagaComissao((boolean) o[18]);
                ep.setLocacao(String.valueOf(o[19]));
                ep.setLiberadoContagem((boolean) o[20]);
                ep.setLiberadoCompra((boolean) o[21]);
                ep.setIndiceComissaoMec((float) o[22]);
                ep.setIndiceComissaoGer((float) o[23]);
                ep.setProduto(p);
                ep.setLoja(l);

                p.setCodigoSimilaridade(o[24] instanceof BigDecimal
                        ? ((BigDecimal) o[24]).longValue()
                        : ((BigInteger) o[24]).longValue());

                p.setOrigem((short) o[25]);
                p.setDataAlterado((Date) o[26]);
                p.setDataCadastro((Date) o[27]);

                CodigoTributario ct = new CodigoTributario();
                ct.setIdCodigoTributario(((BigInteger) o[28]).longValue());
                p.setCodigoTributario(ct);

                Fabricante f = new Fabricante();
                f.setIdFabricante(((BigInteger) o[29]).longValue());
                p.setFabricante(f);

                Ncm ncm = new Ncm();
                ncm.setIdNcm((int) o[30]);
                p.setNcm(ncm);

                UnidadeMedida un = new UnidadeMedida();
                un.setIdUnidadeMedida((int) o[31]);
                p.setUnidadeMedida(un);

                Grupo g = new Grupo();
                g.setIdGrupo((int) o[32]);
                p.setGrupoByIdGrupo(g);

                p.setEan(String.valueOf(o[33]));
                p.setAtivo((boolean) o[34]);
                p.setEmissaoEtiqueta((boolean) o[35]);
                p.setPeso((float) o[36]);
                p.setQtdEmbalagem((float) o[37]);
                p.setAplicacao(String.valueOf(o[38]));
                p.setDescricao(String.valueOf(o[39]));
                p.setCodigo(String.valueOf(o[40]));

                ct.setPercAproveitamentoSn((BigDecimal) o[41]);
                ct.setCodigoEcf(String.valueOf(o[42]));
                ct.setOrigem((char) o[43]);
                ct.setSituacaoTributaria(String.valueOf(o[44]));
                ct.setBaseIcmCompra((BigDecimal) o[45]);
                ct.setBaseIcmVenda((BigDecimal) o[46]);
                ct.setDescricao(String.valueOf(o[47]));
                ct.setCodigo((int) o[48]);

                g.setLucroMinimoItem((BigDecimal) o[49]);
                g.setDescricao(String.valueOf(o[50]));

                ncm.setProtocolo(String.valueOf(o[51]));
                ncm.setMvaOpExt((float) o[52]);
                ncm.setMvaOpInt((float) o[53]);
                ncm.setDescricao(String.valueOf(o[54]));
                ncm.setCodigo(String.valueOf(o[55]));

                f.setIndice((int) o[56]);
                f.setDescricao(String.valueOf(o[57]));

                un.setFracionada((boolean) o[58]);
                un.setMnemonico(String.valueOf(o[59]));
                un.setDescricao(String.valueOf(o[60]));

                obj = ep;
            }

            if (obj == null) {
                throw new ObjetoNaoEncontradoException();
            }
        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return obj;
    }

    @SuppressWarnings("unchecked")
    public List<EstoqueProduto> listaEstoque(Date ultimaAtualizacao, String session, Loja loja) {
        List<EstoqueProduto> lista = null;
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }

            String hql = " SELECT obj from EstoqueProduto as obj"
                    + " INNER JOIN FETCH obj.produto"
                    + " where 1=1"
                    + " and (obj.produto.dataCadastro > :ULTIMAALTERACAO "
                    + " OR"
                    + " obj.ultimaAtualizacao > :ULTIMAALTERACAO) "
                    + " and obj.loja.empresa.cnpj = :CNPJ"
                    + " order by obj.idEstoque";

            Query qry = sessao.createQuery(hql);
            qry.setParameter("ULTIMAALTERACAO", ultimaAtualizacao);
            qry.setParameter("CNPJ", loja.getEmpresa().getCnpj());

            lista = qry.list();

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return lista;
    }

    @SuppressWarnings("unchecked")
    public List<EstoqueProduto> listaEstoqueOutrasLojas(Date ultimaAtualizacao, String session, Loja loja) {
        List<EstoqueProduto> lista = null;
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }

            String hql = " SELECT obj from EstoqueProduto as obj"
                    + " INNER JOIN FETCH obj.produto"
                    + " where 1=1"
                    + " and (obj.produto.dataCadastro > :ULTIMAALTERACAO "
                    + " OR"
                    + " obj.ultimaAtualizacao > :ULTIMAALTERACAO) "
                    + " and obj.loja.empresa.cnpj <> :CNPJ"
                    + " order by obj.idEstoque";

            Query qry = sessao.createQuery(hql);
            qry.setParameter("ULTIMAALTERACAO", ultimaAtualizacao);
            qry.setParameter("CNPJ", loja.getEmpresa().getCnpj());

            lista = qry.list();

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return lista;
    }

    @SuppressWarnings("unchecked")
    public List<EstoqueProduto> listaEstoqueByDataCadastro(Loja loja, Date ultimaAtualizacao, String session) {
        List<EstoqueProduto> lista = null;
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }

            String hql = " SELECT obj from EstoqueProduto as obj"
                    + " INNER JOIN FETCH obj.produto"
                    + " where 1=1"
                    + " and obj.loja.idLoja = :LOJA"
                    + " and obj.produto.dataCadastro > :ULTIMAALTERACAO "
                    + "order by obj.idEstoque";

            Query qry = sessao.createQuery(hql);
            qry.setParameter("LOJA", loja.getIdLoja());
            qry.setParameter("ULTIMAALTERACAO", ultimaAtualizacao);

            lista = qry.list();

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return lista;
    }

    @SuppressWarnings("unchecked")
    public List<Object[]> listaSaldos(Date ultimaAtualizacao, String session, Loja loja) {
        List<Object[]> lista = null;
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }

            String hql = " SELECT obj.loja.empresa.cnpj, obj.produto.codigo, obj.qtdDisponivel, obj.ultimoMvtoEstoque from EstoqueProduto as obj"
                    + " where 1=1"
                    + " and (obj.ultimoMvtoEstoque > :ULTIMAALTERACAO) ";
            if (loja != null) {
                hql += " and obj.loja.empresa.cnpj = :CNPJ ";
            }
            hql += "order by obj.loja.empresa.cnpj, obj.produto.codigo";

            Query qry = sessao.createQuery(hql);
            qry.setParameter("ULTIMAALTERACAO", ultimaAtualizacao);
            if (loja != null) {
                qry.setParameter("CNPJ", loja.getEmpresa().getCnpj());
            }

            lista = qry.list();

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return lista;
    }

    @SuppressWarnings("unchecked")
    public List<Object[]> listaSaldosOutrasLojas(Date ultimaAtualizacao, String session, Loja loja) {
        List<Object[]> lista = null;
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }

            String hql = " SELECT obj.loja.empresa.cnpj, obj.produto.codigo, obj.qtdDisponivel, obj.ultimoMvtoEstoque from EstoqueProduto as obj"
                    + " where 1=1"
                    + " and (obj.ultimoMvtoEstoque > :ULTIMAALTERACAO) ";
            if (loja != null) {
                hql += " and obj.loja.empresa.cnpj <> :CNPJ ";
            }
            hql += "order by obj.loja.empresa.cnpj, obj.produto.codigo";

            Query qry = sessao.createQuery(hql);
            qry.setParameter("ULTIMAALTERACAO", ultimaAtualizacao);
            if (loja != null) {
                qry.setParameter("CNPJ", loja.getEmpresa().getCnpj());
            }

            lista = qry.list();

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return lista;
    }

    void atualizaEstoque(List<Object[]> listaEstoque, String session) {
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }
            for (Object[] obj : listaEstoque) {

                String sql = " update estoque_produto set "
                        + "qtd_disponivel = :QTD "
                        + ",ultimo_mvto_estoque = :DATA"
                        + " from ( "
                        + "	select ep.id_estoque  "
                        + "	from estoque_produto ep "
                        + "	inner join loja l on ep.id_loja = l.id_loja "
                        + "	inner join empresa e on e.codigo_id = l.id_empresa  "
                        + "		and e.cnpj = :CNPJ "
                        + "	inner join produto p on ep.id_produto = p.id_produto "
                        + "	where p.codigo = :CODIGO "
                        + ") as b where estoque_produto.id_estoque = b.id_estoque";

                Query qry = sessao.createSQLQuery(sql);
                qry.setParameter("CNPJ", (String) obj[0]);
                qry.setParameter("CODIGO", (String) obj[1]);
                qry.setParameter("QTD", (BigDecimal) obj[2]);
                qry.setParameter("DATA", (Date) obj[3]);

                qry.executeUpdate();
            }

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        } catch (Exception e) {
            throw new InfraEstruturaException(e);
        }
    }
}
