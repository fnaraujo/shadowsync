package br.com.controle;

import br.com.modelo.Empresa;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author FlÃ¡vio Augusto
 */
public class DAO {
    private Connection con = null;
    private String ipAdress = null;
    private String porta = null;
    private String database = null;
    private String user = null;
    private String senha = null;

    public DAO(String ip, String porta, String database, String user, String passwd){
        
        this.ipAdress = ip;
        this.porta = porta;
        this.database = database;
        this.user = user;
        this.senha = passwd;
        
        try {
            Class.forName("org.postgresql.Driver");
        }
        catch (ClassNotFoundException e) {
            System.out.println("Nao encontrou o driver do postgres");
        }
    }

    public boolean connect(){
        if(con == null){
            try {
                con = DriverManager.getConnection("jdbc:postgresql://"+ipAdress+":"+porta+"/"+database, user, senha);
                System.out.println("Conexao com servidor "+ipAdress+":"+porta+"/"+database + " aberta com sucesso");                

            } catch (SQLException e) {
                con = null;
                e.getMessage();
            }
        }
        return con != null;
    }
    
    public boolean connect(String ipEcf){
        if(con == null){

            try {
                con = DriverManager.getConnection("jdbc:postgresql://"+ipEcf+"/SYSTEC", "sa", "123456");                

            } catch (SQLException e) {
                con = null;
                e.getMessage();
            }
        }
        return con != null;
    }

    public Connection retornaConexao(){
        return this.con;
    }

    public boolean close() throws SQLException {
        if (con == null) {
            throw new SQLException();
        }
        try {
            con.close();
            //System.out.println("Fechou Conexao com Banco de Dados!!!");
            if (con.isClosed()) {
                con = null;
                return true;
            } else {
                return false;
            }
        } catch (SQLException e) {
            con = null;
            return con == null;
        }
    }
    
    public Empresa retornaEmpresa(){
        Empresa empresa = null;
        try{
            Statement st = con.createStatement();

            //Pega o nome da coluna que tem o ID
            ResultSet rs = st.executeQuery("select cnpj, nome from empresa where codigo_id = 1");
            while(rs.next()){            
                empresa = new Empresa();
                empresa.setCnpj(rs.getString("cnpj"));
                empresa.setNome(rs.getString("nome"));                
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return empresa;
    }

    public long contaLinhas(String nomeTabela){
        long total = -1;
        try{
            Statement st = con.createStatement();

            //Pega o nome da coluna que tem o ID
            ResultSet rs1 = st.executeQuery("select column_name,* from information_schema.columns "+
                                            "where table_name = '"+nomeTabela+"' AND ordinal_position = 1");
            if(rs1.next()){
                ResultSet rs = st.executeQuery("Select sum("+rs1.getString("column_name")+") as qnt from "+nomeTabela);
                total = 0;
                while(rs.next()){
                    total= rs.getLong("qnt");
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return total;
    }

    public String getChave(String sql){
        String chave=null;
        try{
            Statement st = con.createStatement();

            //Pega o nome da coluna que tem o ID
            ResultSet rs1 = st.executeQuery(sql);
            if(rs1.next()){
                chave = rs1.getString("chave");
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return chave;
    }
}


