package br.com.controle;

import br.com.controle.exception.InfraEstruturaException;
import br.com.controle.exception.ObjetoNaoEncontradoException;
import br.com.modelo.TabelaPreco;
import br.com.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Sisbras
 */
public class TabelaPrecoDAO {

    public long inclui(TabelaPreco tabelaPreco, String session) {
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }
            sessao.save(tabelaPreco);

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return tabelaPreco.getIdTabelaPreco();
    }

    public TabelaPreco retornaUmProduto(float indice, String session) throws ObjetoNaoEncontradoException {
        TabelaPreco obj = null;
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }

            String hql = " SELECT obj FROM TabelaPreco as obj "
                    + " WHERE 1=1 "
                    + " AND obj.indice = :INDICE ";

            Query qry = sessao.createQuery(hql);
            qry.setParameter("INDICE", indice);
            
            obj = (TabelaPreco) qry.setMaxResults(1).uniqueResult();

            if (obj == null) {
                throw new ObjetoNaoEncontradoException();
            }
        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return obj;
    }
}
