package br.com.controle;

import br.com.controle.exception.InfraEstruturaException;
import br.com.controle.exception.ObjetoNaoEncontradoException;
import br.com.modelo.CodigoTributario;
import br.com.util.HibernateUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author flavio.nunes
 */
public class CodigoTributarioDAO {

    public long inclui(CodigoTributario cst, String session) {
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }            
            sessao.save(cst);

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return cst.getIdCodigoTributario();
    }

    public CodigoTributario retornaUmCodigoTributario(String sigla, String session) throws ObjetoNaoEncontradoException {
        CodigoTributario obj = null;
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }

            String hql = " SELECT obj from CodigoTributario as obj"                    
                    + " WHERE 1=1"
                    + "AND obj.situacaoTributaria = '" + sigla + "'";

            List codigos = sessao.createQuery(hql).list();
            if(codigos != null
                    && !codigos.isEmpty()){
                obj = (CodigoTributario) codigos.get(0);
            }

            if (obj == null) {
                throw new ObjetoNaoEncontradoException();
            }
        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return obj;
    }
    
    @SuppressWarnings("unchecked")
    public List<CodigoTributario> listaGrupo(String session) {
        List<CodigoTributario> lista = null;
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }

            String hql = " SELECT obj from CodigoTributario as obj"                    
                    + " WHERE 1=1";

            Query qry = sessao.createQuery(hql);

            lista = qry.list();


        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return lista;
    }
}
