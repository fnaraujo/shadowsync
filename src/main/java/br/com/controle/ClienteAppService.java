package br.com.controle;

import br.com.controle.exception.InfraEstruturaException;
import br.com.controle.exception.ObjetoNaoEncontradoException;
import br.com.modelo.Bairro;
import br.com.modelo.Cep;
import br.com.modelo.Cliente;
import br.com.modelo.EnderecoCliente;
import br.com.modelo.Municipio;
import br.com.util.HibernateUtil;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Sisbras
 */
public class ClienteAppService {

    private ClienteDAO clienteDAO = new ClienteDAO();
    private FuncionarioDAO funcionarioDAO = new FuncionarioDAO();
    private FormaPagamentoDAO formaPagamentoDAO = new FormaPagamentoDAO();
    private CepDAO cepDAO = new CepDAO();
    private BairroDAO bairroDAO = new BairroDAO();
    private MunicipioDAO municipioDAO = new MunicipioDAO();

    private void inicializaTransacao(String session) throws InfraEstruturaException {
        switch (session.toLowerCase()) {
            case "local":
                HibernateUtil.beginTransaction();
                break;
            case "remote":
                HibernateUtil.beginTransactionRemote();
                break;
            default:
                HibernateUtil.beginTransaction();

        }
    }

    private void confirmaTransacao(String session) throws InfraEstruturaException {
        switch (session.toLowerCase()) {
            case "local":
                HibernateUtil.commitTransaction();
                break;
            case "remote":
                HibernateUtil.commitTransactionRemote();
                break;
            default:
                HibernateUtil.commitTransaction();

        }

    }

    private void retornaTransacao(String session) throws InfraEstruturaException {
        switch (session.toLowerCase()) {
            case "local":
                HibernateUtil.rollbackTransaction();
                break;
            case "remote":
                HibernateUtil.rollbackTransactionRemote();
                break;
            default:
                HibernateUtil.rollbackTransaction();

        }

    }

    public List<Cliente> listaCliente(Date ultimaAtualizacao, String session) {
        List<Cliente> lista = null;

        try {
            inicializaTransacao(session);

            lista = clienteDAO.listaClientes(ultimaAtualizacao, session);

            confirmaTransacao(session);
        } catch (Exception e) {
            try {
                retornaTransacao(session);
            } catch (InfraEstruturaException ie) {
            }

            throw e;
        }

        return lista;
    }

    public void atualizaCadastro(List<Cliente> clientes, String sessao) throws Exception {

        try {

            inicializaTransacao(sessao);

            for (Cliente c : clientes) {

                Cliente c1 = c.clone();

                // tenta recuperar o id na base remota
                c1.setIdCliente(clienteDAO.retornaIdCliente(c1.getCpfCnpj(), sessao));

                // deleta os contatos e endereços para adicionar os mais recentes
                clienteDAO.delataContatosEnderecosCliente(c1.getIdCliente(), sessao);

                for (EnderecoCliente ec : c1.getEnderecos()) {

                    Cep cep;
                    try {
                        cep = cepDAO.retornaUmCep(ec.getCep().getNumero(), sessao);
                    } catch (ObjetoNaoEncontradoException e) {
                        cep = null;
                    }

                    ec.setCep(cep);
                }

                // atualizar o vendedor padrão resgatando o 
                // idFuncionario correto referenciado na base remota
                if (c.getVendedorPadrao() != null) {
                    try {
                        c1.setVendedorPadrao(funcionarioDAO.retornaUmFuncionario(c.getVendedorPadrao().getCpf(), sessao));
                    } catch (ObjetoNaoEncontradoException e) {
                        c1.setVendedorPadrao(null);
                    }
                }

                if (c.getFormaPagamento() != null) {
                    try {
                        c1.setFormaPagamento(formaPagamentoDAO.retornaUmaFormaDePagamento(c.getFormaPagamento().getDescricao(), sessao));
                    } catch (ObjetoNaoEncontradoException e) {
                        c1.setFormaPagamento(null);
                    }
                }

                // se produto tiver id, insere caso o contrario atualiza
                clienteDAO.saveOrUpdate(c1, sessao);
            }

            confirmaTransacao(sessao);

        } catch (Exception e) {
            retornaTransacao(sessao);
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * 0 - List<Bairro>
     * 1 - List<Cep>
     */
    public Object[] retornaBairrosCepsCliente(List<Cliente> clientes) {

        List<Bairro> bairros = new ArrayList<>();
        List<Cep> ceps = new ArrayList<>();
        for (Cliente c : clientes) {
            for (EnderecoCliente ec : c.getEnderecos()) {
                bairros.add(ec.getCep().getBairro());
                ceps.add(ec.getCep());
            }
        }

        Object obj[] = {bairros, ceps};

        return obj;
    }
}
