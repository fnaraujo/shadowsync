package br.com.controle;

import br.com.controle.exception.InfraEstruturaException;
import br.com.controle.exception.ObjetoNaoEncontradoException;
import br.com.modelo.Bairro;
import br.com.modelo.Municipio;
import br.com.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Sisbras
 */
public class BairroDAO {

    public long inclui(Bairro bairro, String session) {
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }
            sessao.save(bairro);

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return bairro.getIdBairro();
    }
    
    public Bairro retornaUmBairro(String nome, long idMunicipio, String session) throws ObjetoNaoEncontradoException {

        Bairro obj = null;
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }

            String hql = "SELECT obj FROM Bairro as obj "
                    + " where obj.nome = :NOME "
                    + " and obj.municipio.idMunicipio = :ID_MUNICIPIO";

            Query qry = sessao.createQuery(hql);

            qry.setParameter("NOME", nome);
            qry.setParameter("ID_MUNICIPIO", idMunicipio);

            obj = (Bairro) qry.uniqueResult();

            if (obj == null) {
                throw new ObjetoNaoEncontradoException();
            }

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return obj;
    }
    
    public Bairro retornaUmBairro(String nome, Municipio municipio, String session) throws ObjetoNaoEncontradoException {

        Bairro obj = null;
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }

            String hql = "SELECT obj FROM Bairro as obj "
                    + " where obj.nome = :NOME "
                    + " and obj.municipio.nome = :NOME_MUN "
                    + " and obj.municipio.unidadeFederacao.idUnidadeFederacao = :ID_UNIDADE_FEDERACAO "
                    ;

            Query qry = sessao.createQuery(hql);

            qry.setParameter("NOME", nome);
            qry.setParameter("NOME_MUN", municipio.getNome());
            qry.setParameter("ID_UNIDADE_FEDERACAO", municipio.getUnidadeFederacao().getIdUnidadeFederacao());

            obj = (Bairro) qry.uniqueResult();

            if (obj == null) {
                throw new ObjetoNaoEncontradoException();
            }

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return obj;
    }
}
