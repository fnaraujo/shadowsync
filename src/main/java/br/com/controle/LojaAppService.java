package br.com.controle;

import java.util.List;
import br.com.controle.exception.InfraEstruturaException;
import br.com.controle.exception.ObjetoNaoEncontradoException;
import br.com.modelo.Empresa;
import br.com.modelo.Loja;
import br.com.util.HibernateUtil;
import br.com.visao.FrmGerenciaConexao;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Administrador
 */
public class LojaAppService {

    private LojaDAO lojaDAO = new LojaDAO();
    private EstoqueProdutoDAO estoqueProdutoDAO = new EstoqueProdutoDAO();

    private void inicializaTransacao(String session) throws InfraEstruturaException {
        switch (session.toLowerCase()) {
            case "local":
                HibernateUtil.beginTransaction();
                break;
            case "remote":
                HibernateUtil.beginTransactionRemote();
                break;
            default:
                HibernateUtil.beginTransaction();

        }
    }

    private void confirmaTransacao(String session) throws InfraEstruturaException {
        switch (session.toLowerCase()) {
            case "local":
                HibernateUtil.commitTransaction();
                break;
            case "remote":
                HibernateUtil.commitTransactionRemote();
                break;
            default:
                HibernateUtil.commitTransaction();

        }

    }

    private void retornaTransacao(String session) throws InfraEstruturaException {
        switch (session.toLowerCase()) {
            case "local":
                HibernateUtil.rollbackTransaction();
                break;
            case "remote":
                HibernateUtil.rollbackTransactionRemote();
                break;
            default:
                HibernateUtil.rollbackTransaction();

        }

    }

    public long inclui(Loja loja, String session) {
        try {

            inicializaTransacao(session);

            long numero = lojaDAO.inclui(loja, session);

            confirmaTransacao(session);

            return numero;
        } catch (InfraEstruturaException e) {
            try {
                retornaTransacao(session);
            } catch (InfraEstruturaException ie) {
            }

            throw e;
        }
    }

    public void altera(Loja loja, String session) {
        try {
            inicializaTransacao(session);

            lojaDAO.altera(loja, session);

            confirmaTransacao(session);

        } catch (InfraEstruturaException e) {
            try {
                retornaTransacao(session);
            } catch (InfraEstruturaException ie) {
            }

            throw e;
        }
    }

    public Loja retornaUmaLoja(String cnpj, String session)
            throws Exception {
        try {
            inicializaTransacao(session);

            Loja loja = lojaDAO.retornaUmaLoja(cnpj, session);

            confirmaTransacao(session);

            return loja;
        } catch (ObjetoNaoEncontradoException e) {
            try {
                retornaTransacao(session);
            } catch (InfraEstruturaException ie) {
                throw ie;
            }

            throw e;
        } catch (InfraEstruturaException e) {
            try {
                HibernateUtil.rollbackTransaction();
            } catch (InfraEstruturaException ie) {
            }

            throw e;
        }
    }

    public Loja retornaLojaMaster(String session)
            throws Exception {
        try {
            inicializaTransacao(session);

            Loja loja = lojaDAO.retornaLojaMaster(session);

            confirmaTransacao(session);

            return loja;
        } catch (ObjetoNaoEncontradoException e) {
            try {
                retornaTransacao(session);
            } catch (InfraEstruturaException ie) {
                throw ie;
            }

            throw e;
        } catch (InfraEstruturaException e) {
            try {
                HibernateUtil.rollbackTransaction();
            } catch (InfraEstruturaException ie) {
            }

            throw e;
        }
    }

    public List<Loja> listaLojas(Date ultimaAtualizacao, String session) {
        List<Loja> lista = null;

        try {
            inicializaTransacao(session);

            lista = lojaDAO.listaLojas(ultimaAtualizacao, session);

            confirmaTransacao(session);
        } catch (InfraEstruturaException e) {
            try {
                retornaTransacao(session);
            } catch (InfraEstruturaException ie) {
            }

            throw e;
        } catch (Exception e) {
            try {
                retornaTransacao(session);
            } catch (InfraEstruturaException ie) {
            }

            throw e;
        }

        return lista;
    }
    
    public List<Loja> listaOutrasLojas(Loja loja, String session) {
        List<Loja> lista = null;

        try {
            inicializaTransacao(session);

            lista = lojaDAO.listaOutrasLojas(session, loja);

            confirmaTransacao(session);
        } catch (InfraEstruturaException e) {
            try {
                retornaTransacao(session);
            } catch (InfraEstruturaException ie) {
            }

            throw e;
        } catch (Exception e) {
            try {
                retornaTransacao(session);
            } catch (InfraEstruturaException ie) {
            }

            throw e;
        }

        return lista;
    }

    public void implantaLojaRemota() {

        try {

            inicializaTransacao(FrmGerenciaConexao.BASE_REMOTA);

            Loja loja = lojaDAO.retornaLojaMasterRomotaNaoImplatada();

            if (loja != null) {
                
                Calendar cal = Calendar.getInstance();
                cal.set(1990, 1, 1);

                estoqueProdutoDAO.incluiParaOutrasLojas(loja, cal.getTime(), FrmGerenciaConexao.BASE_REMOTA);
                
                loja.setDataImplantacaoRemota(Sistema.getInstance().retornaDataBancoSemBlocoTransacao(FrmGerenciaConexao.BASE_REMOTA));
                
                lojaDAO.altera(loja, FrmGerenciaConexao.BASE_REMOTA);
            }

            confirmaTransacao(FrmGerenciaConexao.BASE_REMOTA);

        } catch (InfraEstruturaException e) {
            try {
                retornaTransacao(FrmGerenciaConexao.BASE_REMOTA);
            } catch (InfraEstruturaException ie) {
            }

            throw e;
        } catch (Exception e) {
            try {
                retornaTransacao(FrmGerenciaConexao.BASE_REMOTA);
            } catch (InfraEstruturaException ie) {
            }

            throw e;
        }
    }

    public void atualizaCadastros(List<Loja> lojas, String session)
            throws Exception {
        try {
            inicializaTransacao(session);

            for (Loja loja : lojas) {

                Loja lojaAtualizacao = null;

                try {
                    lojaAtualizacao = lojaDAO.retornaUmaLoja(loja.getEmpresa().getCnpj(), session);
                } catch (ObjetoNaoEncontradoException e) {
                    lojaAtualizacao = new Loja();
                    lojaAtualizacao.setEmpresa(new Empresa());
                }

                lojaAtualizacao.getEmpresa().setCnpj(loja.getEmpresa().getCnpj());
                lojaAtualizacao.getEmpresa().setCodigoCnae(loja.getEmpresa().getCodigoCnae());
                lojaAtualizacao.getEmpresa().setCodigoRegistro(loja.getEmpresa().getCodigoRegistro());
                lojaAtualizacao.getEmpresa().setComplemento(loja.getEmpresa().getComplemento());
                lojaAtualizacao.getEmpresa().setConexaoSSL(loja.getEmpresa().isConexaoSSL());
                lojaAtualizacao.getEmpresa().setCrt(loja.getEmpresa().getCrt());
                lojaAtualizacao.getEmpresa().setCtrlQtd(loja.getEmpresa().getCtrlQtd());
                lojaAtualizacao.getEmpresa().setDataValidade(loja.getEmpresa().getDataValidade());
                lojaAtualizacao.getEmpresa().setEmail(loja.getEmpresa().getEmail());
                lojaAtualizacao.getEmpresa().setHostSmtp(loja.getEmpresa().getHostSmtp());
                lojaAtualizacao.getEmpresa().setLogo(loja.getEmpresa().getLogo());
                lojaAtualizacao.getEmpresa().setNome(loja.getEmpresa().getNome());
                lojaAtualizacao.getEmpresa().setNumero(loja.getEmpresa().getNumero());
                lojaAtualizacao.getEmpresa().setPortaStmp(loja.getEmpresa().getPortaStmp());
                lojaAtualizacao.getEmpresa().setRegistroEstadual(loja.getEmpresa().getRegistroEstadual());
                lojaAtualizacao.getEmpresa().setRegistroMunicipal(loja.getEmpresa().getRegistroMunicipal());
                lojaAtualizacao.getEmpresa().setSenhaPortal(loja.getEmpresa().getSenhaPortal());
                lojaAtualizacao.getEmpresa().setSenhaSmtp(loja.getEmpresa().getSenhaSmtp());
                lojaAtualizacao.getEmpresa().setTelefone(loja.getEmpresa().getTelefone());
                lojaAtualizacao.getEmpresa().setTelefone2(loja.getEmpresa().getTelefone2());
                lojaAtualizacao.getEmpresa().setUrlPortal(loja.getEmpresa().getUrlPortal());
                lojaAtualizacao.getEmpresa().setUsaDav(loja.getEmpresa().getUsaDav());
                lojaAtualizacao.getEmpresa().setUserSmtp(loja.getEmpresa().getUserSmtp());
                lojaAtualizacao.getEmpresa().setVersaoPaf(loja.getEmpresa().getVersaoPaf());

                lojaAtualizacao.setNome(loja.getNome());
                lojaAtualizacao.setAbaixaPrecoEntrada(loja.isAbaixaPrecoEntrada());
                lojaAtualizacao.setTipoExibicaoAposCadProduto(loja.getTipoExibicaoAposCadProduto());
                lojaAtualizacao.setMoraDiaria(loja.getMoraDiaria());
                lojaAtualizacao.setMultaContasReceber(loja.getMultaContasReceber());
                lojaAtualizacao.setLiberaMaximoDiasAtraso(loja.isLiberaMaximoDiasAtraso());
                lojaAtualizacao.setMaximoDiasAtraso(loja.getMaximoDiasAtraso());
                lojaAtualizacao.setVendaAbaixoCusto(loja.getVendaAbaixoCusto());
                lojaAtualizacao.setSenhaAbaixoCusto(loja.getSenhaAbaixoCusto());
                lojaAtualizacao.setVendaEstoqueNegativo(loja.getVendaEstoqueNegativo());
                lojaAtualizacao.setSenhaEstoqueNegativo(loja.getSenhaEstoqueNegativo());
                lojaAtualizacao.setTipoImpressaoDav(loja.getTipoImpressaoDav());
                lojaAtualizacao.setFechaImpressaoDav(loja.getFechaImpressaoDav());
                lojaAtualizacao.setSaltoFimMatricial(loja.getSaltoFimMatricial());
                lojaAtualizacao.setQtdViasImpressao(loja.getQtdViasImpressao());
                lojaAtualizacao.setAbcInicial(loja.getAbcInicial());
                lojaAtualizacao.setTipoImpressaoRomaneio(loja.getTipoImpressaoRomaneio());
                lojaAtualizacao.setDavDiretoImpressora(loja.getDavDiretoImpressora());
                lojaAtualizacao.setPrazoDevolucao(loja.getPrazoDevolucao());
                lojaAtualizacao.setPrazoCancelamento(loja.getPrazoCancelamento());
                lojaAtualizacao.setDescontoMaximo(loja.getDescontoMaximo());
                lojaAtualizacao.setDescontoMaximoAcima(loja.getDescontoMaximoAcima());
                lojaAtualizacao.setSenhaDescontoAcima(loja.getSenhaDescontoAcima());
                lojaAtualizacao.setLimiteNegativoMaximo(loja.getLimiteNegativoMaximo());
                lojaAtualizacao.setLucroMinimoItem(loja.getLucroMinimoItem());
                lojaAtualizacao.setLucroMinimoVenda(loja.getLucroMinimoVenda());
                lojaAtualizacao.setSenhaLimiteNegativo(loja.getSenhaLimiteNegativo());
                lojaAtualizacao.setPrmFocusPesqProduto(loja.isPrmFocusPesqProduto());
                lojaAtualizacao.setImpressoraEtiqueta(loja.getImpressoraEtiqueta());
                lojaAtualizacao.setModeloImpressaoOsJatoTinta(loja.getModeloImpressaoOsJatoTinta());
                lojaAtualizacao.setCasasDecimaisPrecoEntrada(loja.getCasasDecimaisPrecoEntrada());
                lojaAtualizacao.setUsaVariosPrecos(loja.isUsaVariosPrecos());
                lojaAtualizacao.setAbaixaPrecoEntrada(loja.isAbaixaPrecoEntrada());
                lojaAtualizacao.setBotaoAddItemVenda(loja.isBotaoAddItemVenda());
                lojaAtualizacao.setPrmDePesqProduto(loja.isPrmDePesqProduto());
                lojaAtualizacao.setBaixaEstoqueOficinaOS(loja.isBaixaEstoqueOficinaOS());
                lojaAtualizacao.setInformativoItemVenda(loja.isInformativoItemVenda());
                lojaAtualizacao.setIdentificaClienteDevolucao(loja.isIdentificaClienteDevolucao());
                lojaAtualizacao.setPermiteOrcamentoVenda(loja.isPermiteOrcamentoVenda());
                lojaAtualizacao.setVendaTrocaVendedor(loja.isVendaTrocaVendedor());
                lojaAtualizacao.setCaixaRapido(loja.isCaixaRapido());
                lojaAtualizacao.setImprimeSaldoDevolucao(loja.isImprimeSaldoDevolucao());
                lojaAtualizacao.setDevolucaoDescontaFatura(loja.isDevolucaoDescontaFatura());
                lojaAtualizacao.setEscolhePrecoPromocao(loja.isEscolhePrecoPromocao());
                lojaAtualizacao.setCalcPrecoDireto(loja.isCalcPrecoDireto());
                lojaAtualizacao.setDiminuirPrecoVenda(loja.isDiminuirPrecoVenda());
                lojaAtualizacao.setIdentificaClienteVenda(loja.isIdentificaClienteVenda());
                lojaAtualizacao.setIdentificaClienteOrcamento(loja.isIdentificaClienteOrcamento());
                lojaAtualizacao.setSolicitaObsDAV(loja.isSolicitaObsDAV());
                lojaAtualizacao.setRegistrarPagtoDevolucao(loja.isRegistrarPagtoDevolucao());
                lojaAtualizacao.setEfetuaBackUp(loja.isEfetuaBackUp());
                lojaAtualizacao.setMensagemOS(loja.getMensagemOS());
                lojaAtualizacao.setCabecalhoDav(loja.getCabecalhoDav());
                lojaAtualizacao.setCabecalhoOrcamento(loja.getCabecalhoOrcamento());
                lojaAtualizacao.setRodapeDav(loja.getRodapeDav());
                lojaAtualizacao.setRodapeOrcamento(loja.getRodapeOrcamento());
                lojaAtualizacao.setModeloEtiqueta(loja.getModeloEtiqueta());
                lojaAtualizacao.setExibeLocacaoOrcamento(loja.isExibeLocacaoOrcamento());
                lojaAtualizacao.setCalculaComissaFechaOS(loja.isCalculaComissaFechaOS());
                lojaAtualizacao.setDefineMecanicoFechaOS(loja.isDefineMecanicoFechaOS());
                lojaAtualizacao.setOfereceAgregado(loja.isOfereceAgregado());
                lojaAtualizacao.setMarkupEstoque(loja.getMarkupEstoque());

                if (lojaAtualizacao.getIdLoja() > 0) {
                    lojaDAO.altera(lojaAtualizacao, session);
                } else {
                    lojaDAO.inclui(lojaAtualizacao, session);
                }

            }

            confirmaTransacao(session);

        } catch (InfraEstruturaException e) {
            try {
                HibernateUtil.rollbackTransaction();
            } catch (InfraEstruturaException ie) {
            }

            throw e;
        } catch (Exception e) {
            try {
                HibernateUtil.rollbackTransaction();
            } catch (InfraEstruturaException ie) {
            }

            throw e;
        }
    }
}
