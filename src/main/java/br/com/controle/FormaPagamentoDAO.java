package br.com.controle;

import br.com.controle.exception.InfraEstruturaException;
import br.com.controle.exception.ObjetoNaoEncontradoException;
import br.com.modelo.FormaPagamento;
import br.com.util.HibernateUtil;
import java.util.Date;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author flavio.nunes
 */
public class FormaPagamentoDAO {

    public long inclui(FormaPagamento formaPgto, String session) {
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }            
            sessao.save(formaPgto);

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return formaPgto.getIdFormaPagamento();
    }

    public void altera(FormaPagamento formaPgto, String session) {
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }
                       
            sessao.update(formaPgto);            

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }
        
    }    
    
    public FormaPagamento retornaUmaFormaDePagamento(String descricao, String session) throws ObjetoNaoEncontradoException {
        FormaPagamento objForma = null;
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }

            String hql = " SELECT obj from FormaPagamento as obj"
                    + " LEFT JOIN FETCH obj.prazosPagamento"
                    + " WHERE 1=1"
                    + "AND obj.descricao = '" + descricao + "'";

            objForma = (FormaPagamento) sessao.createQuery(hql).uniqueResult();

            if (objForma == null) {
                throw new ObjetoNaoEncontradoException();
            }
        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return objForma;
    }

    @SuppressWarnings("unchecked")
    public List<FormaPagamento> listaFormasDePagamento(Date ultimaAtualizacao, String session) {
        List<FormaPagamento> lista = null;
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }

            String hql = " SELECT obj from FormaPagamento as obj"
                    + " LEFT OUTER JOIN FETCH obj.prazosPagamento"
                    + " where 1=1"
                    + " and (obj.ultimaAlteracao > :ULTIMAALTERACAO OR (obj.ultimaAlteracao is null and obj.dataCadastro > :ULTIMAALTERACAO))"
                    + "order by obj.descricao";
            
            Query qry = sessao.createQuery(hql);
            qry.setParameter("ULTIMAALTERACAO", ultimaAtualizacao);

            lista = qry.list();


        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return lista;
    }
}
