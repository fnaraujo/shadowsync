package br.com.controle;

import br.com.controle.exception.InfraEstruturaException;
import br.com.controle.exception.ObjetoNaoEncontradoException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import br.com.util.HibernateUtil;
import br.com.modelo.Empresa;

/**
 *
 * @author flavio.nunes
 */
public class EmpresaDAO {

    public long inclui(Empresa empresa) {
        try {
            Session sessao = HibernateUtil.getSession();

            sessao.save(empresa);

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return empresa.getCodigoid();
    }

    public long exclui(Empresa empresa) {
        try {
            Session sessao = HibernateUtil.getSession();

            sessao.delete(empresa);

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return empresa.getCodigoid();
    }

    public long altera(Empresa empresa) {
        try {
            Session sessao = HibernateUtil.getSession();
            sessao.update(empresa);

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return empresa.getCodigoid();
    }

    public Empresa retornaUmaEmpresa(long id, String session) throws ObjetoNaoEncontradoException {
        Empresa empresa = null;
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }
            empresa = (Empresa) sessao.get(Empresa.class, new Long(id));

            if (empresa == null) {
                throw new ObjetoNaoEncontradoException();
            }
        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return empresa;
    }

    public Empresa retornaUmEmpresa(String cpfCnpj) throws ObjetoNaoEncontradoException {
        Empresa empresa = null;
        try {
            Session sessao = HibernateUtil.getSession();

            String qry = " SELECT c from Empresa as c"
                    + " where "
                    + "c.cnpj = '" + cpfCnpj + "'";

            empresa = (Empresa) sessao.createQuery(qry).uniqueResult();

            if (empresa == null) {
                throw new ObjetoNaoEncontradoException();
            }
        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return empresa;
    }

    public Empresa retornaPrimeiraEmpresa(String session) {
        Empresa empresa = null;
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }

            String qry = " SELECT c from Empresa  as c  ";

            empresa = (Empresa) sessao.createQuery(qry).list().get(0);

            if (empresa == null) {
                try {
                    throw new ObjetoNaoEncontradoException();
                } catch (ObjetoNaoEncontradoException ex) {
                    Logger.getLogger(EmpresaDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return empresa;
    }

    @SuppressWarnings("unchecked")
    public List<Empresa> retornaEmpresas(String pesquisa) {
        List<Empresa> listaEmpresas = null;
        try {
            Session sessao = HibernateUtil.getSession();

            String qry = " SELECT ep from Empresa as ep"
                    + " where 1=1"
                    + " and upper(ep.nome) like upper('%" + pesquisa + "%') order by ep.nome asc";

            listaEmpresas = sessao.createQuery(qry).list();


        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return listaEmpresas;
    }
}
