package br.com.controle;

import br.com.controle.exception.InfraEstruturaException;
import br.com.controle.exception.ObjetoNaoEncontradoException;
import br.com.modelo.Funcionario;
import br.com.util.HibernateUtil;
import java.util.Date;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author flavio.nunes
 */
public class FuncionarioDAO {

    public long inclui(Funcionario funcionario, String session) {
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }            
            sessao.save(funcionario);

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return funcionario.getIdFuncionario();
    }

    public void altera(Funcionario funcionario, String session) {
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }
                       
            sessao.update(funcionario);            

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }
        
    }    
    
    public Funcionario retornaUmFuncionario(String cpf, String session) throws ObjetoNaoEncontradoException {
        Funcionario obj = null;
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }

            String hql = " SELECT obj from Funcionario as obj"                    
                    + " WHERE 1=1"
                    + "AND obj.cpf = '" + cpf + "'";

            obj = (Funcionario) sessao.createQuery(hql).uniqueResult();

            if (obj == null) {
                throw new ObjetoNaoEncontradoException();
            }
        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return obj;
    }

    @SuppressWarnings("unchecked")
    public List<Funcionario> listaFuncionario(Date ultimaAtualizacao, String session) {
        List<Funcionario> lista = null;
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }

            String hql = " SELECT obj from Funcionario as obj "
                    + " left outer join fetch obj.cep "
                    + " where 1=1"
                    + " and (obj.ultimaAtualizacao > :ULTIMAALTERACAO)"
                    + "order by obj.idFuncionario";
            
            Query qry = sessao.createQuery(hql);
            qry.setParameter("ULTIMAALTERACAO", ultimaAtualizacao);

            lista = qry.list();


        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return lista;
    }
    
    
}
