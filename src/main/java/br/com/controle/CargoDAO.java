package br.com.controle;

import br.com.controle.exception.InfraEstruturaException;
import br.com.controle.exception.ObjetoNaoEncontradoException;
import br.com.modelo.Cargo;
import br.com.util.HibernateUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author flavio.nunes
 */
public class CargoDAO {

    public long inclui(Cargo cargo, String session) {
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }            
            sessao.save(cargo);

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return cargo.getIdCargo();
    }

    public Cargo retornaUmCargo(String descricao, String session) throws ObjetoNaoEncontradoException {
        Cargo obj = null;
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }

            String hql = " SELECT obj from Cargo as obj"                    
                    + " WHERE 1=1"
                    + "AND obj.descricao = '" + descricao + "'";

            obj = (Cargo) sessao.createQuery(hql).uniqueResult();

            if (obj == null) {
                throw new ObjetoNaoEncontradoException();
            }
        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return obj;
    }
    
    @SuppressWarnings("unchecked")
    public List<Cargo> listaCargos(String session) {
        List<Cargo> lista = null;
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }

            String hql = " SELECT obj from Cargo as obj"                    
                    + " WHERE 1=1";

            Query qry = sessao.createQuery(hql);

            lista = qry.list();


        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return lista;
    }
}
