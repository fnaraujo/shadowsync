package br.com.controle;

import br.com.controle.exception.InfraEstruturaException;
import br.com.controle.exception.ObjetoNaoEncontradoException;
import br.com.modelo.Cliente;
import br.com.util.HibernateUtil;
import java.util.Date;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author flavio.nunes
 */
public class ClienteDAO {

    public long inclui(Cliente cliente, String session) {
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }
            sessao.save(cliente);

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return cliente.getIdCliente();
    }

    public long saveOrUpdate(Cliente cliente, String session) {
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }
            sessao.saveOrUpdate(cliente);

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return cliente.getIdCliente();
    }
    
    public long merge(Cliente cliente, String session) {
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }
            sessao.merge(cliente);

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return cliente.getIdCliente();
    }

    public Cliente retornaUmCliente(String cpfCnpj, String session) throws ObjetoNaoEncontradoException {
        Cliente obj = null;
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }

            String hql = " SELECT obj from Cliente as obj"
                    + " WHERE 1=1"
                    + "AND obj.cpfCnpj = '" + cpfCnpj + "'";

            obj = (Cliente) sessao.createQuery(hql).uniqueResult();

            if (obj == null) {
                throw new ObjetoNaoEncontradoException();
            }
        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return obj;
    }

    public long retornaIdCliente(String cpfCnpj, String session) throws ObjetoNaoEncontradoException {
        long id = 0;
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();
            }

            String hql = " SELECT obj.idCliente from Cliente as obj"
                    + " WHERE 1=1"
                    + "AND obj.cpfCnpj = '" + cpfCnpj + "'";

            Long obj = (Long) sessao.createQuery(hql).uniqueResult();

            if (obj != null) {
                id = obj;
            }

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return id;
    }

    public void delataContatosEnderecosCliente(long id, String session) throws ObjetoNaoEncontradoException {
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();
            }

            String hql = "DELETE FROM EnderecoCliente ec"
                    + " where ec.cliente.idCliente = " + id;

            sessao.createQuery(hql).executeUpdate();

            hql = "DELETE FROM ContatoCliente cc"
                    + " where cc.cliente.idCliente = " + id;

            sessao.createQuery(hql).executeUpdate();

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

    }

    @SuppressWarnings("unchecked")
    public List<Cliente> listaClientes(String session) {
        List<Cliente> lista = null;
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }

            String hql = " SELECT obj from Cliente as obj"
                    + " WHERE 1=1";

            Query qry = sessao.createQuery(hql);

            lista = qry.list();

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return lista;
    }

    public List<Cliente> listaClientes(Date ultimaAtualizacao, String session) {
        List<Cliente> lista = null;
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }

            String hql = " SELECT obj from Cliente as obj "
                    + " WHERE 1=1 "
                    + " and (obj.ultimaAlteracao > :ULTIMAALTERACAO OR (obj.ultimaAlteracao is null and obj.dataCadastro > :ULTIMAALTERACAO))"
                    + " order by obj.idCliente ";

            Query qry = sessao.createQuery(hql);

            qry.setParameter("ULTIMAALTERACAO", ultimaAtualizacao);

            lista = qry.list();

            if (lista != null && !lista.isEmpty()) {
                Hibernate.initialize(lista.get(0).getEnderecos());
                Hibernate.initialize(lista.get(0).getContatos());
            }

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return lista;
    }
}
