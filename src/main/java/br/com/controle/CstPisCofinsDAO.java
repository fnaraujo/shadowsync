package br.com.controle;

import br.com.controle.exception.InfraEstruturaException;
import br.com.controle.exception.ObjetoNaoEncontradoException;
import br.com.modelo.CstPisCofins;
import br.com.util.HibernateUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author flavio.nunes
 */
public class CstPisCofinsDAO {

    public long inclui(CstPisCofins cstPisCofins, String session) {
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }            
            sessao.save(cstPisCofins);

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return cstPisCofins.getIdCstPisCofins();
    }

    public CstPisCofins retornaUmCstPisCofins(String codigo, String session) throws ObjetoNaoEncontradoException {
        CstPisCofins obj = null;
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }

            String hql = " SELECT obj from CstPisCofins as obj"                    
                    + " WHERE 1=1"
                    + "AND obj.codigo = '" + codigo + "'";

            obj = (CstPisCofins) sessao.createQuery(hql).uniqueResult();

            if (obj == null) {
                throw new ObjetoNaoEncontradoException();
            }
        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return obj;
    }
    
    @SuppressWarnings("unchecked")
    public List<CstPisCofins> listaCstPisCofins(String session) {
        List<CstPisCofins> lista = null;
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }

            String hql = " SELECT obj from CstPisCofins as obj"                    
                    + " WHERE 1=1";

            Query qry = sessao.createQuery(hql);

            lista = qry.list();


        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return lista;
    }
}
