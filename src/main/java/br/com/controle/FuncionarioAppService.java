package br.com.controle;

import br.com.controle.exception.InfraEstruturaException;
import br.com.controle.exception.ObjetoNaoEncontradoException;
import br.com.modelo.Bairro;
import br.com.modelo.Cargo;
import br.com.modelo.Cep;
import br.com.modelo.Funcionario;
import br.com.modelo.StatusFunc;
import br.com.util.HibernateUtil;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Administrador
 */
public class FuncionarioAppService {

    private FuncionarioDAO funcionarioDAO = new FuncionarioDAO();
    private CargoDAO cargoDAO = new CargoDAO();
    private StatusFuncDAO statusFuncDAO = new StatusFuncDAO();
    private CepDAO cepDAO = new CepDAO();
    private BairroDAO bairroDAO = new BairroDAO();

    private void inicializaTransacao(String session) throws InfraEstruturaException {
        switch (session.toLowerCase()) {
            case "local":
                HibernateUtil.beginTransaction();
                break;
            case "remote":
                HibernateUtil.beginTransactionRemote();
                break;
            default:
                HibernateUtil.beginTransaction();

        }
    }

    private void confirmaTransacao(String session) throws InfraEstruturaException {
        switch (session.toLowerCase()) {
            case "local":
                HibernateUtil.commitTransaction();
                break;
            case "remote":
                HibernateUtil.commitTransactionRemote();
                break;
            default:
                HibernateUtil.commitTransaction();

        }

    }

    private void retornaTransacao(String session) throws InfraEstruturaException {
        switch (session.toLowerCase()) {
            case "local":
                HibernateUtil.rollbackTransaction();
                break;
            case "remote":
                HibernateUtil.rollbackTransactionRemote();
                break;
            default:
                HibernateUtil.rollbackTransaction();

        }

    }

    public long inclui(Funcionario funcionario, String session) {
        try {

            inicializaTransacao(session);

            long numero = funcionarioDAO.inclui(funcionario, session);

            confirmaTransacao(session);

            return numero;
        } catch (InfraEstruturaException e) {
            try {
                retornaTransacao(session);
            } catch (InfraEstruturaException ie) {
            }

            throw e;
        }
    }

    public void altera(Funcionario funcionario, String session) {
        try {
            inicializaTransacao(session);

            funcionarioDAO.altera(funcionario, session);

            confirmaTransacao(session);

        } catch (InfraEstruturaException e) {
            try {
                retornaTransacao(session);
            } catch (InfraEstruturaException ie) {
            }

            throw e;
        }
    }

    public Funcionario retornaUmFuncionario(String cpf, String session)
            throws Exception {
        try {
            inicializaTransacao(session);

            Funcionario funcionario = funcionarioDAO.retornaUmFuncionario(cpf, session);

            confirmaTransacao(session);

            return funcionario;
        } catch (ObjetoNaoEncontradoException e) {
            try {
                retornaTransacao(session);
            } catch (InfraEstruturaException ie) {
                throw ie;
            }

            throw e;
        } catch (InfraEstruturaException e) {
            try {
                HibernateUtil.rollbackTransaction();
            } catch (InfraEstruturaException ie) {
            }

            throw e;
        }
    }

    public List<Funcionario> listaFuncionarios(Date ultimaAtualizacao, String session) {
        List<Funcionario> lista = null;

        try {
            inicializaTransacao(session);

            lista = funcionarioDAO.listaFuncionario(ultimaAtualizacao, session);

            confirmaTransacao(session);
        } catch (InfraEstruturaException e) {
            try {
                retornaTransacao(session);
            } catch (InfraEstruturaException ie) {
            }

            throw e;
        }

        return lista;
    }

    public void atualizaCadastros(List<Funcionario> funcionarios, String session)
            throws Exception {
        try {
            inicializaTransacao(session);
            List<Cargo> cargos = cargoDAO.listaCargos(session);
            List<StatusFunc> status = statusFuncDAO.listaCargos(session);

            for (Funcionario funcionario : funcionarios) {

                Funcionario funcAtualizacao = null;

                Cep cep = null;
                if (funcionario.getCep() != null) {

                    Bairro bairro;
                    try {
                        bairro = bairroDAO.retornaUmBairro(funcionario.getCep()
                                .getBairro().getNome(), funcionario.getCep()
                                .getBairro().getMunicipio().getIdMunicipio(), session);

                    } catch (ObjetoNaoEncontradoException e) {
                        bairro = funcionario.getCep().getBairro();
                        bairro.setIdBairro(0);
                        bairroDAO.inclui(bairro, session);
                    }

                    try {
                        cep = cepDAO.retornaUmCep(funcionario.getCep().getNumero(), session);
                        cep.setBairro(bairro);
                    } catch (ObjetoNaoEncontradoException e) {
                        cep = funcionario.getCep();
                        cep.setBairro(bairro);
                        cepDAO.inclui(cep, session);
                    }

                }

                try {
                    funcAtualizacao = funcionarioDAO.retornaUmFuncionario(funcionario.getCpf(), session);
                } catch (ObjetoNaoEncontradoException e) {
                    funcAtualizacao = new Funcionario();
                    funcAtualizacao.setCpf(funcionario.getCpf());
                }

                funcAtualizacao.setCep(cep);

                funcAtualizacao.setAtivo(funcionario.getAtivo());

                Cargo cargoAlt = null;
                if (funcAtualizacao.getCargo() == null) {
                    for (int i = 0; i < cargos.size(); i++) {
                        if (funcionario.getCargo().getDescricao().equals(cargos.get(i).getDescricao())) {
                            cargoAlt = cargos.get(i);
                        }
                    }

                    if (cargoAlt == null) {
                        cargoAlt = new Cargo();
                        cargoAlt.setDescricao(funcionario.getCargo().getDescricao());

                        cargoDAO.inclui(cargoAlt, session);
                        cargos.add(cargoAlt);

                    }
                    funcAtualizacao.setCargo(cargoAlt);

                } else if (!funcAtualizacao.getCargo().getDescricao().equals(funcionario.getCargo().getDescricao())) {
                    for (int i = 0; i < cargos.size(); i++) {
                        if (funcionario.getCargo().getDescricao().equals(cargos.get(i).getDescricao())) {
                            cargoAlt = cargos.get(i);
                        }
                    }

                    if (cargoAlt == null) {
                        cargoAlt = new Cargo();
                        cargoAlt.setDescricao(funcionario.getCargo().getDescricao());

                        cargoDAO.inclui(cargoAlt, session);
                        cargos.add(cargoAlt);

                    }
                    funcAtualizacao.setCargo(cargoAlt);
                }

                StatusFunc statusAlt = null;
                if (funcAtualizacao.getStatusFunc() == null) {
                    for (int i = 0; i < status.size(); i++) {
                        if (funcionario.getStatusFunc().getDescricao().equals(status.get(i).getDescricao())) {
                            statusAlt = status.get(i);
                        }
                    }
                    
                    
                    

                    if (statusAlt == null) {
                        statusAlt = new StatusFunc();
                        statusAlt.setDescricao(funcionario.getStatusFunc().getDescricao());

                        statusFuncDAO.inclui(statusAlt, session);
                        status.add(statusAlt);

                    }
                    funcAtualizacao.setStatusFunc(statusAlt);

                } else if (!funcAtualizacao.getStatusFunc().getDescricao().equals(funcionario.getStatusFunc().getDescricao())) {
                    for (int i = 0; i < status.size(); i++) {
                        if (funcionario.getStatusFunc().getDescricao().equals(status.get(i).getDescricao())) {
                            statusAlt = status.get(i);
                        }
                    }

                    if (statusAlt == null) {
                        statusAlt = new StatusFunc();
                        statusAlt.setDescricao(funcionario.getStatusFunc().getDescricao());

                        statusFuncDAO.inclui(statusAlt, session);
                        status.add(statusAlt);

                    }
                    funcAtualizacao.setStatusFunc(statusAlt);
                }

                funcAtualizacao.setDataCadastro(funcionario.getDataCadastro());

                funcAtualizacao.setComplemento(funcionario.getComplemento());
                funcAtualizacao.setDataDemissao(funcionario.getDataDemissao());
                funcAtualizacao.setDataNascimento(funcionario.getDataNascimento());
                funcAtualizacao.setEmail(funcionario.getEmail());
                funcAtualizacao.setFoto(funcionario.getFoto());
                funcAtualizacao.setIdentidade(funcionario.getIdentidade());
                funcAtualizacao.setMatricula(funcionario.getMatricula());
                funcAtualizacao.setNome(funcionario.getNome());
                funcAtualizacao.setNumero(funcionario.getNumero());
                funcAtualizacao.setOrgExp(funcionario.getOrgExp());
                funcAtualizacao.setSexo(funcionario.getSexo());
                funcAtualizacao.setTelefone(funcionario.getTelefone());
                funcAtualizacao.setTelefone2(funcionario.getTelefone2());
                funcAtualizacao.setUltimaAtualizacao(funcionario.getUltimaAtualizacao());
                funcAtualizacao.setSalarioBase(funcionario.getSalarioBase());
                funcAtualizacao.setUsuario(funcionario.getUsuario());
//                funcAtualizacao.setSenha(funcionario.getSenha());

                if (funcAtualizacao.getIdFuncionario() > 0) {
                    funcionarioDAO.altera(funcAtualizacao, session);
                } else {
                    funcionarioDAO.inclui(funcAtualizacao, session);
                }
            }

            confirmaTransacao(session);

        } catch (Exception e) {
            try {
                retornaTransacao(session);
            } catch (InfraEstruturaException ie) {
            }

            throw e;
        }
    }
}
