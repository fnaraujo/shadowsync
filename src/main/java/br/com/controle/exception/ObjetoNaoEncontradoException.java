package br.com.controle.exception;

/**
 *
 * @author Flávio Augusto
 */
public class ObjetoNaoEncontradoException extends Exception {
    private final static long serialVersionUID = 1;

    public ObjetoNaoEncontradoException() {
    }
}
