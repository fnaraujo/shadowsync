package br.com.controle.exception;

/**
 *
 * @author Flavio Nunes
 */
public class InfraEstruturaException extends RuntimeException{
    private final static long serialVersionUID = 1;

    public InfraEstruturaException(Exception e) {
        super(e);
    }
}