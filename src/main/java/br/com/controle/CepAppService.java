package br.com.controle;

import br.com.controle.exception.InfraEstruturaException;
import br.com.controle.exception.ObjetoNaoEncontradoException;
import br.com.modelo.Bairro;
import br.com.modelo.Cep;
import br.com.util.HibernateUtil;
import java.util.List;

/**
 *
 * @author Sisbras
 */
public class CepAppService {

    private CepDAO cepDAO = new CepDAO();
    private BairroDAO bairroDAO = new BairroDAO();

    private void inicializaTransacao(String session) throws InfraEstruturaException {
        switch (session.toLowerCase()) {
            case "local":
                HibernateUtil.beginTransaction();
                break;
            case "remote":
                HibernateUtil.beginTransactionRemote();
                break;
            default:
                HibernateUtil.beginTransaction();

        }
    }

    private void confirmaTransacao(String session) throws InfraEstruturaException {
        switch (session.toLowerCase()) {
            case "local":
                HibernateUtil.commitTransaction();
                break;
            case "remote":
                HibernateUtil.commitTransactionRemote();
                break;
            default:
                HibernateUtil.commitTransaction();

        }

    }

    private void retornaTransacao(String session) throws InfraEstruturaException {
        switch (session.toLowerCase()) {
            case "local":
                HibernateUtil.rollbackTransaction();
                break;
            case "remote":
                HibernateUtil.rollbackTransactionRemote();
                break;
            default:
                HibernateUtil.rollbackTransaction();

        }

    }

    public void atualizaCadastro(List<Cep> ceps, String sessao) throws Exception {
        try {

            inicializaTransacao(sessao);

            for (Cep cep : ceps) {
                try {
                    cepDAO.retornaUmCep(cep.getNumero(), sessao);
                } catch (ObjetoNaoEncontradoException e) {
                    try {
                        Bairro b = bairroDAO.retornaUmBairro(cep.getBairro().getNome(), cep.getBairro().getMunicipio(), sessao);
                        cep = cep.clone();
                        cep.setBairro(b);
                        cepDAO.inclui(cep, sessao);
                    } catch (ObjetoNaoEncontradoException e1) {
                    }
                }
            }

            confirmaTransacao(sessao);

        } catch (Exception e) {
            retornaTransacao(sessao);
            e.printStackTrace();
            throw e;
        }
    }
}
