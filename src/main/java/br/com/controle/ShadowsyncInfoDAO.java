package br.com.controle;

import br.com.controle.exception.InfraEstruturaException;
import br.com.modelo.ShadowsyncInfo;
import br.com.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;

/**
 *
 * @author SAMUEL
 */
public class ShadowsyncInfoDAO {

    public void altera(ShadowsyncInfo shadowsyncInfo, String session) {
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }

            sessao.update(shadowsyncInfo);

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }
    }
}
