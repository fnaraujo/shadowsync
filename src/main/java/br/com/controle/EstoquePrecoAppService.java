package br.com.controle;

import br.com.controle.exception.InfraEstruturaException;
import br.com.controle.exception.ObjetoNaoEncontradoException;
import br.com.modelo.EstoquePreco;
import br.com.modelo.EstoqueProduto;
import br.com.modelo.Loja;
import br.com.modelo.TabelaPreco;
import br.com.util.HibernateUtil;
import java.util.Date;
import java.util.List;
import org.hibernate.Hibernate;

/**
 *
 * @author SAMUEL
 */
public class EstoquePrecoAppService {

    private EstoquePrecoDAO estoquePrecoDAO = new EstoquePrecoDAO();
    private TabelaPrecoDAO tabelaPrecoDAO = new TabelaPrecoDAO();
    private EstoqueProdutoDAO estoqueProdutoDAO = new EstoqueProdutoDAO();

    private void inicializaTransacao(String session) throws InfraEstruturaException {
        switch (session.toLowerCase()) {
            case "local":
                HibernateUtil.beginTransaction();
                break;
            case "remote":
                HibernateUtil.beginTransactionRemote();
                break;
            default:
                HibernateUtil.beginTransaction();
        }
    }

    private void confirmaTransacao(String session) throws InfraEstruturaException {
        switch (session.toLowerCase()) {
            case "local":
                HibernateUtil.commitTransaction();
                break;
            case "remote":
                HibernateUtil.commitTransactionRemote();
                break;
            default:
                HibernateUtil.commitTransaction();
        }
    }

    private void retornaTransacao(String session) throws InfraEstruturaException {
        switch (session.toLowerCase()) {
            case "local":
                HibernateUtil.rollbackTransaction();
                break;
            case "remote":
                HibernateUtil.rollbackTransactionRemote();
                break;
            default:
                HibernateUtil.rollbackTransaction();
        }
    }

    public List<EstoquePreco> listaEstoquePreco(Date ultimaAtualizacao, String session) {
        List<EstoquePreco> lista = null;
        try {
            inicializaTransacao(session);

            lista = estoquePrecoDAO.listaPrecos(ultimaAtualizacao, session);

            confirmaTransacao(session);
        } catch (InfraEstruturaException e) {
            try {
                retornaTransacao(session);
            } catch (InfraEstruturaException ie) {
            }
            throw e;
        }
        return lista;
    }

    public List<EstoquePreco> listaEstoquePreco(Date ultimaAtualizacao, Loja loja, String session) {
        List<EstoquePreco> lista = null;
        try {
            inicializaTransacao(session);

            lista = estoquePrecoDAO.listaPrecos(ultimaAtualizacao, loja, session);

            confirmaTransacao(session);
        } catch (InfraEstruturaException e) {
            try {
                retornaTransacao(session);
            } catch (InfraEstruturaException ie) {
            }
            throw e;
        }
        return lista;
    }

    public void atualizaCadastros(List<EstoquePreco> listaPreco, String session, Loja loja, boolean AtualizaOutrasLojas) throws Exception {
        try {

            List<Loja> lojas = new LojaAppService().listaOutrasLojas(loja, session);
            loja = new LojaAppService().retornaUmaLoja(loja.getEmpresa().getCnpj(), session);

            inicializaTransacao(session);

            for (EstoquePreco p : listaPreco) {

                atualizaCadastro(p, loja, session);

                if (AtualizaOutrasLojas) {
                    for (Loja l : lojas) {
                        atualizaCadastro(p, l, session);
                    }
                }
            }

            confirmaTransacao(session);
        } catch (Exception e) {
            try {
                retornaTransacao(session);
            } catch (InfraEstruturaException ie) {
            }
            throw e;
        }
    }

    public void recebeCadastros(List<EstoquePreco> listaPreco, String session) throws Exception {
        try {

            inicializaTransacao(session);

            for (EstoquePreco p : listaPreco) {

                Loja loja = new LojaDAO().retornaUmaLoja(p.getEstoqueProduto().getLoja().getEmpresa().getCnpj(), session);

                atualizaCadastro(p, loja, session);
            }

            confirmaTransacao(session);
        } catch (Exception e) {
            try {
                retornaTransacao(session);
            } catch (InfraEstruturaException ie) {
            }
            throw e;
        }
    }

    private void atualizaCadastro(EstoquePreco p, Loja loja, String session) {

        EstoquePreco preco = estoquePrecoDAO
                .retornaUmPreco(p.getEstoqueProduto().getProduto()
                        .getCodigo(), p.getNumOrdem(), loja, session);

        estoquePrecoDAO.registraLog(preco, session);

        if (preco == null) {
            try {
                EstoqueProduto ep = estoqueProdutoDAO.retornaUmEstoque(p.getEstoqueProduto(), loja, session);
                if (ep == null) {
                    return;
                }
                preco = criaPreco(p, ep, loja, session);
            } catch (Exception e) {
                return;
            }
        }

        TabelaPreco tab = retornaTabelaPreco(p.getTabelaPreco().getIndice(), loja, session);

        preco.setTabelaPreco(tab);
        preco.setPercentualPromocao(p.getPercentualPromocao());
        preco.setValorDesconto(p.getValorDesconto());
        preco.setDataLimiteDesconto(p.getDataLimiteDesconto());
        preco.setValorVenda(p.getValorVenda());
        preco.setAtualizacaoPreco(Sistema.getInstance().retornaDataBancoSemBlocoTransacao(session));

        estoquePrecoDAO.altera(preco, session);
    }

    private TabelaPreco retornaTabelaPreco(float indice, Loja loja, String session) {
        TabelaPreco tab;
        try {
            tab = tabelaPrecoDAO.retornaUmProduto(indice, session);
        } catch (ObjetoNaoEncontradoException e) {
            tab = new TabelaPreco(0, loja, ("TAB INDICE=" + indice).replaceAll("\\.", ","), indice, true, true);
            tabelaPrecoDAO.inclui(tab, session);
        }
        return tab;
    }

    private EstoquePreco criaPreco(EstoquePreco precoOutraBase, EstoqueProduto ep, Loja loja, String session) {
        EstoquePreco preco = new EstoquePreco();

        preco.setAtualizacaoPreco(precoOutraBase.getAtualizacaoPreco());
        preco.setDataLimiteDesconto(precoOutraBase.getDataLimiteDesconto());
        preco.setEstoqueProduto(ep);
        preco.setNumOrdem(precoOutraBase.getNumOrdem());
        preco.setPercentualPromocao(precoOutraBase.getPercentualPromocao());
        preco.setPrecoPadrao(precoOutraBase.getPrecoPadrao());
        preco.setTabelaPreco(retornaTabelaPreco(precoOutraBase.getTabelaPreco().getIndice(), loja, session));
        preco.setValorDesconto(precoOutraBase.getValorDesconto());
        preco.setValorVenda(precoOutraBase.getValorVenda());

        estoquePrecoDAO.inclui(preco, session);

        return preco;
    }
}
