package br.com.controle;

import br.com.controle.exception.InfraEstruturaException;
import br.com.controle.exception.ObjetoNaoEncontradoException;
import br.com.modelo.Municipio;
import br.com.util.HibernateUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Sisbras
 */
public class MunicipioDAO {

    public long inclui(Municipio municipio, String session) {
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }
            sessao.save(municipio);

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return municipio.getIdMunicipio();
    }

    public Municipio retornaUmMunicipio(String nome, int idUnidadeFederacao, String session) throws ObjetoNaoEncontradoException {

        Municipio obj = null;
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }

            String hql = "SELECT obj FROM Municipio as obj "
                    + " where obj.nome = :NOME "
                    + " and obj.unidadeFederacao.idUnidadeFederacao = :ID_UNIDADE_FEDERACAO "
                    + " order by obj.idMunicipio ";

            Query qry = sessao.createQuery(hql);

            qry.setParameter("NOME", nome);
            qry.setParameter("ID_UNIDADE_FEDERACAO", idUnidadeFederacao);

            List lista = qry.list();
            if (lista != null & !lista.isEmpty()) {
                obj = (Municipio) lista.get(0);
            }

            if (obj == null) {
                throw new ObjetoNaoEncontradoException();
            }

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return obj;
    }
}
