package br.com.controle;

import br.com.controle.exception.InfraEstruturaException;
import br.com.controle.exception.ObjetoNaoEncontradoException;
import br.com.modelo.Ncm;
import br.com.modelo.UnidadeMedida;
import br.com.util.HibernateUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author flavio.nunes
 */
public class UnidadeMedidaDAO {

    public long inclui(UnidadeMedida unidade, String session) {
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }            
            sessao.save(unidade);

        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return unidade.getIdUnidadeMedida();
    }

    public UnidadeMedida retornaUnidadeBySigla(String sigla, String session) throws ObjetoNaoEncontradoException {
        UnidadeMedida obj = null;
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }

            String hql = " SELECT obj from UnidadeMedida as obj"                    
                    + " WHERE 1=1"
                    + "AND obj.mnemonico = :SIGLA";

            Query qry  = sessao.createQuery(hql);
            
            qry.setParameter("SIGLA", sigla);
            
            obj = (UnidadeMedida) qry.setMaxResults(1).uniqueResult();

            if (obj == null) {
                throw new ObjetoNaoEncontradoException();
            }
        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return obj;
    }
    
    @SuppressWarnings("unchecked")
    public List<UnidadeMedida> listaUnidades(String session) {
        List<UnidadeMedida> lista = null;
        try {
            Session sessao;
            switch (session.toLowerCase()) {
                case "local":
                    sessao = HibernateUtil.getSession();
                    break;
                case "remote":
                    sessao = HibernateUtil.getSessionRemote();
                    break;
                default:
                    sessao = HibernateUtil.getSession();

            }

            String hql = " SELECT obj from UnidadeMedida as obj"                    
                    + " WHERE 1=1";

            Query qry = sessao.createQuery(hql);

            lista = qry.list();


        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }

        return lista;
    }
}
