package br.com.controle;

import br.com.controle.exception.InfraEstruturaException;
import br.com.controle.exception.ObjetoNaoEncontradoException;
import br.com.modelo.Bairro;
import br.com.modelo.Municipio;
import br.com.util.HibernateUtil;
import java.util.List;

/**
 *
 * @author Sisbras
 */
public class BairroAppService {

    private BairroDAO bairroDAO = new BairroDAO();
    private MunicipioDAO municipioDAO = new MunicipioDAO();

    private void inicializaTransacao(String session) throws InfraEstruturaException {
        switch (session.toLowerCase()) {
            case "local":
                HibernateUtil.beginTransaction();
                break;
            case "remote":
                HibernateUtil.beginTransactionRemote();
                break;
            default:
                HibernateUtil.beginTransaction();

        }
    }

    private void confirmaTransacao(String session) throws InfraEstruturaException {
        switch (session.toLowerCase()) {
            case "local":
                HibernateUtil.commitTransaction();
                break;
            case "remote":
                HibernateUtil.commitTransactionRemote();
                break;
            default:
                HibernateUtil.commitTransaction();

        }

    }

    private void retornaTransacao(String session) throws InfraEstruturaException {
        switch (session.toLowerCase()) {
            case "local":
                HibernateUtil.rollbackTransaction();
                break;
            case "remote":
                HibernateUtil.rollbackTransactionRemote();
                break;
            default:
                HibernateUtil.rollbackTransaction();

        }

    }

    public void atualizaCadastro(List<Bairro> bairros, String sessao) throws Exception {

        try {

            inicializaTransacao(sessao);

            for (Bairro b : bairros) {

                Municipio m;
                try {
                    m = municipioDAO.retornaUmMunicipio(b.getMunicipio().getNome(), b.getMunicipio().getUnidadeFederacao().getIdUnidadeFederacao(), sessao);
                } catch (ObjetoNaoEncontradoException e) {
                    m = b.getMunicipio().clone();
                    municipioDAO.inclui(m, sessao);
                }

                try {
                    bairroDAO.retornaUmBairro(b.getNome(), m.getIdMunicipio(), sessao);
                } catch (ObjetoNaoEncontradoException e) {
                    b.setMunicipio(m);
                    bairroDAO.inclui(b.clone(), sessao);
                }
            }

            confirmaTransacao(sessao);

        } catch (Exception e) {
            retornaTransacao(sessao);
            e.printStackTrace();
            throw e;
        }
    }
}
