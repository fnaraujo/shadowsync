package br.com.controle;

import br.com.controle.exception.InfraEstruturaException;
import br.com.controle.exception.ObjetoNaoEncontradoException;
import br.com.modelo.CodigoTributario;
import br.com.modelo.CstIPI;
import br.com.modelo.CstPisCofins;
import br.com.modelo.EstoquePreco;
import br.com.modelo.EstoqueProduto;
import br.com.modelo.Fabricante;
import br.com.modelo.Grupo;
import br.com.modelo.Loja;
import br.com.modelo.LojaDepartamento;
import br.com.modelo.Ncm;
import br.com.modelo.Produto;
import br.com.modelo.TabelaPreco;
import br.com.modelo.UnidadeMedida;
import br.com.util.HibernateUtil;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.hibernate.Hibernate;

/**
 *
 * @author Administrador
 */
public class EstoqueProdutoAppService {

    private EstoqueProdutoDAO estoqueDAO = new EstoqueProdutoDAO();
    private LojaDAO lojaDAO = new LojaDAO();
    private GrupoDAO grupoDAO = new GrupoDAO();
    private UnidadeMedidaDAO unidadeMedidaDAO = new UnidadeMedidaDAO();
    private FabricanteDAO fabricanteDAO = new FabricanteDAO();
    private NcmDAO ncmDAO = new NcmDAO();
    private CodigoTributarioDAO codigoTributarioDAO = new CodigoTributarioDAO();
    private CstPisCofinsDAO cstPisCofinsDAO = new CstPisCofinsDAO();
    private CstIpiDAO cstIpiDAO = new CstIpiDAO();
    private TabelaPrecoDAO tabelaPrecoDAO = new TabelaPrecoDAO();
    private EstoquePrecoDAO estoquePrecoDAO = new EstoquePrecoDAO();

    List<LojaDepartamento> departamentos;

    private void inicializaTransacao(String session) throws InfraEstruturaException {
        switch (session.toLowerCase()) {
            case "local":
                HibernateUtil.beginTransaction();
                break;
            case "remote":
                HibernateUtil.beginTransactionRemote();
                break;
            default:
                HibernateUtil.beginTransaction();

        }
    }

    private void confirmaTransacao(String session) throws InfraEstruturaException {
        switch (session.toLowerCase()) {
            case "local":
                HibernateUtil.commitTransaction();
                break;
            case "remote":
                HibernateUtil.commitTransactionRemote();
                break;
            default:
                HibernateUtil.commitTransaction();

        }

    }

    private void retornaTransacao(String session) throws InfraEstruturaException {
        switch (session.toLowerCase()) {
            case "local":
                HibernateUtil.rollbackTransaction();
                break;
            case "remote":
                HibernateUtil.rollbackTransactionRemote();
                break;
            default:
                HibernateUtil.rollbackTransaction();

        }

    }

    public long inclui(EstoqueProduto estoque, String session) {
        try {

            inicializaTransacao(session);

            long numero = estoqueDAO.inclui(estoque, null, session);

            confirmaTransacao(session);

            return numero;
        } catch (InfraEstruturaException e) {
            try {
                retornaTransacao(session);
            } catch (InfraEstruturaException ie) {
            }

            throw e;
        }
    }

    public void altera(EstoqueProduto estoque, String session) {
        try {
            inicializaTransacao(session);

            estoqueDAO.altera(estoque, session);

            confirmaTransacao(session);

        } catch (InfraEstruturaException e) {
            try {
                retornaTransacao(session);
            } catch (InfraEstruturaException ie) {
            }

            throw e;
        }
    }

    public EstoqueProduto retornaUmEstoque(String codigo, String session, Loja loja)
            throws Exception {
        try {
            inicializaTransacao(session);

            EstoqueProduto estoque = estoqueDAO.retornaUmEstoque(codigo, session, loja);

            confirmaTransacao(session);

            return estoque;
        } catch (ObjetoNaoEncontradoException e) {
            try {
                retornaTransacao(session);
            } catch (InfraEstruturaException ie) {
                throw ie;
            }

            throw e;
        } catch (InfraEstruturaException e) {
            try {
                HibernateUtil.rollbackTransaction();
            } catch (InfraEstruturaException ie) {
            }

            throw e;
        }
    }

    public List<EstoqueProduto> listaEstoque(Date ultimaAtualizacao, String session, Loja loja) {
        List<EstoqueProduto> lista = null;

        try {
            inicializaTransacao(session);

            lista = estoqueDAO.listaEstoque(ultimaAtualizacao, session, loja);

            for (EstoqueProduto estoqueProduto : lista) {
                Hibernate.initialize(estoqueProduto.getEstoquePrecos());
                Hibernate.initialize(estoqueProduto.getDepartamento());
                for (EstoquePreco preco : estoqueProduto.getEstoquePrecos()) {
                    Hibernate.initialize(preco.getTabelaPreco());
                }
            }

            confirmaTransacao(session);
        } catch (InfraEstruturaException e) {
            try {
                retornaTransacao(session);
            } catch (InfraEstruturaException ie) {
            }

            throw e;
        }

        return lista;
    }

    public List<EstoqueProduto> listaEstoqueOutrasLojas(Date ultimaAtualizacao, String session, Loja loja) {
        List<EstoqueProduto> lista = null;

        try {
            inicializaTransacao(session);

            lista = estoqueDAO.listaEstoqueOutrasLojas(ultimaAtualizacao, session, loja);

            for (EstoqueProduto estoqueProduto : lista) {
                Hibernate.initialize(estoqueProduto.getEstoquePrecos());
                Hibernate.initialize(estoqueProduto.getDepartamento());
                Hibernate.initialize(estoqueProduto.getLoja());
                for (EstoquePreco preco : estoqueProduto.getEstoquePrecos()) {
                    Hibernate.initialize(preco.getTabelaPreco());
                }
            }

            confirmaTransacao(session);
        } catch (InfraEstruturaException e) {
            try {
                retornaTransacao(session);
            } catch (InfraEstruturaException ie) {
            }
            throw e;
        }

        return lista;
    }

    /*
     * para atualizar o produto da loja especifica
     */
    public void atualizaCadastros(List<EstoqueProduto> listaEstoque, String session, Loja loja)
            throws Exception {
        String codigo = "";
        try {

            List<Loja> lojas = new LojaAppService().listaOutrasLojas(loja, session);
            loja = new LojaAppService().retornaUmaLoja(loja.getEmpresa().getCnpj(), session);

            inicializaTransacao(session);

            departamentos = lojaDAO.listaDepartamentos(loja, session);

            for (EstoqueProduto estoque : listaEstoque) {

                codigo = estoque.getProduto().getCodigo();
                EstoqueProduto objAtualizacao = null;

                try {

                    objAtualizacao = estoqueDAO.retornaUmEstoque(estoque.getProduto().getCodigo(), session, loja);

                    this.comparaProduto(estoque, objAtualizacao, session);

                    estoqueDAO.altera(objAtualizacao, session);

                    this.comparaDepartamento(estoque, objAtualizacao, loja, session);
//                    this.comparaPrecos(estoque, objAtualizacao, loja, session);

                } catch (ObjetoNaoEncontradoException e) {

                    //Inicializar tudo que é referente ao estoque com os valores padroes
                    objAtualizacao = new EstoqueProduto();

                    try {
                        objAtualizacao.setProduto(new ProdutoDAO().retornaUmProduto(estoque.getProduto().getCodigo(), session));
                    } catch (ObjetoNaoEncontradoException ex) {
                        objAtualizacao.setProduto(new Produto());
                    }

                    objAtualizacao.getProduto().setCodigo(estoque.getProduto().getCodigo());
                    objAtualizacao.getProduto().setDataCadastro(estoque.getProduto().getDataCadastro());

                    objAtualizacao.setLoja(loja);
//                    objAtualizacao.setDepartamento(new LojaDepartamento(0, 0, "", loja));
                    objAtualizacao.setClasseAbc(loja.getAbcInicial());
                    objAtualizacao.setQtdMinimo(BigDecimal.ZERO);
                    objAtualizacao.setQtdMaximo(BigDecimal.ZERO);
//                    objAtualizacao.setQtdDisponivel(BigDecimal.ZERO);
                    objAtualizacao.setCustoMedio(estoque.getCustoMedio());
                    objAtualizacao.setCustoReposicao(estoque.getCustoReposicao());
                    objAtualizacao.setDemandaMes(BigDecimal.ZERO);
                    objAtualizacao.setLocacao("");
                    objAtualizacao.setPagaComissao(estoque.getPagaComissao());
                    objAtualizacao.setEsgotamentoDiario(BigDecimal.ZERO);
                    objAtualizacao.setLiberadoCompra(true);
                    objAtualizacao.setLiberadoContagem(true);

                    this.comparaProduto(estoque, objAtualizacao, session);

                    estoqueDAO.inclui(objAtualizacao, estoque.getEstoquePrecos(), lojas, loja, session);

                    this.comparaDepartamento(estoque, objAtualizacao, loja, session);
//                    this.comparaPrecos(estoque, objAtualizacao, loja, session);
                }

//                if (objAtualizacao.getIdEstoque() > 0) {
//                    estoqueDAO.altera(objAtualizacao, session);
//                } else {
//                    estoqueDAO.inclui(objAtualizacao, lojas, session);
//                }
                switch (session.toLowerCase()) {
                    case "local":
                        HibernateUtil.flushLocal();
                        break;
                    case "remote":
                        HibernateUtil.flushRemote();
                        break;
                    default:
                        HibernateUtil.flushLocal();
                }
            }

            confirmaTransacao(session);

        } catch (InfraEstruturaException e) {
            System.out.println("----------------->" + codigo);
            try {
                retornaTransacao(session);
            } catch (InfraEstruturaException ie) {
            }
            throw e;
        } catch (Exception e) {
            System.out.println("----------------->" + codigo);
            try {
                retornaTransacao(session);
            } catch (InfraEstruturaException ie) {
            }

            throw e;
        }
    }

    /*
     * para atualizar todos produtos das outras lojas
     */
    public void atualizaCadastrosOutrasLojas(List<EstoqueProduto> listaEstoque, String session, Loja loja)
            throws Exception {
        try {

            List<Loja> lojas = new LojaAppService().listaOutrasLojas(loja, session);
            loja = new LojaAppService().retornaUmaLoja(loja.getEmpresa().getCnpj(), session);

            inicializaTransacao(session);

            departamentos = lojaDAO.listaDepartamentos(loja, session);
//            List<Loja> lojas = lojaDAO.listaOutrasLojas(session, loja);

            for (EstoqueProduto estoque : listaEstoque) {

                EstoqueProduto objAtualizacao = null;

                try {

                    // verifica se a instancia da outra loja possui na base local
                    objAtualizacao = estoqueDAO.retornaUmEstoque(estoque, session);

                    this.comparaProduto(estoque, objAtualizacao, session);
                    this.comparaDepartamento(estoque, objAtualizacao, loja, session);
//                    this.comparaPrecos(estoque, objAtualizacao, loja, session);

                    estoqueDAO.altera(objAtualizacao, session);

                } catch (ObjetoNaoEncontradoException e) {

                    try {
                        // verifica se o produto ja possui instancia ne loja local
                        // para evitar duplicidades
                        objAtualizacao = estoqueDAO.retornaUmEstoque(estoque.getProduto().getCodigo(), session, loja);

//                        estoqueDAO.inclui(objAtualizacao, lojas, session);
                        estoqueDAO.inclui(objAtualizacao, estoque.getEstoquePrecos(), lojas, loja, session);

                    } catch (ObjetoNaoEncontradoException e1) {

                        //Inicializar tudo que é referente ao estoque com os valores padroes
                        objAtualizacao = new EstoqueProduto();

                        try {
                            objAtualizacao.setProduto(new ProdutoDAO().retornaUmProduto(estoque.getProduto().getCodigo(), session));
                        } catch (ObjetoNaoEncontradoException ex) {
                            objAtualizacao.setProduto(new Produto());
                        }

                        objAtualizacao.getProduto().setCodigo(estoque.getProduto().getCodigo());
                        objAtualizacao.getProduto().setDataCadastro(estoque.getProduto().getDataCadastro());

                        objAtualizacao.setLoja(loja);
//                    objAtualizacao.setDepartamento(new LojaDepartamento(0, 0, "", loja));
                        objAtualizacao.setClasseAbc(loja.getAbcInicial());
                        objAtualizacao.setQtdMinimo(BigDecimal.ZERO);
                        objAtualizacao.setQtdMaximo(BigDecimal.ZERO);
//                    objAtualizacao.setQtdDisponivel(BigDecimal.ZERO);
                        objAtualizacao.setCustoMedio(estoque.getCustoMedio());
                        objAtualizacao.setCustoReposicao(estoque.getCustoReposicao());
                        objAtualizacao.setDemandaMes(BigDecimal.ZERO);
                        objAtualizacao.setLocacao("");
                        objAtualizacao.setPagaComissao(estoque.getPagaComissao());
                        objAtualizacao.setEsgotamentoDiario(BigDecimal.ZERO);
                        objAtualizacao.setLiberadoCompra(true);
                        objAtualizacao.setLiberadoContagem(true);

                        this.comparaProduto(estoque, objAtualizacao, session);

//                        estoqueDAO.inclui(objAtualizacao, lojas, session);
                        estoqueDAO.inclui(objAtualizacao, estoque.getEstoquePrecos(), lojas, loja, session);

                        this.comparaDepartamento(estoque, objAtualizacao, loja, session);
//                        this.comparaPrecos(estoque, objAtualizacao, loja, session);

                    }

                }

                switch (session.toLowerCase()) {
                    case "local":
                        HibernateUtil.flushLocal();
                        break;
                    case "remote":
                        HibernateUtil.flushRemote();
                        break;
                    default:
                        HibernateUtil.flushLocal();

                }
            }

            confirmaTransacao(session);

        } catch (InfraEstruturaException e) {
            try {
                retornaTransacao(session);
            } catch (InfraEstruturaException ie) {
            }
            throw e;
        } catch (Exception e) {
            try {
                retornaTransacao(session);
            } catch (InfraEstruturaException ie) {
            }

            throw e;
        }
    }

    private void comparaProduto(EstoqueProduto objRemoto, EstoqueProduto objLocal, String session) {

        objLocal.getProduto().setDescricao(objRemoto.getProduto().getDescricao());
        objLocal.getProduto().setAplicacao(objRemoto.getProduto().getAplicacao());
        objLocal.getProduto().setAtivo(objRemoto.getProduto().getAtivo());
        objLocal.getProduto().setOrigem(objRemoto.getProduto().getOrigem());
        objLocal.getProduto().setEan(objRemoto.getProduto().getEan());
        objLocal.getProduto().setDataAlterado(objRemoto.getProduto().getDataAlterado());
        objLocal.getProduto().setPeso(objRemoto.getProduto().getPeso());
        objLocal.getProduto().setAplicacao(objRemoto.getProduto().getAplicacao());
        objLocal.getProduto().setEmissaoEtiqueta(objRemoto.getProduto().getEmissaoEtiqueta());

        try {
            objLocal.getProduto().setCodigoTributario(codigoTributarioDAO.retornaUmCodigoTributario(objRemoto.getProduto().getCodigoTributario().getSituacaoTributaria(), session));
        } catch (ObjetoNaoEncontradoException e) {

            CodigoTributario cst = null;

            if (objRemoto.getProduto().getCodigoTributario() != null) {
                cst = new CodigoTributario();
                cst.setSituacaoTributaria(objRemoto.getProduto().getCodigoTributario().getSituacaoTributaria());
                cst.setDescricao(objRemoto.getProduto().getCodigoTributario().getDescricao());
                cst.setBaseIcmCompra(objRemoto.getProduto().getCodigoTributario().getBaseIcmCompra());
                cst.setBaseIcmVenda(objRemoto.getProduto().getCodigoTributario().getBaseIcmVenda());
                cst.setOrigem(objRemoto.getProduto().getCodigoTributario().getOrigem());
                cst.setCodigo(objRemoto.getProduto().getCodigoTributario().getCodigo());
                cst.setPercAproveitamentoSn(BigDecimal.ZERO);
                cst.setCodigoEcf(objRemoto.getProduto().getCodigoTributario().getCodigoEcf());
                codigoTributarioDAO.inclui(cst, session);
            }

            objLocal.getProduto().setCodigoTributario(cst);
        }

        try {
            objLocal.getProduto().setGrupoByIdGrupo(grupoDAO.retornaUmGrupo(objRemoto.getProduto().getGrupoByIdGrupo().getDescricao(), session));
        } catch (ObjetoNaoEncontradoException e) {
            Grupo grupo = null;

            if (objRemoto.getProduto().getGrupoByIdGrupo() != null) {
                grupo = new Grupo();
                grupo.setDescricao(objRemoto.getProduto().getGrupoByIdGrupo().getDescricao());
                grupo.setLucroMinimoItem(BigDecimal.ZERO);
                grupoDAO.inclui(grupo, session);
            }

            objLocal.getProduto().setGrupoByIdGrupo(grupo);
        }

        try {
            objLocal.getProduto().setUnidadeMedida(unidadeMedidaDAO.retornaUnidadeBySigla(objRemoto.getProduto().getUnidadeMedida().getMnemonico(), session));
        } catch (ObjetoNaoEncontradoException e) {
            UnidadeMedida unidade = null;

            if (objRemoto.getProduto().getUnidadeMedida() != null) {
                unidade = new UnidadeMedida();
                unidade.setDescricao(objRemoto.getProduto().getUnidadeMedida().getDescricao());
                unidade.setFracionada(objRemoto.getProduto().getUnidadeMedida().isFracionada());
                unidade.setMnemonico(objRemoto.getProduto().getUnidadeMedida().getMnemonico());
                unidadeMedidaDAO.inclui(unidade, session);
            }

            objLocal.getProduto().setUnidadeMedida(unidade);
        }

        try {
            objLocal.getProduto().setNcm(ncmDAO.retornaUmNcm(objRemoto.getProduto().getNcm().getCodigo(), session));
        } catch (ObjetoNaoEncontradoException e) {

            Ncm ncm = null;

            if (objRemoto.getProduto().getNcm() != null) {

                ncm = new Ncm();
                ncm.setCodigo(objRemoto.getProduto().getNcm().getCodigo());
                ncm.setDescricao(objRemoto.getProduto().getNcm().getDescricao());
                ncm.setProtocolo(objRemoto.getProduto().getNcm().getProtocolo());
                ncm.setMvaOpInt(objRemoto.getProduto().getNcm().getMvaOpInt());
                ncm.setMvaOpExt(objRemoto.getProduto().getNcm().getMvaOpExt());

                CstPisCofins pis;
                try {
                    pis = cstPisCofinsDAO.retornaUmCstPisCofins(objRemoto.getProduto().getNcm().getCstPisCofins().getCodigo(), session);
                } catch (ObjetoNaoEncontradoException ePis) {
                    pis = new CstPisCofins(0,
                            objRemoto.getProduto().getNcm().getCstPisCofins().getCodigo(),
                            objRemoto.getProduto().getNcm().getCstPisCofins().getDescricao(),
                            objRemoto.getProduto().getNcm().getCstPisCofins().getBaseCalculo(),
                            objRemoto.getProduto().getNcm().getCstPisCofins().getAliquotaPis(),
                            objRemoto.getProduto().getNcm().getCstPisCofins().getAliquotaCofins());
                }
                ncm.setCstPisCofins(pis);

                if (objRemoto.getProduto().getNcm().getCstIpi() != null) {
                    CstIPI ipi;
                    try {
                        ipi = cstIpiDAO.retornaUmCstIpi(objRemoto.getProduto().getNcm().getCstIpi().getCodigo(), session);
                    } catch (ObjetoNaoEncontradoException eIpi) {
                        ipi = new CstIPI(0,
                                objRemoto.getProduto().getNcm().getCstIpi().getCodigo(),
                                objRemoto.getProduto().getNcm().getCstIpi().getDescricao(),
                                objRemoto.getProduto().getNcm().getCstIpi().getBaseCalculo(),
                                objRemoto.getProduto().getNcm().getCstIpi().getAliquota());
                    }
                    ncm.setCstIpi(ipi);
                }

                ncmDAO.inclui(ncm, session);
            }

            objLocal.getProduto().setNcm(ncm);
        } catch (Exception e1) {
            // excecao calada deu ruim
        }

        try {
            objLocal.getProduto().setFabricante(fabricanteDAO.retornaFabricante(objRemoto.getProduto().getFabricante().getDescricao(), session));
        } catch (ObjetoNaoEncontradoException e) {
            Fabricante fabr = null;

            if (objRemoto.getProduto().getFabricante() != null) {
                fabr = new Fabricante();
                fabr.setDescricao(objRemoto.getProduto().getFabricante().getDescricao());
                fabr.setIndice(objRemoto.getProduto().getFabricante().getIndice());
                fabricanteDAO.inclui(fabr, session);
            }

            objLocal.getProduto().setFabricante(fabr);
        }

        objLocal.setClasseAbc(objRemoto.getClasseAbc());
        objLocal.setCustoReposicao(objRemoto.getCustoReposicao());
        objLocal.setQtdDisponivel(objRemoto.getQtdDisponivel());
        objLocal.setUltimaAtualizacao(objRemoto.getUltimaAtualizacao());
        objLocal.setUltimaVenda(objRemoto.getUltimaVenda());
        objLocal.setUltimaCompra(objRemoto.getUltimaCompra());
    }

    private void comparaDepartamento(EstoqueProduto objRemoto, EstoqueProduto objLocal, Loja loja, String session) {
        LojaDepartamento depto = null;

        if (objRemoto.getDepartamento() == null) {
            return;
        }

//        if ((objLocal.getDepartamento() != null && objRemoto.getDepartamento() != null)
//                && !objLocal.getDepartamento().getNome().equals(objRemoto.getDepartamento().getNome())) {
        int max = 0;
        for (LojaDepartamento lojaDepartamento : departamentos) {
            max = lojaDepartamento.getCodigo();
            if (lojaDepartamento.getNome().equals(objRemoto.getDepartamento().getNome())) {
                depto = lojaDepartamento;
            }
        }

        if (depto == null) {
            depto = new LojaDepartamento();
            depto.setLoja(loja);
            depto.setCodigo(++max);
            depto.setNome(objRemoto.getDepartamento().getNome());

            lojaDAO.incluiDepartamento(depto, session);
            departamentos.add(depto);
        }

        objLocal.setDepartamento(depto);
//        }

    }

    private void comparaPrecos(EstoqueProduto objRemoto, EstoqueProduto objLocal, Loja loja, String session) {

        estoquePrecoDAO.deletePrecos(objLocal, session);

        int cont = 1;
        for (EstoquePreco precoBase : objRemoto.getEstoquePrecos()) {

            EstoquePreco precoAtualizacao = null;

            TabelaPreco tab;
            try {
                tab = tabelaPrecoDAO.retornaUmProduto(precoBase.getTabelaPreco().getIndice(), session);
            } catch (ObjetoNaoEncontradoException e) {
                TabelaPreco tabBase = precoBase.getTabelaPreco();

                tab = new TabelaPreco(0, loja, ("TAB INDICE=" + tabBase.getIndice()).replaceAll("\\.", ","), tabBase.getIndice(), true, true);
                tabelaPrecoDAO.inclui(tab, session);
            }

            if (precoAtualizacao == null) {
                precoAtualizacao = new EstoquePreco(objLocal);
            }

            precoAtualizacao.setValorVenda(precoBase.getValorVenda());
            precoAtualizacao.setValorDesconto(precoBase.getValorDesconto());
            precoAtualizacao.setAtualizacaoPreco(precoBase.getAtualizacaoPreco());
            precoAtualizacao.setDataLimiteDesconto(precoBase.getDataLimiteDesconto());
            precoAtualizacao.setFormasPgtoPromocao(precoBase.getFormasPgtoPromocao());
            precoAtualizacao.setPercentualPromocao(precoBase.getPercentualPromocao());
            precoAtualizacao.setPrecoPadrao(precoBase.getPrecoPadrao());
            precoAtualizacao.setTabelaPreco(tab);
            precoAtualizacao.setNumOrdem(cont);
            cont++;

            estoquePrecoDAO.inclui(precoAtualizacao, session);
        }
    }

//    private void comparaPrecos(EstoqueProduto objRemoto, EstoqueProduto objLocal, Loja loja, String session) {
//        
//        EstoquePreco precoBase = objRemoto.retornaPrecoPadrao();
//        EstoquePreco precoAtualizacao = objLocal.retornaPrecoPadrao();
//
//        if (precoBase != null) {
//
//            if (precoAtualizacao == null
//                    //
//                    || (!precoBase.getValorVenda().equals(precoAtualizacao.getValorVenda()))
//                    //
//                    || ((precoBase.getValorDesconto() != null && precoAtualizacao.getValorDesconto() != null)
//                    && !precoBase.getValorDesconto().equals(precoAtualizacao.getValorDesconto()))
//                    //
//                    || ((precoBase.getAtualizacaoPreco() != null && precoAtualizacao.getAtualizacaoPreco() != null)
//                    && !precoBase.getAtualizacaoPreco().equals(precoAtualizacao.getAtualizacaoPreco()))
//                    //
//                    || ((precoBase.getFormasPgtoPromocao() != null && precoAtualizacao.getFormasPgtoPromocao() != null)
//                    && !precoBase.getFormasPgtoPromocao().equals(precoAtualizacao.getFormasPgtoPromocao()))
//                    //
//                    || ((precoBase.getPercentualPromocao() != null && precoAtualizacao.getPercentualPromocao() != null)
//                    && !precoBase.getPercentualPromocao().equals(precoAtualizacao.getPercentualPromocao()))
//                    //
//                    || ((precoBase.getDataLimiteDesconto() != null && precoAtualizacao.getDataLimiteDesconto() != null)
//                    && !precoBase.getDataLimiteDesconto().equals(precoAtualizacao.getDataLimiteDesconto()))
//                    //
//                    || ((precoBase.getTabelaPreco() != null && precoAtualizacao.getTabelaPreco() != null)
//                    && precoBase.getTabelaPreco().getIndice() != precoAtualizacao.getTabelaPreco().getIndice()) //
//                    ) {
//
//                TabelaPreco tab;
//                try {
//                    tab = tabelaPrecoDAO.retornaUmProduto(precoBase.getTabelaPreco().getIndice(), session);
//                } catch (ObjetoNaoEncontradoException e) {
//                    TabelaPreco tabBase = precoBase.getTabelaPreco();
//
//                    tab = new TabelaPreco(0, loja, ("TAB INDICE=" + tabBase.getIndice()).replaceAll("\\.", ","), tabBase.getIndice(), true, true);
//                    tabelaPrecoDAO.inclui(tab, session);
//                }
//
//                if (precoAtualizacao == null) {
//                    precoAtualizacao = new EstoquePreco(objLocal);
//                }
//
//                precoAtualizacao.setValorVenda(precoBase.getValorVenda());
//                precoAtualizacao.setValorDesconto(precoBase.getValorDesconto());
//                precoAtualizacao.setAtualizacaoPreco(precoBase.getAtualizacaoPreco());
//                precoAtualizacao.setDataLimiteDesconto(precoBase.getDataLimiteDesconto());
//                precoAtualizacao.setFormasPgtoPromocao(precoBase.getFormasPgtoPromocao());
//                precoAtualizacao.setPercentualPromocao(precoBase.getPercentualPromocao());
//                precoAtualizacao.setPrecoPadrao(precoBase.getPrecoPadrao());
//                precoAtualizacao.setTabelaPreco(tab);
//
//                if (precoAtualizacao.getIdEstoquePreco() > 0) {
//                    estoquePrecoDAO.altera(precoAtualizacao, session);
//                } else {
//                    precoAtualizacao.setNumOrdem(1);
//                    estoquePrecoDAO.inclui(precoAtualizacao, session);
//                }
//            }
//        }
//
//    }
    public List<Object[]> listaSaldos(Date ultimaAlteracao, String session, Loja lojaExcept) {
        List<Object[]> lista = null;

        try {
            inicializaTransacao(session);

            lista = estoqueDAO.listaSaldos(ultimaAlteracao, session, lojaExcept);

            confirmaTransacao(session);
        } catch (InfraEstruturaException e) {
            try {
                retornaTransacao(session);
            } catch (InfraEstruturaException ie) {
            }

            throw e;
        }
        return lista;
    }

    public List<Object[]> listaSaldosOutrasLojas(Date ultimaAlteracao, String session, Loja lojaExcept) {
        List<Object[]> lista = null;

        try {
            inicializaTransacao(session);

            lista = estoqueDAO.listaSaldosOutrasLojas(ultimaAlteracao, session, lojaExcept);

            confirmaTransacao(session);
        } catch (InfraEstruturaException e) {
            try {
                retornaTransacao(session);
            } catch (InfraEstruturaException ie) {
            }

            throw e;
        }
        return lista;
    }

    public void atualizaEstoque(List<Object[]> listaEstoque, String session, Loja lojaLocal) {
        try {
            inicializaTransacao(session);

            estoqueDAO.atualizaEstoque(listaEstoque, session);

            confirmaTransacao(session);
        } catch (InfraEstruturaException e) {
            try {
                retornaTransacao(session);
            } catch (InfraEstruturaException ie) {
            }

            throw e;
        }
    }

    public void atualizaEstoqueImplantacao(List<Object[]> listaEstoque, String session, Loja lojaLocal) {
        try {
            inicializaTransacao(session);

            estoqueDAO.atualizaEstoque(listaEstoque, session);

            lojaLocal.setDataImplantacaoRemota(Sistema.getInstance().retornaDataBancoSemBlocoTransacao(session));

            lojaDAO.altera(lojaLocal, session);

            confirmaTransacao(session);
        } catch (InfraEstruturaException e) {
            try {
                retornaTransacao(session);
            } catch (InfraEstruturaException ie) {
            }

            throw e;
        }
    }

    public List<EstoqueProduto> atualizaCadastroProdutoLojasRede(Loja lojaLocal, Date ultimaAlteracao, String session) {
        List<EstoqueProduto> lista = null;

        try {
            inicializaTransacao(session);

            lista = estoqueDAO.listaEstoqueByDataCadastro(lojaLocal, ultimaAlteracao, session);

            if (!lista.isEmpty()) {

                List<Loja> lojas = lojaDAO.listaLojas(session);

                for (EstoqueProduto estoque : lista) {
                    for (Loja loja : lojas) {
                        if (loja.getNome().equals(lojaLocal.getNome())) {
                            continue;
                        } else {
                            EstoqueProduto estoqueProduto = new EstoqueProduto();
                            estoqueProduto.setProduto(estoque.getProduto());
                            estoqueProduto.setLoja(loja);
                            estoqueProduto.setClasseAbc('N');
                            estoqueProduto.setQtdMinimo(BigDecimal.ZERO);
                            estoqueProduto.setQtdMaximo(BigDecimal.ZERO);
                            estoqueProduto.setQtdDisponivel(BigDecimal.ZERO);
                            estoqueProduto.setCustoMedio(estoque.getCustoMedio());
                            estoqueProduto.setCustoReposicao(estoque.getCustoReposicao());
                            estoqueProduto.setDemandaMes(BigDecimal.ZERO);
                            estoqueProduto.setLocacao("");
                            estoqueProduto.setPagaComissao(estoque.getPagaComissao());
                            estoqueProduto.setEsgotamentoDiario(BigDecimal.ZERO);
                            estoqueProduto.setLiberadoCompra(true);
                            estoqueProduto.setLiberadoContagem(true);

                            estoqueDAO.incluiParaOutrasLojas(estoqueProduto, session);
                        }
                    }
                }
            }

            confirmaTransacao(session);
        } catch (InfraEstruturaException e) {
            try {
                retornaTransacao(session);
            } catch (InfraEstruturaException ie) {
            }

            throw e;
        }

        return lista;
    }

    public void incluiParaOutrasLojas(Loja lojaLocal, Date dataCadastro, String sessionFrom, String sessionTo) throws Exception {
        try {
            inicializaTransacao(sessionFrom);

            inicializaTransacao(sessionTo);

            // cria estoque para cada loja local
            // com o intuito de receber o saldo da rede
            estoqueDAO.incluiParaOutrasLojas(lojaLocal, dataCadastro, sessionFrom);

            // busca saldo na base local
            List<Object[]> listaEstoque = estoqueDAO.listaSaldos(dataCadastro, sessionFrom, lojaLocal);

            //atualiza saldo na base remota
            estoqueDAO.atualizaEstoque(listaEstoque, sessionTo);

            // defina na loja local a data implantação da comunicação remota
            Date dataImplantacao = Sistema.getInstance().retornaDataBancoSemBlocoTransacao(sessionTo);
            lojaLocal.setDataImplantacaoRemota(dataImplantacao);

            // altera loja local
            lojaDAO.altera(lojaLocal, sessionFrom);

            // recupera referencia da loja na base remota
            Loja lojaRemota = lojaDAO.retornaUmaLoja(lojaLocal.getEmpresa().getCnpj(), sessionTo);

            // defina na referencia da loja remota a data implantação da comunicação remota
            lojaRemota.setDataImplantacaoRemota(dataImplantacao);

            // altera refencia da loja remota
            lojaDAO.altera(lojaRemota, sessionTo);

            confirmaTransacao(sessionTo);

            confirmaTransacao(sessionFrom);

        } catch (Exception e) {
            try {
                retornaTransacao(sessionFrom);
                retornaTransacao(sessionTo);
            } catch (InfraEstruturaException ie) {
            }

            throw e;
        }
    }
}
