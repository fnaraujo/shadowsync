package br.com.controle;

import java.util.List;
//import br.com.controle.exception.AplicacaoException;
import br.com.controle.exception.InfraEstruturaException;
import br.com.controle.exception.ObjetoNaoEncontradoException;
import br.com.modelo.Empresa;
import br.com.util.HibernateUtil;

/**
 *
 * @author Administrador
 */
public class EmpresaAppService {

    private EmpresaDAO empresaDAO = new EmpresaDAO();

    public long inclui(Empresa empresa) {
        try {
            HibernateUtil.beginTransaction();

            long numero = empresaDAO.inclui(empresa);

            HibernateUtil.commitTransaction();

            return numero;
        } catch (InfraEstruturaException e) {
            try {
                HibernateUtil.rollbackTransaction();
            } catch (InfraEstruturaException ie) {
            }

            throw e;
        }
    }

    public long exclui(Empresa empresa) {
        try {
            HibernateUtil.beginTransaction();

            long numero = empresaDAO.exclui(empresa);

            HibernateUtil.commitTransaction();

            return numero;
        } catch (InfraEstruturaException e) {
            try {
                HibernateUtil.rollbackTransaction();
            } catch (InfraEstruturaException ie) {
            }

            throw e;
        }
    }

    public long altera(Empresa empresa) {
        try {
            HibernateUtil.beginTransaction();

            long numero = empresaDAO.altera(empresa);

            HibernateUtil.commitTransaction();

            return numero;
        } catch (InfraEstruturaException e) {
            try {
                HibernateUtil.rollbackTransaction();
            } catch (InfraEstruturaException ie) {
            }

            throw e;
        }
    }

    public Empresa retornaUmaEmpresa(long numero, String session)
            throws Exception {
        try {
            HibernateUtil.beginTransaction();

            Empresa empresa = empresaDAO.retornaUmaEmpresa(numero, session);

            HibernateUtil.commitTransaction();

            return empresa;
        } catch (InfraEstruturaException e) {
            try {
                HibernateUtil.rollbackTransaction();
            } catch (InfraEstruturaException ie) {
            }

            throw e;
        }

    }

    public Empresa retornaPrimeiraEmpresa(String session)
            throws Exception {
        try {
            switch (session.toLowerCase()) {
                case "local":
                    HibernateUtil.beginTransaction();
                    break;
                case "remote":
                    HibernateUtil.beginTransactionRemote();
                    break;
                default:
                    HibernateUtil.beginTransaction();

            }


        } catch (Exception e) {
            try {

                switch (session.toLowerCase()) {
                    case "local":
                        HibernateUtil.rollbackTransaction();
                        break;
                    case "remote":
                        HibernateUtil.rollbackTransactionRemote();
                        break;
                    default:
                        HibernateUtil.rollbackTransaction();
                }
            } catch (Exception ie) {
            }
        }
        try {

            Empresa empresa = empresaDAO.retornaPrimeiraEmpresa(session);

            switch (session.toLowerCase()) {
                case "local":
                    HibernateUtil.commitTransaction();
                    break;
                case "remote":
                    HibernateUtil.commitTransactionRemote();
                    break;
                default:
                    HibernateUtil.commitTransaction();

            }


            return empresa;
        } catch (InfraEstruturaException e) {
            try {
                switch (session.toLowerCase()) {
                    case "local":
                        HibernateUtil.rollbackTransaction();
                        break;
                    case "remote":
                        HibernateUtil.rollbackTransactionRemote();
                        break;
                    default:
                        HibernateUtil.rollbackTransaction();

                }
            } catch (InfraEstruturaException ie) {
            }

            throw e;
        }

    }

    public Empresa retornaUmEmpresa(String cpfCnpj)
            throws Exception {
        try {
            HibernateUtil.beginTransaction();

            Empresa empresa = empresaDAO.retornaUmEmpresa(cpfCnpj);

            HibernateUtil.commitTransaction();

            return empresa;
        } catch (ObjetoNaoEncontradoException e) {
            try {
                HibernateUtil.rollbackTransaction();
            } catch (InfraEstruturaException ie) {
                throw ie;
            }

            throw new Exception("Empresa não encontrado");
        } catch (InfraEstruturaException e) {
            try {
                HibernateUtil.rollbackTransaction();
            } catch (InfraEstruturaException ie) {
            }

            throw e;
        }
    }

    public List<Empresa> listaEmpresa(String pesquisa) {
        List<Empresa> listaEmpresas = null;

        try {
            HibernateUtil.beginTransaction();

            listaEmpresas = empresaDAO.retornaEmpresas(pesquisa);

            HibernateUtil.commitTransaction();
        } catch (InfraEstruturaException e) {
            try {
                HibernateUtil.rollbackTransaction();
            } catch (InfraEstruturaException ie) {
            }

            throw e;
        }

        return listaEmpresas;
    }

    public boolean isEmpresaDaRede(String cpfCnpj) {
        try {
            this.retornaUmEmpresa(cpfCnpj);
            return true;
        } catch (Exception e) {
            return false;
        }


    }
}
