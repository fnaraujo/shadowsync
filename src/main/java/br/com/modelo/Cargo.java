package br.com.modelo;

import com.towel.el.annotation.Resolvable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="cargo"
    ,schema="public"
)
@SequenceGenerator(name = "SEQUENCIA",
    sequenceName = "sq_cargo",
    allocationSize = 1)
public class Cargo  implements java.io.Serializable {

      public static final String NOME_TABELA="cargo";

     private long idCargo;
     @Resolvable(colName = "descricao")
     private String descricao;

    public Cargo() {
    }

    public Cargo(long idCargo, String descricao) {
       this.idCargo = idCargo;
       this.descricao = descricao;
    }
   
    @Id 
    @GeneratedValue(strategy=GenerationType.AUTO, generator="SEQUENCIA")
    @Column(name="id_cargo", unique=true, nullable=false)
    public long getIdCargo() {
        return this.idCargo;
    }
    
    public void setIdCargo(long idCargo) {
        this.idCargo = idCargo;
    }
    
    @Column(name="desc_cargo", nullable=false)
    public String getDescricao() {
        return this.descricao;
    }
    
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }   
} 

