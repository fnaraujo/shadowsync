package br.com.modelo;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="status_funcionario"
    ,schema="public")
@SequenceGenerator(name = "SEQUENCIA",
    sequenceName = "sq_status_func",
    allocationSize = 1)
public class StatusFunc extends Base {

    private String descricao; 
     
    public StatusFunc() {
    }

	
    public StatusFunc(int idStatusFunc, String descricao) {
        this.id = idStatusFunc;
        this.descricao = descricao;
    }

   
    @Id 
    @GeneratedValue(strategy=GenerationType.AUTO,generator="SEQUENCIA")
    @Column(name="id_status_funcionario", unique=true, nullable=false)
    public long getIdStatusFunc() {
        return this.id;
    }
    
    public void setIdStatusFunc(long idStatusFunc) {
        this.id = idStatusFunc;
    }
    
    @Column(name="desc_status", nullable=false)
    public String getDescricao() {
        return this.descricao;
    }
    
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public boolean equals(Object obj){
        if(obj instanceof StatusFunc)
            return this.descricao.equals(((StatusFunc)obj).descricao);
        else
            return false;
    }
}


