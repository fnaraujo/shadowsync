package br.com.modelo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author SAMUEL
 */
@Entity
@Table(name = "shadowsync_info", schema = "public")
public class ShadowsyncInfo implements Serializable {

    private long idShadowsyncInfo;
    private Date ultAltFormaPgto;
    private Date ultAltFuncionario;
    private Date ultAltEstoque;
    private Date ultAltSaldo;
    private Date ultEnvioEstoque;
    private Date ultEnvioSaldo;
    private Date ultAltPreco;
    private Date ultEnvioPreco;
    
    @Id
    @Column(name = "id_shadowsync_info")
    public long getIdShadowsyncInfo() {
        return idShadowsyncInfo;
    }

    public void setIdShadowsyncInfo(long idShadowsyncInfo) {
        this.idShadowsyncInfo = idShadowsyncInfo;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ult_alt_forma_pgto")
    public Date getUltAltFormaPgto() {
        return ultAltFormaPgto;
    }

    public void setUltAltFormaPgto(Date ultAltFormaPgto) {
        this.ultAltFormaPgto = ultAltFormaPgto;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ult_alt_funcionario")
    public Date getUltAltFuncionario() {
        return ultAltFuncionario;
    }

    public void setUltAltFuncionario(Date ultAltFuncionario) {
        this.ultAltFuncionario = ultAltFuncionario;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ult_alt_estoque")
    public Date getUltAltEstoque() {
        return ultAltEstoque;
    }

    public void setUltAltEstoque(Date ultAltEstoque) {
        this.ultAltEstoque = ultAltEstoque;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ult_alt_saldo")
    public Date getUltAltSaldo() {
        return ultAltSaldo;
    }

    public void setUltAltSaldo(Date ultAltSaldo) {
        this.ultAltSaldo = ultAltSaldo;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ult_envio_estoque")
    public Date getUltEnvioEstoque() {
        return ultEnvioEstoque;
    }

    public void setUltEnvioEstoque(Date ultEnvioEstoque) {
        this.ultEnvioEstoque = ultEnvioEstoque;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ult_envio_saldo")
    public Date getUltEnvioSaldo() {
        return ultEnvioSaldo;
    }

    public void setUltEnvioSaldo(Date ultEnvioSaldo) {
        this.ultEnvioSaldo = ultEnvioSaldo;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ult_alt_preco")
    public Date getUltAltPreco() {
        return ultAltPreco;
    }

    public void setUltAltPreco(Date ultAltPreco) {
        this.ultAltPreco = ultAltPreco;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ult_envio_preco")
    public Date getUltEnvioPreco() {
        return ultEnvioPreco;
    }

    public void setUltEnvioPreco(Date ultEnvioPreco) {
        this.ultEnvioPreco = ultEnvioPreco;
    }
}
