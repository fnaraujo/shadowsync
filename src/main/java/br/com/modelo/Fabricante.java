package br.com.modelo;

import com.towel.el.annotation.Resolvable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="fabricante"
    ,schema="public"
)
@SequenceGenerator(name = "SEQUENCIA",
    sequenceName = "sq_fabricante",
    allocationSize = 1)
public class Fabricante  implements java.io.Serializable {

    public static final String NOME_TABELA="fabricante";

     private long idFabricante;
     @Resolvable(colName = "descricao")
     private String descricao;
     private int indice;
     private Set produtos = new HashSet(0);

    public Fabricante() {
    }

	
    public Fabricante(long idFabricante, String descricao, int indice) {
        this.idFabricante = idFabricante;
        this.descricao = descricao;
        this.indice = indice;
    }
    public Fabricante(long idFabricante, String descricao, int indice, Set produtos) {
       this.idFabricante = idFabricante;
       this.descricao = descricao;
       this.indice = indice;
       this.produtos = produtos;
    }
   
    @Id 
    @GeneratedValue(strategy=GenerationType.AUTO, generator="SEQUENCIA")
    @Column(name="id_fabricante", unique=true, nullable=false)
    public long getIdFabricante() {
        return this.idFabricante;
    }
    
    public void setIdFabricante(long idFabricante) {
        this.idFabricante = idFabricante;
    }
    
    @Column(name="descricao", nullable=false, length=40)
    public String getDescricao() {
        return this.descricao;
    }
    
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    
    @Column(name="indice", nullable=false)
    public int getIndice() {
        return this.indice;
    }
    
    public void setIndice(int indice) {
        this.indice = indice;
    }

@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="fabricante", targetEntity=Produto.class)
    public Set getProdutos() {
        return this.produtos;
    }
    
    public void setProdutos(Set produtos) {
        this.produtos = produtos;
    }

    public String retornaNome(int i) {
        if(this.descricao.length() <= i){
            return this.descricao;
        }else{
            return this.descricao.substring(0, i-1);
        }
    }


    public String retornaDescricao(int maxLength) {
       String nome =  this.descricao;
        
        if(nome.length() < maxLength){
            return nome;
        }else{
            return nome.substring(0, maxLength);
        }
    }
}


