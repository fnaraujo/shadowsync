package br.com.modelo;

import com.towel.el.annotation.Resolvable;
import java.util.Calendar;
import java.util.Date;
import javax.persistence.*;

@Entity
@Table(name = "empresa", schema = "public", uniqueConstraints =
@UniqueConstraint(columnNames = "cnpj"))
@SequenceGenerator(name = "SEQUENCIA",
sequenceName = "sq_empresa",
allocationSize = 1)
public class Empresa implements java.io.Serializable {

    private long codigoid;
    @Resolvable(colName = "nome")
    private String nome;
    @Resolvable(colName = "cnpj")
    private String cnpj;
    private String registroEstadual;
    private String registroMunicipal;
    private String codigoRegistro;
    private String crt;
    private String dataValidade;
    private boolean usaDav;
    private boolean ctrlQtd;
    private Boolean usaPaf;
    private String versaoPaf;
    private String codigoCnae;
    private String numero;
    private String complemento;
    private String telefone;
    private String telefone2;
    private String email;
    private byte[] logo;
    private String hostSmtp;
    private String portaStmp;
    private String userSmtp;
    private String senhaSmtp;
    private Boolean conexaoSSL;    
    private String urlPortal;
    private String senhaPortal;

    public Empresa() {
    }

    public Empresa(long codigoid, String nome, String cnpj, String registroestadual, String codigoregistro, String crt, String datavalidade) {
        this.codigoid = codigoid;
        this.nome = nome;
        this.cnpj = cnpj;
        this.registroEstadual = registroestadual;
        this.codigoRegistro = codigoregistro;
        this.crt = crt;
        this.dataValidade = datavalidade;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQUENCIA")
    @Column(name = "codigo_id", unique = true, nullable = false)
    public long getCodigoid() {
        return this.codigoid;
    }

    public void setCodigoid(long codigoid) {
        this.codigoid = codigoid;
    }

    @Column(name = "nome", nullable = false)
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Column(name = "cnpj", nullable = false)
    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    @Column(name = "registro_estadual", nullable = true)
    public String getRegistroEstadual() {
        return registroEstadual;
    }

    public void setRegistroEstadual(String registroestadual) {
        this.registroEstadual = registroestadual;
    }

    @Column(name = "registro_municipal", nullable = true)
    public String getRegistroMunicipal() {
        return registroMunicipal;
    }

    public void setRegistroMunicipal(String registromunicipal) {
        this.registroMunicipal = registromunicipal;
    }

    @Column(name = "codigo_registro", nullable = true)
    public String getCodigoRegistro() {
        return codigoRegistro;
    }

    public void setCodigoRegistro(String codigoregistro) {
        this.codigoRegistro = codigoregistro;
    }

    @Column(name = "crt", nullable = true)
    public String getCrt() {
        return crt;
    }

    public void setCrt(String crt) {
        this.crt = crt;
    }

    @Column(name = "data_validade", nullable = false, length = 8)
    public String getDataValidade() {
        return this.dataValidade;
    }

    public void setDataValidade(String datavalidade) {
        this.dataValidade = datavalidade;
    }

    @Column(name = "ctrl_qtd")
    public boolean getCtrlQtd() {
        return ctrlQtd;
    }

    public void setCtrlQtd(boolean ctrlQtd) {
        this.ctrlQtd = ctrlQtd;
    }

    @Column(name = "usa_dav")
    public boolean getUsaDav() {
        return usaDav;
    }

    public void setUsaDav(boolean usaDav) {
        this.usaDav = usaDav;
    }

    @Column(name = "usa_paf")
    public Boolean isUsaPaf() {
        return usaPaf;
    }

    public void setUsaPaf(boolean usaPaf) {
        this.usaPaf = usaPaf;
    }

    @Column(name = "versao_paf")
    public String getVersaoPaf() {
        return versaoPaf;
    }

    public void setVersaoPaf(String versaoPaf) {
        this.versaoPaf = versaoPaf;
    }

    @Column(name = "codigo_cnae", nullable = false, length = 15)
    public String getCodigoCnae() {
        return codigoCnae;
    }

    public void setCodigoCnae(String codigoCnae) {
        this.codigoCnae = codigoCnae;
    }

//    @ManyToOne(fetch = FetchType.EAGER)
//    @JoinColumn(name = "id_cep")
//    public Cep getCep() {
//        return this.cep;
//    }
//
//    public void setCep(Cep cep) {
//        this.cep = cep;
//    }

    @Column(name = "numero")
    public String getNumero() {
        return this.numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    @Column(name = "complemento")
    public String getComplemento() {
        return this.complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    @Column(name = "telefone")
    public String getTelefone() {
        return this.telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    @Column(name = "telefone2")
    public String getTelefone2() {
        return this.telefone2;
    }

    public void setTelefone2(String telefone2) {
        this.telefone2 = telefone2;
    }

    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(name = "logo")
    public byte[] getLogo() {
        return this.logo;
    }

    public void setLogo(byte[] logo) {
        this.logo = logo;
    }

    @Column(name = "host_smtp")
    public String getHostSmtp() {
        return hostSmtp;
    }

    public void setHostSmtp(String hostSmtp) {
        this.hostSmtp = hostSmtp;
    }

    @Column(name = "porta_smtp")
    public String getPortaStmp() {
        return portaStmp;
    }

    public void setPortaStmp(String portaStmp) {
        this.portaStmp = portaStmp;
    }

    @Column(name = "user_smtp")
    public String getUserSmtp() {
        return userSmtp;
    }

    public void setUserSmtp(String userSmtp) {
        this.userSmtp = userSmtp;
    }

    @Column(name = "senha_smtp")
    public String getSenhaSmtp() {
        return senhaSmtp;
    }

    public void setSenhaSmtp(String senhaSmtp) {
        this.senhaSmtp = senhaSmtp;
    }

    @Column(name = "conexao_ssl")
    public Boolean isConexaoSSL() {
        return conexaoSSL;
    }

    public void setConexaoSSL(Boolean conexaoSSL) {
        this.conexaoSSL = conexaoSSL;
    }

//    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
//    @JoinColumn(name = "id_cliente", referencedColumnName = "id_cliente")
//    public Cliente getCliente() {
//        return cliente;
//    }
//
//    public void setCliente(Cliente cliente) {
//        this.cliente = cliente;
//    }

    @Column(name = "url_portal")
    public String getUrlPortal() {
        return urlPortal;
    }

    public void setUrlPortal(String urlPortal) {
        this.urlPortal = urlPortal;
    }

    @Column(name = "senha_portal")
    public String getSenhaPortal() {
        return senhaPortal;
    }

    public void setSenhaPortal(String senhaPortal) {
        this.senhaPortal = senhaPortal;
    }

//    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "empresa", targetEntity = EmpresaContador.class)
//    public List<EmpresaContador> getContadores() {
//        return contadores;
//    }
//
//    public void setContadores(List<EmpresaContador> contadores) {
//        this.contadores = contadores;
//    }

//    public void ativacao(int codigoAtivacao) throws CodigoInvalidoException {
//        try {
//            String data = Encrypt.dataAutenticacao(codigoAtivacao, Integer.parseInt(this.codigoRegistro), Sistema.PATH);
//            this.dataValidade = data;
//        } catch (EncryptException ex) {
//            ex.printStackTrace();
//        } catch (GeneralSecurityException ex) {
//            ex.printStackTrace();
//        }
//    }

//    public int diasFaltando() {
//        int dias = Encrypt.diasFaltandoData(this.dataValidade, Sistema.PATH);
//        return dias;
//    }

//    public void ativacaoInicial() {
//        try {
//            int codigo = Encrypt.codigoAutenticacao(5, Integer.parseInt(this.codigoRegistro));
//            ativacao(codigo);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    public Integer retornaCrtNFe() {
        int crt = Integer.valueOf(this.crt);

        return crt - 1;
    }

//    public void adicionaContador(Cliente contador, Cliente empresaContador, String crc) {
//        EmpresaContador novoContador = new EmpresaContador();
//
//        novoContador.setClienteContador(contador);
//        novoContador.setClienteEscritorio(empresaContador);
//        novoContador.setDataCadastro(new Date());
//        if (crc != null) {
//            novoContador.setCrc(Integer.valueOf(crc));
//        }
//
//        contadores.add(novoContador);
//    }

    public Object retornaEndereco() {
//        if (this.getCep() == null) {
//            return "Endereco Não Cadastrado";
//        } else {
//            try {
//                return (this.getCep().getTipo() != null ? this.getCep().getTipo() : "") + " "
//                        + (this.getCep().getLogradouro() != null ? this.getCep().getLogradouro() : "") + ", "
//                        + (this.getNumero() != null ? this.getNumero() : "") + " "
//                        + (this.getComplemento() != null ? this.getComplemento() : "") + " "
//                        + "\nBairro: " + (this.getCep().getBairro().getNome() != null ? this.getCep().getBairro().getNome() : "") + "  "
//                        + "Cidade: " + (this.getCep().getBairro().getMunicipio().getNome() != null ? this.getCep().getBairro().getMunicipio().getNome() : "") + " - " + "RJ"
//                        + "\nCEP: " + (this.getCep().getNumero() != null ? this.getCep().getNumero() : "") + " - " + retornaTelefones();
//            } catch (Exception e) {
//                return "N/D";
//            }
//        }
        return "";
    }

//    public Object retornaEnderecoLinha() {
//        if (this.getCep() == null) {
//            return "Endereco Não Cadastrado";
//        } else {
//            try {
//                return (this.getCep().getTipo() != null ? this.getCep().getTipo() : "") + " "
//                        + (this.getCep().getLogradouro() != null ? this.getCep().getLogradouro() : "") + ", "
//                        + (this.getNumero() != null ? this.getNumero() : "") + " "
//                        + (this.getComplemento() != null ? this.getComplemento() : "") + " "
//                        + (this.getCep().getBairro().getNome() != null ? this.getCep().getBairro().getNome() : "") + "  "
//                        + (this.getCep().getBairro().getMunicipio().getNome() != null ? this.getCep().getBairro().getMunicipio().getNome() : "") + " - " + "RJ"
//                        + (this.getCep().getNumero() != null ? " - " + this.getCep().getNumero() : "");
//            } catch (Exception e) {
//                return "N/D";
//            }
//        }
//    }

    public String retornaTelefones() {
        String resp = "Tel.: ";
        if (!this.telefone.isEmpty()) {
            resp += this.telefone;
        }

        if (!this.telefone2.isEmpty()) {
            if (!resp.isEmpty()) {
                resp += " /";
            }

            resp += this.telefone2;
        }
        return resp;
    }

    public boolean emailConfigurado() {
        return this.hostSmtp != null
                && this.portaStmp != null
                && this.userSmtp != null
                && this.senhaSmtp != null;
    }

    public String geraCodigoDoisDias() {

        Calendar c = Calendar.getInstance();
        c.setTime(new Date());

        int codigoEmpresa;
        try {
            codigoEmpresa = Integer.parseInt(codigoRegistro.trim());
        } catch (NumberFormatException ex) {
            return "";
        }
        int dia = c.get(Calendar.DAY_OF_MONTH);

        int codigoLiberacao = (codigoEmpresa + dia);

        return String.valueOf(codigoLiberacao);
    }
}
