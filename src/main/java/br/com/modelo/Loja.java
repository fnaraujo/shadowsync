package br.com.modelo;

import com.towel.el.annotation.Resolvable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Entity
@Table(name = "loja", schema = "public")
@SequenceGenerator(name = "SEQUENCIA",
        sequenceName = "sq_loja",
        allocationSize = 1)
public class Loja implements java.io.Serializable {

    private int idLoja;
    @Resolvable(colName = "nome")
    private String nome;
    private Empresa empresa;
    private boolean vendaAbaixoCusto;
    private boolean vendaEstoqueNegativo;
    private boolean davDiretoImpressora;
    private float moraDiaria;
    private float multaContasReceber;
    private Date dataUltAlteracao;
    private int prazoDevolucao;
    private int prazoCancelamento;
    private boolean lojaMaster;
    private String tipoImpressaoDav;
    private String tipoImpressaoRomaneio;
    private Integer codigoTelaVenda;
    private Integer codigoTelaCompra;
    private String codigoModelPesqVenda;
    private boolean caixaDiario;
    private boolean vendaDescontoAcima;
    private BigDecimal descontoMaximo;//limite
    private BigDecimal descontoMaximoAcima;//acima do limite
    private boolean vendaLimiteNegativo;
    private BigDecimal limiteNegativoMaximo;//maximo de credito negativo;
    private boolean senhaDescontoAcima;
    private boolean senhaLimiteNegativo;
    private boolean senhaEstoqueNegativo;
    private boolean senhaAbaixoCusto;
    private int nivelClassificacao;
    private int metodoClassificacao;//0 - percentual , 1 - ultima venda   
    private boolean fechaImpressaoDav;
    private Integer saltoFimMatricial;
    private Integer qtdViasImpressao;
    private boolean baixaEstoqueOficinaOS;
    private int casasDecimaisPrecoEntrada; //default 2
    private boolean abaixaPrecoEntrada;
    private Integer diasEstoqueInativo;
    private boolean botaoAddItemVenda;
    private boolean prmDePesqProduto;//se false, pesquisar produtos que contenham determinada palavra; se true pesquisar produtos que comecem com determinada palavra;
    private boolean prmFocusPesqProduto; //se true, focar no codigo; se false, focar na descricao
    private int telaCadOs;// 0- default tela sem dav, 1- tela com dav
    private boolean atualizarPrecoFabricante;
    private boolean informativoItemVenda;
    private boolean estoquePorDepartamento;
    private boolean identificaClienteDevolucao;
    private boolean permiteOrcamentoVenda;
    private BigDecimal lucroMinimoItem;
    private BigDecimal lucroMinimoVenda;
    private boolean caixaRapido; // Se true a tela de venda aparece com o valor 1 como padrao
    private boolean imprimeSaldoDevolucao; // Se true o sistema ira imprimir o saldo da devolucao no fechamento da venda
    private boolean devolucaoDescontaFatura; // se true, quando houver devolução: verificar se para aquela venda existe contas a receber e abater o total devolucao na fatura
    private boolean escolhePrecoPromocao; // se true, ao vender um item exibirá janela para escolher o preço normal ou promocional
    private boolean diminuirPrecoVenda; // se true, ao vender um item permitira alterar o preco para menor
    private boolean identificaClienteVenda;// se true, o estado inicial da venda solicitará a identificação do cliente\ vendedor
    private boolean identificaClienteOrcamento;
    private boolean solicitaObsDAV;// se true, solicitara no fechamento do dav uma observacao
    private String ImpressoraEtiqueta;
    private boolean defineDataEntregaVenda;
    private int qtdMaxEstoqueReserva;
    private char abcInicial;
    private String emailMatriz;
    private String telaClienteVenda;
    private boolean liberaMaximoDiasAtraso;
    private int maximoDiasAtraso;
    private Integer diasBloqueioCliente;
    private boolean calcPrecoDireto; //se true, tela de ajuste de preco calcula indice de lucro * custo
    private boolean habilitaPortal;
    private boolean vendaTrocaVendedor;
    /*
     * 1 - Pimaco 6280 
     * 2 - Matricial 1 Col
     * 3 - Argox OS-214plus
     * 4 - A4 (3 colunas)
     * 5 - Argox OS-214 3 Col
     * 6 - Argox OS-214TT
     * 7 - Matricial 1 Col 8,1 x 2,2
     * 8 - Argox OS-214TT Joias
     */
    private int modeloEtiqueta;
    private boolean exibeLocacaoOrcamento;
    /*
     * padrao - 
     */
    private String modeloImpressaoOsJatoTinta;
    private boolean calculaComissaFechaOS;
    private boolean defineMecanicoFechaOS;
    private boolean usaVariosPrecos;
    private boolean registrarPagtoDevolucao;
    private boolean efetuaBackUp;
    private boolean usaPermissaoOperacaoOS;
    /*
     * 0 - sem exibição
     * 1 - exibir cadastro de cod relação
     * 2 - exibir cadastro de cod similar
     */
    private int tipoExibicaoAposCadProduto;
    private boolean ofereceAgregado;
    private BigDecimal markupEstoque;
    private BigDecimal custoTransferencia;
    private boolean pemiteOrcamentoEstoqueNegativo;
    private boolean registraVendaPerdida;
    private String mensagemOS;
    private boolean entradaSomenteContasPagar;
    /*
     * variaveis de mapeamento do servidor de impressão
     */
    private String ipServidorImpressao;
    private int socketServidorImpressao;

    private int diasVencimentoOrcamento;

    private boolean permiteTrocarVendedorOrcamento;
    private boolean permiteOrcamentoClienteBloqueado;

    private int qtdViaOrcamento;

    private String rodapeDav;
    private String cabecalhoDav;
    private String rodapeOrcamento;
    private String cabecalhoOrcamento;

    private boolean codigoProdutoComSiglaGrupo;

    private BigDecimal minVendaParaDesconto;

    private Date dataImplantacaoRemota;

    private ShadowsyncInfo shadowsyncInfo;

    public Loja() {
    }

    public Loja(int idLoja, String nome) {
        this.idLoja = idLoja;
        this.nome = nome;
    }

    public Loja(int idLoja, String nome, Empresa empresa) {
        this.idLoja = idLoja;
        this.nome = nome;
        this.empresa = empresa;
        this.dataUltAlteracao = new Date();
        this.vendaEstoqueNegativo = true;
        this.vendaAbaixoCusto = true;
        this.moraDiaria = 10;
        this.multaContasReceber = 1;
    }

    public Loja(int idLoja, String nome, Empresa empresa, boolean vendaAbaixoCusto, float moraDiaria, float multaContasReceber) {
        this.idLoja = idLoja;
        this.nome = nome;
        this.empresa = empresa;
        this.vendaAbaixoCusto = vendaAbaixoCusto;
        this.moraDiaria = moraDiaria;
        this.multaContasReceber = multaContasReceber;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQUENCIA")
    @Column(name = "id_loja", unique = true, nullable = false)
    public int getIdLoja() {
        return this.idLoja;
    }

    public void setIdLoja(int idLoja) {
        this.idLoja = idLoja;
    }

    @Column(name = "nome", nullable = false, length = 20)
    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String retornaNome() {
        return this.nome.toLowerCase();
    }

    @Column(name = "venda_abaixo_custo", nullable = false)
    public boolean getVendaAbaixoCusto() {
        return this.vendaAbaixoCusto;
    }

    public void setVendaAbaixoCusto(boolean vendaAbaixoCusto) {
        this.vendaAbaixoCusto = vendaAbaixoCusto;
    }

    @Column(name = "mora_diaria", nullable = false)
    public float getMoraDiaria() {
        return moraDiaria;
    }

    public void setMoraDiaria(float moraDiaria) {
        this.moraDiaria = moraDiaria;
    }

    @Column(name = "multa_contas_receber", nullable = false)
    public float getMultaContasReceber() {
        return multaContasReceber;
    }

    public void setMultaContasReceber(float multaContasReceber) {
        this.multaContasReceber = multaContasReceber;
    }

    @Column(name = "qtd_max_estoque_reserva")
    public int getQtdMaxEstoqueReserva() {
        return qtdMaxEstoqueReserva;
    }

    public void setQtdMaxEstoqueReserva(int qtdMaxEstoqueReserva) {
        this.qtdMaxEstoqueReserva = qtdMaxEstoqueReserva;
    }

    @Column(name = "venda_estoque_negativo", nullable = false)
    public boolean getVendaEstoqueNegativo() {
        return vendaEstoqueNegativo;
    }

    public void setVendaEstoqueNegativo(boolean vendaEstoqueNegativo) {
        this.vendaEstoqueNegativo = vendaEstoqueNegativo;
    }

    @Column(name = "dav_direto_impressora", nullable = false)
    public boolean getDavDiretoImpressora() {
        return davDiretoImpressora;
    }

    public void setDavDiretoImpressora(boolean davDiretoImpressora) {
        this.davDiretoImpressora = davDiretoImpressora;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_ult_alteracao", nullable = false, length = 29)
    public Date getDataUltAlteracao() {
        return dataUltAlteracao;
    }

    public void setDataUltAlteracao(Date dataUltAlteracao) {
        this.dataUltAlteracao = dataUltAlteracao;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_empresa", nullable = false, insertable = true, updatable = true)
    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    @Column(name = "define_data_entrega_venda", nullable = false)
    public boolean isDefineDataEntregaVenda() {
        return defineDataEntregaVenda;
    }

    public void setDefineDataEntregaVenda(boolean defineDataEntregaVenda) {
        this.defineDataEntregaVenda = defineDataEntregaVenda;
    }

    @Column(name = "identifica_cliente_venda")
    public boolean isIdentificaClienteVenda() {
        return identificaClienteVenda;
    }

    public void setIdentificaClienteVenda(boolean identificaClienteVenda) {
        this.identificaClienteVenda = identificaClienteVenda;
    }

    @Column(name = "prazo_devolucao", nullable = false)
    public int getPrazoDevolucao() {
        return prazoDevolucao;
    }

    @Column(name = "desconto_maximo", nullable = false)
    public BigDecimal getDescontoMaximo() {
        return descontoMaximo;
    }

    public void setDescontoMaximo(BigDecimal descontoMaximo) {
        this.descontoMaximo = descontoMaximo;
    }

    public void setPrazoDevolucao(int prazoDevolucao) {
        this.prazoDevolucao = prazoDevolucao;
    }

    @Column(name = "prazo_cancelamento", nullable = false)
    public int getPrazoCancelamento() {
        return prazoCancelamento;
    }

    public void setPrazoCancelamento(int prazoCancelamento) {
        this.prazoCancelamento = prazoCancelamento;
    }

    @Column(name = "loja_master")
    public boolean isLojaMaster() {
        return lojaMaster;
    }

    public void setLojaMaster(boolean lojaMaster) {
        this.lojaMaster = lojaMaster;
    }

    @Column(name = "tipo_impressao_dav")
    public String getTipoImpressaoDav() {
        return tipoImpressaoDav;
    }

    public void setTipoImpressaoDav(String tipoImpressaoDav) {
        this.tipoImpressaoDav = tipoImpressaoDav;
    }

    @Column(name = "tipo_impressao_romaneio")
    public String getTipoImpressaoRomaneio() {
        return tipoImpressaoRomaneio;
    }

    public void setTipoImpressaoRomaneio(String tipoImpressaoRomaneio) {
        this.tipoImpressaoRomaneio = tipoImpressaoRomaneio;
    }

    @Column(name = "impressora_etiqueta")
    public String getImpressoraEtiqueta() {
        return ImpressoraEtiqueta;
    }

    public void setImpressoraEtiqueta(String ImpressoraEtiqueta) {
        this.ImpressoraEtiqueta = ImpressoraEtiqueta;
    }

    @Column(name = "codigo_tela_venda")
    public Integer getCodigoTelaVenda() {
        return codigoTelaVenda;
    }

    public void setCodigoTelaVenda(Integer codigoTelaVenda) {
        this.codigoTelaVenda = codigoTelaVenda;
    }

    @Column(name = "codigo_tela_compra")
    public Integer getCodigoTelaCompra() {
        return codigoTelaCompra;
    }

    public void setCodigoTelaCompra(Integer codigoTelaCompra) {
        this.codigoTelaCompra = codigoTelaCompra;
    }

    @Column(name = "caixa_diario")
    public boolean getCaixaDiario() {
        return caixaDiario;
    }

    public void setCaixaDiario(boolean caixaDiario) {
        this.caixaDiario = caixaDiario;
    }

    @Column(name = "baixa_estoque_oficina_os")
    public boolean isBaixaEstoqueOficinaOS() {
        return baixaEstoqueOficinaOS;
    }

    public void setBaixaEstoqueOficinaOS(boolean baixaEstoqueOficinaOS) {
        this.baixaEstoqueOficinaOS = baixaEstoqueOficinaOS;
    }

    public boolean usaCaixaDiario() {
        return caixaDiario;
    }

    @Column(name = "abc_inicial")
    public char getAbcInicial() {
        return abcInicial;
    }

    public void setAbcInicial(char abcInicial) {
        this.abcInicial = abcInicial;
    }

    @Column(name = "efetua_back_up")
    public boolean isEfetuaBackUp() {
        return efetuaBackUp;
    }

    public void setEfetuaBackUp(boolean efetuaBackUp) {
        this.efetuaBackUp = efetuaBackUp;
    }

    @Column(name = "usa_permissao_operacao_os")
    public boolean isUsaPermissaoOperacaoOS() {
        return usaPermissaoOperacaoOS;
    }

    public void setUsaPermissaoOperacaoOS(boolean usaPermissaoOperacaoOS) {
        this.usaPermissaoOperacaoOS = usaPermissaoOperacaoOS;
    }

    @Column(name = "dias_validade_orcamento")
    public int getDiasVencimentoOrcamento() {
        return diasVencimentoOrcamento;
    }

    public void setDiasVencimentoOrcamento(int diasVencimentoOrcamento) {
        this.diasVencimentoOrcamento = diasVencimentoOrcamento;
    }

    public boolean camposObrigatoriosValidos() {
        return this.empresa != null
                && this.empresa.getCnpj() != null
                && this.empresa.getNome() != null
                && this.getEmpresa().getCodigoRegistro() != null
                && this.getEmpresa().getCodigoCnae() != null;
    }

    @Column(name = "venda_desconto_acima")
    public boolean getVendaDescontoAcima() {
        return vendaDescontoAcima;
    }

    public void setVendaDescontoAcima(boolean vendaDescontoAcima) {
        this.vendaDescontoAcima = vendaDescontoAcima;
    }

    @Column(name = "limite_negativo")
    public boolean getVendaLimiteNegativo() {
        return vendaLimiteNegativo;
    }

    public void setVendaLimiteNegativo(boolean vendaLimiteNegativo) {
        this.vendaLimiteNegativo = vendaLimiteNegativo;
    }

    @Column(name = "desconto_maximo_acima")
    public BigDecimal getDescontoMaximoAcima() {
        return descontoMaximoAcima;
    }

    public void setDescontoMaximoAcima(BigDecimal descontoMaximoAcima) {
        this.descontoMaximoAcima = descontoMaximoAcima;
    }

    @Column(name = "limite_negativo_maximo")
    public BigDecimal getLimiteNegativoMaximo() {
        return limiteNegativoMaximo;
    }

    public void setLimiteNegativoMaximo(BigDecimal limiteNegativoMaximo) {
        this.limiteNegativoMaximo = limiteNegativoMaximo;
    }

    @Column(name = "senha_desconto_acima")
    public boolean getSenhaDescontoAcima() {
        return senhaDescontoAcima;
    }

    public void setSenhaDescontoAcima(boolean senhaDescontoAcima) {
        this.senhaDescontoAcima = senhaDescontoAcima;
    }

    @Column(name = "senha_limite_negativo")
    public boolean getSenhaLimiteNegativo() {
        return senhaLimiteNegativo;
    }

    public void setSenhaLimiteNegativo(boolean senhaLimiteNegativo) {
        this.senhaLimiteNegativo = senhaLimiteNegativo;
    }

    @Column(name = "senha_estoque_negativo")
    public boolean getSenhaEstoqueNegativo() {
        return senhaEstoqueNegativo;
    }

    public void setSenhaEstoqueNegativo(boolean senhaEstoqueNegativo) {
        this.senhaEstoqueNegativo = senhaEstoqueNegativo;
    }

    @Column(name = "senha_abaixo_custo")
    public boolean getSenhaAbaixoCusto() {
        return senhaAbaixoCusto;
    }

    public void setSenhaAbaixoCusto(boolean senhaAbaixoCusto) {
        this.senhaAbaixoCusto = senhaAbaixoCusto;
    }

    @Column(name = "venda_troca_vendedor")
    public boolean isVendaTrocaVendedor() {
        return vendaTrocaVendedor;
    }

    public void setVendaTrocaVendedor(boolean vendaTrocaVendedor) {
        this.vendaTrocaVendedor = vendaTrocaVendedor;
    }

    public int retornaVersaoPAF() {
        if (this.empresa.getVersaoPaf().contains("3.")) {
            return 3;
        } else if (this.empresa.getVersaoPaf().contains("4.")) {
            return 4;
        } else {
            return 1;
        }
    }

    public boolean fechaCaixaNoPaf() {
        return this.empresa.getUsaDav()
                && (this.empresa.getVersaoPaf().contains("3."));
    }

    @Column(name = "metodo_classificacao")
    public int getMetodoClassificacao() {
        return metodoClassificacao;
    }

    public void setMetodoClassificacao(int metodoClassificacao) {
        this.metodoClassificacao = metodoClassificacao;
    }

    @Column(name = "nivel_classificacao")
    public int getNivelClassificacao() {
        return nivelClassificacao;
    }

    public void setNivelClassificacao(int nivelClassificacao) {
        this.nivelClassificacao = nivelClassificacao;
    }

    @Column(name = "fecha_impressao_dav")
    public boolean getFechaImpressaoDav() {
        return fechaImpressaoDav;
    }

    public void setFechaImpressaoDav(boolean fechaImpressaoDav) {
        this.fechaImpressaoDav = fechaImpressaoDav;
    }

    @Column(name = "salto_fim_matricial")
    public Integer getSaltoFimMatricial() {
        if (saltoFimMatricial == null) {
            return 0;
        }
        return saltoFimMatricial;
    }

    public void setSaltoFimMatricial(Integer saltoFimMatricial) {
        this.saltoFimMatricial = saltoFimMatricial;
    }

    @Column(name = "qtd_via_dav")
    public Integer getQtdViasImpressao() {
        return qtdViasImpressao;
    }

    public void setQtdViasImpressao(Integer qtdViasImpressao) {
        this.qtdViasImpressao = qtdViasImpressao;
    }

    @Column(name = "abaixa_preco_entrada")
    public boolean isAbaixaPrecoEntrada() {
        return abaixaPrecoEntrada;
    }

    public void setAbaixaPrecoEntrada(boolean abaixaPrecoEntrada) {
        this.abaixaPrecoEntrada = abaixaPrecoEntrada;
    }

    @Column(name = "casas_decimais_preco_entrada")
    public int getCasasDecimaisPrecoEntrada() {
        return casasDecimaisPrecoEntrada;
    }

    public void setCasasDecimaisPrecoEntrada(int casasDecimaisPrecoEntrada) {
        this.casasDecimaisPrecoEntrada = casasDecimaisPrecoEntrada;
    }

    @Column(name = "codigo_model_pesqVenda")
    public String getCodigoModelPesqVenda() {
        return codigoModelPesqVenda;
    }

    public void setCodigoModelPesqVenda(String codigoModelPesqVenda) {
        this.codigoModelPesqVenda = codigoModelPesqVenda;
    }

    @Column(name = "dias_inativo")
    public Integer getDiasEstoqueInativo() {
        return diasEstoqueInativo;
    }

    public void setDiasEstoqueInativo(Integer diasEstoqueInativo) {
        this.diasEstoqueInativo = diasEstoqueInativo;
    }

    @Column(name = "botao_add_item_venda")
    public boolean isBotaoAddItemVenda() {
        return botaoAddItemVenda;
    }

    public void setBotaoAddItemVenda(boolean botaoAddItemVenda) {
        this.botaoAddItemVenda = botaoAddItemVenda;
    }

    @Column(name = "prm_pesq_produto")
    public boolean isPrmDePesqProduto() {
        return prmDePesqProduto;
    }

    public void setPrmDePesqProduto(boolean prmDePesqProduto) {
        this.prmDePesqProduto = prmDePesqProduto;
    }

    @Column(name = "prm_focus_pesq_produto")
    public boolean isPrmFocusPesqProduto() {
        return prmFocusPesqProduto;
    }

    public void setPrmFocusPesqProduto(boolean prmFocusPesqProduto) {
        this.prmFocusPesqProduto = prmFocusPesqProduto;
    }

    @Column(name = "tela_cad_os")
    public int getTelaCadOs() {
        return telaCadOs;
    }

    public void setTelaCadOs(int telaCadOs) {
        this.telaCadOs = telaCadOs;
    }

    @Column(name = "atualizar_preco_fabricante")
    public boolean isAtualizarPrecoFabricante() {
        return atualizarPrecoFabricante;
    }

    public void setAtualizarPrecoFabricante(boolean atualizarPrecoFabricante) {
        this.atualizarPrecoFabricante = atualizarPrecoFabricante;
    }

    @Column(name = "info_item_venda")
    public boolean isInformativoItemVenda() {
        return informativoItemVenda;
    }

    public void setInformativoItemVenda(boolean informativoItemVenda) {
        this.informativoItemVenda = informativoItemVenda;
    }

    @Column(name = "estoque_por_departamento")
    public boolean isEstoquePorDepartamento() {
        return estoquePorDepartamento;
    }

    public void setEstoquePorDepartamento(boolean estoquePorDepartamento) {
        this.estoquePorDepartamento = estoquePorDepartamento;
    }

    @Column(name = "identifica_cliente_devolucao")
    public boolean isIdentificaClienteDevolucao() {
        return identificaClienteDevolucao;
    }

    public void setIdentificaClienteDevolucao(boolean identificaClienteDevolucao) {
        this.identificaClienteDevolucao = identificaClienteDevolucao;
    }

    @Column(name = "permite_orcamento_venda")
    public boolean isPermiteOrcamentoVenda() {
        return permiteOrcamentoVenda;
    }

    public void setPermiteOrcamentoVenda(boolean permiteOrcamentoVenda) {
        this.permiteOrcamentoVenda = permiteOrcamentoVenda;
    }

    @Column(name = "lucro_minimo_item", nullable = true)
    public BigDecimal getLucroMinimoItem() {
        return lucroMinimoItem;
    }

    public void setLucroMinimoItem(BigDecimal lucroMinimoItem) {
        this.lucroMinimoItem = lucroMinimoItem;
    }

    @Column(name = "lucro_minimo_venda")
    public BigDecimal getLucroMinimoVenda() {
        return lucroMinimoVenda;
    }

    public void setLucroMinimoVenda(BigDecimal lucroMinimoVenda) {
        this.lucroMinimoVenda = lucroMinimoVenda;
    }

    @Column(name = "caixa_rapido")
    public boolean isCaixaRapido() {
        return caixaRapido;
    }

    public void setCaixaRapido(boolean caixaRapido) {
        this.caixaRapido = caixaRapido;
    }

    @Column(name = "imprime_saldo_devolucao")
    public boolean isImprimeSaldoDevolucao() {
        return imprimeSaldoDevolucao;
    }

    public void setImprimeSaldoDevolucao(boolean imprimeSaldoDevolucao) {
        this.imprimeSaldoDevolucao = imprimeSaldoDevolucao;
    }

    @Column(name = "devolucao_desconta_fatura")
    public boolean isDevolucaoDescontaFatura() {
        return devolucaoDescontaFatura;
    }

    public void setDevolucaoDescontaFatura(boolean devolucaoDescontaFatura) {
        this.devolucaoDescontaFatura = devolucaoDescontaFatura;
    }

    @Column(name = "escolhe_preco_promocao")
    public boolean isEscolhePrecoPromocao() {
        return escolhePrecoPromocao;
    }

    public void setEscolhePrecoPromocao(boolean escolhePrecoPromocao) {
        this.escolhePrecoPromocao = escolhePrecoPromocao;
    }

    @Column(name = "diminuir_preco_venda")
    public boolean isDiminuirPrecoVenda() {
        return diminuirPrecoVenda;
    }

    public void setDiminuirPrecoVenda(boolean diminuirPrecoVenda) {
        this.diminuirPrecoVenda = diminuirPrecoVenda;
    }

    @Column(name = "rodape_dav")
    public String getRodapeDav() {
        return rodapeDav;
    }

    public void setRodapeDav(String rodapeDav) {
        this.rodapeDav = rodapeDav;
    }

    @Column(name = "cabecalho_dav")
    public String getCabecalhoDav() {
        return cabecalhoDav;
    }

    public void setCabecalhoDav(String cabecalhoDav) {
        this.cabecalhoDav = cabecalhoDav;
    }

    @Column(name = "rodape_orcamento")
    public String getRodapeOrcamento() {
        return rodapeOrcamento;
    }

    public void setRodapeOrcamento(String rodapeOrcamento) {
        this.rodapeOrcamento = rodapeOrcamento;
    }

    @Column(name = "cabecalho_orcamento")
    public String getCabecalhoOrcamento() {
        return cabecalhoOrcamento;
    }

    public void setCabecalhoOrcamento(String cabecalhoOrcamento) {
        this.cabecalhoOrcamento = cabecalhoOrcamento;
    }

    @Column(name = "libera_maximo_dias_atraso")
    public boolean isLiberaMaximoDiasAtraso() {
        return liberaMaximoDiasAtraso;
    }

    public void setLiberaMaximoDiasAtraso(boolean liberaMaximoDiasAtraso) {
        this.liberaMaximoDiasAtraso = liberaMaximoDiasAtraso;
    }

    @Column(name = "dias_bloqueio_cliente")
    public Integer getDiasBloqueioCliente() {
        return diasBloqueioCliente;
    }

    public void setDiasBloqueioCliente(Integer diasBloqueioCliente) {
        this.diasBloqueioCliente = diasBloqueioCliente;
    }

    @Column(name = "maximo_dias_atraso")
    public int getMaximoDiasAtraso() {
        return maximoDiasAtraso;
    }

    public void setMaximoDiasAtraso(int maximoDiasAtraso) {
        this.maximoDiasAtraso = maximoDiasAtraso;
    }

    public boolean usaTalaoOSManual() {
        return !this.getEmpresa().getCnpj().equals("03264771000184");
    }

    @Column(name = "identifica_cliente_orcamento")
    public boolean isIdentificaClienteOrcamento() {
        return identificaClienteOrcamento;
    }

    public void setIdentificaClienteOrcamento(boolean identificaClienteOrcamento) {
        this.identificaClienteOrcamento = identificaClienteOrcamento;
    }

    @Column(name = "email_matriz")
    public String getEmailMatriz() {
        return emailMatriz;
    }

    public void setEmailMatriz(String emailMatriz) {
        this.emailMatriz = emailMatriz;
    }

    @Column(name = "solicita_obs_dav")
    public boolean isSolicitaObsDAV() {
        return solicitaObsDAV;
    }

    public void setSolicitaObsDAV(boolean solicitaObsDAV) {
        this.solicitaObsDAV = solicitaObsDAV;
    }

    @Column(name = "calc_preco_direto")
    public boolean isCalcPrecoDireto() {
        return calcPrecoDireto;
    }

    public void setCalcPrecoDireto(boolean calcPrecoDireto) {
        this.calcPrecoDireto = calcPrecoDireto;
    }

    @Column(name = "tela_cliente_venda")
    public String getTelaClienteVenda() {
        return telaClienteVenda;
    }

    public void setTelaClienteVenda(String telaClienteVenda) {
        this.telaClienteVenda = telaClienteVenda;
    }

    @Column(name = "modelo_etiqueta")
    public int getModeloEtiqueta() {
        return modeloEtiqueta;
    }

    public void setModeloEtiqueta(int modeloEtiqueta) {
        this.modeloEtiqueta = modeloEtiqueta;
    }

    @Column(name = "exibe_locacao_orcamento")
    public boolean isExibeLocacaoOrcamento() {
        return exibeLocacaoOrcamento;
    }

    public void setExibeLocacaoOrcamento(boolean exibeLocacaoOrcamento) {
        this.exibeLocacaoOrcamento = exibeLocacaoOrcamento;
    }

    @Column(name = "tipo_impressao_os")
    public String getModeloImpressaoOsJatoTinta() {
        return modeloImpressaoOsJatoTinta;
    }

    public void setModeloImpressaoOsJatoTinta(String modeloImpressaoOsJatoTinta) {
        this.modeloImpressaoOsJatoTinta = modeloImpressaoOsJatoTinta;
    }

    @Column(name = "calcula_comissao_fecha_os")
    public boolean isCalculaComissaFechaOS() {
        return calculaComissaFechaOS;
    }

    public void setCalculaComissaFechaOS(boolean calculaComissaFechaOS) {
        this.calculaComissaFechaOS = calculaComissaFechaOS;
    }

    @Column(name = "define_mecanico_fecha_os")
    public boolean isDefineMecanicoFechaOS() {
        return defineMecanicoFechaOS;
    }

    public void setDefineMecanicoFechaOS(boolean defineMecanicoFechaOS) {
        this.defineMecanicoFechaOS = defineMecanicoFechaOS;
    }

    @Column(name = "usa_varios_precos")
    public boolean isUsaVariosPrecos() {
        return usaVariosPrecos;
    }

    public void setUsaVariosPrecos(boolean usaVariosPrecos) {
        this.usaVariosPrecos = usaVariosPrecos;
    }

    @Column(name = "habilita_portal")
    public boolean isHabilitaPortal() {
        return habilitaPortal;
    }

    public void setHabilitaPortal(boolean habilitaPortal) {
        this.habilitaPortal = habilitaPortal;
    }

    @Column(name = "registra_pagto_devolucao")
    public boolean isRegistrarPagtoDevolucao() {
        return registrarPagtoDevolucao;
    }

    public void setRegistrarPagtoDevolucao(boolean registrarPagtoDevolucao) {
        this.registrarPagtoDevolucao = registrarPagtoDevolucao;
    }

    @Column(name = "tipo_exibicao_apos_cad_produto")
    public int getTipoExibicaoAposCadProduto() {
        return tipoExibicaoAposCadProduto;
    }

    public void setTipoExibicaoAposCadProduto(int tipoExibicaoAposCadProduto) {
        this.tipoExibicaoAposCadProduto = tipoExibicaoAposCadProduto;
    }

    @Column(name = "oferece_agregado")
    public boolean isOfereceAgregado() {
        return ofereceAgregado;
    }

    public void setOfereceAgregado(boolean ofereceAgregado) {
        this.ofereceAgregado = ofereceAgregado;
    }

    @Column(name = "markup_estoque")
    public BigDecimal getMarkupEstoque() {
        if (markupEstoque == null) {
            return BigDecimal.ONE;
        }
        return markupEstoque;
    }

    public void setMarkupEstoque(BigDecimal markupEstoque) {
        this.markupEstoque = markupEstoque;
    }

    @Column(name = "permite_orcamento_estoque_negativo")
    public boolean isPemiteOrcamentoEstoqueNegativo() {
        return pemiteOrcamentoEstoqueNegativo;
    }

    public void setPemiteOrcamentoEstoqueNegativo(boolean pemiteOrcamentoEstoqueNegativo) {
        this.pemiteOrcamentoEstoqueNegativo = pemiteOrcamentoEstoqueNegativo;
    }

    @Column(name = "registra_venda_perdida")
    public boolean isRegistraVendaPerdida() {
        return registraVendaPerdida;
    }

    public void setRegistraVendaPerdida(boolean registraVendaPerdida) {
        this.registraVendaPerdida = registraVendaPerdida;
    }

    @Column(name = "mensagem_os")
    public String getMensagemOS() {
        return mensagemOS;
    }

    public void setMensagemOS(String mensagemOS) {
        this.mensagemOS = mensagemOS;
    }

    @Column(name = "custo_transferencia")
    public BigDecimal getCustoTransferencia() {
        return custoTransferencia;
    }

    public void setCustoTransferencia(BigDecimal custoTransferencia) {
        this.custoTransferencia = custoTransferencia;
    }

    @Column(name = "entrada_somente_contas_pagar")
    public boolean isEntradaSomenteContasPagar() {
        return entradaSomenteContasPagar;
    }

    public void setEntradaSomenteContasPagar(boolean entradaSomenteContasPagar) {
        this.entradaSomenteContasPagar = entradaSomenteContasPagar;
    }

    @Column(name = "ip_servidor_impressao")
    public String getIpServidorImpressao() {
        return ipServidorImpressao;
    }

    public void setIpServidorImpressao(String ipServidorImpressao) {
        this.ipServidorImpressao = ipServidorImpressao;
    }

    @Column(name = "socket_servidor_impressao")
    public int getSocketServidorImpressao() {
        return socketServidorImpressao;
    }

    public void setSocketServidorImpressao(int socketServidorImpressao) {
        this.socketServidorImpressao = socketServidorImpressao;
    }

    @Column(name = "permite_trocar_vendedor_orcamento")
    public boolean isPermiteTrocarVendedorOrcamento() {
        return permiteTrocarVendedorOrcamento;
    }

    public void setPermiteTrocarVendedorOrcamento(boolean permiteTrocarVendedorOrcamento) {
        this.permiteTrocarVendedorOrcamento = permiteTrocarVendedorOrcamento;
    }

    @Column(name = "permite_orcamento_cliente_bloqueado")
    public boolean isPermiteOrcamentoClienteBloqueado() {
        return permiteOrcamentoClienteBloqueado;
    }

    public void setPermiteOrcamentoClienteBloqueado(boolean permiteOrcamentoClienteBloqueado) {
        this.permiteOrcamentoClienteBloqueado = permiteOrcamentoClienteBloqueado;
    }

    @Column(name = "codigo_produto_com_sigla_grupo")
    public boolean isCodigoProdutoComSiglaGrupo() {
        return codigoProdutoComSiglaGrupo;
    }

    public void setCodigoProdutoComSiglaGrupo(boolean codigoProdutoComSiglaGrupo) {
        this.codigoProdutoComSiglaGrupo = codigoProdutoComSiglaGrupo;
    }

    @Column(name = "qtd_via_orcamento")
    public int getQtdViaOrcamento() {
        return qtdViaOrcamento;
    }

    public void setQtdViaOrcamento(int qtdViaOrcamento) {
        this.qtdViaOrcamento = qtdViaOrcamento;
    }

    @Column(name = "min_venda_para_desconto")
    public BigDecimal getMinVendaParaDesconto() {
        return minVendaParaDesconto;
    }

    public void setMinVendaParaDesconto(BigDecimal minVendaParaDesconto) {
        this.minVendaParaDesconto = minVendaParaDesconto;
    }

    @Column(name = "data_implantacao_remota")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getDataImplantacaoRemota() {
        return dataImplantacaoRemota;
    }

    public void setDataImplantacaoRemota(Date dataImplantacaoRemota) {
        this.dataImplantacaoRemota = dataImplantacaoRemota;
    }

    public boolean verificaPrecoCusto() {
        return this.getEmpresa().getCnpj().equals("10686332000125")
                || this.getEmpresa().getCnpj().equals("10757676000188")
                || this.getEmpresa().getCnpj().equals("73323412000306")
                || this.getEmpresa().getCnpj().equals("04404391000160")
                || this.getEmpresa().getCnpj().equals("73323412000136");
    }

    public boolean ehGrupoFafe() {
        return this.getNome().contains("QUINZAO")
                || this.getNome().contains("FAFE")
                || this.getEmpresa().getCnpj().contains("09482014000163")
                || this.getEmpresa().getCnpj().contains("09087852000132")
                || this.getEmpresa().getCnpj().contains("04905258000198")
                || this.getEmpresa().getCnpj().contains("05894736000255")
                || this.getEmpresa().getCnpj().contains("02749319000140");
    }

    public boolean ehEmpresaDaFafe(String cnpj) {
        return cnpj.contains("32170565000103")
                || cnpj.contains("09482014000163")
                || cnpj.contains("09087852000132")
                || cnpj.contains("04905258000198")
                || cnpj.contains("05894736000255")
                || cnpj.contains("02749319000140");
    }

    public boolean mantemVendaEstoqueReserva() {
        return this.getEmpresa().getNome().contains("REAL RIO");
    }

    @Transient
    public boolean isGrupoPontoSoldas() {
        return this.getNome().contains("NEW SOLDAS") || this.getNome().contains("PONTO DAS SOLDAS");
    }

    public boolean enviaArquivos() {
        return this.getEmpresa().getCnpj().contains("05894736000100");
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_shadowsync_info")
    public ShadowsyncInfo getShadowsyncInfo() {
        return shadowsyncInfo;
    }

    public void setShadowsyncInfo(ShadowsyncInfo shadowsyncInfo) {
        this.shadowsyncInfo = shadowsyncInfo;
    }
}
