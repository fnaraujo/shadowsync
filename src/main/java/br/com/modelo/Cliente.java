package br.com.modelo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "cliente", schema = "public", uniqueConstraints
        = @UniqueConstraint(columnNames = "cpf_cnpj"))
@SequenceGenerator(name = "SEQUENCIA",
        sequenceName = "sq_cliente",
        allocationSize = 1)
public class Cliente implements java.io.Serializable, Cloneable {

    private long idCliente;
    private int tipoCodigo;
    private String cpfCnpj;
    private int tipo;
    private String inscricaoEstadual;
    private String razaoSocial;
    private char sexo;
    private BigDecimal limiteCredito;
    private BigDecimal limiteDisponivel;
    private String telefone;
    private String emailFinanceiro;
    private String site;
    private String email;
    private String areaAtuacao;
    private Categoria categoria;
    private UnidadeFederacao uf;
    private String apelido;
    private String orgaoExp;
    private String referenciaBancaria;
    private String referenciaComercial;
    private String consultaSerasa;
    private String consultaBanco;
    private Date dataCadastro;
    private Date ultimaAlteracao;
    private int status;
    private String ministerio;
    private String comando;
    private String contato;
    private FormaPagamento formaPagamento;
    private BigDecimal descontoPadrao;
    private Funcionario vendedorPadrao;
    private boolean somentePgtoCadastrado;
    private int maximoDiasAtraso;
    private int diaVencimento;

    private List<EnderecoCliente> enderecos = new ArrayList<>(0);
    private List<ContatoCliente> contatos = new ArrayList<>(0);

    public Cliente() {
    }

    public Cliente(long idCliente) {
        this.idCliente = idCliente;
    }

    public Cliente(long idCliente, String cpfCnpj) {
        this.idCliente = idCliente;
        this.cpfCnpj = cpfCnpj;
    }

    public Cliente(long idCliente, int tipoCodigo, String cpfCnpj, int tipo, String inscricaoEstadual, String razaoSocial, char sexo, BigDecimal limiteCredito, BigDecimal limiteDisponivel, String telefone, String emailFinanceiro, String site, String email, String areaAtuacao, Categoria categoria, UnidadeFederacao uf) {
        this.idCliente = idCliente;
        this.tipoCodigo = tipoCodigo;
        this.cpfCnpj = cpfCnpj;
        this.tipo = tipo;
        this.inscricaoEstadual = inscricaoEstadual;
        this.razaoSocial = razaoSocial;
        this.sexo = sexo;
        this.limiteCredito = limiteCredito;
        this.limiteDisponivel = limiteDisponivel;
        this.telefone = telefone;
        this.emailFinanceiro = emailFinanceiro;
        this.site = site;
        this.email = email;
        this.areaAtuacao = areaAtuacao;
        this.categoria = categoria;
        this.uf = uf;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQUENCIA")
    @Column(name = "id_cliente", unique = true, nullable = false)
    public long getIdCliente() {
        return this.idCliente;
    }

    public void setIdCliente(long idCliente) {
        this.idCliente = idCliente;
    }

    @Column(name = "tipo_codigo", nullable = false)
    public int getTipoCodigo() {
        return this.tipoCodigo;
    }

    public void setTipoCodigo(int tipoCodigo) {
        this.tipoCodigo = tipoCodigo;
    }

    @Column(name = "cpf_cnpj", unique = true, nullable = false, length = 20)
    public String getCpfCnpj() {
        return this.cpfCnpj;
    }

    public void setCpfCnpj(String cpfCnpj) {
        this.cpfCnpj = cpfCnpj;
    }

    @Column(name = "tipo", nullable = false)
    public int getTipo() {
        return this.tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    @Column(name = "inscricao_estadual", nullable = false, length = 20)
    public String getInscricaoEstadual() {
        return this.inscricaoEstadual;
    }

    public void setInscricaoEstadual(String inscricaoEstadual) {
        this.inscricaoEstadual = inscricaoEstadual;
    }

    @Column(name = "razao_social", nullable = false, length = 100)
    public String getRazaoSocial() {
        return this.razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    @Column(name = "sexo", nullable = false, length = 1)
    public char getSexo() {
        return this.sexo;
    }

    public void setSexo(char sexo) {
        this.sexo = sexo;
    }

    @Column(name = "limite_credito", nullable = false, precision = 15)
    public BigDecimal getLimiteCredito() {
        return this.limiteCredito;
    }

    public void setLimiteCredito(BigDecimal limiteCredito) {
        this.limiteCredito = limiteCredito;
    }

    @Column(name = "limite_disponivel", nullable = false, precision = 15)
    public BigDecimal getLimiteDisponivel() {
        return this.limiteDisponivel;
    }

    public void setLimiteDisponivel(BigDecimal limiteDisponivel) {
        this.limiteDisponivel = limiteDisponivel;
    }

    @Column(name = "telefone", nullable = false, length = 15)
    public String getTelefone() {
        return this.telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    @Column(name = "email_financeiro", nullable = true)
    public String getEmailFinanceiro() {
        return this.emailFinanceiro;
    }

    public void setEmailFinanceiro(String emailFinanceiro) {
        this.emailFinanceiro = emailFinanceiro;
    }

    @Column(name = "site", nullable = false, length = 50)
    public String getSite() {
        return this.site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    @Column(name = "email", nullable = false, length = 50)
    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(name = "area_atuacao", nullable = true, length = 50)
    public String getAreaAtuacao() {
        return this.areaAtuacao;
    }

    public void setAreaAtuacao(String areaAtuacao) {
        this.areaAtuacao = areaAtuacao;
    }

    @Column(name = "referencia_bancaria", nullable = true, length = 80)
    public String getReferenciaBancaria() {
        return referenciaBancaria;
    }

    public void setReferenciaBancaria(String referenciaBancaria) {
        this.referenciaBancaria = referenciaBancaria;
    }

    @Column(name = "referencia_comercial", nullable = true, length = 80)
    public String getReferenciaComercial() {
        return referenciaComercial;
    }

    public void setReferenciaComercial(String referenciaComercial) {
        this.referenciaComercial = referenciaComercial;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_categoria")
    public Categoria getCategoria() {
        return this.categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_unidade_federacao")
    public UnidadeFederacao getUnidadeFederacao() {
        return this.uf;
    }

    public void setUnidadeFederacao(UnidadeFederacao uf) {
        this.uf = uf;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_vendedor_padrao")
    public Funcionario getVendedorPadrao() {
        return vendedorPadrao;
    }

    public void setVendedorPadrao(Funcionario vendedorPadrao) {
        this.vendedorPadrao = vendedorPadrao;
    }

    @Column(name = "apelido", nullable = true, length = 50)
    public String getApelido() {
        return apelido;
    }

    public void setApelido(String apelido) {
        this.apelido = apelido;
    }

    @Column(name = "orgao_exp", nullable = true, length = 50)
    public String getOrgaoExp() {
        return orgaoExp;
    }

    public void setOrgaoExp(String orgaoExp) {
        this.orgaoExp = orgaoExp;
    }

    @Column(name = "consulta_banco", nullable = true, length = 50)
    public String getConsultaBanco() {
        return consultaBanco;
    }

    public void setConsultaBanco(String consultaBanco) {
        this.consultaBanco = consultaBanco;
    }

    @Column(name = "consulta_serasa", nullable = true, length = 50)
    public String getConsultaSerasa() {
        return consultaSerasa;
    }

    public void setConsultaSerasa(String consultaSerasa) {
        this.consultaSerasa = consultaSerasa;
    }

    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Column(name = "comando")
    public String getComando() {
        return comando;
    }

    public void setComando(String comando) {
        this.comando = comando;
    }

    @Column(name = "ministerio")
    public String getMinisterio() {
        return ministerio;
    }

    public void setMinisterio(String ministerio) {
        this.ministerio = ministerio;
    }

    @Column(name = "contato")
    public String getContato() {
        return contato;
    }

    public void setContato(String contato) {
        this.contato = contato;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_cadastro", nullable = false, length = 29)
    public Date getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ultima_alteracao", nullable = true, length = 29)
    public Date getUltimaAlteracao() {
        return ultimaAlteracao;
    }

    public void setUltimaAlteracao(Date ultimaAlteracao) {
        this.ultimaAlteracao = ultimaAlteracao;
    }

    @Column(name = "desconto_padrao")
    public BigDecimal getDescontoPadrao() {
        return descontoPadrao;
    }

    public void setDescontoPadrao(BigDecimal descontoPadrao) {
        this.descontoPadrao = descontoPadrao;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_forma_pagamento")
    public FormaPagamento getFormaPagamento() {
        return formaPagamento;
    }

    public void setFormaPagamento(FormaPagamento formaPagamento) {
        this.formaPagamento = formaPagamento;
    }

    @Column(name = "somente_pgto_cadastrado")
    public boolean isSomentePgtoCadastrado() {
        return somentePgtoCadastrado;
    }

    public void setSomentePgtoCadastrado(boolean somentePgtoCadastrado) {
        this.somentePgtoCadastrado = somentePgtoCadastrado;
    }

    @Column(name = "maximo_dias_atraso")
    public int getMaximoDiasAtraso() {
        return maximoDiasAtraso;
    }

    public void setMaximoDiasAtraso(int maximoDiasAtraso) {
        this.maximoDiasAtraso = maximoDiasAtraso;
    }

    @Column(name = "dia_vencimento")
    public int getDiaVencimento() {
        return diaVencimento;
    }

    public void setDiaVencimento(int diaVencimento) {
        this.diaVencimento = diaVencimento;
    }

    @Fetch(FetchMode.SUBSELECT)
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "cliente", targetEntity = EnderecoCliente.class)
    public List<EnderecoCliente> getEnderecos() {
        return this.enderecos;
    }

    public void setEnderecos(List enderecos) {
        this.enderecos = enderecos;
    }

    @Fetch(FetchMode.SUBSELECT)
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "cliente", targetEntity = ContatoCliente.class)
    public List<ContatoCliente> getContatos() {
        return contatos;
    }

    public void setContatos(List<ContatoCliente> contatos) {
        this.contatos = contatos;
    }

    public boolean camposObrigatoriosValidos() {
        boolean resp = true;

        if ((this.getCpfCnpj().isEmpty()) || (this.getRazaoSocial().isEmpty())
                || (this.getInscricaoEstadual().isEmpty()) || (this.getCategoria() == null)) {
            resp = false;
        }

        return resp;
    }

    public boolean camposObrigatoriosValidosSimples() {
        boolean resp = true;

        if ((this.getCpfCnpj().isEmpty()) || (this.getRazaoSocial().isEmpty())) {
            resp = false;
        }

        return resp;
    }

    public String retornaNome(int maxLength) {
        if (this.razaoSocial.length() <= maxLength) {
            return this.razaoSocial;
        } else {
            return this.razaoSocial.substring(0, maxLength - 1);
        }
    }

    public boolean somenteFornecedor() {
        return this.categoria.getIdCategoria() == 2;
    }

    public boolean verificaClienteBloqueado() {
        return this.status == 2;
    }

    @Override
    public Cliente clone() throws CloneNotSupportedException {

        Cliente c = (Cliente) super.clone();
        c.setIdCliente(0);

        List<EnderecoCliente> enderecos1 = new ArrayList<>();
        c.setEnderecos(enderecos1);
        if (enderecos != null && !enderecos.isEmpty()) {
            for (EnderecoCliente e : enderecos) {
                EnderecoCliente e1 = e.clone();
                e1.setCliente(c);
                enderecos1.add(e1);
            }
        }

        List<ContatoCliente> contatos1 = new ArrayList<>();
        c.setContatos(contatos1);
        if (contatos != null && !contatos.isEmpty()) {
            for (ContatoCliente cc : contatos) {
                ContatoCliente cc1 = cc.clone();
                cc1.setCliente(c);
                contatos1.add(cc1);
            }
        }

        return c;
    }

    @Override
    public String toString() {
        return "Cliente{" + "idCliente=" + idCliente + ", tipoCodigo=" + tipoCodigo + ", cpfCnpj=" + cpfCnpj + ", tipo=" + tipo + ", inscricaoEstadual=" + inscricaoEstadual + ", razaoSocial=" + razaoSocial + ", sexo=" + sexo + ", limiteCredito=" + limiteCredito + ", limiteDisponivel=" + limiteDisponivel + ", telefone=" + telefone + ", emailFinanceiro=" + emailFinanceiro + ", site=" + site + ", email=" + email + ", areaAtuacao=" + areaAtuacao + ", categoria=" + categoria + ", uf=" + uf + ", apelido=" + apelido + ", orgaoExp=" + orgaoExp + ", referenciaBancaria=" + referenciaBancaria + ", referenciaComercial=" + referenciaComercial + ", consultaSerasa=" + consultaSerasa + ", consultaBanco=" + consultaBanco + ", dataCadastro=" + dataCadastro + ", ultimaAlteracao=" + ultimaAlteracao + ", status=" + status + ", ministerio=" + ministerio + ", comando=" + comando + ", contato=" + contato + ", formaPagamento=" + formaPagamento + ", descontoPadrao=" + descontoPadrao + ", vendedorPadrao=" + vendedorPadrao + ", somentePgtoCadastrado=" + somentePgtoCadastrado + ", maximoDiasAtraso=" + maximoDiasAtraso + ", enderecos=" + enderecos + ", contatos=" + contatos + '}';
    }
}
