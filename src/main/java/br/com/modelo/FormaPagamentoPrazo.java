package br.com.modelo;

import com.towel.el.annotation.Resolvable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Flávio Nunes
 */
@Entity
@Table(name="forma_pagamento_prazo"
    ,schema="public"
)
@SequenceGenerator(name = "SEQUENCIA",
    sequenceName = "sq_pagamento_prazo",
    allocationSize = 1)
public class FormaPagamentoPrazo implements java.io.Serializable, Comparable{

    private int idPagamentoPrazo;
    private FormaPagamento formaPagamento;
    @Resolvable(colName = "qtdDias")
    private int qtdDias;    
    private float percentual;

    public FormaPagamentoPrazo() {
    }

    public FormaPagamentoPrazo(int idPagamentoPrazo, FormaPagamento formaPagamento, int qtdDias, float percentual) {
        this.idPagamentoPrazo = idPagamentoPrazo;
        this.formaPagamento = formaPagamento;
        this.qtdDias = qtdDias;
        this.percentual = percentual;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO, generator="SEQUENCIA")
    @Column(name="id_pagamento_prazo", unique=true, nullable=false)
    public int getIdPagamentoPrazo() {
        return idPagamentoPrazo;
    }

    public void setIdPagamentoPrazo(int idPagamentoPrazo) {
        this.idPagamentoPrazo = idPagamentoPrazo;
    }

    @Column(name="percentual", nullable=false, precision=8, scale=8)
    public float getPercentual() {
        return percentual;
    }

    public void setPercentual(float percentual) {
        this.percentual = percentual;
    }

    @Column(name="qtd_dias", nullable=false)
    public int getQtdDias() {
        return qtdDias;
    }

    public void setQtdDias(int qtdDias) {
        this.qtdDias = qtdDias;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="id_forma_pagamento", nullable=false)
    public FormaPagamento getFormaPagamento() {
        return formaPagamento;
    }

    public void setFormaPagamento(FormaPagamento formaPagamento) {
        this.formaPagamento = formaPagamento;
    }

    @Override
    public int compareTo(Object o) {
        FormaPagamentoPrazo forma1 = (FormaPagamentoPrazo) o;
        
        return Integer.compare(this.getQtdDias(), forma1.getQtdDias());
    }

}
