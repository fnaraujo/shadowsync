package br.com.modelo;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "item_venda", schema = "public")
@SequenceGenerator(name = "SEQUENCIA",
sequenceName = "sq_item_venda",
allocationSize = 1)
public class ItemVenda implements java.io.Serializable, Comparable{

    public static final String NOME_TABELA = "item_venda";
    private long idItemVenda;
    private Venda venda;
    private Produto produto;
    private String hash;
    private BigDecimal qtdVenda = BigDecimal.ZERO;
    private BigDecimal precoVenda = BigDecimal.ZERO;
    private BigDecimal totalDesconto = BigDecimal.ZERO;
    private BigDecimal totalAcrescimo = BigDecimal.ZERO;
    private BigDecimal precoTotal = BigDecimal.ZERO;
    private Integer numOrdem;
    private int statusItem;
    private Integer codigoFiscalOperacao;
    private BigDecimal totalIcms = BigDecimal.ZERO;
    private BigDecimal totalPis = BigDecimal.ZERO;
    private BigDecimal totalCofins = BigDecimal.ZERO;
    private float vlBaseICMS;
    private float aliquotaICMS;
    private float vlBasePis;
    private float aliquotaPis;
    private float vlBaseCofins;
    private float aliquotaCofins;
    private BigDecimal vlBaseIPI = BigDecimal.ZERO;
    private BigDecimal aliquotaIPI = BigDecimal.ZERO;
    private BigDecimal totalIPI = BigDecimal.ZERO;
    private String ncmVenda;
    private String codigoProduto;
    private String descricaoProduto;
    private String unidadeMedida;
    private Long idDav;
    private Date dataInclusao;
    private BigDecimal custoReposicao = BigDecimal.ZERO;
    private BigDecimal lucroVenda;
    private BigDecimal qtdDevolvida = BigDecimal.ZERO;
    private BigDecimal valorDevolucao = BigDecimal.ZERO;
    private BigDecimal custoDevolucao = BigDecimal.ZERO;
    private BigDecimal lucroLiquido = BigDecimal.ZERO;
    private String locacao;
    private boolean precoPromocional;
    private boolean itemConsumo;
    private boolean gerouComissao;//esse campo não será persistido
    private String formasPgtoPromocao;
    private BigDecimal qtdEmbalagem = BigDecimal.ZERO;
    private BigDecimal valorOriginal = BigDecimal.ZERO;
    private BigDecimal ultimaQtd = BigDecimal.ZERO;

    public ItemVenda() {
    }

    public ItemVenda(Long idItemVenda) {
        this.idItemVenda = idItemVenda;
    }

    public ItemVenda(long idItemVenda, Venda venda, Produto produto, BigDecimal qtdVenda, BigDecimal precoVenda, BigDecimal totalDesconto, BigDecimal totalAcrescimo, BigDecimal precoTotal, int statusItem) {
        this.idItemVenda = idItemVenda;
        this.venda = venda;
        this.produto = produto;
        this.qtdVenda = qtdVenda;
        this.precoVenda = precoVenda;
        this.totalDesconto = totalDesconto;
        this.totalAcrescimo = totalAcrescimo;
        this.precoTotal = precoTotal;
        this.statusItem = statusItem;
    }

    public ItemVenda(long idItemVenda, Venda venda, Produto produto, BigDecimal qtdVenda, BigDecimal precoVenda, BigDecimal totalDesconto, BigDecimal totalAcrescimo, BigDecimal precoTotal, Integer numOrdem, int statusItem) {
        this.idItemVenda = idItemVenda;
        this.venda = venda;
        this.produto = produto;
        this.qtdVenda = qtdVenda;
        this.precoVenda = precoVenda;
        this.totalDesconto = totalDesconto;
        this.totalAcrescimo = totalAcrescimo;
        this.precoTotal = precoTotal;
        this.numOrdem = numOrdem;
        this.statusItem = statusItem;
    }

    public ItemVenda(long idItemVenda, Venda venda, Produto produto, BigDecimal qtdVenda, BigDecimal precoVenda, BigDecimal totalDesconto, BigDecimal totalAcrescimo, BigDecimal precoTotal, Integer numOrdem, int statusItem, Integer codigoFiscalOperacao, BigDecimal totalIcms) {
        this.idItemVenda = idItemVenda;
        this.venda = venda;
        this.produto = produto;
        this.qtdVenda = qtdVenda;
        this.precoVenda = precoVenda;
        this.totalDesconto = totalDesconto;
        this.totalAcrescimo = totalAcrescimo;
        this.precoTotal = precoTotal;
        this.numOrdem = numOrdem;
        this.statusItem = statusItem;
        this.codigoFiscalOperacao = codigoFiscalOperacao;
        this.totalIcms = totalIcms;
    }

    public ItemVenda(long idItemVenda, Venda venda, Produto produto, BigDecimal qtdVenda, BigDecimal precoVenda, BigDecimal totalDesconto, BigDecimal totalAcrescimo, BigDecimal precoTotal, Integer numOrdem, int statusItem, Integer codigoFiscalOperacao, BigDecimal totalIcms, BigDecimal pis, BigDecimal cofins) {
        this.idItemVenda = idItemVenda;
        this.venda = venda;
        this.produto = produto;
        this.qtdVenda = qtdVenda;
        this.precoVenda = precoVenda;
        this.totalDesconto = totalDesconto;
        this.totalAcrescimo = totalAcrescimo;
        this.precoTotal = precoTotal;
        this.numOrdem = numOrdem;
        this.statusItem = statusItem;
        this.codigoFiscalOperacao = codigoFiscalOperacao;
        this.totalIcms = totalIcms;
        this.totalPis = pis;
        this.totalCofins = cofins;
    }

    public ItemVenda(long idItemVenda, Venda venda, Produto produto, BigDecimal qtdVenda, BigDecimal precoVenda, BigDecimal totalDesconto, BigDecimal totalAcrescimo, BigDecimal precoTotal, Integer numOrdem, int statusItem, Integer codigoFiscalOperacao, BigDecimal totalIcms, BigDecimal totalPis, BigDecimal totalCofins, float vlBaseICMS, float aliquotaICMS, float vlBasePis, float aliquotaPis) {
        this.idItemVenda = idItemVenda;
        this.venda = venda;
        this.produto = produto;
        this.qtdVenda = qtdVenda;
        this.precoVenda = precoVenda;
        this.totalDesconto = totalDesconto;
        this.totalAcrescimo = totalAcrescimo;
        this.precoTotal = precoTotal;
        this.numOrdem = numOrdem;
        this.statusItem = statusItem;
        this.codigoFiscalOperacao = codigoFiscalOperacao;
        this.totalIcms = totalIcms;
        this.totalPis = totalPis;
        this.totalCofins = totalCofins;
        this.vlBaseICMS = vlBaseICMS;
        this.aliquotaICMS = aliquotaICMS;
        this.vlBasePis = vlBasePis;
        this.aliquotaPis = aliquotaPis;
    }

    public ItemVenda(long idItemVenda, Venda venda, Produto produto, BigDecimal qtdVenda, BigDecimal precoVenda, BigDecimal totalDesconto, BigDecimal totalAcrescimo, BigDecimal precoTotal, Integer numOrdem, int statusItem, Integer codigoFiscalOperacao, BigDecimal totalIcms, BigDecimal totalPis, BigDecimal totalCofins, float vlBaseICMS, float aliquotaICMS, float vlBasePis, float aliquotaPis, float vlBaseCofins, float aliquotaCofins) {
        this.idItemVenda = idItemVenda;
        this.venda = venda;
        this.produto = produto;
        this.qtdVenda = qtdVenda;
        this.precoVenda = precoVenda;
        this.totalDesconto = totalDesconto;
        this.totalAcrescimo = totalAcrescimo;
        this.precoTotal = precoTotal;
        this.numOrdem = numOrdem;
        this.statusItem = statusItem;
        this.codigoFiscalOperacao = codigoFiscalOperacao;
        this.totalIcms = totalIcms;
        this.totalPis = totalPis;
        this.totalCofins = totalCofins;
        this.vlBaseICMS = vlBaseICMS;
        this.aliquotaICMS = aliquotaICMS;
        this.vlBasePis = vlBasePis;
        this.aliquotaPis = aliquotaPis;
        this.vlBaseCofins = vlBaseCofins;
        this.aliquotaCofins = aliquotaCofins;
    }

    public ItemVenda(long idItemVenda, Venda venda, Produto produto, BigDecimal qtdVenda, BigDecimal precoVenda, BigDecimal totalDesconto, BigDecimal totalAcrescimo, BigDecimal precoTotal, Integer numOrdem, int statusItem, Integer codigoFiscalOperacao, BigDecimal totalIcms, BigDecimal totalPis, BigDecimal totalCofins, float vlBaseICMS, float aliquotaICMS, float vlBasePis, float aliquotaPis, float vlBaseCofins, float aliquotaCofins, String ncmVenda, String codigoProduto, String descricaoProduto, String unidadeMedida, long idDav, Date dataInclusao) {
        this.idItemVenda = idItemVenda;
        this.venda = venda;
        this.produto = produto;
        this.qtdVenda = qtdVenda;
        this.precoVenda = precoVenda;
        this.totalDesconto = totalDesconto;
        this.totalAcrescimo = totalAcrescimo;
        this.precoTotal = precoTotal;
        this.numOrdem = numOrdem;
        this.statusItem = statusItem;
        this.codigoFiscalOperacao = codigoFiscalOperacao;
        this.totalIcms = totalIcms;
        this.totalPis = totalPis;
        this.totalCofins = totalCofins;
        this.vlBaseICMS = vlBaseICMS;
        this.aliquotaICMS = aliquotaICMS;
        this.vlBasePis = vlBasePis;
        this.aliquotaPis = aliquotaPis;
        this.vlBaseCofins = vlBaseCofins;
        this.aliquotaCofins = aliquotaCofins;
        this.ncmVenda = ncmVenda;
        this.codigoProduto = codigoProduto;
        this.descricaoProduto = descricaoProduto;
        this.unidadeMedida = unidadeMedida;
        this.idDav = idDav;
        this.dataInclusao = dataInclusao;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQUENCIA")
    @Column(name = "id_item_venda", unique = true, nullable = false)
    public long getIdItemVenda() {
        return this.idItemVenda;
    }

    public void setIdItemVenda(long idItemVenda) {
        this.idItemVenda = idItemVenda;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_venda", nullable = false, insertable = true, updatable = true)
    public Venda getVenda() {
        return this.venda;
    }

    public void setVenda(Venda venda) {
        this.venda = venda;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_produto", nullable = false, insertable = true, updatable = true)
    public Produto getProduto() {
        return this.produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    @Column(name = "qtd_venda", nullable = false, precision = 15, scale = 4)
    public BigDecimal getQtdVenda() {
        return this.qtdVenda;
    }

    public void setQtdVenda(BigDecimal qtdVenda) {
        this.qtdVenda = qtdVenda;
    }

    @Column(name = "preco_venda", nullable = false, precision = 21, scale = 10)
    public BigDecimal getPrecoVenda() {
        return this.precoVenda;
    }

    public void setPrecoVenda(BigDecimal precoVenda) {
        this.precoVenda = precoVenda;
    }

    @Column(name = "total_desconto", nullable = false, precision = 15, scale = 4)
    public BigDecimal getTotalDesconto() {
        return this.totalDesconto;
    }

    public void setTotalDesconto(BigDecimal totalDesconto) {
        this.totalDesconto = totalDesconto;
    }

    @Column(name = "total_acrescimo", nullable = false, precision = 15, scale = 4)
    public BigDecimal getTotalAcrescimo() {
        return this.totalAcrescimo;
    }

    public void setTotalAcrescimo(BigDecimal totalAcrescimo) {
        this.totalAcrescimo = totalAcrescimo;
    }

    @Column(name = "preco_total", nullable = false, precision = 15, scale = 2)
    public BigDecimal getPrecoTotal() {
        return this.precoTotal;
    }

    public void setPrecoTotal(BigDecimal precoTotal) {
        this.precoTotal = precoTotal;
    }

    @Column(name = "num_ordem")
    public Integer getNumOrdem() {
        return this.numOrdem;
    }

    public void setNumOrdem(Integer numOrdem) {
        this.numOrdem = numOrdem;
    }

    @Column(name = "status_item", nullable = false)
    public int getStatusItem() {
        return this.statusItem;
    }

    public void setStatusItem(int statusItem) {
        this.statusItem = statusItem;
    }

    @Column(name = "id_codigo_fiscal_operacao")
    public Integer getCodigoFiscalOperacao() {
        return this.codigoFiscalOperacao;
    }

    public void setCodigoFiscalOperacao(Integer codigoFiscalOperacao) {
        this.codigoFiscalOperacao = codigoFiscalOperacao;
    }

    @Column(name = "total_icms", nullable = false, precision = 9)
    public BigDecimal getTotalIcms() {
        return this.totalIcms;
    }

    public void setTotalIcms(BigDecimal totalIcms) {
        this.totalIcms = totalIcms;
    }

    @Column(name = "total_pis", nullable = true, precision = 9)
    public BigDecimal getTotalPis() {
        return this.totalPis;
    }

    public void setTotalPis(BigDecimal totalPis) {
        this.totalPis = totalPis;
    }

    @Column(name = "total_cofins", nullable = true, precision = 9)
    public BigDecimal getTotalCofins() {
        return this.totalCofins;
    }

    public void setTotalCofins(BigDecimal totalCofins) {
        this.totalCofins = totalCofins;
    }

    @Column(name = "vl_base_icms")
    public float getVlBaseICMS() {
        return vlBaseICMS;
    }

    public void setVlBaseICMS(float vlBaseICMS) {
        this.vlBaseICMS = vlBaseICMS;
    }

    @Column(name = "aliq_icms")
    public float getAliquotaICMS() {
        return aliquotaICMS;
    }

    public void setAliquotaICMS(float aliquotaICMS) {
        this.aliquotaICMS = aliquotaICMS;
    }

    @Column(name = "vl_base_pis")
    public float getVlBasePis() {
        return vlBasePis;
    }

    public void setVlBasePis(float vlBasePis) {
        this.vlBasePis = vlBasePis;
    }

    @Column(name = "aliq_pis")
    public float getAliquotaPis() {
        return aliquotaPis;
    }

    public void setAliquotaPis(float aliquotaPis) {
        this.aliquotaPis = aliquotaPis;
    }

    @Column(name = "vl_base_cofins")
    public float getVlBaseCofins() {
        return vlBaseCofins;
    }

    public void setVlBaseCofins(float vlBaseCofins) {
        this.vlBaseCofins = vlBaseCofins;
    }

    @Column(name = "aliq_cofins")
    public float getAliquotaCofins() {
        return aliquotaCofins;
    }

    public void setAliquotaCofins(float aliquotaCofins) {
        this.aliquotaCofins = aliquotaCofins;
    }

    @Column(name = "vl_base_ipi")
    public BigDecimal getVlBaseIPI() {
        return vlBaseIPI;
    }

    public void setVlBaseIPI(BigDecimal vlBaseIPI) {
        this.vlBaseIPI = vlBaseIPI;
    }

    @Column(name = "aliq_ipi")
    public BigDecimal getAliquotaIPI() {
        return aliquotaIPI;
    }

    public void setAliquotaIPI(BigDecimal aliquotaIPI) {
        this.aliquotaIPI = aliquotaIPI;
    }

    @Column(name = "total_ipi")
    public BigDecimal getTotalIPI() {
        return totalIPI;
    }

    public void setTotalIPI(BigDecimal totalIPI) {
        this.totalIPI = totalIPI;
    }

    @Column(name = "item_consumo")
    public boolean isItemConsumo() {
        return itemConsumo;
    }

    public void setItemConsumo(boolean itemConsumo) {
        this.itemConsumo = itemConsumo;
    }

    @Column(name = "ncm_venda", nullable = true, length = 8)
    public String getNcmVenda() {
        return ncmVenda;
    }

    public void setNcmVenda(String ncmVenda) {
        this.ncmVenda = ncmVenda;
    }

    @Column(name = "codigo_produto", nullable = true, length = 20)
    public String getCodigoProduto() {
        return codigoProduto;
    }

    public void setCodigoProduto(String codigoProduto) {
        this.codigoProduto = codigoProduto;
    }

    @Column(name = "descricao_produto", nullable = true, length = 100)
    public String getDescricaoProduto() {
        return descricaoProduto;
    }

    public void setDescricaoProduto(String descricaoProduto) {
        this.descricaoProduto = descricaoProduto;
    }

    @Column(name = "unidade_medida", nullable = true, length = 10)
    public String getUnidadeMedida() {
        return unidadeMedida;
    }

    public void setUnidadeMedida(String unidadeMedida) {
        this.unidadeMedida = unidadeMedida;
    }

    @Column(name = "id_dav", nullable = true)
    public Long getIdDav() {
        return this.idDav;
    }

    public void setIdDav(Long idDav) {
        this.idDav = idDav;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_inclusao", length = 29, insertable = false, updatable = false)
    public Date getDataInclusao() {
        return this.dataInclusao;
    }

    public void setDataInclusao(Date dataInclusao) {
        this.dataInclusao = dataInclusao;
    }

    @Column(name = "custo_reposicao", precision = 15, scale = 2)
    public BigDecimal getCustoReposicao() {
        return custoReposicao;
    }

    public void setCustoReposicao(BigDecimal custoReposicao) {
        this.custoReposicao = custoReposicao;
    }

    @Column(name = "lucro_venda", nullable = true, precision = 15, scale = 2)
    public BigDecimal getLucroVenda() {
        if (lucroVenda == null) {
            return BigDecimal.ZERO;
        }
        return lucroVenda;
    }

    public void setLucroVenda(BigDecimal lucroVenda) {
        this.lucroVenda = lucroVenda;
    }

    @Column(name = "qtd_devolvida", nullable = true, precision = 15, scale = 2)
    public BigDecimal getQtdDevolvida() {
        return qtdDevolvida;
    }

    public void setQtdDevolvida(BigDecimal qtdDevolvida) {
        this.qtdDevolvida = qtdDevolvida;
    }

    @Column(name = "valor_devolucao", nullable = true, precision = 15, scale = 2)
    public BigDecimal getValorDevolucao() {
        return valorDevolucao;
    }

    public void setValorDevolucao(BigDecimal valorDevolucao) {
        this.valorDevolucao = valorDevolucao;
    }

    @Column(name = "custo_devolucao", nullable = true, precision = 15, scale = 2)
    public BigDecimal getCustoDevolucao() {
        return custoDevolucao;
    }

    public void setCustoDevolucao(BigDecimal custoDevolucao) {
        this.custoDevolucao = custoDevolucao;
    }

    @Column(name = "lucro_liquido", nullable = true, precision = 15, scale = 2)
    public BigDecimal getLucroLiquido() {
        if (lucroLiquido == null) {
            return BigDecimal.ZERO;
        }
        return lucroLiquido;
    }

    public void setLucroLiquido(BigDecimal lucroLiquido) {
        this.lucroLiquido = lucroLiquido;
    }

    @Column(name = "locacao")
    public String getLocacao() {
        return locacao;
    }

    public void setLocacao(String locacao) {
        this.locacao = locacao;
    }

    @Column(name = "preco_promocional")
    public boolean isPrecoPromocional() {
        return precoPromocional;
    }

    public void setPrecoPromocional(boolean precoPromocional) {
        this.precoPromocional = precoPromocional;
    }

    @Column(name = "formas_pgto_promocao")
    public String getFormasPgtoPromocao() {
        return formasPgtoPromocao;
    }

    public void setFormasPgtoPromocao(String formasPgtoPromocao) {
        this.formasPgtoPromocao = formasPgtoPromocao;
    }

    @Column(name = "hash", nullable = true)
    public String getHash() {
        return this.hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    @Transient
    public boolean isGerouComissao() {
        return gerouComissao;
    }

    public void setGerouComissao(boolean gerouComissao) {
        this.gerouComissao = gerouComissao;
    }

    @Column(name = "qtd_embalagem")
    public BigDecimal getQtdEmbalagem() {
        return qtdEmbalagem;
    }

    public void setQtdEmbalagem(BigDecimal qtdEmbalagem) {
        this.qtdEmbalagem = qtdEmbalagem;
    }

    @Transient
    public BigDecimal getValorOriginal() {
        return valorOriginal;
    }

    public void setValorOriginal(BigDecimal valorOriginal) {
        this.valorOriginal = valorOriginal;
    }

    @Transient
    public BigDecimal getUltimaQtd() {
        return ultimaQtd;
    }

    public void setUltimaQtd(BigDecimal ultimaQtd) {
        this.ultimaQtd = ultimaQtd;
    }

    public void calculaTotal() {
        this.setCustoReposicao(this.getCustoReposicao().multiply(this.getQtdVenda()));
        this.setPrecoTotal(this.precoVenda.multiply(this.qtdVenda));
        if (this.totalDesconto != null) {
            this.setPrecoTotal(this.precoTotal.subtract(this.totalDesconto));
        } else {
            this.totalDesconto = BigDecimal.ZERO;
        }
    }

    public void calculaTotalSemCusto() {
        if (this.qtdVenda == null) {
            this.qtdVenda = BigDecimal.ONE;
        }
        this.setPrecoTotal(this.precoVenda.multiply(this.qtdVenda));
        if (this.totalDesconto != null) {
            this.setPrecoTotal(this.precoTotal.subtract(this.totalDesconto));
        } else {
            this.totalDesconto = BigDecimal.ZERO;
        }
    }

    public boolean verificaValorFracionadoAbaixoCusto() {
        if (this.produto.getUnidadeMedida().isFracionada()) {
            try {
                this.qtdVenda.intValueExact();

            } catch (ArithmeticException e) {
                int qtd = this.qtdVenda.intValue() + 1;
                if (BigDecimal.valueOf(qtd).multiply(this.precoVenda).floatValue() < this.custoReposicao.floatValue()) {
                    return true;
                }
            }
        } else {
            return true;
        }
        return false;
    }

    public void acrescentaQuantidade(BigDecimal qtdSoma) {
        this.qtdVenda = this.qtdVenda.add(qtdSoma);
        this.calculaTotal();
    }
    

    public boolean descontoLojaPermitido(BigDecimal indice) {
        if (indice.compareTo(BigDecimal.ZERO) == 0) {
            return true;
        }
        System.out.println("Indice: " + this.retornaIndiceDesconto());
        return indice.compareTo(this.retornaIndiceDesconto()) == 1 || indice.compareTo(this.retornaIndiceDesconto()) == 0;

    }

    public int compareTo(Object o) {
        int resp = 0;
        ItemVenda item = (ItemVenda) o;
        if (this.numOrdem == item.numOrdem) {
            resp = 0;
        } else if (this.numOrdem < item.numOrdem) {
            resp = -1;
        } else if (this.numOrdem > item.numOrdem) {
            resp = 1;
        }
        return resp;
    }

    public void defineTotalDesconto(BigDecimal indice) {
        if ((this.qtdVenda == null) || (this.qtdVenda.compareTo(BigDecimal.ZERO) <= 0)) {
            return;
        }
        try {
            indice = indice.divide(BigDecimal.valueOf(100));
            this.totalDesconto = ((this.getPrecoVenda().multiply(indice))).multiply(this.qtdVenda).setScale(2, RoundingMode.HALF_UP);
            this.calculaTotalSemCusto();
        } catch (Exception e) {
            this.totalDesconto = BigDecimal.ZERO;
        }
    }

    public BigDecimal retornaIndiceDesconto() {
        BigDecimal valor = this.precoTotal.add(this.totalDesconto);

        BigDecimal valor1 = BigDecimal.ZERO;
        if (totalDesconto.compareTo(BigDecimal.ZERO) > 0) {
            valor1 = this.totalDesconto.divide(valor, BigDecimal.ROUND_HALF_UP);
        }

        BigDecimal valor2 = valor1.multiply(BigDecimal.valueOf(100)).setScale(2, RoundingMode.UP);
        return valor2;
    }

    public void defineQtdVenda(BigDecimal qtd) {

        if (this.qtdVenda.compareTo(BigDecimal.ZERO) <= 0) {
            this.setUltimaQtd(BigDecimal.ONE);
        } else {
            this.setUltimaQtd(this.getQtdVenda());
        }

        BigDecimal descontoUnitario;
        if (this.totalDesconto.compareTo(BigDecimal.ZERO) != 0) {
            descontoUnitario = this.totalDesconto.divide(this.qtdVenda, RoundingMode.HALF_UP);
            this.qtdVenda = qtd;
            this.totalDesconto = this.qtdVenda.multiply(descontoUnitario);
        } else {
            this.qtdVenda = qtd;
            this.totalDesconto = BigDecimal.ZERO;
        }

        this.calculaTotalSemCusto();
    }

    public void defineValorUnitario(BigDecimal valorUnitario) {
        this.precoVenda = valorUnitario;
        if (totalDesconto == null) {
            this.totalDesconto = BigDecimal.ZERO;
        }
        this.calculaTotalSemCusto();
    }    

    public boolean valorAbaixoCusto() {
        return (this.getPrecoTotal()).floatValue() < this.custoReposicao.floatValue(); //|| this.custoReposicao==0;           
    }

    public boolean descontoAbaixoLimiteMaximo(BigDecimal descontoMaximo, BigDecimal descontoProduto) {
        return descontoProduto.compareTo(descontoMaximo) == -1 || descontoProduto.compareTo(descontoMaximo) == 0;
    }    

    public boolean atingiuLimiteDesconto(BigDecimal valorIndice) {
        return valorIndice.compareTo(this.totalDesconto) == 0;
    }
    

    public void defineCustoTotal(BigDecimal custoReposicao) {
        BigDecimal aux = this.qtdVenda.multiply(new BigDecimal(this.produto.getQtdEmbalagem()));
        this.custoReposicao = custoReposicao.multiply(aux);
    }

    public void defineLucroObtido() {
        this.lucroVenda = this.precoTotal.subtract(this.custoReposicao);
        this.lucroLiquido = this.lucroVenda;
        if (this.custoDevolucao != null && this.qtdDevolvida != null) {
            BigDecimal aux = this.lucroVenda.divide(this.qtdVenda, MathContext.DECIMAL128).multiply(this.qtdDevolvida);
            this.lucroLiquido = this.lucroVenda.subtract(aux);
        }
    }

    public void ajustaValoresDevolucao() {

        if (lucroVenda == null) {
            lucroVenda = BigDecimal.ZERO;
        }

        BigDecimal auxCusto = this.custoReposicao.divide(qtdVenda, MathContext.DECIMAL128);
        auxCusto = auxCusto.multiply(this.qtdDevolvida);

        this.custoDevolucao = auxCusto;


        BigDecimal auxLucro = this.lucroVenda.divide(qtdVenda, MathContext.DECIMAL128);
        auxLucro = auxLucro.multiply(this.qtdDevolvida);
        auxLucro = this.lucroVenda.subtract(auxLucro);

        this.lucroLiquido = auxLucro;
    }

    public float retornaLucroReal() {
        float lucroReal = 0;
        if (qtdVenda == null) {
            return 0;
        }
        if (qtdVenda.compareTo(BigDecimal.ZERO) <= 0) {
            return 0;
        }

        if (this.getCustoReposicao().floatValue() > 0) {
            BigDecimal totalDescItem = BigDecimal.ZERO;
            if (this.totalDesconto != null) {
                totalDescItem = this.totalDesconto.divide(qtdVenda, RoundingMode.HALF_UP);
            }
            BigDecimal custoUn = this.getCustoReposicao().divide(this.getQtdVenda(), RoundingMode.HALF_UP);
            lucroReal = ((this.getPrecoVenda().subtract(totalDescItem).floatValue() / custoUn.floatValue()) - 1) * 100;
        }
        return lucroReal;
    }

    public float retornaIndicePercentual(float indice) {
        return (indice - 1) * 100;
    }

    public boolean hasSaldoDisponivel(BigDecimal qtdDevolucao) {
        BigDecimal qtdDisponivel;

        if (this.qtdDevolvida == null) {
            this.qtdDevolvida = BigDecimal.ZERO;
        }

        qtdDisponivel = this.qtdVenda.subtract(qtdDevolvida);
        return qtdDisponivel.compareTo(qtdDevolucao) >= 0;
    }

    public void addItemDevolucao(BigDecimal qtdDevolucao, BigDecimal valorDevolucao) {
        if (this.qtdDevolvida == null) {
            this.qtdDevolvida = BigDecimal.ZERO;
        }
        this.qtdDevolvida = this.qtdDevolvida.add(qtdDevolucao);
        this.valorDevolucao = this.valorDevolucao.add(valorDevolucao);
        this.ajustaValoresDevolucao();
        this.defineLucroObtido();
    }    

    public BigDecimal retornaCusto() {
        if (custoReposicao.compareTo(BigDecimal.ZERO) == 0) {
            return new BigDecimal(0.01);
        }

        return custoReposicao;
    }
    
}
