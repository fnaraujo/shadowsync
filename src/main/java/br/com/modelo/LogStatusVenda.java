package br.com.modelo;

import com.towel.el.annotation.Resolvable;
import java.util.Date;
import java.util.List;
import javax.persistence.*;

/**
 *
 * @author Flavio Nunes
 */
@Entity
@Table(name = "log_status_venda", schema = "public")
@SequenceGenerator(name = "SEQUENCIA",
sequenceName = "sq_log_status_venda",
allocationSize = 1)
public class LogStatusVenda implements java.io.Serializable {

    private long idLogStatusVenda;
    private Venda venda;
    private StatusVenda statusVenda;
    private Funcionario funcionario;
    private Date dataCadastro;

    public LogStatusVenda() {
    }

    public LogStatusVenda(long idLogStatusVenda, Venda venda, StatusVenda statusVenda, Funcionario funcionario, Date dataCadastro) {
        this.idLogStatusVenda = idLogStatusVenda;
        this.venda = venda;
        this.statusVenda = statusVenda;
        this.funcionario = funcionario;
        this.dataCadastro = dataCadastro;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQUENCIA")
    @Column(name = "id_log_status_venda", unique = true, nullable = false)
    public long getIdLogStatusVenda() {
        return this.idLogStatusVenda;
    }

    public void setIdLogStatusVenda(long idLogStatusVenda) {
        this.idLogStatusVenda = idLogStatusVenda;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_venda", nullable = false)
    public Venda getVenda() {
        return venda;
    }

    public void setVenda(Venda venda) {
        this.venda = venda;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_status_venda", nullable = false)
    public StatusVenda getStatusVenda() {
        return statusVenda;
    }

    public void setStatusVenda(StatusVenda statusVenda) {
        this.statusVenda = statusVenda;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_funcionario", nullable = false)
    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_cadastro", length = 29, insertable = false, updatable = false)
    public Date getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

}
