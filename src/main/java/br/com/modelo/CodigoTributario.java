package br.com.modelo;

import com.towel.el.annotation.Resolvable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;


@Entity
@Table(name="codigo_tributario"
    ,schema="public"
    , uniqueConstraints = @UniqueConstraint(columnNames="descricao") 
)
@SequenceGenerator(name = "SEQUENCIA",
    sequenceName = "sq_codigo_tributario",
    allocationSize = 1)
public class CodigoTributario  implements java.io.Serializable {


     private long idCodigoTributario;
     @Resolvable(colName = "codigo")
     private int codigo;
     @Resolvable(colName = "descricao")
     private String descricao;
     private BigDecimal baseIcmVenda;
     private BigDecimal baseIcmCompra;     
     private String situacaoTributaria;
     private char origem;
     private String codigoEcf;
     private BigDecimal percAproveitamentoSn;
     private Set produtos = new HashSet(0);

    public CodigoTributario() {
    }

	
    public CodigoTributario(long idCodigoTributario, int codigo, String descricao, BigDecimal baseIcmVenda, BigDecimal baseIcmCompra, String situacaoTributaria, char origem, String codigoEcf) {
        this.idCodigoTributario = idCodigoTributario;
        this.codigo = codigo;
        this.descricao = descricao;
        this.baseIcmVenda = baseIcmVenda;
        this.baseIcmCompra = baseIcmCompra;
        this.situacaoTributaria = situacaoTributaria;
        this.origem = origem;
        this.codigoEcf = codigoEcf;
    }
    public CodigoTributario(long idCodigoTributario, int codigo, String descricao, BigDecimal baseIcmVenda, BigDecimal baseIcmCompra, String situacaoTributaria, char origem, String codigoEcf, Set produtos) {
       this.idCodigoTributario = idCodigoTributario;
       this.codigo = codigo;
       this.descricao = descricao;
       this.baseIcmVenda = baseIcmVenda;
       this.baseIcmCompra = baseIcmCompra;       
       this.situacaoTributaria = situacaoTributaria;
       this.origem = origem;
       this.codigoEcf = codigoEcf;
       this.produtos = produtos;
    }
   
    @Id 
    @GeneratedValue(strategy=GenerationType.AUTO, generator="SEQUENCIA")
    @Column(name="id_codigo_tributario", unique=true, nullable=false)
    public long getIdCodigoTributario() {
        return this.idCodigoTributario;
    }
    
    public void setIdCodigoTributario(long idCodigoTributario) {
        this.idCodigoTributario = idCodigoTributario;
    }
    
    @Column(name="codigo", nullable=false)
    public int getCodigo() {
        return this.codigo;
    }
    
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }
    
    @Column(name="descricao", unique=true, nullable=false, length=40)
    public String getDescricao() {
        return this.descricao;
    }
    
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    
    @Column(name="base_icm_venda", nullable=false, precision=5)
    public BigDecimal getBaseIcmVenda() {
        return this.baseIcmVenda;
    }
    
    public void setBaseIcmVenda(BigDecimal baseIcmVenda) {
        this.baseIcmVenda = baseIcmVenda;
    }
    
    @Column(name="base_icm_compra", nullable=false, precision=5)
    public BigDecimal getBaseIcmCompra() {
        return this.baseIcmCompra;
    }
    
    public void setBaseIcmCompra(BigDecimal baseIcmCompra) {
        this.baseIcmCompra = baseIcmCompra;
    }

    @Column(name="situacao_tributaria", nullable=false, length=3)
    public String getSituacaoTributaria() {
        return this.situacaoTributaria;
    }

    public void setSituacaoTributaria(String situacaoTributaria) {
        this.situacaoTributaria = situacaoTributaria;
    }
    
    
    @Column(name="origem", nullable=false, length=1)
    public char getOrigem() {
        return this.origem;
    }
    
    public void setOrigem(char origem) {
        this.origem = origem;
    }
    
    @Column(name="codigo_ecf", nullable=false, length=2)
    public String getCodigoEcf() {
        return this.codigoEcf;
    }
    
    public void setCodigoEcf(String codigoEcf) {
        this.codigoEcf = codigoEcf;
    }

    @Column(name="perc_aprov_sn")
    public BigDecimal getPercAproveitamentoSn() {
        return percAproveitamentoSn;
    }

    public void setPercAproveitamentoSn(BigDecimal percAproveitamentoSn) {
        this.percAproveitamentoSn = percAproveitamentoSn;
    }    
    
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="codigoTributario", targetEntity=Produto.class)
    public Set getProdutos() {
        return this.produtos;
    }
    
    public void setProdutos(Set produtos) {
        this.produtos = produtos;
    }

    public String retornaNome(int i) {
        if(this.descricao.length()<=i){
            return this.descricao;            
        }else{
            return this.descricao.substring(0, i-1); 
        }
    }
}


