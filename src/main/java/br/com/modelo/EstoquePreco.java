package br.com.modelo;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "estoque_preco", schema = "public")
@SequenceGenerator(name = "SEQUENCIA",
sequenceName = "sq_estoque_preco",
allocationSize = 1)
public class EstoquePreco implements java.io.Serializable, Comparable {

    public static final String NOME_TABELA = "estoque_preco";
    private long idEstoquePreco;
    private TabelaPreco tabelaPreco;
    private EstoqueProduto estoqueProduto;
    private BigDecimal valorVenda;
    private Date atualizacaoPreco;
    private String hash;
    private boolean precoPadrao;
    private BigDecimal valorDesconto;
    private Date dataLimiteDesconto;
    private int numOrdem;
    private String formasPgtoPromocao;
    private BigDecimal percentualPromocao = BigDecimal.ZERO;
    private Funcionario funcionario;

    public EstoquePreco() {
    }

    public EstoquePreco(EstoqueProduto estoqueProduto) {
        this.estoqueProduto = estoqueProduto;
    }

    public EstoquePreco(long idEstoquePreco, TabelaPreco tabelaPreco, EstoqueProduto estoqueProduto, BigDecimal valorVenda) {
        this.idEstoquePreco = idEstoquePreco;
        this.tabelaPreco = tabelaPreco;
        this.estoqueProduto = estoqueProduto;
        this.valorVenda = valorVenda;
    }

    public EstoquePreco(long idEstoquePreco, TabelaPreco tabelaPreco, EstoqueProduto estoqueProduto, BigDecimal valorVenda, int numOrdem) {
        this.idEstoquePreco = idEstoquePreco;
        this.tabelaPreco = tabelaPreco;
        this.estoqueProduto = estoqueProduto;
        this.valorVenda = valorVenda;
        this.numOrdem = numOrdem;
    }

    public EstoquePreco(long idEstoquePreco, TabelaPreco tabelaPreco, EstoqueProduto estoqueProduto, BigDecimal valorVenda, Date atualizacaoPreco, boolean precoPadrao, int numOrdem) {
        this.idEstoquePreco = idEstoquePreco;
        this.tabelaPreco = tabelaPreco;
        this.estoqueProduto = estoqueProduto;
        this.valorVenda = valorVenda;
        this.atualizacaoPreco = atualizacaoPreco;
        this.precoPadrao = precoPadrao;
        this.numOrdem = numOrdem;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQUENCIA")
    @Column(name = "id_estoque_preco", unique = true, nullable = false)
    public long getIdEstoquePreco() {
        return this.idEstoquePreco;
    }

    public void setIdEstoquePreco(long idEstoquePreco) {
        this.idEstoquePreco = idEstoquePreco;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_tabela_preco", nullable = false)
    public TabelaPreco getTabelaPreco() {
        return this.tabelaPreco;
    }

    public void setTabelaPreco(TabelaPreco tabelaPreco) {
        this.tabelaPreco = tabelaPreco;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_estoque", nullable = false)
    public EstoqueProduto getEstoqueProduto() {
        return this.estoqueProduto;
    }

    public void setEstoqueProduto(EstoqueProduto estoqueProduto) {
        this.estoqueProduto = estoqueProduto;
    }

    @Column(name = "valor_venda", nullable = false, precision = 15)
    public BigDecimal getValorVenda() {
        return this.valorVenda;
    }

    public void setValorVenda(BigDecimal valorVenda) {
        this.valorVenda = valorVenda;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "atualizacao_preco", length = 29)
    public Date getAtualizacaoPreco() {
        return this.atualizacaoPreco;
    }

    public void setAtualizacaoPreco(Date atualizacaoPreco) {
        this.atualizacaoPreco = atualizacaoPreco;
    }

    @Column(name = "hash", nullable = true)
    public String getHash() {
        return this.hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    @Column(name = "preco_padrao")
    public boolean getPrecoPadrao() {
        return this.precoPadrao;
    }

    public void setPrecoPadrao(boolean precoPadrao) {
        this.precoPadrao = precoPadrao;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_limite_desconto", length = 29)
    public Date getDataLimiteDesconto() {
        return dataLimiteDesconto;
    }

    public void setDataLimiteDesconto(Date dataLimiteDesconto) {
        this.dataLimiteDesconto = dataLimiteDesconto;
    }

    @Column(name = "valor_desconto", nullable = true, precision = 15)
    public BigDecimal getValorDesconto() {
        return valorDesconto;
    }

    public void setValorDesconto(BigDecimal valorDesconto) {
        this.valorDesconto = valorDesconto;
    }

    @Column(name = "formas_pgto_promocao")
    public String getFormasPgtoPromocao() {
        return formasPgtoPromocao;
    }

    public void setFormasPgtoPromocao(String formasPgtoPromocao) {
        this.formasPgtoPromocao = formasPgtoPromocao;
    }

    @Column(name = "num_ordem")
    public int getNumOrdem() {
        return numOrdem;
    }

    public void setNumOrdem(int numOrdem) {
        this.numOrdem = numOrdem;
    }

    @Column(name = "percentual_promocao")
    public BigDecimal getPercentualPromocao() {
        return percentualPromocao;
    }

    public void setPercentualPromocao(BigDecimal percentualPromocao) {
        this.percentualPromocao = percentualPromocao;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_funcionario")
    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    @Override
    public int compareTo(Object o) {
        int resp = 0;
        EstoquePreco preco = (EstoquePreco) o;
        if (this.numOrdem == preco.numOrdem) {
            resp = 0;
        } else if (this.numOrdem < preco.numOrdem) {
            resp = -1;
        } else if (this.numOrdem > preco.numOrdem) {
            resp = 1;
        }
        return resp;
    }

    public float retornaIndicePromocao() {
        if (this.valorDesconto != null) {
            return this.valorDesconto.floatValue() / this.valorVenda.floatValue();
        } else {
            return 0;
        }
    }

    public void aplicaPercentualPromocao() {

        if (percentualPromocao == null
                || percentualPromocao.compareTo(BigDecimal.ZERO) <= 0) {
            return;
        }

        valorDesconto = valorVenda
                .subtract(percentualPromocao
                .multiply(valorVenda
                .divide(new BigDecimal("100")).setScale(4, RoundingMode.UP)));
    }
}
