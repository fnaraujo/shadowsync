package br.com.modelo;


import com.towel.el.annotation.Resolvable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="unidade_federacao"
    ,schema="public"
)
public class UnidadeFederacao  implements java.io.Serializable {


     private int idUnidadeFederacao;
     @Resolvable(colName = "sigla")
     private String sigla;
     private String descricao;

    public UnidadeFederacao() {
    }

	
    public UnidadeFederacao(int idUnidadeFederacao, String sigla, String descricao) {
        this.idUnidadeFederacao = idUnidadeFederacao;
        this.sigla = sigla;
        this.descricao = descricao;
    }
    
   
    @Id     
    @Column(name="id_unidade_federacao", unique=true, nullable=false)
    public int getIdUnidadeFederacao() {
        return this.idUnidadeFederacao;
    }
    
    public void setIdUnidadeFederacao(int idUnidadeFederacao) {
        this.idUnidadeFederacao = idUnidadeFederacao;
    }
    
    @Column(name="sigla", nullable=false, length=2)
    public String getSigla() {
        return this.sigla;
    }
    
    public void setSigla(String sigla) {
        this.sigla = sigla;
    }
    
    @Column(name="descricao", nullable=false, length=40)
    public String getDescricao() {
        return this.descricao;
    }
    
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

}


