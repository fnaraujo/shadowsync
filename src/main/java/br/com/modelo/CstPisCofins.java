package br.com.modelo;

import com.towel.el.annotation.Resolvable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Flávio Augusto
 */
@Entity
@Table(name="cst_pis_cofins"
    ,schema="public"
)
@SequenceGenerator(name = "SEQUENCIA",
    sequenceName = "sq_cst_pis_cofins",
    allocationSize = 1)
public class CstPisCofins implements java.io.Serializable {

    private int idCstPisCofins;
    @Resolvable(colName = "codigo")
    private String codigo;
    @Resolvable(colName = "descricao")
    private String descricao;
    @Resolvable(colName = "baseCalculo")
    private float baseCalculo;
    @Resolvable(colName = "aliquotaPis")
    private float aliquotaPis;
    @Resolvable(colName = "aliquotaCofins")
    private float aliquotaCofins;

    public CstPisCofins() {
    }

    public CstPisCofins(int idCstPisCofins, String codigo, String descricao) {
        this.idCstPisCofins = idCstPisCofins;
        this.codigo = codigo;
        this.descricao = descricao;
    }

    public CstPisCofins(int idCstPisCofins, String codigo, String descricao, float baseCalculo, float aliquotaPis, float aliquotaCofins) {
        this.idCstPisCofins = idCstPisCofins;
        this.codigo = codigo;
        this.descricao = descricao;
        this.baseCalculo = baseCalculo;
        this.aliquotaPis = aliquotaPis;
        this.aliquotaCofins = aliquotaCofins;
    }


    @Id
    @GeneratedValue(strategy=GenerationType.AUTO, generator="SEQUENCIA")
    @Column(name="id_cst_pis_cofins", unique=true, nullable=false)
    public int getIdCstPisCofins() {
        return idCstPisCofins;
    }

    public void setIdCstPisCofins(int idCstPisCofins) {
        this.idCstPisCofins = idCstPisCofins;
    }

    @Column(name="aliquota_pis", nullable=false, precision=8, scale=8)
    public float getAliquotaPis() {
        return aliquotaPis;
    }

    public void setAliquotaPis(float aliquotaPis) {
        this.aliquotaPis = aliquotaPis;
    }

    @Column(name="aliquota_cofins", nullable=false, precision=8, scale=8)
    public float getAliquotaCofins() {
        return aliquotaCofins;
    }

    public void setAliquotaCofins(float aliquotaCofins) {
        this.aliquotaCofins = aliquotaCofins;
    }

    @Column(name="base_calculo", nullable=false, precision=8, scale=8)
    public float getBaseCalculo() {
        return baseCalculo;
    }

    public void setBaseCalculo(float baseCalculo) {
        this.baseCalculo = baseCalculo;
    }

    @Column(name="codigo", nullable=false, length=3)
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @Column(name="descricao", nullable=false, length=300)
    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

}
