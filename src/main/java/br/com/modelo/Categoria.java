package br.com.modelo;

import com.towel.el.annotation.Resolvable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="categoria"
    ,schema="public"
)
@SequenceGenerator(name = "SEQUENCIA",
    sequenceName = "sq_categoria",
    allocationSize = 1)
public class Categoria  implements java.io.Serializable {


     private long idCategoria;
     private int codigo;
     @Resolvable(colName = "descricao")
     private String descricao;

    public Categoria() {
    }

    public Categoria(long idCategoria, int codigo, String descricao) {
       this.idCategoria = idCategoria;
       this.codigo = codigo;
       this.descricao = descricao;
    }
   
    @Id 
    @GeneratedValue(strategy=GenerationType.AUTO, generator="SEQUENCIA")
    @Column(name="id_categoria", unique=true, nullable=false)
    public long getIdCategoria() {
        return this.idCategoria;
    }
    
    public void setIdCategoria(long idCategoria) {
        this.idCategoria = idCategoria;
    }
    
    @Column(name="codigo", nullable=false)
    public int getCodigo() {
        return this.codigo;
    }
    
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }
    
    @Column(name="descricao", nullable=false, length=40)
    public String getDescricao() {
        return this.descricao;
    }
    
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}


