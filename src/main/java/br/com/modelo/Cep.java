package br.com.modelo;

import com.towel.el.annotation.Resolvable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "cep", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "numero")
)
@SequenceGenerator(name = "SEQUENCIA",
        sequenceName = "sq_cep",
        allocationSize = 1)
public class Cep implements java.io.Serializable, Cloneable {

    private long idCep;
    private Bairro bairro;
    @org.hibernate.annotations.Index(name = "cep_codigo_idx")
    private String numero;
    @Resolvable(colName = "logradouro")
    private String logradouro;
    @Resolvable(colName = "logradouroAbrev")
    private String logradouroAbrev;
    @Resolvable(colName = "tipo")
    private String tipo;

    public Cep() {
    }

    public Cep(long idCep, String numero) {
        this.idCep = idCep;
        this.numero = numero;
    }

    public Cep(long idCep, Bairro bairro, String numero, String logradouro, String logradouroAbrev, String tipo) {
        this.idCep = idCep;
        this.bairro = bairro;
        this.numero = numero;
        this.logradouro = logradouro;
        this.logradouroAbrev = logradouroAbrev;
        this.tipo = tipo;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQUENCIA")
    @Column(name = "id_cep", unique = true, nullable = false)
    public long getIdCep() {
        return this.idCep;
    }

    public void setIdCep(long idCep) {
        this.idCep = idCep;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_bairro", nullable = false)
    public Bairro getBairro() {
        return this.bairro;
    }

    public void setBairro(Bairro bairro) {
        this.bairro = bairro;
    }

    @Column(name = "numero", unique = true, nullable = false)
    public String getNumero() {
        return this.numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    @Column(name = "logradouro", nullable = false, length = 80)
    public String getLogradouro() {
        return this.logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    @Column(name = "logradouro_abrev", nullable = false, length = 80)
    public String getLogradouroAbrev() {
        return this.logradouroAbrev;
    }

    public void setLogradouroAbrev(String logradouroAbrev) {
        this.logradouroAbrev = logradouroAbrev;
    }

    @Column(name = "tipo", nullable = false, length = 20)
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Override
    public Cep clone() throws CloneNotSupportedException {
        Cep c = (Cep) super.clone();
        c.setIdCep(0);
        return c;
    }
}
