package br.com.modelo;


import com.towel.el.annotation.Resolvable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="ncm"
    ,schema="public"
)
@SequenceGenerator(name = "SEQUENCIA",
    sequenceName = "sq_ncm",
    allocationSize = 1)
public class Ncm  implements java.io.Serializable {


     private int idNcm;
     @Resolvable(colName = "codigo")
     private String codigo;
     @Resolvable(colName = "descricao")
     private String descricao;
     private float mvaOpInt;
     private float mvaOpExt;
     private CstPisCofins cstPisCofins;
     private CstIPI cstIPI;
     @Resolvable(colName = "protocolo")
     private String protocolo;

    public Ncm() {
    }

	
    public Ncm(int idNcm, String codigo, String descricao, float mvaOpInt, float mvaOpExt) {
        this.idNcm = idNcm;
        this.codigo = codigo;
        this.descricao = descricao;
        this.mvaOpInt = mvaOpInt;
        this.mvaOpExt = mvaOpExt;
    }    

    public Ncm(int idNcm, String codigo, String descricao, float mvaOpInt, float mvaOpExt, CstPisCofins cstPis) {
       this.idNcm = idNcm;
       this.codigo = codigo;
       this.descricao = descricao;
       this.mvaOpInt = mvaOpInt;
       this.mvaOpExt = mvaOpExt;
       this.cstPisCofins = cstPis;
    }

    public Ncm(int idNcm, String codigo, String descricao, float mvaOpInt, float mvaOpExt, CstPisCofins cstPis, CstIPI cstIpi) {
       this.idNcm = idNcm;
       this.codigo = codigo;
       this.descricao = descricao;
       this.mvaOpInt = mvaOpInt;
       this.mvaOpExt = mvaOpExt;
       this.cstPisCofins = cstPis;
       this.cstIPI = cstIpi;
    }

    public Ncm(int idNcm, String codigo, String descricao, float mvaOpInt, float mvaOpExt, CstPisCofins cstPis, CstIPI cstIpi, String protocolo) {
       this.idNcm = idNcm;
       this.codigo = codigo;
       this.descricao = descricao;
       this.mvaOpInt = mvaOpInt;
       this.mvaOpExt = mvaOpExt;
       this.cstPisCofins = cstPis;
       this.cstIPI = cstIpi;
       this.protocolo = protocolo;
    }
   
    @Id 
    @GeneratedValue(strategy=GenerationType.AUTO,generator="SEQUENCIA")
    @Column(name="id_ncm", unique=true, nullable=false)
    public int getIdNcm() {
        return this.idNcm;
    }
    
    public void setIdNcm(int idNcm) {
        this.idNcm = idNcm;
    }
    
    @Column(name="codigo", nullable=false, length=8)
    public String getCodigo() {
        return this.codigo;
    }
    
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
    
    @Column(name="descricao", nullable=true, length=200)
    public String getDescricao() {
        return this.descricao;
    }
    
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    
    @Column(name="mva_op_int", nullable=false, precision=8, scale=8)
    public float getMvaOpInt() {
        return this.mvaOpInt;
    }
    
    public void setMvaOpInt(float mvaOpInt) {
        this.mvaOpInt = mvaOpInt;
    }
    
    @Column(name="mva_op_ext", nullable=false, precision=8, scale=8)
    public float getMvaOpExt() {
        return this.mvaOpExt;
    }
    
    public void setMvaOpExt(float mvaOpExt) {
        this.mvaOpExt = mvaOpExt;
    }

@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="id_cst_pis_cofins", nullable=false,insertable=true, updatable=true)
    public CstPisCofins getCstPisCofins() {
        return this.cstPisCofins;
    }
    
    public void setCstPisCofins(CstPisCofins cstPisCofins) {
        this.cstPisCofins = cstPisCofins;
    }

@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="id_cst_ipi", nullable=true,insertable=true, updatable=true)
    public CstIPI getCstIpi() {
        return this.cstIPI;
    }

    public void setCstIpi(CstIPI cstIpi) {
        this.cstIPI = cstIpi;
    }

    @Column(name="protocolo", nullable=true, precision=8, scale=8)
    public String getProtocolo() {
        return this.protocolo;
    }

    public void setProtocolo(String protocolo) {
        this.protocolo = protocolo;
    }

}


