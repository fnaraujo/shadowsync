package br.com.modelo;


import com.towel.el.annotation.Resolvable;
import javax.persistence.*;

@Entity
@Table(name="loja_departamento"
    ,schema="public"
)
@SequenceGenerator(name = "SEQUENCIA",
    sequenceName = "sq_loja_departamento",
    allocationSize = 1)
public class LojaDepartamento  implements java.io.Serializable {


     private int idLojaDepartamento;
     @Resolvable(colName = "codigo")
     private int codigo;
     @Resolvable(colName = "nome")
     private String nome;
     private Loja loja;     
     
    public LojaDepartamento() {
    }

	
    public LojaDepartamento(int idLojaDepartamento, String nome) {
        this.idLojaDepartamento = idLojaDepartamento;
        this.nome = nome;
    }

    public LojaDepartamento(int idLojaDepartamento,int codigo, String nome, Loja loja) {
        this.idLojaDepartamento = idLojaDepartamento;
        this.codigo = codigo;
        this.nome = nome;
        this.loja = loja;        
    }

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO,generator="SEQUENCIA")
    @Column(name="id_loja_departamento", unique=true, nullable=false)
    public int getIdLojaDepartamento() {
        return this.idLojaDepartamento;
    }
    
    public void setIdLojaDepartamento(int idLojaDepartamento) {
        this.idLojaDepartamento = idLojaDepartamento;
    }

    @Column(name="codigo", nullable=false)
    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }
    
    @Column(name="nome", nullable=false)
    public String getNome() {
        return this.nome;
    }
    
    public void setNome(String nome) {
        this.nome = nome;
    }
    
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="id_loja", nullable=false,insertable=true, updatable=true)
    public Loja getLoja() {
        return loja;
    }

    public void setLoja(Loja loja) {
        this.loja = loja;
    } 
}


