package br.com.modelo;

import com.towel.el.annotation.Resolvable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "produto", schema = "public", uniqueConstraints =
@UniqueConstraint(columnNames = "codigo"))
@SequenceGenerator(name = "SEQUENCIA",
sequenceName = "sq_produto",
allocationSize = 1)
public class Produto implements java.io.Serializable {

    public static final String NOME_TABELA = "produto";
    private long idProduto;
    private CodigoTributario codigoTributario;
    private Grupo grupoByIdSubgrupo;
    private Ncm ncm;
    private UnidadeMedida unidadeMedida;
    private Grupo grupoByIdGrupo;
    private Fabricante fabricante;
    @Resolvable(colName = "codigo")
    @org.hibernate.annotations.Index(name = "produto_codigo_idx")
    private String codigo;
    @Resolvable(colName = "descricao")
    private String descricao;
    @Resolvable(colName = "aplicacao")
    private String aplicacao;
    @Resolvable(colName = "qtdEmbalagem")
    private float qtdEmbalagem;
    @Resolvable(colName = "peso")
    private float peso;
    private Boolean emissaoEtiqueta;
    private Boolean ativo;
    @Resolvable(colName = "ean")
    private String ean;
    private Date dataCadastro;
    private Date dataAlterado;
    private int origem;
    private String hash;
    private List relacoes = new ArrayList(0);
    private long codigoSimilaridade;
    @Resolvable(colName = "descricaoAplicacao")
    private String descricaoAplicacao;

    public Produto() {
    }

    public Produto(long idProduto) {
        this.idProduto = idProduto;
    }

    public Produto(long idProduto, String descricao) {
        this.idProduto = idProduto;
        this.descricao = descricao;
    }

    public Produto(long idProduto, CodigoTributario codigoTributario, Grupo grupoByIdSubgrupo, Grupo grupoByIdGrupo, Fabricante fabricante, String codigo, String descricao, String aplicacao, float qtdEmbalagem, float peso, Date dataCadastro) {
        this.idProduto = idProduto;
        this.codigoTributario = codigoTributario;
        this.grupoByIdSubgrupo = grupoByIdSubgrupo;
        this.grupoByIdGrupo = grupoByIdGrupo;
        this.fabricante = fabricante;
        this.codigo = codigo;
        this.descricao = descricao;
        this.aplicacao = aplicacao;
        this.qtdEmbalagem = qtdEmbalagem;
        this.peso = peso;
        this.dataCadastro = dataCadastro;
        this.descricaoAplicacao = this.descricao + this.aplicacao;
    }

    public Produto(long idProduto, CodigoTributario codigoTributario, Grupo grupoByIdSubgrupo, Ncm ncm, UnidadeMedida unidadeMedida, Grupo grupoByIdGrupo, Fabricante fabricante, String codigo, String descricao, String aplicacao, float qtdEmbalagem, float peso, Boolean emissaoEtiqueta, Boolean ativo, String ean, Date dataCadastro, Date dataAlterado, int origem) {
        this.idProduto = idProduto;
        this.codigoTributario = codigoTributario;
        this.grupoByIdSubgrupo = grupoByIdSubgrupo;
        this.ncm = ncm;
        this.unidadeMedida = unidadeMedida;
        this.grupoByIdGrupo = grupoByIdGrupo;
        this.fabricante = fabricante;
        this.codigo = codigo;
        this.descricao = descricao;
        this.aplicacao = aplicacao;
        this.qtdEmbalagem = qtdEmbalagem;
        this.peso = peso;
        this.emissaoEtiqueta = emissaoEtiqueta;
        this.ativo = ativo;
        this.ean = ean;
        this.dataCadastro = dataCadastro;
        this.dataAlterado = dataAlterado;
        this.origem = origem;
        this.descricaoAplicacao = this.descricao + this.aplicacao;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQUENCIA")
    @Column(name = "id_produto", unique = true, nullable = false)
    public long getIdProduto() {
        return this.idProduto;
    }

    public void setIdProduto(long idProduto) {
        this.idProduto = idProduto;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_codigo_tributario", nullable = false)
    public CodigoTributario getCodigoTributario() {
        return this.codigoTributario;
    }

    public void setCodigoTributario(CodigoTributario codigoTributario) {
        this.codigoTributario = codigoTributario;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_subgrupo", nullable = true)
    public Grupo getGrupoByIdSubgrupo() {
        return this.grupoByIdSubgrupo;
    }

    public void setGrupoByIdSubgrupo(Grupo grupoByIdSubgrupo) {
        this.grupoByIdSubgrupo = grupoByIdSubgrupo;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_ncm")
    public Ncm getNcm() {
        return this.ncm;
    }

    public void setNcm(Ncm ncm) {
        this.ncm = ncm;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_unidade_medida")
    public UnidadeMedida getUnidadeMedida() {
        return this.unidadeMedida;
    }

    public void setUnidadeMedida(UnidadeMedida unidadeMedida) {
        this.unidadeMedida = unidadeMedida;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_grupo", nullable = false)
    public Grupo getGrupoByIdGrupo() {
        return this.grupoByIdGrupo;
    }

    public void setGrupoByIdGrupo(Grupo grupoByIdGrupo) {
        this.grupoByIdGrupo = grupoByIdGrupo;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_fabricante", nullable = false)
    public Fabricante getFabricante() {
        return this.fabricante;
    }

    public void setFabricante(Fabricante fabricante) {
        this.fabricante = fabricante;
    }

    @Column(name = "codigo", nullable = false, length = 20)
    public String getCodigo() {
        return this.codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @Column(name = "descricao", nullable = false, length = 40)
    public String getDescricao() {
        return this.descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Column(name = "aplicacao", nullable = false, length = 1000)
    public String getAplicacao() {
        return this.aplicacao;
    }

    public void setAplicacao(String aplicacao) {
        this.aplicacao = aplicacao;
    }

    @Column(name = "qtd_embalagem", nullable = false, precision = 8, scale = 8)
    public float getQtdEmbalagem() {
        return this.qtdEmbalagem;
    }

    public void setQtdEmbalagem(float qtdEmbalagem) {
        this.qtdEmbalagem = qtdEmbalagem;
    }

    @Column(name = "peso", nullable = false, precision = 8, scale = 8)
    public float getPeso() {
        return this.peso;
    }

    public void setPeso(float peso) {
        this.peso = peso;
    }

    @Column(name = "emissao_etiqueta")
    public Boolean getEmissaoEtiqueta() {
        return this.emissaoEtiqueta;
    }

    public void setEmissaoEtiqueta(Boolean emissaoEtiqueta) {
        this.emissaoEtiqueta = emissaoEtiqueta;
    }

    @Column(name = "ativo")
    public Boolean getAtivo() {
        return this.ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    @Column(name = "ean", length = 15)
    public String getEan() {
        return this.ean;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_cadastro", nullable = false, length = 29)
    public Date getDataCadastro() {
        return this.dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_alterado", length = 29)
    public Date getDataAlterado() {
        return this.dataAlterado;
    }

    public void setDataAlterado(Date dataAlterado) {
        this.dataAlterado = dataAlterado;
    }

    @Column(name = "origem")
    public int getOrigem() {
        return origem;
    }

    public void setOrigem(int origem) {
        this.origem = origem;
    }

    @Column(name = "hash")
    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "produto", targetEntity = CodigoRelacao.class)
    public List<CodigoRelacao> getRelacoes() {
        return this.relacoes;
    }

    public void setRelacoes(List<CodigoRelacao> relacoes) {
        this.relacoes = relacoes;
    }

    @Column(name = "codigo_similaridade")
    public long getCodigoSimilaridade() {
        return codigoSimilaridade;
    }

    public void setCodigoSimilaridade(long codigoSimilaridade) {
        this.codigoSimilaridade = codigoSimilaridade;
    }

    public boolean camposObrigatoriosValidos() {
        boolean resp = true;

        if ((codigo.isEmpty()) || (descricao.isEmpty())
                || (codigoTributario == null) || (fabricante == null)
                || (grupoByIdGrupo == null) || (unidadeMedida == null)) {
            resp = false;
        }

        return resp;
    }

    public String retornaNome(int i) {
        String nome = this.descricao + " " + this.aplicacao;
        if (nome.length() <= i) {
            return nome;
        } else {
            return nome.substring(0, i - 1);
        }
    }

    public boolean verificaGrupoConsumo() {
        return this.getGrupoByIdGrupo().getDescricao().contains("CONSUMO") ||
               this.getGrupoByIdGrupo().getDescricao().contains("MAO") ||
               this.getGrupoByIdGrupo().getDescricao().contains("MAO OBRA") ||
               this.getGrupoByIdGrupo().getDescricao().contains("REVISAO") ||
               this.getGrupoByIdGrupo().getDescricao().contains("FILME");
    }
}
