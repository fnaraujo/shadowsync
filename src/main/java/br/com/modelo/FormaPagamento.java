package br.com.modelo;

import com.towel.el.annotation.Resolvable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "forma_pagamento", schema = "public")
@SequenceGenerator(name = "SEQUENCIA",
sequenceName = "sq_forma_pagamento",
allocationSize = 1)
public class FormaPagamento implements java.io.Serializable {

    public static final String NOME_TABELA = "forma_pagamento";
    private int idFormaPagamento;
    @Resolvable(colName = "descricao")
    private String descricao;
    @Resolvable(colName = "geraContasReceber")
    private Boolean geraContasReceber;
    @Resolvable(colName = "ativo")
    private Boolean ativo;
    @Resolvable(colName = "dataCadastro")
    private Date dataCadastro;
    private Date ultimaAlteracao;
    private Funcionario funcionario;
    private Boolean preFaturamento;
    private float limiteMinimo;
    private int tipoPagamento;
    private boolean creditoDevolucao;
    private boolean permiteDesconto;
    private BigDecimal maximoDesconto;
    private Long idGrupoContabil;
    private int precedencia;
    private List<FormaPagamentoPrazo> prazosPagamento = new ArrayList<>(0);

    public FormaPagamento() {
    }

    public FormaPagamento(int idFormaPagamento, String descricao) {
        this.idFormaPagamento = idFormaPagamento;
        this.descricao = descricao;
    }
    
    public FormaPagamento(int idFormaPagamento, String descricao, Boolean ativo, Date dataCadastro) {
        this.idFormaPagamento = idFormaPagamento;
        this.descricao = descricao;
        this.ativo = ativo;
        this.dataCadastro = dataCadastro;
    }

    public FormaPagamento(int idFormaPagamento, String descricao, Boolean geraContasReceber, Boolean ativo, Date dataCadastro, Funcionario funcionario) {
        this.idFormaPagamento = idFormaPagamento;
        this.descricao = descricao;
        this.geraContasReceber = geraContasReceber;
        this.ativo = ativo;
        this.dataCadastro = dataCadastro;
        this.funcionario = funcionario;
    }

    public FormaPagamento(int idFormaPagamento, String descricao, Boolean geraContasReceber, Boolean ativo, Date dataCadastro, Funcionario funcionario, Boolean preFaturamento, float limiteMinimo) {
        this.idFormaPagamento = idFormaPagamento;
        this.descricao = descricao;
        this.geraContasReceber = geraContasReceber;
        this.ativo = ativo;
        this.dataCadastro = dataCadastro;
        this.funcionario = funcionario;
        this.preFaturamento = preFaturamento;
        this.limiteMinimo = limiteMinimo;
    }

    public FormaPagamento(int idFormaPagamento, String descricao, Boolean geraContasReceber, Boolean ativo, Date dataCadastro, Funcionario funcionario, Boolean preFaturamento, float limiteMinimo, int tipoPagamento, Boolean permiteDesconto, BigDecimal maximoDesconto, Long idGrupoContabil) {
        this.idFormaPagamento = idFormaPagamento;
        this.descricao = descricao;
        this.geraContasReceber = geraContasReceber;
        this.ativo = ativo;
        this.dataCadastro = dataCadastro;
        this.funcionario = funcionario;
        this.preFaturamento = preFaturamento;
        this.limiteMinimo = limiteMinimo;
        this.tipoPagamento = tipoPagamento;
        this.permiteDesconto = permiteDesconto;
        this.maximoDesconto = maximoDesconto;
        this.idGrupoContabil = idGrupoContabil;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQUENCIA")
    @Column(name = "id_forma_pagamento", unique = true, nullable = false)
    public int getIdFormaPagamento() {
        return this.idFormaPagamento;
    }

    public void setIdFormaPagamento(int idFormaPagamento) {
        this.idFormaPagamento = idFormaPagamento;
    }

    @Column(name = "descricao", nullable = false, length = 40)
    public String getDescricao() {
        return this.descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Column(name = "gera_contas_receber")
    public Boolean getgeraContasReceber() {
        return this.geraContasReceber;
    }

    public void setgeraContasReceber(Boolean geraReceber) {
        this.geraContasReceber = geraReceber;
    }

    @Column(name = "credito_devolucao")
    public boolean isCreditoDevolucao() {
        return creditoDevolucao;
    }

    public void setCreditoDevolucao(boolean creditoDevolucao) {
        this.creditoDevolucao = creditoDevolucao;
    }

    @Column(name = "ativo")
    public Boolean getAtivo() {
        return this.ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_cadastro", nullable = false, length = 29)
    public Date getDataCadastro() {
        return this.dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ultima_alteracao", nullable = true, length = 29)
    public Date getUltimaAlteracao() {
        return ultimaAlteracao;
    }

    public void setUltimaAlteracao(Date ultimaAlteracao) {
        this.ultimaAlteracao = ultimaAlteracao;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_funcionario", nullable = false)
    public Funcionario getFuncionario() {
        return this.funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    @Column(name = "pre_faturamento")
    public Boolean getPreFaturamento() {
        return this.preFaturamento;
    }

    public void setPreFaturamento(Boolean preFaturamento) {
        this.preFaturamento = preFaturamento;
    }

    @Column(name = "limite_minimo", nullable = false, precision = 8, scale = 8)
    public float getLimiteMinimo() {
        return this.limiteMinimo;
    }

    public void setLimiteMinimo(float limiteMinimo) {
        this.limiteMinimo = limiteMinimo;
    }

    @Column(name = "tipo_pagamento")
    public int getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(int tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }

    @Column(name = "id_grupo_contabil", nullable = true)
    public Long getIdGrupoContabil() {
        return idGrupoContabil;
    }

    public void setIdGrupoContabil(Long idGrupoContabil) {
        this.idGrupoContabil = idGrupoContabil;
    }

    @Column(name = "maximo_desconto", nullable = true)
    public BigDecimal getMaximoDesconto() {
        return maximoDesconto;
    }

    public void setMaximoDesconto(BigDecimal maximoDesconto) {
        this.maximoDesconto = maximoDesconto;
    }

    @Column(name = "permite_desconto")
    public Boolean isPermiteDesconto() {
        return permiteDesconto;
    }

    public void setPermiteDesconto(Boolean permiteDesconto) {
        this.permiteDesconto = permiteDesconto;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "formaPagamento", targetEntity = FormaPagamentoPrazo.class)
    public List<FormaPagamentoPrazo> getPrazosPagamento() {
        return this.prazosPagamento;
    }

    public void setPrazosPagamento(List<FormaPagamentoPrazo> prazos) {
        this.prazosPagamento = prazos;
    }

    @Column(name = "precedencia")
    public int getPrecedencia() {
        return this.precedencia;
    }

    public void setPrecedencia(int precedencia) {
        this.precedencia = precedencia;
    }

    public boolean permiteClienteBloqueado() {
        return ((this.geraContasReceber == false) && (this.preFaturamento == false));
    }

    public boolean ultrapassou100PorCento() {

        BigDecimal porcentagem = BigDecimal.ZERO;
        for (FormaPagamentoPrazo p : this.prazosPagamento) {
            porcentagem = porcentagem.add(BigDecimal.valueOf(p.getPercentual()).setScale(2, RoundingMode.HALF_UP));
        }

        return porcentagem.compareTo(new BigDecimal(100)) > 0;
    }
}
