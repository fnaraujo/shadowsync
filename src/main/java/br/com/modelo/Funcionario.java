package br.com.modelo;

import com.towel.el.annotation.Resolvable;
import java.util.Date;
import javax.persistence.*;

@Entity
@Table(name = "funcionario", schema = "public", uniqueConstraints =
@UniqueConstraint(columnNames = "cpf"))
@SequenceGenerator(name = "SEQUENCIA",
sequenceName = "sq_funcionario",
allocationSize = 1)
public class Funcionario extends Base implements java.io.Serializable {

    @Resolvable(colName = "cpf")
    private String cpf;
    @Resolvable(colName = "nome")
    private String nome;
    private char sexo;
    private String identidade;
    private String orgExp;
    private Date dataEmissao;
    private String usuario;
    private String senha;
    private boolean ativo;
    @Resolvable(colName = "matricula")
    private String matricula;
    private Date dataCadastro;
    private Cargo cargo;
    private StatusFunc statusFunc;
    private float salarioBase;
    private Date dataDemissao;
    private Date ultimaAtualizacao;
    private Date dataNascimento;
    private Cep cep;
    private String numero;
    private String complemento;
    private String telefone;
    private String telefone2;
    private String email;
    private boolean recadastro;
    private boolean perfilTeste;
    private byte[] foto;
    private int osEspecializacao; //1-Os Normal 2- Os Residencia;
    private boolean todosDepartamentos;

    public Funcionario() {
    }

    public Funcionario(long idFuncionario, String cpf, String nome, char sexo) {
        super.id = idFuncionario;
        this.cpf = cpf;
        this.nome = nome;
        this.sexo = sexo;
    }

    public Funcionario(long idFuncionario, String cpf, String nome, char sexo, String identidade, String orgExp, Date dataEmissao, String usuario, String senha, Boolean ativo, String matricula, Date dataCadastro, Cargo cargo, StatusFunc statusFunc, Float salarioBase, Date dataDemissao, Date ultimaAtualizacao, Date dataNascimento, String endereco, String numero, String complemento, Cep cep, String telefone, String telefone2, String email, boolean recadastro, boolean perfilTeste) {
        super.id = idFuncionario;
        this.cpf = cpf;
        this.nome = nome;
        this.sexo = sexo;
        this.identidade = identidade;
        this.orgExp = orgExp;
        this.dataEmissao = dataEmissao;
        this.usuario = usuario;
        this.senha = senha;
        this.ativo = ativo;
        this.matricula = matricula;
        this.dataCadastro = dataCadastro;
        this.cargo = cargo;
        this.statusFunc = statusFunc;
        this.salarioBase = salarioBase;
        this.dataDemissao = dataDemissao;
        this.ultimaAtualizacao = ultimaAtualizacao;
        this.dataNascimento = dataNascimento;
        this.numero = numero;
        this.complemento = complemento;
        this.cep = cep;
        this.telefone = telefone;
        this.telefone2 = telefone2;
        this.email = email;
        this.recadastro = recadastro;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQUENCIA")
    @Column(name = "id_funcionario", unique = true, nullable = false)
    public long getIdFuncionario() {
        return super.id;
    }

    public void setIdFuncionario(long idFuncionario) {
        super.id = idFuncionario;
    }

    @Column(name = "cpf", unique = true, nullable = false, length = 11)
    public String getCpf() {
        return this.cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    @Column(name = "nome", nullable = false, length = 100)
    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Column(name = "sexo", nullable = false, length = 1)
    public char getSexo() {
        return this.sexo;
    }

    public void setSexo(char sexo) {
        this.sexo = sexo;
    }

    @Column(name = "identidade", length = 15)
    public String getIdentidade() {
        return this.identidade;
    }

    public void setIdentidade(String identidade) {
        this.identidade = identidade;
    }

    @Column(name = "org_exp", length = 10)
    public String getOrgExp() {
        return this.orgExp;
    }

    public void setOrgExp(String orgExp) {
        this.orgExp = orgExp;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_emissao", length = 29)
    public Date getDataEmissao() {
        return this.dataEmissao;
    }

    public void setDataEmissao(Date dataEmissao) {
        this.dataEmissao = dataEmissao;
    }

    @Column(name = "usuario", length = 30)
    public String getUsuario() {
        return this.usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    @Column(name = "senha", length = 100)
    public String getSenha() {
        return this.senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    @Column(name = "ativo")
    public Boolean getAtivo() {
        return this.ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    @Column(name = "matricula")
    public String getMatricula() {
        return this.matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    @Column(name = "data_cadastro")
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getDataCadastro() {
        return this.dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_cargo")
    public Cargo getCargo() {
        return this.cargo;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_status_funcionario", insertable = true, updatable = true)
    public StatusFunc getStatusFunc() {
        return this.statusFunc;
    }

    public void setStatusFunc(StatusFunc statusFunc) {
        this.statusFunc = statusFunc;
    }

    @Column(name = "salario_base")
    public float getSalarioBase() {
        return this.salarioBase;
    }

    public void setSalarioBase(float salarioBase) {
        this.salarioBase = salarioBase;
    }

    @Column(name = "data_demissao")
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getDataDemissao() {
        return this.dataDemissao;
    }

    public void setDataDemissao(Date dataDemissao) {
        this.dataDemissao = dataDemissao;
    }

    @Column(name = "ultima_atualizacao", insertable = false)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getUltimaAtualizacao() {
        return this.ultimaAtualizacao;
    }

    public void setUltimaAtualizacao(Date ultimaAtualizacao) {
        this.ultimaAtualizacao = ultimaAtualizacao;
    }

    @Column(name = "data_nascimento")
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getDataNascimento() {
        return this.dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    @Column(name = "numero")
    public String getNumero() {
        return this.numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    @Column(name = "complemento")
    public String getComplemento() {
        return this.complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_cep")
    public Cep getCep() {
        return this.cep;
    }

    public void setCep(Cep cep) {
        this.cep = cep;
    }

    @Column(name = "telefone")
    public String getTelefone() {
        return this.telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    @Column(name = "telefone2")
    public String getTelefone2() {
        return this.telefone2;
    }

    public void setTelefone2(String telefone2) {
        this.telefone2 = telefone2;
    }

    @Column(name = "email")
    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(name = "recadastro")
    public boolean getRecadastro() {
        return this.recadastro;
    }

    public void setRecadastro(boolean recadastro) {
        this.recadastro = recadastro;
    }

    @Column(name = "perfil_teste")
    public boolean getPerfilTeste() {
        return this.perfilTeste;
    }

    public void setPerfilTeste(boolean perfilTeste) {
        this.perfilTeste = perfilTeste;
    }

    @Column(name = "foto")
    public byte[] getFoto() {
        return this.foto;
    }

    public void setFoto(byte[] foto) {
        this.foto = foto;
    }


    @Column(name = "todos_departamentos")
    public boolean isTodosDepartamentos() {
        return todosDepartamentos;
    }

    public void setTodosDepartamentos(boolean todosDepartamentos) {
        this.todosDepartamentos = todosDepartamentos;
    }

    @Column(name = "os_especializacao")
    public int getOsEspecializacao() {
        return osEspecializacao;
    }

    public void setOsEspecializacao(int osEspecializacao) {
        this.osEspecializacao = osEspecializacao;
    }

    public String retornaNome(int length) {
        if (this.nome.length() <= length) {
            return this.nome;
        } else {
            return this.nome.substring(0, length);
        }
    }

    public boolean acessoLiberado() {
        if (this.usuario.equals("ADMIN")) {
            return true;
        } else {
            return this.statusFunc.getIdStatusFunc() != 4
                    && this.ativo;
        }
    }    
}
