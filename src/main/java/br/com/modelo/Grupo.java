package br.com.modelo;

import com.towel.el.annotation.Resolvable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "grupo", schema = "public", uniqueConstraints =
@UniqueConstraint(columnNames = "descricao"))
@SequenceGenerator(name = "SEQUENCIA",
sequenceName = "sq_grupo",
allocationSize = 1)
public class Grupo implements java.io.Serializable {

    private int idGrupo;
    @Resolvable(colName = "descricao")
    private String descricao;
    private Set produtosForIdGrupo = new HashSet(0);
    private Set produtosForIdSubgrupo = new HashSet(0);
    private BigDecimal lucroMinimoItem;

    public Grupo() {
    }

    public Grupo(int idGrupo, String descricao) {
        this.idGrupo = idGrupo;
        this.descricao = descricao;
    }
    
    public Grupo(int idGrupo, String descricao, BigDecimal lucroMinimoItem) {
        this.idGrupo = idGrupo;
        this.descricao = descricao;
        this.lucroMinimoItem = lucroMinimoItem;
    }

    public Grupo(int idGrupo, String descricao, Set produtosForIdGrupo, Set produtosForIdSubgrupo) {
        this.idGrupo = idGrupo;
        this.descricao = descricao;
        this.produtosForIdGrupo = produtosForIdGrupo;
        this.produtosForIdSubgrupo = produtosForIdSubgrupo;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQUENCIA")
    @Column(name = "id_grupo", unique = true, nullable = false)
    public int getIdGrupo() {
        return this.idGrupo;
    }

    public void setIdGrupo(int idGrupo) {
        this.idGrupo = idGrupo;
    }

    @Column(name = "descricao", unique = true, nullable = false, length = 40)
    public String getDescricao() {
        return this.descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "grupoByIdGrupo", targetEntity = Produto.class)
    public Set getProdutosForIdGrupo() {
        return this.produtosForIdGrupo;
    }

    public void setProdutosForIdGrupo(Set produtosForIdGrupo) {
        this.produtosForIdGrupo = produtosForIdGrupo;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "grupoByIdSubgrupo", targetEntity = Produto.class)
    public Set getProdutosForIdSubgrupo() {
        return this.produtosForIdSubgrupo;
    }

    public void setProdutosForIdSubgrupo(Set produtosForIdSubgrupo) {
        this.produtosForIdSubgrupo = produtosForIdSubgrupo;
    }

    @Column(name = "lucro_minimo_item")
    public BigDecimal getLucroMinimoItem() {
        return lucroMinimoItem;
    }

    public void setLucroMinimoItem(BigDecimal lucroMinimoItem) {
        this.lucroMinimoItem = lucroMinimoItem;
    }
}
