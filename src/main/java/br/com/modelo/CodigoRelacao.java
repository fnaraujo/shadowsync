package br.com.modelo;

import com.towel.el.annotation.Resolvable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author flavio.nunes
 */
@Entity
@Table(name="codigo_relacao"
    ,schema="public"
)
@SequenceGenerator(name = "SEQUENCIA",
        sequenceName = "sq_codigo_relacao",
        allocationSize = 1)
public class CodigoRelacao implements java.io.Serializable{

    private long idCodigoRelacao;
    private Produto produto;
    @Resolvable(colName = "codigo")
    @org.hibernate.annotations.Index(name = "relacao_codigo_idx")
    private String codigo;
    @Resolvable(colName = "observacao")
    private String observacao;
    private Date dataCadastro;
    private Cliente cliente;
    private Funcionario funcionario;
    @Resolvable(colName = "codPrincipal")
    private boolean codPrincipal;

    public CodigoRelacao() {
        
    }
    
    public CodigoRelacao(Produto produto, String codigo, String observacao, Date dataCadastro, Cliente cliente, Funcionario funcionario) {
        this.produto = produto;
        this.codigo = codigo;
        this.observacao = observacao;
        this.dataCadastro = dataCadastro;
        this.cliente = cliente;
        this.funcionario = funcionario;
    }
    
    public CodigoRelacao(Produto produto, String codigo, String observacao, Date dataCadastro, Cliente cliente, Funcionario funcionario, boolean codPrincipal) {
        this.produto = produto;
        this.codigo = codigo;
        this.observacao = observacao;
        this.dataCadastro = dataCadastro;
        this.cliente = cliente;
        this.funcionario = funcionario;
        this.codPrincipal = codPrincipal;
    }

    public CodigoRelacao(Produto produto, String codigo, String observacao, Date dataCadastro) {
        this.produto = produto;
        this.codigo = codigo;
        this.observacao = observacao;
        this.dataCadastro = dataCadastro;
    }

    public CodigoRelacao(long idCodigoRelacao, Produto produto, String codigo) {
        this.idCodigoRelacao = idCodigoRelacao;
        this.produto = produto;
        this.codigo = codigo;
    }

    public CodigoRelacao(long idCodigoRelacao, Produto produto, String codigo, Date dataCadastro) {
        this.idCodigoRelacao = idCodigoRelacao;
        this.produto = produto;
        this.codigo = codigo;
        this.dataCadastro = dataCadastro;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO, generator="SEQUENCIA")
    @Column(name="id_codigo_relacao", unique=true, nullable=false)
    public long getIdCodigoRelacao() {
        return idCodigoRelacao;
    }

    public void setIdCodigoRelacao(long idCodigoRelacao) {
        this.idCodigoRelacao = idCodigoRelacao;
    }

    @Column(name="codigo", nullable=false, length=20)
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @Column(name="observacao", nullable=false, length=100)
    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="data_cadastro", nullable=false, length=29)
    public Date getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }


@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="id_produto", nullable=false)
    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="id_cliente", nullable=false)
    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="id_funcionario", nullable=false)
    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    @Column(name="cod_principal")
    public boolean isCodPrincipal() {
        return codPrincipal;
    }

    public void setCodPrincipal(boolean codPrincipal) {
        this.codPrincipal = codPrincipal;
    }
    
}
