package br.com.modelo;

import com.towel.el.annotation.Resolvable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="bairro"
    ,schema="public"
    , uniqueConstraints = @UniqueConstraint(columnNames={"id_municipio", "nome"}) 
)
@SequenceGenerator(name = "SEQUENCIA",
    sequenceName = "sq_bairro",
    allocationSize = 1)
public class Bairro  implements java.io.Serializable, Cloneable {
    
    
     private long idBairro;
     private Municipio municipio;
     @Resolvable(colName = "nome")
     private String nome;
     @Resolvable(colName = "nomeAbreviado")
     private String nomeAbreviado;
     private Set ceps = new HashSet(0);

    public Bairro() {
    }

	
    public Bairro(long idBairro, Municipio municipio, String nome, String nomeAbreviado) {
        this.idBairro = idBairro;
        this.municipio = municipio;
        this.nome = nome;
        this.nomeAbreviado = nomeAbreviado;
    }
    public Bairro(long idBairro, Municipio municipio, String nome, String nomeAbreviado, Set ceps) {
       this.idBairro = idBairro;
       this.municipio = municipio;
       this.nome = nome;
       this.nomeAbreviado = nomeAbreviado;
       this.ceps = ceps;
    }
   
    @Id 
    @GeneratedValue(strategy=GenerationType.AUTO,generator="SEQUENCIA")
    @Column(name="id_bairro", unique=true, nullable=false)
    public long getIdBairro() {
        return this.idBairro;
    }
    
    public void setIdBairro(long idBairro) {
        this.idBairro = idBairro;
    }
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="id_municipio", nullable=false)
    public Municipio getMunicipio() {
        return this.municipio;
    }
    
    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }
    
    @Column(name="nome", nullable=false, length=80)
    public String getNome() {
        return this.nome;
    }
    
    public void setNome(String nome) {
        this.nome = nome;
    }
    
    @Column(name="nome_abreviado", nullable=false, length=40)
    public String getNomeAbreviado() {
        return this.nomeAbreviado;
    }
    
    public void setNomeAbreviado(String nomeAbreviado) {
        this.nomeAbreviado = nomeAbreviado;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="bairro", targetEntity=Cep.class)
    public Set getCeps() {
        return this.ceps;
    }
    
    public void setCeps(Set ceps) {
        this.ceps = ceps;
    }

    @Override
    public Bairro clone() throws CloneNotSupportedException {
        Bairro b = (Bairro) super.clone();
        b.setIdBairro(0);
        b.setCeps(null);
        return b; 
    }
}


