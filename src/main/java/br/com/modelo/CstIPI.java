package br.com.modelo;

import com.towel.el.annotation.Resolvable;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Flávio Augusto
 */
@Entity
@Table(name="cst_ipi"
    ,schema="public"
)
@SequenceGenerator(name = "SEQUENCIA",
    sequenceName = "sq_cst_ipi",
    allocationSize = 1)

public class CstIPI implements Serializable {
    
    
    private int idCstIpi;
    @Resolvable(colName = "codigo")
    private String codigo;
    @Resolvable(colName = "descricao")
    private String descricao;
    @Resolvable(colName = "baseCalculo")
    private float baseCalculo;
    @Resolvable(colName = "aliquota")
    private float aliquota;

    public CstIPI() {
    }

    public CstIPI(int idCstIpi, String codigo, String descricao) {
        this.idCstIpi = idCstIpi;
        this.codigo = codigo;
        this.descricao = descricao;
    }

    public CstIPI(int idCstIpi, String codigo, String descricao, float baseCalculo, float aliquota) {
        this.idCstIpi = idCstIpi;
        this.codigo = codigo;
        this.descricao = descricao;
        this.baseCalculo = baseCalculo;
        this.aliquota = aliquota;
    }


    @Id
    @GeneratedValue(strategy=GenerationType.AUTO, generator="SEQUENCIA")
    @Column(name="id_cst_ipi", unique=true, nullable=false)
    public int getIdCstIpi() {
        return idCstIpi;
    }

    public void setIdCstIpi(int idCstIpi) {
        this.idCstIpi = idCstIpi;
    }

    @Column(name="aliquota", nullable=false, precision=8, scale=8)
    public float getAliquota() {
        return aliquota;
    }

    public void setAliquota(float aliquota) {
        this.aliquota = aliquota;
    }

    @Column(name="base_calculo", nullable=false, precision=8, scale=8)
    public float getBaseCalculo() {
        return baseCalculo;
    }

    public void setBaseCalculo(float baseCalculo) {
        this.baseCalculo = baseCalculo;
    }

    @Column(name="codigo", nullable=false, length=3)
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @Column(name="descricao", nullable=false, length=200)
    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

}
