package br.com.modelo;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "venda", schema = "public")
@SequenceGenerator(name = "SEQUENCIA",
        sequenceName = "sq_venda",
        allocationSize = 1)
public class Venda implements java.io.Serializable {

    public static final String NOME_TABELA = "venda";
    private long idVenda;
    private Funcionario funcionario;
    private Date dataVenda;
    private BigDecimal totalVenda;
    private BigDecimal totalDesconto = BigDecimal.ZERO;
    private BigDecimal totalAcrescimo = BigDecimal.ZERO;
    private BigDecimal totalPagar = BigDecimal.ZERO;
    private int statusVenda;
    private Cliente cliente;
    private Loja loja;
    private Integer numeroCcf;
    private List<ItemVenda> itemVendas = new ArrayList<>(0);
    private String hash;
    private BigDecimal custoReposicao = BigDecimal.ZERO;
    private BigDecimal lucroVenda = BigDecimal.ZERO;
    private BigDecimal totalDevolucao = BigDecimal.ZERO;
    private BigDecimal custoDevolucao = BigDecimal.ZERO;
    private BigDecimal lucroLiquido = BigDecimal.ZERO;
    private boolean transferencia;
    private String enderecoIp;
    private LojaDepartamento departamento;
    private List<PagamentoVenda> pagamentos = new ArrayList<>(0);
    private boolean gerouComissao;
    private BigDecimal totalComissao = BigDecimal.ZERO;
    private Date dataEntrega;
    private String observacao;
    private String opcaoEntrega;
    private boolean gerarBoleto;
    private boolean boletoEmCarne;
    private Long idPedidoWeb = null;

    /*
     * variavel transient 
     * utilizada em validações relacionadas a orçamento em tela de venda
     */
    private boolean fromOrcamento;

    public Venda() {
    }

    public Venda(long idVenda, Funcionario funcionario, Date dataVenda, int statusVenda, Cliente cliente, Loja loja) {
        this.idVenda = idVenda;
        this.funcionario = funcionario;
        this.dataVenda = dataVenda;
        this.statusVenda = statusVenda;
        this.cliente = cliente;
        this.loja = loja;
    }

    public Venda(long idVenda, Funcionario funcionario, Date dataVenda, BigDecimal totalVenda, BigDecimal totalDesconto, int statusVenda, Cliente cliente, Loja loja, Integer numeroCcf, List itemVendas) {
        this.idVenda = idVenda;
        this.funcionario = funcionario;
        this.dataVenda = dataVenda;
        this.totalVenda = totalVenda;
        this.totalDesconto = totalDesconto;
        this.statusVenda = statusVenda;
        this.cliente = cliente;
        this.loja = loja;
        this.numeroCcf = numeroCcf;
        this.itemVendas = itemVendas;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQUENCIA")
    @Column(name = "id_venda", unique = true, nullable = false)
    public long getIdVenda() {
        return this.idVenda;
    }

    public void setIdVenda(long idVenda) {
        this.idVenda = idVenda;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_vendedor", nullable = false)
    public Funcionario getFuncionario() {
        return this.funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_venda", length = 29, insertable = false, updatable = false)
    public Date getDataVenda() {
        return this.dataVenda;
    }

    public void setDataVenda(Date dataVenda) {
        this.dataVenda = dataVenda;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_entrega")
    public Date getDataEntrega() {
        return dataEntrega;
    }

    public void setDataEntrega(Date dataEntrega) {
        this.dataEntrega = dataEntrega;
    }    

    @Column(name = "total_venda", precision = 2)
    public BigDecimal getTotalVenda() {
        return this.totalVenda;
    }

    public void setTotalVenda(BigDecimal totalVenda) {
        this.totalVenda = totalVenda;
    }

    @Column(name = "total_desconto", precision = 2)
    public BigDecimal getTotalDesconto() {
        return this.totalDesconto;
    }

    public void setTotalDesconto(BigDecimal totalDesconto) {
        this.totalDesconto = totalDesconto;
    }

    @Column(name = "total_acrescimo", precision = 2)
    public BigDecimal getTotalAcrescimo() {
        return totalAcrescimo;
    }

    public void setTotalAcrescimo(BigDecimal totalAcrescimo) {
        this.totalAcrescimo = totalAcrescimo;
    }

    @Column(name = "total_pagar", precision = 2)
    public BigDecimal getTotalPagar() {
        return totalPagar;
    }

    public void setTotalPagar(BigDecimal totalPagar) {
        this.totalPagar = totalPagar;
    }

    @Column(name = "status_venda", nullable = false)
    public int getStatusVenda() {
        return this.statusVenda;
    }

    @Column(name = "transferencia")
    public boolean isTransferencia() {
        return transferencia;
    }

    public void setTransferencia(boolean transferencia) {
        this.transferencia = transferencia;
    }

    public void setStatusVenda(int statusVenda) {
        this.statusVenda = statusVenda;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_cliente", nullable = false)
    public Cliente getCliente() {
        return this.cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_loja", nullable = false)
    public Loja getLoja() {
        return this.loja;
    }

    public void setLoja(Loja loja) {
        this.loja = loja;
    }

    @Column(name = "numero_ccf")
    public Integer getNumeroCcf() {
        return this.numeroCcf;
    }

    public void setNumeroCcf(Integer numeroCcf) {
        this.numeroCcf = numeroCcf;
    }

    @Column(name = "custo_reposicao")
    public BigDecimal getCustoReposicao() {
        return custoReposicao;
    }

    public void setCustoReposicao(BigDecimal custoReposicao) {
        this.custoReposicao = custoReposicao;
    }

    @Column(name = "lucro_venda", nullable = true, scale = 9, precision = 2)
    public BigDecimal getLucroVenda() {
        return lucroVenda;
    }

    public void setLucroVenda(BigDecimal lucroVenda) {
        this.lucroVenda = lucroVenda;
    }

    @Column(name = "total_devolucao", nullable = true, scale = 9, precision = 2)
    public BigDecimal getTotalDevolucao() {
        return totalDevolucao;
    }

    public void setTotalDevolucao(BigDecimal totalDevolucao) {
        this.totalDevolucao = totalDevolucao;
    }

    @Column(name = "custo_devolucao", nullable = true, scale = 9, precision = 2)
    public BigDecimal getCustoDevolucao() {
        return custoDevolucao;
    }

    public void setCustoDevolucao(BigDecimal custoDevolucao) {
        this.custoDevolucao = custoDevolucao;
    }

    @Column(name = "lucro_liquido", nullable = true, scale = 9, precision = 2)
    public BigDecimal getLucroLiquido() {
        return lucroLiquido;
    }

    public void setLucroLiquido(BigDecimal lucroLiquido) {
        this.lucroLiquido = lucroLiquido;
    }

    @Column(name = "id_pedido_web")
    public Long getIdPedidoWeb() {
        return idPedidoWeb;
    }

    public void setIdPedidoWeb(Long idPedidoWeb) {
        this.idPedidoWeb = idPedidoWeb;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "venda", targetEntity = ItemVenda.class)
    public List<ItemVenda> getItemVendas() {
        Collections.sort(this.itemVendas);
        return this.itemVendas;
    }

    public void setItemVendas(List<ItemVenda> itemVendas) {
        this.itemVendas = itemVendas;
    }

    @Column(name = "hash", nullable = true)
    public String getHash() {
        return this.hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    @Column(name = "endereco_ip")
    public String getEnderecoIp() {
        return enderecoIp;
    }

    public void setEnderecoIp(String enderecoIp) {
        this.enderecoIp = enderecoIp;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_loja_departamento")
    public LojaDepartamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(LojaDepartamento departamento) {
        this.departamento = departamento;
    }

    @Column(name = "gerou_comissao")
    public boolean isGerouComissao() {
        return gerouComissao;
    }

    public void setGerouComissao(boolean gerouComissao) {
        this.gerouComissao = gerouComissao;
    }

    @Column(name = "opcao_entrega")
    public String getOpcaoEntrega() {
        return opcaoEntrega;
    }

    public void setOpcaoEntrega(String opcaoEntrega) {
        this.opcaoEntrega = opcaoEntrega;
    }

    @Column(name = "total_comissao")
    public BigDecimal getTotalComissao() {
        return totalComissao;
    }

    public void setTotalComissao(BigDecimal totalComissao) {
        this.totalComissao = totalComissao;
    }

    @Column(name = "observacao")
    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    @Transient
    public boolean isGerarBoleto() {
        return gerarBoleto;
    }

    public void setGerarBoleto(boolean gerarBoleto) {
        this.gerarBoleto = gerarBoleto;
    }

    @Transient
    public boolean isBoletoEmCarne() {
        return boletoEmCarne;
    }

    public void setBoletoEmCarne(boolean boletoEmCarne) {
        this.boletoEmCarne = boletoEmCarne;
    }

    @Transient
    public boolean isFromOrcamento() {
        return fromOrcamento;
    }

    public void setFromOrcamento(boolean fromOrcamento) {
        this.fromOrcamento = fromOrcamento;
    }
    

    public void calculaTotalDaVenda() {
        BigDecimal descontoVenda;
        BigDecimal descontoItens;

        descontoItens = this.somaItensComDesconto();
        this.totalVenda = this.totalDosItens();
        if (totalDesconto.compareTo(BigDecimal.ZERO) == 0) {
            descontoVenda = this.totalDesconto;
            this.totalDesconto = descontoVenda.add(descontoItens);

        }

        this.totalPagar = this.totalVenda.subtract(this.totalDesconto);
        this.totalPagar = this.totalPagar.add(totalAcrescimo);
    }

            public BigDecimal totalDosItens() {
        BigDecimal total = BigDecimal.ZERO;
        for (int i = 0; i < this.itemVendas.size(); i++) {
            ItemVenda item = (ItemVenda) this.itemVendas.get(i);
            if (item.getStatusItem() > 0) {
                total = total.add(item.getPrecoVenda().multiply(item.getQtdVenda()));
            }
        }
        return total;
    }

    public void somaLucroItens() {
        this.lucroVenda = BigDecimal.ZERO;
        this.lucroLiquido = BigDecimal.ZERO;
        for (int i = 0; i < this.itemVendas.size(); i++) {
            ItemVenda item = (ItemVenda) this.itemVendas.get(i);
            if (item.getStatusItem() > 0) {
                lucroVenda = lucroVenda.add(item.getLucroVenda());
                lucroLiquido = lucroLiquido.add(item.getLucroLiquido());
            }
        }
    }    

    public void atualizaComissao(BigDecimal totalComissao) {
        this.totalComissao = totalComissao;
        lucroLiquido = lucroLiquido.subtract(this.totalComissao);
    }
    

    public BigDecimal totalAPagar() {
        BigDecimal total = BigDecimal.ZERO;
        BigDecimal desconto = BigDecimal.ZERO;
        if ((this.totalDesconto != null) && (!this.hasItensComDesconto())) {
            desconto = this.totalDesconto;
        }

        desconto = desconto.add(this.somaItensComDesconto());

        for (int i = 0; i < this.itemVendas.size(); i++) {
            ItemVenda item = (ItemVenda) this.itemVendas.get(i);
            if (item.getStatusItem() > 0) {
                total = total.add(item.getPrecoVenda().multiply(item.getQtdVenda()));
            }
        }
        return total.subtract(desconto).setScale(2, RoundingMode.HALF_UP);
    }

    public boolean hasItensComDesconto() {
        boolean resp = false;
        for (int i = 0; i < this.itemVendas.size(); i++) {
            ItemVenda item = (ItemVenda) this.itemVendas.get(i);
            if (item.getStatusItem() > 0) {
                if (item.getTotalDesconto().floatValue() > 0) {
                    resp = true;
                }
            }
        }
        return resp;
    }

    public boolean hasItensEmPromocao() {
        boolean resp = false;
        for (int i = 0; i < this.itemVendas.size(); i++) {
            ItemVenda item = (ItemVenda) this.itemVendas.get(i);
            if ((item.getStatusItem() == 1) && (item.isPrecoPromocional())) {
                resp = true;
            }
        }
        return resp;
    }

    public boolean hasItensForaPromocao() {
        boolean resp = false;
        for (int i = 0; i < this.itemVendas.size(); i++) {
            ItemVenda item = (ItemVenda) this.itemVendas.get(i);
            if ((item.getStatusItem() == 1) && (!item.isPrecoPromocional())) {
                resp = true;
            }
        }
        return resp;
    }

    public BigDecimal somaItensComDesconto() {
        BigDecimal desconto = BigDecimal.ZERO;
        for (int i = 0; i < this.itemVendas.size(); i++) {
            ItemVenda item = (ItemVenda) this.itemVendas.get(i);
            if (item.getStatusItem() > 0) {
                if (item.getTotalDesconto().compareTo(BigDecimal.ZERO) > 0) {
                    desconto = desconto.add(item.getTotalDesconto());
                }
            }
        }
        return desconto;
    }

    public BigDecimal totalPagamentos() {
        BigDecimal total = BigDecimal.ZERO;
        for (int i = 0; i < this.pagamentos.size(); i++) {
            total = total.add(this.pagamentos.get(i).getTotalPago());
        }
        return total.setScale(2, RoundingMode.HALF_UP);
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "venda", targetEntity = PagamentoVenda.class)
    public List<PagamentoVenda> getPagamentos() {
        return this.pagamentos;
    }

    public void setPagamentos(List<PagamentoVenda> pagamentos) {
        this.pagamentos = pagamentos;
    }

    public boolean hasPagamentosAReceber() {
        boolean resp = false;
        for (int i = 0; i < this.pagamentos.size(); i++) {
            PagamentoVenda pagto = this.pagamentos.get(i);
            if (pagto.getGeraContasReceber()) {
                resp = true;
            }
        }
        return resp;
    }

    public boolean imprimeSomenteUmaVidaCartao(Loja loja) {
        boolean resp = false;
        for (int i = 0; i < this.pagamentos.size(); i++) {
            PagamentoVenda pagto = this.pagamentos.get(i);
            if (pagto.getFormaPagamento().getTipoPagamento() == 3) {
                resp = true;
                break;
            }
        }
        return resp && ((loja.getNome().contains("PARADA 994")) || (loja.getNome().contains("TIGRAO")));
    }

    public float totalBaseICMS() {
        float total = 0f;
        for (int i = 0; i < this.itemVendas.size(); i++) {
            ItemVenda item = (ItemVenda) this.itemVendas.get(i);
            if (item.getStatusItem() > 0) {
                total += (item.getVlBaseICMS());
            }
        }
        return total;
    }

    public float totalICMS() {
        float total = 0f;
        for (int i = 0; i < this.itemVendas.size(); i++) {
            ItemVenda item = (ItemVenda) this.itemVendas.get(i);
            if (item.getStatusItem() > 0) {
                total += (item.getTotalIcms().floatValue());
            }
        }
        return total;
    }

    public BigDecimal retornaSaldoAPagar() {

        BigDecimal aPagar;
        BigDecimal pago;

        aPagar = this.totalAPagar();
        pago = this.totalPagamentos();

        aPagar = aPagar.subtract(pago);

        return aPagar;
    }

    public ItemVenda hasItem(ItemVenda itemVenda) {
        ItemVenda item = null;
        for (int i = 0; i < this.getItemVendas().size(); i++) {
            if (((ItemVenda) this.getItemVendas().get(i)).getProduto().getIdProduto() == itemVenda.getProduto().getIdProduto()) {
                item = (ItemVenda) this.getItemVendas().get(i);
            }
        }
        return item;
    }

    public void defineAcrescimo(BigDecimal totalAcrescimo) {
        this.setTotalAcrescimo(totalAcrescimo);
        this.calculaTotalDaVenda();
    }

    public void defineDesconto(BigDecimal valor) {
        this.totalDesconto = valor.setScale(2, RoundingMode.HALF_UP);
        this.calculaTotalDaVenda();
    }    

    public void defineTotalDaVenda(BigDecimal valor) {
        if (this.totalDesconto == BigDecimal.ZERO) {
            this.totalAcrescimo = valor.subtract(this.totalVenda);
        } else {
            this.totalAcrescimo = valor.subtract(this.totalDesconto)
                    .subtract(this.totalVenda);
        }
        this.calculaTotalDaVenda();
    }

    public void defineLucroObtido() {
        this.somaLucroItens();
    }

    public float retornaPercentualDeDesconto() {
        if (this.totalDesconto == null) {
            return 0;
        }
        if (this.totalDesconto.equals(BigDecimal.ZERO)) {
            return 0;
        }

        return ((this.totalDesconto.floatValue() / this.totalVenda.floatValue()) * 100);
    }
    
    public boolean naoPrecisaLiberar() {
        return this.statusVenda != 5;
    }

    public boolean pagamentosAvaliacao(PagamentoVenda pagamento) {
        return pagamento.getGeraContasReceber() || pagamento.getPreFaturamento();
    }

    public void recalcularPrecoItens() {
        for (int i = 0; i < this.itemVendas.size(); i++) {
            ItemVenda item = (ItemVenda) this.itemVendas.get(i);
            item.setTotalDesconto(null);
            item.calculaTotal();
        }
    }

    public boolean descontoLojaPermitido(BigDecimal desconto) {
        if (this.cliente.getDescontoPadrao() != null) {
            return (desconto.floatValue() <= this.getLoja().getDescontoMaximo().floatValue() || (desconto.floatValue() <= this.cliente.getDescontoPadrao().floatValue()));
        } else {
            return (desconto.floatValue() <= this.getLoja().getDescontoMaximo().floatValue());
        }
    }

    public boolean descontoAbaixoLimiteMaximo(BigDecimal desconto) {
        return desconto.floatValue() <= this.getLoja().getDescontoMaximoAcima().floatValue();
    }

    public boolean isLucroMinItemValido(BigDecimal valorDesconto, Loja loja) throws Exception {

        BigDecimal indice, valorARetirar, acm = BigDecimal.ZERO;

        //transforma o percentual de desconto em decimal
        BigDecimal percDesconto = BigDecimal.valueOf(valorDesconto.floatValue() / (this.totalDosItens().floatValue())).setScale(6, RoundingMode.DOWN);

        if (loja.getLucroMinimoItem().compareTo(BigDecimal.ZERO) > 0) {
            if (loja.getLucroMinimoItem().compareTo(percDesconto.multiply(new BigDecimal(100))) < 0) {
                throw new Exception("O percentual de desconto dos itens está abaixo mínimo permitido pela loja");
            }
        }

        return true;
    }
    

    /**
     * <strong>Avalia se o percentual de desconto é valido</strong>
     *
     * @return 0 - desconto permito<br/> 1 - referente a solicitação de senha
     * para liberar operação;<br/> 2 - erro: desconto maior que o maximo apos o
     * exceder o limite desconto permitido;<br/> 3 - Venda com desconto acima do
     * limte não permitida;
     */
    public int descontoPermitido(BigDecimal percDesconto) {

        //se desconto da venda for maior que o maximo definido pela loja, entra.
        if (!this.descontoLojaPermitido(percDesconto)) {

            //se loja não permitir desconto acima do definido, entra.
            if (!this.getLoja().getVendaDescontoAcima()) {

                //se loja permitir senha para desconto acima, entra. 
                if (this.getLoja().getSenhaDescontoAcima()) {

                    //se desconto for menor que o valor maximo apos o exceder o
                    //limite permitido de desconto, entra
                    if (this.descontoAbaixoLimiteMaximo(percDesconto)) {
                        return 1; //gera excecao
                    } else {
                        return 2; // erro: Desconto acima do limite máximo permitido pela loja
                    }
                } else {
                    return 3; // erro: Não é permitido Venda com desconto acima do limite
                }
            }
        }

        return 0; //nao gera excecao
    }

    public boolean produtoComPrecoPromocional(Produto produto) {
        boolean resp = false;
        for (int i = 0; i < this.getItemVendas().size(); i++) {
            ItemVenda item = ((ItemVenda) this.getItemVendas().get(i));

            if (item.getProduto().getIdProduto() == produto.getIdProduto()) {
                resp = item.isPrecoPromocional();
            }
        }
        return resp;
    }

    public boolean operacaoDeTransferencia() {

        String base = "";
        String comparativo = "";
        try {
            if (this.cliente.getCpfCnpj().equals("CONSUMIDOR")) {
                return false;
            }

            if (this.cliente.getCpfCnpj().length() == 11) {
                return false;
            }

            base = this.loja.getEmpresa().getCnpj().substring(0, 8);
            comparativo = this.cliente.getCpfCnpj().substring(0, 8);
            return base.equals(comparativo);
        } catch (Exception e) {
        }
        return base.equals(comparativo);
    }

    public BigDecimal retornaSaldoAPagarPagtoPreFaturado() {
        BigDecimal saldoAPagar = BigDecimal.ZERO;
        for (int i = 0; i < this.pagamentos.size(); i++) {
            PagamentoVenda pagto = this.pagamentos.get(i);
            if (pagto.getPreFaturamento()) {
                saldoAPagar = saldoAPagar.add(pagto.getTotalPago());
            }
        }
        return saldoAPagar;
    }

    public boolean hasPagtoPreFaturamento() {
        for (int i = 0; i < this.pagamentos.size(); i++) {
            if (this.pagamentos.get(i).getPreFaturamento() && this.pagamentos.get(i).getDataPagamento() == null) {
                return true;
            }
        }
        return false;
    }

    public BigDecimal totalPreFaturamento() {
        BigDecimal total = BigDecimal.ZERO;
        for (int i = 0; i < this.pagamentos.size(); i++) {
            if (this.pagamentos.get(i).getPreFaturamento() && this.pagamentos.get(i).getDataPagamento() == null) {
                total = total.add(this.pagamentos.get(i).getTotalPago());
            }
        }
        return total;
    }

    public float retornaLucroRealVenda() {
        float lucro = 0;
        for (int i = 0; i < this.getItemVendas().size(); i++) {
            if (this.getItemVendas().get(i).getStatusItem() != 0) {
                lucro += this.getItemVendas().get(i).getLucroVenda().floatValue();
            }
        }
        return lucro;
    }

    public BigDecimal retornaPercentualDeLucro() {
        BigDecimal lucroItem = BigDecimal.ZERO;
        BigDecimal custo = BigDecimal.ZERO;
        for (int i = 0; i < this.itemVendas.size(); i++) {
            ItemVenda item = (ItemVenda) this.itemVendas.get(i);
            if (item.getStatusItem() > 0) {
                lucroItem = lucroItem.add(item.getLucroVenda());
                custo = custo.add(item.getCustoReposicao());
            }
        }

        if (custo.compareTo(BigDecimal.ZERO) == 0) {
            custo = BigDecimal.ONE;
        }

        BigDecimal percentual = lucroItem.divide(custo, RoundingMode.HALF_UP);
        return percentual.multiply(new BigDecimal(100));
    }

    public boolean acimaLucroMinimo() {
        if (this.getLoja().getLucroMinimoVenda() == null
                || this.getLoja().getLucroMinimoVenda().compareTo(BigDecimal.ZERO) == 0) {
            return true;
        }

        BigDecimal lucro = this.retornaPercentualDeLucro();

        return (lucro.compareTo(this.getLoja().getLucroMinimoVenda()) >= 0);
    }

    public boolean formaDePagamentoPermitida(FormaPagamento pg) {
        for (ItemVenda itemVenda : itemVendas) {
            if (itemVenda.getFormasPgtoPromocao() != null) {
                if (!itemVenda.getFormasPgtoPromocao().contains(pg.getDescricao()) && (!pg.isCreditoDevolucao())) {
                    return false;
                }
            }
        }
        return true;
    }

    public String retornaFormasPgtoPromocionais() {
        for (ItemVenda itemVenda : itemVendas) {
            if (itemVenda.getFormasPgtoPromocao() != null) {
                return itemVenda.getFormasPgtoPromocao();
            }
        }

        return "";
    }

    public boolean gerouDocumento() {
        return this.statusVenda == 2
                || this.statusVenda == 3
                || this.statusVenda == 6
                || this.statusVenda == 9
                || this.statusVenda == 10;
    }
}

