package br.com.modelo;

import com.towel.el.annotation.Resolvable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="status_venda"
    ,schema="public")
public class StatusVenda implements java.io.Serializable {

    private int idStatusVenda; 
    @Resolvable(colName="descricao")
    private String descricao; 
    private boolean ativo; 
     
    public StatusVenda() {
    }

	
    public StatusVenda(int idStatusVenda, String descricao) {
        this.idStatusVenda = idStatusVenda;
        this.descricao = descricao;
    }

   
    @Id     
    @Column(name="id_status_venda", unique=true, nullable=false)
    public int getIdStatusVenda() {
        return this.idStatusVenda;
    }
    
    public void setIdStatusVenda(int idStatusVenda) {
        this.idStatusVenda = idStatusVenda;
    }
    
    @Column(name="descricao", nullable=false)
    public String getDescricao() {
        return this.descricao;
    }
    
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Column(name="ativo")
    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }
}


