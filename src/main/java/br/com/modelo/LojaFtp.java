package br.com.modelo;


import java.util.Date;
import javax.persistence.*;

@Entity
@Table(name="loja_ftp"
    ,schema="public"
)
public class LojaFtp  implements java.io.Serializable {


     private int idLojaFtp;  
     private String servidor;
     private String usuario;
     private String senha;
     private String pathServer;
     private String hostsOuvintes;
     private int periodoAtualizacao;
     private Date ultimoEnvio;
     private Funcionario funcionario;
     private Date ultimoRecto;     
     private Loja loja;     
     
     
    public LojaFtp() {
    }

	
    public LojaFtp(int idLojaDepartamento, Loja loja) {
        this.idLojaFtp = idLojaDepartamento;
        this.loja = loja;
    }

    public LojaFtp(int idLojaFtp, int periodoAtualizacao, Date ultimoEnvio, Funcionario funcionario, Date ultimoRecto, Loja loja) {
        this.idLojaFtp = idLojaFtp;
        this.periodoAtualizacao = periodoAtualizacao;
        this.ultimoEnvio = ultimoEnvio;
        this.funcionario = funcionario;
        this.ultimoRecto = ultimoRecto;
        this.loja = loja;
    }

    @Id    
    @Column(name="id_loja_ftp", unique=true, nullable=false)
    public int getIdLojaFtp() {
        return this.idLojaFtp;
    }
    
    public void setIdLojaFtp(int idLojaFtp) {
        this.idLojaFtp = idLojaFtp;
    }

    @Column(name="servidor", nullable=false)
    public String getServidor() {
        return servidor;
    }

    public void setServidor(String servidor) {
        this.servidor = servidor;
    }

    @Column(name="usuario", nullable=false)
    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    @Column(name="senha", nullable=false)
    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    @Column(name="path_server", nullable=false)
    public String getPathServer() {
        return pathServer;
    }

    public void setPathServer(String pathServer) {
        this.pathServer = pathServer;
    }

    @Column(name="hosts_ouvintes", nullable=false)
    public String getHostsOuvintes() {
        return hostsOuvintes;
    }

    public void setHostsOuvintes(String hostsOuvintes) {
        this.hostsOuvintes = hostsOuvintes;
    }

    @Column(name = "periodo_atualizacao")
    public int getPeriodoAtualizacao() {
        return periodoAtualizacao;
    }

    public void setPeriodoAtualizacao(int periodoAtualizacao) {
        this.periodoAtualizacao = periodoAtualizacao;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ultimo_envio", nullable = true, length = 29)
    public Date getUltimoEnvio() {
        return ultimoEnvio;
    }

    public void setUltimoEnvio(Date ultimoEnvio) {
        this.ultimoEnvio = ultimoEnvio;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_funcionario", nullable = true)
    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ultimo_recto", nullable = true, length = 29)
    public Date getUltimoRecto() {
        return ultimoRecto;
    }

    public void setUltimoRecto(Date ultimoRecto) {
        this.ultimoRecto = ultimoRecto;
    }
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="id_loja", nullable=false,insertable=false, updatable=false)
    public Loja getLoja() {
        return loja;
    }

    public void setLoja(Loja loja) {
        this.loja = loja;
    } 
    
    public boolean hasIp(String ip){
        return this.hostsOuvintes.contains(ip);
    }
}


