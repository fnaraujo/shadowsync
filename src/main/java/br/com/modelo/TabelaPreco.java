package br.com.modelo;


import com.towel.el.annotation.Resolvable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="tabela_preco"
    ,schema="public"
)
@SequenceGenerator(name = "SEQUENCIA",
    sequenceName = "sq_tabela_preco",
    allocationSize = 1)
public class TabelaPreco  implements java.io.Serializable {


     private long idTabelaPreco;
     private Loja loja;
     @Resolvable(colName = "descricao")
     private String descricao;
     private float indice;
     @Resolvable(colName = "ativo")
     private Boolean ativo;
     @Resolvable(colName = "padrao")
     private Boolean padrao;

    public TabelaPreco() {
    }

	
    public TabelaPreco(long idTabelaPreco, Loja loja, String descricao, float indice) {
        this.idTabelaPreco = idTabelaPreco;
        this.loja = loja;
        this.descricao = descricao;
        this.indice = indice;
    }
    public TabelaPreco(long idTabelaPreco, Loja loja, String descricao, float indice, Boolean ativo, Boolean padrao) {
       this.idTabelaPreco = idTabelaPreco;
       this.loja = loja;
       this.descricao = descricao;
       this.indice = indice;
       this.ativo = ativo;
       this.padrao = padrao;
    }
   
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO, generator="SEQUENCIA")
    @Column(name="id_tabela_preco", unique=true, nullable=false)
    public long getIdTabelaPreco() {
        return this.idTabelaPreco;
    }
    
    public void setIdTabelaPreco(long idTabelaPreco) {
        this.idTabelaPreco = idTabelaPreco;
    }
@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="id_loja", nullable=false)
    public Loja getLoja() {
        return this.loja;
    }
    
    public void setLoja(Loja loja) {
        this.loja = loja;
    }
    
    @Column(name="descricao", nullable=false)
    public String getDescricao() {
        return this.descricao;
    }
    
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    
    @Column(name="indice", nullable=false, precision=8, scale=8)
    public float getIndice() {
        return this.indice;
    }
    
    public void setIndice(float indice) {
        this.indice = indice;
    }
    
    @Column(name="ativo")
    public Boolean getAtivo() {
        return this.ativo;
    }
    
    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }
    
    @Column(name="padrao")
    public Boolean getPadrao() {
        return this.padrao;
    }
    
    public void setPadrao(Boolean padrao) {
        this.padrao = padrao;
    }
}


