package br.com.modelo;

import com.towel.el.annotation.Resolvable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.*;

@Entity
@Table(name = "pagamento_venda", schema = "public")
@SequenceGenerator(name = "SEQUENCIA",
sequenceName = "sq_pagamento_venda",
allocationSize = 1)
public class PagamentoVenda implements java.io.Serializable {

    public static final String NOME_TABELA = "pagamento_venda";
    private long idPagamentoVenda;
    private FormaPagamento formaPagamento;
    private String descricao;
    private BigDecimal totalPago;
    private boolean geraContasReceber;
    private boolean preFaturamento;
    private boolean estornado;
    private Venda venda;
    private Long idMovimentacaoConta;
    private Date dataPagamento;

    public PagamentoVenda() {
    }

    public PagamentoVenda(int idPagamentoVenda, String descricao) {
        this.idPagamentoVenda = idPagamentoVenda;
        this.descricao = descricao;
    }

    public PagamentoVenda(int idPagamentoVenda, String descricao, boolean geraContasReceber) {
        this.idPagamentoVenda = idPagamentoVenda;
        this.descricao = descricao;
        this.geraContasReceber = geraContasReceber;
    }

    public PagamentoVenda(int idPagamentoVenda, String descricao, boolean geraContasReceber, BigDecimal totalPago, boolean preFaturamento) {
        this.idPagamentoVenda = idPagamentoVenda;
        this.descricao = descricao;
        this.geraContasReceber = geraContasReceber;
        this.totalPago = totalPago;
        this.preFaturamento = preFaturamento;

    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQUENCIA")
    @Column(name = "id_pagamento_venda", unique = true, nullable = false)
    public long getIdPagamentoVenda() {
        return this.idPagamentoVenda;
    }

    public void setIdPagamentoVenda(long idPagamentoVenda) {
        this.idPagamentoVenda = idPagamentoVenda;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_forma_pagamento", nullable = false)
    public FormaPagamento getFormaPagamento() {
        return formaPagamento;
    }

    public void setFormaPagamento(FormaPagamento formaPagamento) {
        this.formaPagamento = formaPagamento;
    }

    @Column(name = "descricao", nullable = false, length = 40)
    public String getDescricao() {
        return this.descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Column(name = "gera_contas_receber")
    public Boolean getGeraContasReceber() {
        return this.geraContasReceber;
    }

    public void setGeraContasReceber(boolean geraReceber) {
        this.geraContasReceber = geraReceber;
    }

    @Column(name = "pre_faturamento")
    public Boolean getPreFaturamento() {
        return this.preFaturamento;
    }

    public void setPreFaturamento(boolean preFaturamento) {
        this.preFaturamento = preFaturamento;
    }

    @Column(name = "estornado")
    public boolean getEstornado() {
        return estornado;
    }

    public void setEstornado(boolean estornado) {
        this.estornado = estornado;
    }

    @Column(name = "total_pago", nullable = false)
    public BigDecimal getTotalPago() {
        return this.totalPago;
    }

    public void setTotalPago(BigDecimal totalPago) {
        this.totalPago = totalPago;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_venda", nullable = false, insertable = true, updatable = true)
    public Venda getVenda() {
        return venda;
    }

    public void setVenda(Venda venda) {
        this.venda = venda;
    }

    @Column(name = "id_movimentacao_conta", nullable = true)
    public Long getIdMovimentacaoConta() {
        return idMovimentacaoConta;
    }

    public void setIdMovimentacaoConta(Long idMovimentacaoConta) {
        this.idMovimentacaoConta = idMovimentacaoConta;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_pagamento")
    public Date getDataPagamento() {
        return dataPagamento;
    }

    public void setDataPagamento(Date dataPagamento) {
        this.dataPagamento = dataPagamento;
    }    
}