package br.com.modelo;

import com.towel.el.annotation.Resolvable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "endereco_cliente", schema = "public")
@SequenceGenerator(name = "SEQUENCIA",
        sequenceName = "sq_endereco_cliente",
        allocationSize = 1)
public class EnderecoCliente implements java.io.Serializable, Cloneable {

    private long idEnderecoCliente;
    private Cep cep;
    private Cliente cliente;
    private String numero;
    private String complemento;
    private String tipo;
    private boolean cobranca;
    private boolean entrega;
    private Date dataInativacao;
    private String logradouro;
    private String bairro;
    private String referencia;

    public EnderecoCliente() {
    }

    public EnderecoCliente(Cep cep, long idEnderecoCliente, Cliente cliente, String numero, String complemento, String tipo) {
        this.cep = cep;
        this.idEnderecoCliente = idEnderecoCliente;
        this.cliente = cliente;
        this.numero = numero;
        this.complemento = complemento;
        this.tipo = tipo;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQUENCIA")
    @Column(name = "id_endereco_cliente", unique = true, nullable = false)
    public long getIdEnderecoCliente() {
        return this.idEnderecoCliente;
    }

    public void setIdEnderecoCliente(long idEnderecoCliente) {
        this.idEnderecoCliente = idEnderecoCliente;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_cep", nullable = false)
    public Cep getCep() {
        return this.cep;
    }

    public void setCep(Cep cep) {
        this.cep = cep;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_cliente", nullable = false)
    public Cliente getCliente() {
        return this.cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    @Column(name = "numero", nullable = false)
    public String getNumero() {
        return this.numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    @Column(name = "complemento", nullable = false, length = 20)
    public String getComplemento() {
        return this.complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    @Column(name = "tipo", nullable = true, length = 20)
    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Column(name = "cobranca")
    public boolean getCobranca() {
        return cobranca;
    }

    public void setCobranca(boolean cobranca) {
        this.cobranca = cobranca;
    }

    @Column(name = "entrega")
    public boolean getEntrega() {
        return entrega;
    }

    public void setEntrega(boolean entrega) {
        this.entrega = entrega;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_inativacao", nullable = true, length = 29)
    public Date getDataInativacao() {
        return this.dataInativacao;
    }

    public void setDataInativacao(Date dataInativacao) {
        this.dataInativacao = dataInativacao;
    }

    @Column(name = "logradouro")
    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    @Column(name = "bairro")
    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    @Column(name = "referencia")
    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    @Override
    public EnderecoCliente clone() throws CloneNotSupportedException {
        EnderecoCliente ec = (EnderecoCliente) super.clone();
        ec.setIdEnderecoCliente(0);
        return ec;
    }
}
