package br.com.modelo;

import com.towel.el.annotation.Resolvable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "estoque_produto", schema = "public")
@SequenceGenerator(name = "SEQUENCIA",
sequenceName = "sq_estoque",
allocationSize = 1)
public class EstoqueProduto implements java.io.Serializable {

    public static final String NOME_TABELA = "estoque_produto";
    private long idEstoque;
    private Produto produto;
    private Loja loja;
    @Resolvable(colName = "classeAbc")
    private char classeAbc;
    private BigDecimal demandaMes;
    private BigDecimal custoReposicao;
    private BigDecimal custoMedio;
    private BigDecimal qtdDisponivel;
    private BigDecimal qtdMaximo;
    private BigDecimal qtdMinimo;
    private BigDecimal esgotamentoDiario;
    private BigDecimal qtdAlocada;    
    private String locacao;
    private Date ultimaCompra;
    private Date ultimaVenda;
    private Date ultimaAtualizacao;
    private Date ultimoMvtoEstoque;
    private Date dataInventario;
    private BigDecimal qtdContada;
    private List<EstoquePreco> estoquePrecos = new ArrayList(0);
    private String hash;
    private boolean pagaComissao;
    private boolean liberadoContagem;
    private float indiceComissaoGer;
    private float indiceComissaoMec;
    private LojaDepartamento departamento;
    @Resolvable(colName = "liberadoCompra")
    private boolean liberadoCompra;

    public EstoqueProduto() {
    }

    public EstoqueProduto(long idEstoque, Produto produto) {
        this.idEstoque = idEstoque;
        this.produto = produto;
    }

    public EstoqueProduto(long idEstoque, Produto produto, Loja loja, char classeAbc, BigDecimal demandaMes, BigDecimal custoReposicao, BigDecimal custoMedio, BigDecimal qtdDisponivel, BigDecimal qtdMaximo, BigDecimal qtdMinimo, String locacao) {
        this.idEstoque = idEstoque;
        this.produto = produto;
        this.loja = loja;
        this.classeAbc = classeAbc;
        this.demandaMes = demandaMes;
        this.custoReposicao = custoReposicao;
        this.custoMedio = custoMedio;
        this.qtdDisponivel = qtdDisponivel;
        this.qtdMaximo = qtdMaximo;
        this.qtdMinimo = qtdMinimo;
        this.locacao = locacao;
    }

    public EstoqueProduto(long idEstoque, Produto produto, Loja loja, char classeAbc, BigDecimal demandaMes, BigDecimal custoReposicao, BigDecimal custoMedio, BigDecimal qtdDisponivel, BigDecimal qtdMaximo, BigDecimal qtdMinimo, String locacao, Date ultimaCompra, Date ultimaVenda, Date ultimaAtualizacao, Date dataInventario, BigDecimal qtdContada, List<EstoquePreco> estoquePrecos) {
        this.idEstoque = idEstoque;
        this.produto = produto;
        this.loja = loja;
        this.classeAbc = classeAbc;
        this.demandaMes = demandaMes;
        this.custoReposicao = custoReposicao;
        this.custoMedio = custoMedio;
        this.qtdDisponivel = qtdDisponivel;
        this.qtdMaximo = qtdMaximo;
        this.qtdMinimo = qtdMinimo;
        this.locacao = locacao;
        this.ultimaCompra = ultimaCompra;
        this.ultimaVenda = ultimaVenda;
        this.ultimaAtualizacao = ultimaAtualizacao;
        this.dataInventario = dataInventario;
        this.qtdContada = qtdContada;
        this.estoquePrecos = estoquePrecos;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQUENCIA")
    @Column(name = "id_estoque", unique = true, nullable = false)
    public long getIdEstoque() {
        return this.idEstoque;
    }

    public void setIdEstoque(long idEstoque) {
        this.idEstoque = idEstoque;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_produto", nullable = false, insertable = true, updatable = true)
    public Produto getProduto() {
        return this.produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_loja", nullable = false, updatable=false)
    public Loja getLoja() {
        return this.loja;
    }

    public void setLoja(Loja loja) {
        this.loja = loja;
    }

    @Column(name = "classe_abc", nullable = false, length = 1)
    public char getClasseAbc() {
        return this.classeAbc;
    }

    public void setClasseAbc(char classeAbc) {
        this.classeAbc = classeAbc;
    }

    @Column(name = "demanda_mes", nullable = false, precision = 9, scale = 2)
    public BigDecimal getDemandaMes() {
        return this.demandaMes;
    }

    public void setDemandaMes(BigDecimal demandaMes) {
        this.demandaMes = demandaMes;
    }

    @Column(name = "custo_reposicao", nullable = false, precision = 15)
    public BigDecimal getCustoReposicao() {
        return this.custoReposicao;
    }

    public void setCustoReposicao(BigDecimal custoReposicao) {
        this.custoReposicao = custoReposicao;
    }

    @Column(name = "custo_medio", nullable = false, precision = 15)
    public BigDecimal getCustoMedio() {
        return this.custoMedio;
    }

    public void setCustoMedio(BigDecimal custoMedio) {
        this.custoMedio = custoMedio;
    }

    @Column(name = "qtd_disponivel", nullable = false, precision = 9, scale = 2, updatable = false, insertable = false, columnDefinition = "default '0'")
    public BigDecimal getQtdDisponivel() {
        return this.qtdDisponivel;
    }

    public void setQtdDisponivel(BigDecimal qtdDisponivel) {
        this.qtdDisponivel = qtdDisponivel;
    }

    @Column(name = "qtd_maximo", nullable = false, precision = 9, scale = 2)
    public BigDecimal getQtdMaximo() {
        return this.qtdMaximo;
    }

    public void setQtdMaximo(BigDecimal qtdMaximo) {
        this.qtdMaximo = qtdMaximo;
    }

    @Column(name = "qtd_minimo", nullable = false, precision = 9, scale = 2)
    public BigDecimal getQtdMinimo() {
        return this.qtdMinimo;
    }

    public void setQtdMinimo(BigDecimal qtdMinimo) {
        this.qtdMinimo = qtdMinimo;
    }

    @Column(name = "esgotamento_diario", nullable = false, precision = 9, scale = 2)
    public BigDecimal getEsgotamentoDiario() {
        return esgotamentoDiario;
    }

    public void setEsgotamentoDiario(BigDecimal esgotamentoDiario) {
        this.esgotamentoDiario = esgotamentoDiario;
    }

    @Column(name = "qtd_alocada", nullable = false, precision = 9, scale = 3)
    public BigDecimal getQtdAlocada() {
        return qtdAlocada;
    }

    public void setQtdAlocada(BigDecimal qtdAlocada) {
        this.qtdAlocada = qtdAlocada;
    }

    @Column(name = "locacao", nullable = false, length = 100)
    public String getLocacao() {
        return this.locacao;
    }

    public void setLocacao(String locacao) {
        this.locacao = locacao;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ultima_compra", length = 29)
    public Date getUltimaCompra() {
        return this.ultimaCompra;
    }

    public void setUltimaCompra(Date ultimaCompra) {
        this.ultimaCompra = ultimaCompra;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ultima_venda", length = 29)
    public Date getUltimaVenda() {
        return this.ultimaVenda;
    }

    public void setUltimaVenda(Date ultimaVenda) {
        this.ultimaVenda = ultimaVenda;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ultima_atualizacao", length = 29)
    public Date getUltimaAtualizacao() {
        return this.ultimaAtualizacao;
    }

    public void setUltimaAtualizacao(Date ultimaAtualizacao) {
        this.ultimaAtualizacao = ultimaAtualizacao;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_inventario", length = 29)
    public Date getDataInventario() {
        return this.dataInventario;
    }

    public void setDataInventario(Date dataInventario) {
        this.dataInventario = dataInventario;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ultimo_mvto_estoque", length = 29)
    public Date getUltimoMvtoEstoque() {
        return ultimoMvtoEstoque;
    }

    public void setUltimoMvtoEstoque(Date ultimoMvtoEstoque) {
        this.ultimoMvtoEstoque = ultimoMvtoEstoque;
    }

    @Column(name = "qtd_contada", precision = 9, scale = 2)
    public BigDecimal getQtdContada() {
        return this.qtdContada;
    }

    public void setQtdContada(BigDecimal qtdContada) {
        this.qtdContada = qtdContada;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "estoqueProduto", targetEntity = EstoquePreco.class)
    public List<EstoquePreco> getEstoquePrecos() {
        return this.estoquePrecos;
    }

    public void setEstoquePrecos(List<EstoquePreco> estoquePrecos) {
        this.estoquePrecos = estoquePrecos;
    }

    @Column(name = "hash", nullable = true)
    public String getHash() {
        return this.hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    @Column(name = "paga_comissao")
    public boolean getPagaComissao() {
        return pagaComissao;
    }

    public void setPagaComissao(boolean pagaComissao) {
        this.pagaComissao = pagaComissao;
    }

    @Column(name = "liberado_contagem")
    public boolean isLiberadoContagem() {
        return liberadoContagem;
    }

    public void setLiberadoContagem(boolean liberadoContagem) {
        this.liberadoContagem = liberadoContagem;
    }

    @Column(name = "liberado_compra")
    public boolean isLiberadoCompra() {
        return liberadoCompra;
    }

    public void setLiberadoCompra(boolean liberadoCompra) {
        this.liberadoCompra = liberadoCompra;
    }

    @Column(name = "indice_comissao_ger", precision = 8, scale = 8)
    public float getIndiceComissaoGer() {
        return indiceComissaoGer;
    }

    public void setIndiceComissaoGer(float indiceComissaoGer) {
        this.indiceComissaoGer = indiceComissaoGer;
    }

    @Column(name = "indice_comissao_mec", precision = 8, scale = 8)
    public float getIndiceComissaoMec() {
        return indiceComissaoMec;
    }

    public void setIndiceComissaoMec(float indiceComissaoMec) {
        this.indiceComissaoMec = indiceComissaoMec;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_loja_departamento")
    public LojaDepartamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(LojaDepartamento departamento) {
        this.departamento = departamento;
    }

    public float retornaLucroReal(EstoquePreco ep) {
        return ((ep.getValorVenda().floatValue() / this.getCustoReposicao().floatValue()) - 1) * 100;
    }

    public float retornaLucroPromocao(EstoquePreco ep) {
        return ((ep.getValorDesconto().floatValue() / this.getCustoReposicao().floatValue()) - 1) * 100;
    }

    public boolean possuiDepartamento() {
        return this.departamento != null;
    }

    public boolean departamentoPermitido(LojaDepartamento departamento) {
        if (this.possuiDepartamento()) {
            return this.departamento.getIdLojaDepartamento() == departamento.getIdLojaDepartamento();
        } else {
            return true;
        }
    }

    public String retornaLocacao(int maxLength) {
        String nome = this.locacao;

        if (nome.length() < maxLength) {
            return nome;
        } else {
            return nome.substring(0, maxLength);
        }
    }

    public boolean verificaSaldoMultiploQtdEmbalagem() {

        if (this.qtdDisponivel.equals(BigDecimal.ZERO) || this.produto.getQtdEmbalagem() <= 1) {
            return true;
        }

        return (this.qtdDisponivel.floatValue() % this.produto.getQtdEmbalagem() == 0);
    }

    public boolean verificaLucroMinimoMaiorQZero() {

        if (estoquePrecos == null || estoquePrecos.isEmpty()) {
            return false;
        }

        BigDecimal total = BigDecimal.ZERO;
        Iterator it = estoquePrecos.iterator();
        while (it.hasNext()) {

            EstoquePreco ep = (EstoquePreco) it.next();
            total = total.add(BigDecimal.valueOf(ep.getTabelaPreco().getIndice()));
        }
        return total.compareTo(BigDecimal.ZERO) > 0;
    }

    public EstoquePreco retornaPrecoPadrao() {
        for (EstoquePreco estoquePreco : estoquePrecos) {
            if(estoquePreco.getPrecoPadrao()){
                return estoquePreco;
            }            
        }
        
        return null;
    }
}
