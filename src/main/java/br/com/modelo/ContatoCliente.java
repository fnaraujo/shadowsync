package br.com.modelo;

import com.towel.el.annotation.Resolvable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="contato_cliente"
    ,schema="public"
)
@SequenceGenerator(name = "SEQUENCIA",
   sequenceName = "sq_contato_cliente",
   allocationSize = 1)
public class ContatoCliente  implements java.io.Serializable, Cloneable {
     private long idContatoCliente;
     private Cliente cliente;
     private String telefoneEmail;
     private String contato;
     private String observacao;
     

    public ContatoCliente() {
    }

    public ContatoCliente(long idContatoCliente, Cliente cliente, String telefoneEmail, String contato, String observacao) {
        this.idContatoCliente = idContatoCliente;
        this.cliente = cliente;
        this.telefoneEmail = telefoneEmail;
        this.contato = contato;
        this.observacao = observacao;
    }
   
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO, generator="SEQUENCIA")
    @Column(name="id_contato_cliente", unique=true, nullable=false)
    public long getIdContatoCliente() {
        return this.idContatoCliente;
    }
    
    public void setIdContatoCliente(long idContatoCliente) {
        this.idContatoCliente = idContatoCliente;
    }


@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="id_cliente", nullable=false)
    public Cliente getCliente() {
        return this.cliente;
    }
    
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    
    @Column(name="telefone_email", nullable=false)
    public String getTelefoneEmail() {
        return this.telefoneEmail;
    }
    
    public void setTelefoneEmail(String telefoneEmail) {
        this.telefoneEmail = telefoneEmail;
    }
    
    @Column(name="contato", nullable=false, length=20)
    public String getContato() {
        return this.contato;
    }
    
    public void setContato(String contato) {
        this.contato = contato;
    }
    
    @Column(name="observacao", nullable=true, length=20)
    public String getObservacao() {
        return this.observacao;
    }
    
    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    @Override
    public ContatoCliente clone() throws CloneNotSupportedException {
        ContatoCliente cc = (ContatoCliente) super.clone();
        cc.setIdContatoCliente(0);
        return cc;
    }
}


