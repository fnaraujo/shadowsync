package br.com.visao;

import br.com.controle.BairroAppService;
import br.com.controle.CepAppService;
import br.com.controle.ClienteAppService;
import br.com.controle.DAO;
import br.com.controle.EstoquePrecoAppService;
import br.com.controle.EstoqueProdutoAppService;
import br.com.controle.FormaPagamentoAppService;
import br.com.controle.FuncionarioAppService;
import br.com.controle.LojaAppService;
import br.com.controle.ShadowsyncInfoAppService;
import br.com.controle.Sistema;
import br.com.controle.VendaAppService;
import br.com.controle.exception.InfraEstruturaException;
import br.com.modelo.Bairro;
import br.com.modelo.Cep;
import br.com.modelo.Cliente;
import br.com.modelo.EstoquePreco;
import br.com.modelo.EstoqueProduto;
import br.com.modelo.FormaPagamento;
import br.com.modelo.Funcionario;
import br.com.modelo.Loja;
import br.com.modelo.Venda;
import br.com.teste.TrayIconDemo;
import java.awt.AWTException;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**
 *
 * @author Administrador
 */
public class FrmGerenciaConexao extends javax.swing.JFrame {

    static FrmGerenciaConexao conn;
    public static final String BASE_LOCAL = "local";
    public static final String BASE_REMOTA = "remote";
    private Loja lojaLocal;
    private Loja lojaRemoto;
    private Thread t;
    public Calendar cal;
    public Calendar calUltimaAtulizacaoRemoto;
    static TrayIcon trayIcon = null;
    private boolean isConectado = true;

    /**
     * Creates new form FrmGerenciaConexao
     */
    public FrmGerenciaConexao() {
        initComponents();

        cal = Calendar.getInstance();
        calUltimaAtulizacaoRemoto = Calendar.getInstance();
    }

    private void inicilizaLojas() {

        try {
            verificaImplantacao();

            lojaLocal = new LojaAppService().retornaLojaMaster(BASE_LOCAL);

            verificaAtualizacao(1000 * 60 * Sistema.configuracao.getMinutesToRun());
            cal.setTime(lojaLocal.getDataUltAlteracao());

            lojaRemoto = new LojaAppService().retornaLojaMaster(BASE_REMOTA);

            calUltimaAtulizacaoRemoto.setTime(lojaRemoto.getDataUltAlteracao());

        } catch (Exception e) {
            System.out.println("Loja local nao carregada");
            e.printStackTrace();
        }
    }

    private static void setTrayImg(boolean conectado) {
        if (conectado) {
            trayIcon.setImage(createImage("/imagens/icon.png", "tray icon"));
        } else {
            trayIcon.setImage(createImage("/imagens/iconWarning.png", "Sem Conexão"));
        }
    }

    private static void createAndShowTray() {
        //Check the SystemTray support
        if (!SystemTray.isSupported()) {
            System.out.println("SystemTray is not supported");
            return;
        }
        final PopupMenu popup = new PopupMenu();

        trayIcon = new TrayIcon(createImage("/imagens/icon.png", "tray icon"));
        trayIcon.setImageAutoSize(true);
        final SystemTray tray = SystemTray.getSystemTray();

        // Create a popup menu components
        MenuItem aboutItem = new MenuItem("Sobre");
        MenuItem displayMenu = new MenuItem("Atualizações");
        MenuItem displayOptions = new MenuItem("Configuração");
        MenuItem exitItem = new MenuItem("Encerrar");

        //Add components to popup menu
        popup.add(aboutItem);
        popup.addSeparator();
        popup.add(displayMenu);
        popup.add(displayOptions);
        popup.addSeparator();

        popup.add(exitItem);

        trayIcon.setPopupMenu(popup);
        trayIcon.setToolTip("Atualização remota base sispec");

        try {
            tray.add(trayIcon);
        } catch (AWTException e) {
            System.out.println("Componente não pôde ser adicionado a System tray.");
            return;
        }

        trayIcon.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String senha = Sistema.getInstance().getSenha();
                if (senha == null) {
                    // operação abortada
                } else if (senha.equals(Sistema.SENHA)) {
                    conn.setVisible(true);
                } else {
                    JOptionPane.showMessageDialog(null,
                            "Senha incorreta");
                }

            }
        });

        aboutItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
//                JOptionPane.showMessageDialog(null,
//                        "Eu estou rodando poe informação sobre mim");
            }
        });

        displayOptions.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String senha = Sistema.getInstance().getSenha();
                if (senha == null) {
                    // operação abortada
                } else if (senha.equals(Sistema.SENHA)) {
                    new FrmCadastraLoja().setVisible(true);
                } else {
                    JOptionPane.showMessageDialog(null,
                            "Senha incorreta");
                }
            }
        });

        exitItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tray.remove(trayIcon);
                System.exit(0);
            }
        });
    }

    //Obtain the image URL
    protected static Image createImage(String path, String description) {
        URL imageURL = TrayIconDemo.class.getResource(path);

        if (imageURL == null) {
            System.err.println("Resource not found: " + path);
            return null;
        } else {
            return (new ImageIcon(imageURL, description)).getImage();
        }
    }

    private void verificaAtualizacao(final long tempo) {

        t = new Thread(new Runnable() {
            @Override
            public void run() {

                while (true) {
                    try {
                        Thread.sleep(tempo);

                        Date dataAtual = Sistema.getInstance().retornaDataBanco(BASE_REMOTA);
                        Date atualizacao = lojaLocal.getDataUltAlteracao();

                        if (!isConectado) {
                            trayIcon.displayMessage("ShadowSync",
                                    "Conexão Recuperada com sucesso", TrayIcon.MessageType.INFO);
                            isConectado = true;
                            setTrayImg(isConectado);
                        }

                        verificaImplantacao();

                        atualizaCadastroProdutoLojasRede(atualizacao);
                        atualizaFormasPagto(atualizacao);
                        recebeFormasPagto(atualizacao);
                        atualizaFuncionarios(atualizacao);
                        recebeFuncionarios(atualizacao);

                        /*Utilizam a data do shadowsync_info*/
                        atualizaProdutos(dataAtual);
                        recebeProdutos(dataAtual);
                        enviaSaldos(dataAtual);
                        retornaSaldos(dataAtual);
                        enviaPrecos(dataAtual);
                        recebePrecos(dataAtual);
                        /*Utilizam a data do shadowsync_info*/

                        atualizaVendasWeb(atualizacao);
                        atualizaClientes(atualizacao);
                        atualizaLojas(atualizacao);
                        recebeLojas(atualizacao);

                        lojaLocal.setDataUltAlteracao(dataAtual);
                        new LojaAppService().altera(lojaLocal, BASE_LOCAL);

                        atualizaDataLoja(lojaLocal, BASE_REMOTA);
                        List<Loja> outrasLojas = new LojaAppService().listaOutrasLojas(lojaLocal, BASE_REMOTA);
                        for (Loja outrasLoja : outrasLojas) {
                            atualizaDataLoja(outrasLoja, BASE_LOCAL);
                        }
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    } catch (InfraEstruturaException ex) {
                        if (ex.getMessage()
                                .contains("org.hibernate.exception.GenericJDBCException: Could not open connection")) {

                            isConectado = false;
                            setTrayImg(isConectado);
                            trayIcon.displayMessage("ShadowSync(Sem conexão)",
                                    "Verifique sua conexão com a internet", TrayIcon.MessageType.ERROR);

                        } else {
                            ex.printStackTrace();
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });

        t.start();
        System.out.println("Thread started by main");
    }

    private void atualizaDataLoja(Loja loja, String session) throws Exception {

        LojaAppService app = new LojaAppService();
        try {
            Loja loja1 = app.retornaUmaLoja(loja.getEmpresa().getCnpj(), session);

            loja1.setDataUltAlteracao(loja.getDataUltAlteracao());

            app.altera(loja1, session);

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    private void atualizaLojas(Date ultimaAlteracao) throws Exception {
        if (!Sistema.configuracao.getFuncoes().get("P02")) {
            return;
        }

        LojaAppService app = new LojaAppService();
        try {
            List<Loja> lojas = app.listaLojas(ultimaAlteracao, BASE_LOCAL);

            System.out.println("Loja encontradas: " + lojas.size());

            app.atualizaCadastros(lojas, BASE_REMOTA);

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    private void recebeLojas(Date ultimaAlteracao) throws Exception {
        if (!Sistema.configuracao.getFuncoes().get("P03")) {
            return;
        }

        LojaAppService app = new LojaAppService();
        try {
            List<Loja> lojas = app.listaLojas(ultimaAlteracao, BASE_REMOTA);

            System.out.println("Loja encontradas: " + lojas.size());

            app.atualizaCadastros(lojas, BASE_LOCAL);

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    private void atualizaFormasPagto(Date ultimaAlteracao) throws Exception {
        if (!Sistema.configuracao.getFuncoes().get("P04")) {
            return;
        }

        FormaPagamentoAppService appPgto = new FormaPagamentoAppService();
        try {
            List<FormaPagamento> formas = appPgto.listaFormaPagamentos(ultimaAlteracao, BASE_LOCAL);

            System.out.println("Formas de Pagamento para atualizar: " + formas.size());

            appPgto.atualizaCadastros(formas, BASE_REMOTA);

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    private void recebeFormasPagto(Date ultimaAlteracao) throws Exception {
        if (!Sistema.configuracao.getFuncoes().get("P05")) {
            return;
        }

        FormaPagamentoAppService appPgto = new FormaPagamentoAppService();
        try {
            List<FormaPagamento> formas = appPgto
                    .listaFormaPagamentos(lojaLocal
                            .getShadowsyncInfo()
                            .getUltAltFormaPgto(), BASE_REMOTA);

            System.out.println("Formas de Pagamento para atualizar: " + formas.size());

            if (!formas.isEmpty()) {
                appPgto.atualizaCadastros(formas, BASE_LOCAL);

                lojaLocal.getShadowsyncInfo().setUltAltFormaPgto(ultimaAlteracao);
                new ShadowsyncInfoAppService().altera(lojaLocal.getShadowsyncInfo(), BASE_LOCAL);
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    private void atualizaFuncionarios(Date ultimaAlteracao) throws Exception {
        if (!Sistema.configuracao.getFuncoes().get("P06")) {
            return;
        }

        FuncionarioAppService appFunc = new FuncionarioAppService();
        try {
            List<Funcionario> funcionarios = appFunc.listaFuncionarios(ultimaAlteracao, BASE_LOCAL);

            System.out.println("Formas de Pagamento para atualizar: " + funcionarios.size());

            appFunc.atualizaCadastros(funcionarios, BASE_REMOTA);

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    private void recebeFuncionarios(Date ultimaAlteracao) throws Exception {
        if (!Sistema.configuracao.getFuncoes().get("P07")) {
            return;
        }

        FuncionarioAppService appFunc = new FuncionarioAppService();
        try {
            List<Funcionario> funcionarios = appFunc
                    .listaFuncionarios(lojaLocal
                            .getShadowsyncInfo()
                            .getUltAltFuncionario(), BASE_REMOTA);

            System.out.println("Formas de Pagamento para atualizar: " + funcionarios.size());

            if (!funcionarios.isEmpty()) {
                appFunc.atualizaCadastros(funcionarios, BASE_LOCAL);

                lojaLocal.getShadowsyncInfo().setUltAltFuncionario(ultimaAlteracao);
                new ShadowsyncInfoAppService().altera(lojaLocal.getShadowsyncInfo(), BASE_LOCAL);
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    private void atualizaProdutos(Date ultimaAlteracao) throws Exception {
        if (!Sistema.configuracao.getFuncoes().get("P08")) {
            return;
        }

        EstoqueProdutoAppService appEst = new EstoqueProdutoAppService();
        try {

            Calendar ultEnvioEstoque = Calendar.getInstance();
            ultEnvioEstoque.setTime(lojaLocal.getShadowsyncInfo().getUltEnvioEstoque());
            ultEnvioEstoque.add(Calendar.MINUTE, -10);

            List<EstoqueProduto> listaEstoque = appEst.listaEstoque(ultEnvioEstoque.getTime(),
                    BASE_LOCAL, lojaLocal);

            System.out.println("Estoque para atualizar: " + listaEstoque.size());

            if (!listaEstoque.isEmpty()) {

                appEst.atualizaCadastros(listaEstoque, BASE_REMOTA, lojaLocal);

                if (ultimaAlteracao != null) {
                    lojaLocal.getShadowsyncInfo().setUltEnvioEstoque(ultimaAlteracao);
                    new ShadowsyncInfoAppService().altera(lojaLocal.getShadowsyncInfo(), BASE_LOCAL);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    private void recebeProdutos(Date ultimaAlteracao) throws Exception {
        if (!Sistema.configuracao.getFuncoes().get("P09")) {
            return;
        }

        EstoqueProdutoAppService appEst = new EstoqueProdutoAppService();
        try {

            Calendar ultAltEstoque = Calendar.getInstance();
            ultAltEstoque.setTime(lojaLocal.getShadowsyncInfo().getUltAltEstoque());
            ultAltEstoque.add(Calendar.MINUTE, -10);

            List<EstoqueProduto> listaEstoque = appEst
                    .listaEstoqueOutrasLojas(ultAltEstoque.getTime(), BASE_REMOTA, lojaLocal);

            System.out.println("Estoque para atualizar: " + listaEstoque.size());

            if (!listaEstoque.isEmpty()) {
                appEst.atualizaCadastrosOutrasLojas(listaEstoque, BASE_LOCAL, lojaLocal);

                if (ultimaAlteracao != null) {
                    lojaLocal.getShadowsyncInfo().setUltAltEstoque(ultimaAlteracao);
                    new ShadowsyncInfoAppService().altera(lojaLocal.getShadowsyncInfo(), BASE_LOCAL);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    private void recebeProdutosManual(Date ultimaAlteracao) throws Exception {
        if (!Sistema.configuracao.getFuncoes().get("P09")) {
            return;
        }

        EstoqueProdutoAppService appEst = new EstoqueProdutoAppService();
        try {
            List<EstoqueProduto> listaEstoque = appEst
                    .listaEstoqueOutrasLojas(ultimaAlteracao, BASE_REMOTA, lojaLocal);

            System.out.println("Estoque para atualizar: " + listaEstoque.size());

            if (!listaEstoque.isEmpty()) {
                appEst.atualizaCadastrosOutrasLojas(listaEstoque, BASE_LOCAL, lojaLocal);
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    private void enviaSaldos(Date ultimaAlteracao) {
        if (!Sistema.configuracao.getFuncoes().get("P10")) {
            return;
        }

        EstoqueProdutoAppService appEst = new EstoqueProdutoAppService();
        try {

            Calendar ultEnvioSaldo = Calendar.getInstance();
            ultEnvioSaldo.setTime(lojaLocal.getShadowsyncInfo().getUltEnvioSaldo());
            ultEnvioSaldo.add(Calendar.MINUTE, -10);

            List<Object[]> listaEstoque = appEst.listaSaldos(ultEnvioSaldo.getTime(), BASE_LOCAL, lojaLocal);

            System.out.println("Estoque para atualizar: " + listaEstoque.size());

            if (!listaEstoque.isEmpty()) {

                appEst.atualizaEstoque(listaEstoque, BASE_REMOTA, lojaLocal);

                if (ultimaAlteracao != null) {
                    lojaLocal.getShadowsyncInfo().setUltEnvioSaldo(ultimaAlteracao);
                    new ShadowsyncInfoAppService()
                            .altera(lojaLocal.getShadowsyncInfo(), BASE_LOCAL);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }
    
    private void enviaSaldosManual(Date ultimaAlteracao) {
        if (!Sistema.configuracao.getFuncoes().get("P10")) {
            return;
        }

        EstoqueProdutoAppService appEst = new EstoqueProdutoAppService();
        try {

            List<Object[]> listaEstoque = appEst.listaSaldos(ultimaAlteracao, BASE_LOCAL, lojaLocal);

            System.out.println("Estoque para atualizar: " + listaEstoque.size());

            if (!listaEstoque.isEmpty()) {
                appEst.atualizaEstoque(listaEstoque, BASE_REMOTA, lojaLocal);
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    private void atualizaCadastroProdutoLojasRede(Date ultimaAlteracao) {
        if (!Sistema.configuracao.getFuncoes().get("P01")) {
            return;
        }

        EstoqueProdutoAppService appEst = new EstoqueProdutoAppService();
        try {
            appEst.atualizaCadastroProdutoLojasRede(lojaLocal, ultimaAlteracao, BASE_LOCAL);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    private void retornaSaldos(Date ultimaAlteracao) {
        if (!Sistema.configuracao.getFuncoes().get("P11")) {
            return;
        }

        EstoqueProdutoAppService appEst = new EstoqueProdutoAppService();
        try {

            Calendar ultAltSaldo = Calendar.getInstance();
            ultAltSaldo.setTime(lojaLocal.getShadowsyncInfo().getUltAltSaldo());
            ultAltSaldo.add(Calendar.MINUTE, -10);

            List<Object[]> listaEstoque = appEst
                    .listaSaldosOutrasLojas(ultAltSaldo.getTime(), BASE_REMOTA, lojaLocal);

            System.out.println("Estoque para atualizar: " + listaEstoque.size());

            if (!listaEstoque.isEmpty()) {
                appEst.atualizaEstoque(listaEstoque, BASE_LOCAL, lojaLocal);

                if (ultimaAlteracao != null) {
                    lojaLocal.getShadowsyncInfo().setUltAltSaldo(ultimaAlteracao);
                    new ShadowsyncInfoAppService()
                            .altera(lojaLocal.getShadowsyncInfo(), BASE_LOCAL);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    private void retornaSaldosManual(Date ultimaAlteracao) {

        if (!Sistema.configuracao.getFuncoes().get("P11")) {
            return;
        }

        EstoqueProdutoAppService appEst = new EstoqueProdutoAppService();
        try {
            List<Object[]> listaEstoque = appEst
                    .listaSaldosOutrasLojas(ultimaAlteracao, BASE_REMOTA, lojaLocal);

            System.out.println("Estoque para atualizar: " + listaEstoque.size());

            if (!listaEstoque.isEmpty()) {
                appEst.atualizaEstoque(listaEstoque, BASE_LOCAL, lojaLocal);
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    private void atualizaVendasWeb(Date ultimaAlteracao) throws Exception {
        if (!Sistema.configuracao.getFuncoes().get("P12")) {
            return;
        }

        VendaAppService appVenda = new VendaAppService();

        try {

            List<Venda> vendas = appVenda.listaVendasWeb(BASE_REMOTA);

            System.out.println("Vendas para atualizar: " + vendas.size());

            appVenda.atualizaLocal(vendas);

            appVenda.atualizaVendaRemoto(ultimaAlteracao);

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    private void atualizaClientes(Date ultimaAlteracao) throws Exception {
        if (!Sistema.configuracao.getFuncoes().get("P13")) {
            return;
        }

        ClienteAppService appCliente = new ClienteAppService();

        try {

            List<Cliente> clientes = appCliente.listaCliente(ultimaAlteracao, BASE_LOCAL);

            System.out.println("Clientes para atualizar: " + clientes.size());

            if (!clientes.isEmpty()) {

                Object[] obj = appCliente.retornaBairrosCepsCliente(clientes);

                List<Bairro> bairros = (List<Bairro>) obj[0];

                new BairroAppService().atualizaCadastro(bairros, BASE_REMOTA);

                List<Cep> ceps = (List<Cep>) obj[1];

                new CepAppService().atualizaCadastro(ceps, BASE_REMOTA);

                appCliente.atualizaCadastro(clientes, BASE_REMOTA);
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    private void enviaPrecos(Date ultimaAlteracao) throws Exception {
        if (!Sistema.configuracao.getFuncoes().get("P14")
                && !Sistema.configuracao.getFuncoes().get("P15")) {
            return;
        }

        EstoquePrecoAppService appEstoquePreco = new EstoquePrecoAppService();
        try {

//            Calendar ultEnvioPreco = Calendar.getInstance();
//            ultEnvioPreco.setTime(lojaLocal.getShadowsyncInfo().getUltEnvioPreco());
//            ultEnvioPreco.add(Calendar.MINUTE, -10);

            List<EstoquePreco> estoquePrecos = appEstoquePreco
                    .listaEstoquePreco(lojaLocal.getShadowsyncInfo().getUltEnvioPreco(), lojaLocal, BASE_LOCAL);
            System.out.println("Precos para atualizar: " + estoquePrecos.size());

            if (!estoquePrecos.isEmpty()) {
                appEstoquePreco.atualizaCadastros(estoquePrecos, BASE_REMOTA,
                        lojaLocal, Sistema.configuracao.getFuncoes().get("P15"));

                if (ultimaAlteracao != null) {
                    lojaLocal.getShadowsyncInfo().setUltEnvioPreco(ultimaAlteracao);
                    new ShadowsyncInfoAppService()
                            .altera(lojaLocal.getShadowsyncInfo(), BASE_LOCAL);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    private void recebePrecos(Date ultimaAlteracao) throws Exception {
        if (!Sistema.configuracao.getFuncoes().get("P16")) {
            return;
        }

        EstoquePrecoAppService appEstoquePreco = new EstoquePrecoAppService();
        try {

//            Calendar ultAltPreco = Calendar.getInstance();
//            ultAltPreco.setTime(lojaLocal.getShadowsyncInfo().getUltAltPreco());
//            ultAltPreco.add(Calendar.MINUTE, -10);
            List<EstoquePreco> estoquePrecos = appEstoquePreco
                    .listaEstoquePreco(lojaLocal.getShadowsyncInfo().getUltAltPreco(), BASE_REMOTA);
            System.out.println("Precos para atualizar: " + estoquePrecos.size());

            if (!estoquePrecos.isEmpty()) {

//                appEstoquePreco.atualizaCadastros(estoquePrecos, BASE_LOCAL,
//                        lojaLocal, true);
                
                appEstoquePreco.recebeCadastros(estoquePrecos, BASE_LOCAL);

                if (ultimaAlteracao != null) {
                    lojaLocal.getShadowsyncInfo().setUltAltPreco(ultimaAlteracao);
                    new ShadowsyncInfoAppService()
                            .altera(lojaLocal.getShadowsyncInfo(), BASE_LOCAL);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    private void verificaImplantacao() throws Exception {

        LojaAppService appLoja = new LojaAppService();

        appLoja.implantaLojaRemota();

        Calendar cal1 = Calendar.getInstance();
        cal1.set(1990, 1, 1);

        List<Loja> lojas = appLoja.listaLojas(cal1.getTime(), BASE_LOCAL);

        for (Loja loja : lojas) {

            if (loja.getDataImplantacaoRemota() != null) {
                continue;
            }

            if (loja.isLojaMaster()) {

                new EstoqueProdutoAppService().incluiParaOutrasLojas(loja, cal1.getTime(), BASE_LOCAL, BASE_REMOTA);

            } else {

                Loja l = appLoja.retornaUmaLoja(loja.getEmpresa().getCnpj(), BASE_REMOTA);
                if (l.getDataImplantacaoRemota() != null) {

                    List<Object[]> estoques = new EstoqueProdutoAppService().listaSaldos(cal1.getTime(), BASE_REMOTA, l);

                    new EstoqueProdutoAppService().atualizaEstoqueImplantacao(estoques, BASE_LOCAL, loja);
                }
            }
        }

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btP02 = new javax.swing.JButton();
        btP04 = new javax.swing.JButton();
        btP06 = new javax.swing.JButton();
        btP08 = new javax.swing.JButton();
        btP10 = new javax.swing.JButton();
        btP11 = new javax.swing.JButton();
        btP01 = new javax.swing.JButton();
        btP03 = new javax.swing.JButton();
        btP05 = new javax.swing.JButton();
        btP07 = new javax.swing.JButton();
        btP09 = new javax.swing.JButton();
        btP12 = new javax.swing.JButton();
        btP13 = new javax.swing.JButton();

        btP02.setText("P02 - Enviar Lojas");
        btP02.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btP02.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btP02ActionPerformed(evt);
            }
        });

        btP04.setText("P04 - Enviar Formas Pgto");
        btP04.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btP04.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btP04ActionPerformed(evt);
            }
        });

        btP06.setText("P06 - Enviar Funcionários");
        btP06.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btP06.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btP06ActionPerformed(evt);
            }
        });

        btP08.setText("P08 - Enviar Estoque");
        btP08.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btP08.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btP08ActionPerformed(evt);
            }
        });

        btP10.setText("P10 - Envia Saldos");
        btP10.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btP10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btP10ActionPerformed(evt);
            }
        });

        btP11.setText("P11 - Retorna Saldos");
        btP11.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btP11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btP11ActionPerformed(evt);
            }
        });

        btP01.setText("P01 - Prod. Lojas local");
        btP01.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btP01.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btP01ActionPerformed(evt);
            }
        });

        btP03.setText("P03 - Receber Lojas");
        btP03.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btP03.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btP03ActionPerformed(evt);
            }
        });

        btP05.setText("P05 - Receber Formas Pgto");
        btP05.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btP05.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btP05ActionPerformed(evt);
            }
        });

        btP07.setText("P07 - Receber Funcionários");
        btP07.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btP07.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btP07ActionPerformed(evt);
            }
        });

        btP09.setText("P09 - Receber Estoque");
        btP09.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btP09.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btP09ActionPerformed(evt);
            }
        });

        btP12.setText("P12 - Retorna Vendas");
        btP12.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btP12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btP12ActionPerformed(evt);
            }
        });

        btP13.setText("P13 - Enviar Cliente");
        btP13.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btP13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btP13ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btP04, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btP02, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btP01, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btP03, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btP05, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btP06, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btP07, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btP08, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btP10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btP11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btP09, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btP12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btP13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btP08)
                    .addComponent(btP01))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btP02)
                    .addComponent(btP09, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btP03)
                    .addComponent(btP10))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btP04)
                    .addComponent(btP11))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btP05)
                    .addComponent(btP12))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btP13)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btP06)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btP07)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btP02ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btP02ActionPerformed
        try {
            atualizaLojas(cal.getTime());

        } catch (Exception ex) {
            Logger.getLogger(FrmGerenciaConexao.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btP02ActionPerformed

    private void btP04ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btP04ActionPerformed
        try {

            FrmDefineData frm = new FrmDefineData(this, true);
            frm.setVisible(true);

            Date data;
            if (frm.getReturnStatus() == FrmDefineData.RET_OK) {
                data = frm.getDataEntrega();
            } else {
                return;
            }

            atualizaFormasPagto(data);
//            atualizaFormasPagto(cal.getTime());
        } catch (Exception ex) {
            Logger.getLogger(FrmGerenciaConexao.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btP04ActionPerformed

    private void btP06ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btP06ActionPerformed
        try {

            FrmDefineData frm = new FrmDefineData(this, true);
            frm.setVisible(true);

            Date data;
            if (frm.getReturnStatus() == FrmDefineData.RET_OK) {
                data = frm.getDataEntrega();
            } else {
                return;
            }

            atualizaFuncionarios(data);
//            atualizaFuncionarios(cal.getTime());
        } catch (Exception ex) {
            Logger.getLogger(FrmGerenciaConexao.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btP06ActionPerformed

    private void btP08ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btP08ActionPerformed
        try {

            FrmDefineData frm = new FrmDefineData(this, true);
            frm.setVisible(true);

            Date data;
            if (frm.getReturnStatus() == FrmDefineData.RET_OK) {
                data = frm.getDataEntrega();
            } else {
                return;
            }

            atualizaProdutos(data);

//            atualizaProdutos(lojaLocal.getDataUltAlteracao());
        } catch (Exception ex) {
            Logger.getLogger(FrmGerenciaConexao.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btP08ActionPerformed

    private void btP10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btP10ActionPerformed

        FrmDefineData frm = new FrmDefineData(this, true);
        frm.setVisible(true);

        Date data;
        if (frm.getReturnStatus() == FrmDefineData.RET_OK) {
            data = frm.getDataEntrega();
        } else {
            return;
        }

        enviaSaldosManual(data);
//        enviaSaldos(cal.getTime());
    }//GEN-LAST:event_btP10ActionPerformed

    private void btP11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btP11ActionPerformed

        FrmDefineData frm = new FrmDefineData(this, true);
        frm.setVisible(true);

        Date data;
        if (frm.getReturnStatus() == FrmDefineData.RET_OK) {
            data = frm.getDataEntrega();
        } else {
            return;
        }

        retornaSaldosManual(data);
//        retornaSaldos(cal.getTime());
    }//GEN-LAST:event_btP11ActionPerformed

    private void btP01ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btP01ActionPerformed
        atualizaCadastroProdutoLojasRede(cal.getTime());
    }//GEN-LAST:event_btP01ActionPerformed

    private void btP03ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btP03ActionPerformed
        try {
            this.recebeLojas(cal.getTime());

        } catch (Exception ex) {
            Logger.getLogger(FrmGerenciaConexao.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btP03ActionPerformed

    private void btP05ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btP05ActionPerformed
        try {

            FrmDefineData frm = new FrmDefineData(this, true);
            frm.setVisible(true);

            Date data;
            if (frm.getReturnStatus() == FrmDefineData.RET_OK) {
                data = frm.getDataEntrega();
            } else {
                return;
            }

            this.recebeFormasPagto(data);
//            this.recebeFormasPagto(cal.getTime());
        } catch (Exception ex) {
            Logger.getLogger(FrmGerenciaConexao.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btP05ActionPerformed

    private void btP07ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btP07ActionPerformed
        try {

            FrmDefineData frm = new FrmDefineData(this, true);
            frm.setVisible(true);

            Date data;
            if (frm.getReturnStatus() == FrmDefineData.RET_OK) {
                data = frm.getDataEntrega();
            } else {
                return;
            }

            this.recebeFuncionarios(data);
//            this.recebeFuncionarios(cal.getTime());
        } catch (Exception ex) {
            Logger.getLogger(FrmGerenciaConexao.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btP07ActionPerformed

    private void btP09ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btP09ActionPerformed
        try {

            FrmDefineData frm = new FrmDefineData(this, true);
            frm.setVisible(true);

            Date data;
            if (frm.getReturnStatus() == FrmDefineData.RET_OK) {
                data = frm.getDataEntrega();
            } else {
                return;
            }

            this.recebeProdutosManual(data);
//            this.recebeProdutos(cal.getTime());

        } catch (Exception ex) {
            Logger.getLogger(FrmGerenciaConexao.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btP09ActionPerformed

    private void btP12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btP12ActionPerformed
        try {
            atualizaVendasWeb(cal.getTime());

        } catch (Exception ex) {
            Logger.getLogger(FrmGerenciaConexao.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btP12ActionPerformed

    private void btP13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btP13ActionPerformed
        try {

            FrmDefineData frm = new FrmDefineData(this, true);
            frm.setVisible(true);

            Date data;
            if (frm.getReturnStatus() == FrmDefineData.RET_OK) {
                data = frm.getDataEntrega();
            } else {
                return;
            }

            atualizaClientes(data);
//            atualizaClientes(cal.getTime());
        } catch (Exception ex) {
            Logger.getLogger(FrmGerenciaConexao.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btP13ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmGerenciaConexao.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmGerenciaConexao.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmGerenciaConexao.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmGerenciaConexao.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {

                conn = new FrmGerenciaConexao();
                try {

                    Sistema.getInstance().lockInstancia();
//                    new Migracao().rodaMigracoes();

                    conn.inicilizaLojas();
                    createAndShowTray();

                    DAO dao = new DAO(Sistema.configuracao.getIpLocal(),
                            Sistema.configuracao.getPortaLocal(),
                            Sistema.configuracao.getDatabaseLocal(),
                            Sistema.configuracao.getUserLocal(),
                            Sistema.configuracao.getPasswordLocal());

                    if (!dao.connect()) {
                        throw new Exception("Conexão LOCAL não estabelecida");
                    }
                    dao.close();

                    dao = new DAO(Sistema.configuracao.getIpRemote(),
                            Sistema.configuracao.getPortaRemote(),
                            Sistema.configuracao.getDatabaseRemote(),
                            Sistema.configuracao.getUserRemote(),
                            Sistema.configuracao.getPasswordRemote());

                    if (!dao.connect()) {
                        throw new Exception("Conexão REMOTA não estabelecida");
                    }
                    dao.close();

                    trayIcon.displayMessage("ShadowSync",
                            "Módulo em execução", TrayIcon.MessageType.INFO);
                } catch (Exception e) {
                    e.printStackTrace();
                    trayIcon.displayMessage("ShadowSync(Sem conexão)",
                            e.getMessage(), TrayIcon.MessageType.ERROR);
                }
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btP01;
    private javax.swing.JButton btP02;
    private javax.swing.JButton btP03;
    private javax.swing.JButton btP04;
    private javax.swing.JButton btP05;
    private javax.swing.JButton btP06;
    private javax.swing.JButton btP07;
    private javax.swing.JButton btP08;
    private javax.swing.JButton btP09;
    private javax.swing.JButton btP10;
    private javax.swing.JButton btP11;
    private javax.swing.JButton btP12;
    private javax.swing.JButton btP13;
    // End of variables declaration//GEN-END:variables
}
