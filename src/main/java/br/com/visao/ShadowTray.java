package br.com.visao;

/**
 *
 * @author Administrador
 */
/*
 * ShadowTray.java
 */

import br.com.teste.*;
import java.awt.*;
import java.awt.event.*;
import java.net.URL;
import javax.swing.*;

public class ShadowTray {
    
    public static void main(String[] args) {
        
        
        URL config = TrayIconDemo.class.getResource("/config.xml");
        
        if (config == null) {
            FrmCadastraLoja tela = new FrmCadastraLoja(null, true);
            tela.setVisible(true);            
        } else {            
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    createAndShowGUI();
                }
            });
        }
    }
    
    private static void createAndShowGUI() {
        //Check the SystemTray support
        if (!SystemTray.isSupported()) {
            System.out.println("SystemTray is not supported");
            return;
        }
        final PopupMenu popup = new PopupMenu();       
        
        final TrayIcon trayIcon =
                new TrayIcon(createImage("/imagens/icon.png", "tray icon"));
        trayIcon.setImageAutoSize(true);
        final SystemTray tray = SystemTray.getSystemTray();
        
        // Create a popup menu components
        MenuItem aboutItem = new MenuItem("Sobre");
        Menu displayMenu = new Menu("Atualizações");
        MenuItem exitItem = new MenuItem("Encerrar");
        
        //Add components to popup menu
        popup.add(aboutItem);
        popup.addSeparator();
        popup.add(displayMenu);
        popup.add(exitItem);
        
        trayIcon.setPopupMenu(popup);
                
        try {
            tray.add(trayIcon);
        } catch (AWTException e) {
            System.out.println("Componente não pôde ser adicionado a System tray.");
            return;
        }
        
        trayIcon.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                FrmGerenciaConexao janela = new FrmGerenciaConexao();
                janela.setVisible(true);
            }
        });
        
        aboutItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null,
                        "Eu estou rodando poe informação sobre mim");
            }
        });        
        
        exitItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tray.remove(trayIcon);
                System.exit(0);
            }
        });
    }
    
    //Obtain the image URL
    protected static Image createImage(String path, String description) {
        URL imageURL = TrayIconDemo.class.getResource(path);
        
        if (imageURL == null) {
            System.err.println("Resource not found: " + path);
            return null;
        } else {
            return (new ImageIcon(imageURL, description)).getImage();
        }
    }
}
