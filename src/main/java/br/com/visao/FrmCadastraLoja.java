package br.com.visao;

import br.com.controle.DAO;
import br.com.controle.Sistema;
import br.com.modelo.Empresa;
import br.com.sisbrastools.encrypt.Encriptador;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import javax.swing.JOptionPane;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;

/**
 *
 * @author pc2
 */
public class FrmCadastraLoja extends javax.swing.JDialog {

    public FrmCadastraLoja() {
        super();
        initComponents();
        verificaCadastro();
    }

    public FrmCadastraLoja(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        txtPasswdLocal = new javax.swing.JPasswordField();
        txtUserLocal = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        txtPortaLocal = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtIPlocal = new javax.swing.JTextField();
        txtDBLocal = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        btConnexaoLocal = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        txtPasswdRemoto = new javax.swing.JPasswordField();
        txtIPRemoto = new javax.swing.JTextField();
        txtPortaRemoto = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtUserRemoto = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        txtDBRemoto = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        btConexaoRemoto = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        txtRzSocialLocal = new javax.swing.JTextField();
        txtRzSocialRemoto = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        txtCNPJRemoto = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        txtCNPJLocal = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        chkP01 = new javax.swing.JCheckBox();
        chkP04 = new javax.swing.JCheckBox();
        chkP08 = new javax.swing.JCheckBox();
        chkP09 = new javax.swing.JCheckBox();
        chkP11 = new javax.swing.JCheckBox();
        chkP02 = new javax.swing.JCheckBox();
        chkP03 = new javax.swing.JCheckBox();
        chkP06 = new javax.swing.JCheckBox();
        chkP07 = new javax.swing.JCheckBox();
        chkP05 = new javax.swing.JCheckBox();
        chkP10 = new javax.swing.JCheckBox();
        chkP12 = new javax.swing.JCheckBox();
        chkP13 = new javax.swing.JCheckBox();
        chkP16 = new javax.swing.JCheckBox();
        jPanel5 = new javax.swing.JPanel();
        rbP14 = new javax.swing.JRadioButton();
        rbP15 = new javax.swing.JRadioButton();
        btSalvar = new javax.swing.JButton();
        btFechar = new javax.swing.JButton();
        jLabel15 = new javax.swing.JLabel();
        txtExecucao = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Servidor local"));

        jLabel4.setText("Senha");

        txtPasswdLocal.setText("jPasswordField1");

        jLabel1.setText("I.P. server local");

        jLabel3.setText("Usuário");

        jLabel2.setText("Porta local");

        jLabel13.setText("Database");

        btConnexaoLocal.setText("Testar Conexão");
        btConnexaoLocal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btConnexaoLocalActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtDBLocal)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(txtUserLocal))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addGap(45, 45, 45))
                            .addComponent(txtPasswdLocal, javax.swing.GroupLayout.DEFAULT_SIZE, 87, Short.MAX_VALUE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(0, 92, Short.MAX_VALUE))
                            .addComponent(txtIPlocal))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(txtPortaLocal, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel13)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(btConnexaoLocal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtIPlocal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPortaLocal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel13)
                .addGap(1, 1, 1)
                .addComponent(txtDBLocal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtUserLocal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPasswdLocal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btConnexaoLocal))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Servidor Remoto"));

        txtPasswdRemoto.setText("jPasswordField1");

        jLabel7.setText("I.P. server remoto");

        jLabel6.setText("Porta");

        jLabel5.setText("Usuário");

        jLabel8.setText("Senha");

        jLabel14.setText("Database");

        btConexaoRemoto.setText("Testar Conexão");
        btConexaoRemoto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btConexaoRemotoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtDBRemoto)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtUserRemoto)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8)
                            .addComponent(txtPasswdRemoto, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel14)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(btConexaoRemoto, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(txtIPRemoto))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtPortaRemoto, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6))))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtIPRemoto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPortaRemoto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel14)
                .addGap(1, 1, 1)
                .addComponent(txtDBRemoto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtUserRemoto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPasswdRemoto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btConexaoRemoto))
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel9.setText("CNPJ Empresa local");

        txtRzSocialLocal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtRzSocialLocalActionPerformed(evt);
            }
        });

        txtRzSocialRemoto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtRzSocialRemotoActionPerformed(evt);
            }
        });

        jLabel11.setText("Razão Social");

        jLabel10.setText("Razão Social");

        jLabel12.setText("CNPJ Matriz");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel9)
                    .addComponent(jLabel12)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(txtCNPJRemoto, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(txtCNPJLocal, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtRzSocialLocal)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel10)
                            .addComponent(jLabel11))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(txtRzSocialRemoto))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(jLabel10))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCNPJLocal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtRzSocialLocal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(jLabel11))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCNPJRemoto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtRzSocialRemoto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6))
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        chkP01.setText("P01 - Segmentar estoque ");

        chkP04.setText("P04 - Enviar Formas Pgto");

        chkP08.setText("P08 - Enviar Estoque");

        chkP09.setText("P09 - Receber Estoque");

        chkP11.setText("P11 - Retorna Saldos");

        chkP02.setText("P02 - Enviar Lojas");

        chkP03.setText("P03 - Receber Lojas");

        chkP06.setText("P06 - Enviar Funcionários");

        chkP07.setText("P07 - Receber Funcionários");
        chkP07.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkP07ActionPerformed(evt);
            }
        });

        chkP05.setText("P05 - Receber Formas Pgto");

        chkP10.setText("P10 - Enviar Saldos");

        chkP12.setText("P12 - Atualizar Pedidos Web");

        chkP13.setText("P13 - Enviar Clientes");

        chkP16.setText("P16 - Retornar Preços");

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder("Enviar Preços"));

        rbP14.setText("P14 - Informar Preços Desta Loja");
        rbP14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbP14ActionPerformed(evt);
            }
        });

        rbP15.setText("P15 - Atualizar Preços Todas as lojas");
        rbP15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbP15ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(rbP14)
                .addGap(18, 18, 18)
                .addComponent(rbP15)
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rbP14)
                    .addComponent(rbP15))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(chkP02)
                    .addComponent(chkP03)
                    .addComponent(chkP04)
                    .addComponent(chkP05)
                    .addComponent(chkP01)
                    .addComponent(chkP06)
                    .addComponent(chkP07))
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(53, 53, 53)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(chkP10)
                            .addComponent(chkP11)
                            .addComponent(chkP16)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(chkP08)
                                    .addComponent(chkP09))
                                .addGap(26, 26, 26)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(chkP12)
                                    .addComponent(chkP13))))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 53, Short.MAX_VALUE)
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(chkP01)
                            .addComponent(chkP08))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(chkP02)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(chkP03)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(chkP04)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(chkP05)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(chkP06)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(chkP07))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(chkP09)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(chkP12)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(chkP13)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(chkP10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(chkP11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(chkP16)
                .addContainerGap())
        );

        btSalvar.setText("Salvar");
        btSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSalvarActionPerformed(evt);
            }
        });

        btFechar.setText("Fechar");
        btFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btFecharActionPerformed(evt);
            }
        });

        jLabel15.setText("Execução:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(9, 9, 9)
                        .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtExecucao, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btSalvar)
                        .addGap(58, 58, 58)
                        .addComponent(btFechar))
                    .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel15)
                        .addComponent(txtExecucao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btFechar)
                        .addComponent(btSalvar))))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtRzSocialLocalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtRzSocialLocalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtRzSocialLocalActionPerformed

    private void txtRzSocialRemotoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtRzSocialRemotoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtRzSocialRemotoActionPerformed

    private void btConnexaoLocalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btConnexaoLocalActionPerformed
        try {
            DAO dao = new DAO(txtIPlocal.getText(), txtPortaLocal.getText(), txtDBLocal.getText(), txtUserLocal.getText(), String.valueOf(txtPasswdLocal.getPassword()));

            dao.connect();

            Empresa empresa = dao.retornaEmpresa();

            txtCNPJLocal.setText(empresa.getCnpj());
            txtRzSocialLocal.setText(empresa.getNome());

            dao.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btConnexaoLocalActionPerformed

    private void btConexaoRemotoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btConexaoRemotoActionPerformed
        try {
            DAO dao = new DAO(txtIPRemoto.getText(), txtPortaRemoto.getText(), txtDBRemoto.getText(), txtUserRemoto.getText(), String.valueOf(txtPasswdRemoto.getPassword()));

            dao.connect();

            Empresa empresa = dao.retornaEmpresa();

            txtCNPJRemoto.setText(empresa.getCnpj());
            txtRzSocialRemoto.setText(empresa.getNome());

            dao.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btConexaoRemotoActionPerformed

    private void btSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSalvarActionPerformed
        Document doc = new Document();
        Element root = new Element("config");
        root.setAttribute("execucao", (txtExecucao.getText().isEmpty() ? "2" : txtExecucao.getText()));

        Element conexoes = new Element("conexoes");
        Element conexao;

        conexao = new Element("connLocal");
        conexao.addContent(new Element("ip").setText(txtIPlocal.getText()));
        conexao.addContent(new Element("porta").setText(txtPortaLocal.getText()));
        conexao.addContent(new Element("database").setText(txtDBLocal.getText()));
        conexao.addContent(new Element("user").setText(txtUserLocal.getText()));

        String senha = String.valueOf(txtPasswdLocal.getPassword());
        Encriptador encript = new Encriptador(senha);
        try {
            senha = encript.doEncrypt(new File(".\\chaveL.key"));
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(rootPane, "Erro ao configurar a senha"
                    + "\n" + ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            return;
        }
        conexao.addContent(new Element("password").setText(senha));
        conexoes.addContent(conexao);

        conexao = new Element("connRemote");
        conexao.addContent(new Element("ip").setText(txtIPRemoto.getText()));
        conexao.addContent(new Element("porta").setText(txtPortaRemoto.getText()));
        conexao.addContent(new Element("database").setText(txtDBRemoto.getText()));
        conexao.addContent(new Element("user").setText(txtUserRemoto.getText()));

        senha = String.valueOf(txtPasswdRemoto.getPassword());
        encript = new Encriptador(senha);
        try {
            senha = encript.doEncrypt(new File(".\\chaveR.key"));
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(rootPane, "Erro ao configurar a senha"
                    + "\n" + ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            return;
        }
        conexao.addContent(new Element("password").setText(senha));
        conexoes.addContent(conexao);

        root.addContent(conexoes);

        Element params = new Element("params");
        params.addContent(new Element("P01").setText(String.valueOf(chkP01.isSelected())));
        params.addContent(new Element("P02").setText(String.valueOf(chkP02.isSelected())));
        params.addContent(new Element("P03").setText(String.valueOf(chkP03.isSelected())));
        params.addContent(new Element("P04").setText(String.valueOf(chkP04.isSelected())));
        params.addContent(new Element("P05").setText(String.valueOf(chkP05.isSelected())));
        params.addContent(new Element("P06").setText(String.valueOf(chkP06.isSelected())));
        params.addContent(new Element("P07").setText(String.valueOf(chkP07.isSelected())));
        params.addContent(new Element("P08").setText(String.valueOf(chkP08.isSelected())));
        params.addContent(new Element("P09").setText(String.valueOf(chkP09.isSelected())));
        params.addContent(new Element("P10").setText(String.valueOf(chkP10.isSelected())));
        params.addContent(new Element("P11").setText(String.valueOf(chkP11.isSelected())));
        params.addContent(new Element("P12").setText(String.valueOf(chkP12.isSelected())));
        params.addContent(new Element("P13").setText(String.valueOf(chkP13.isSelected())));
        params.addContent(new Element("P14").setText(String.valueOf(rbP14.isSelected())));
        params.addContent(new Element("P15").setText(String.valueOf(rbP15.isSelected())));
        params.addContent(new Element("P16").setText(String.valueOf(chkP16.isSelected())));

        root.addContent(params);

        doc.setRootElement(root);

        XMLOutputter xout = new XMLOutputter();
        try {
//            File f = new File(Sistema.class.getResource("/").getPath() + "config.xml");
            File f = new File(Sistema.PATH_CONFIG);
            OutputStream out = new FileOutputStream(f);
            xout.output(doc, out);
            out.close();
            JOptionPane.showMessageDialog(rootPane, "Configuração salva com sucesso", "Tratamento de execução", JOptionPane.INFORMATION_MESSAGE);
        } catch (IOException ex) {
            ex.printStackTrace();

        }

    }//GEN-LAST:event_btSalvarActionPerformed

    private void chkP07ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkP07ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_chkP07ActionPerformed

    private void btFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btFecharActionPerformed
        this.dispose();
    }//GEN-LAST:event_btFecharActionPerformed

    private void rbP14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbP14ActionPerformed
        if (rbP14.isSelected()) {
            rbP15.setSelected(false);
        }
    }//GEN-LAST:event_rbP14ActionPerformed

    private void rbP15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbP15ActionPerformed
        if (rbP15.isSelected()) {
            rbP14.setSelected(false);
        }
    }//GEN-LAST:event_rbP15ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btConexaoRemoto;
    private javax.swing.JButton btConnexaoLocal;
    private javax.swing.JButton btFechar;
    private javax.swing.JButton btSalvar;
    private javax.swing.JCheckBox chkP01;
    private javax.swing.JCheckBox chkP02;
    private javax.swing.JCheckBox chkP03;
    private javax.swing.JCheckBox chkP04;
    private javax.swing.JCheckBox chkP05;
    private javax.swing.JCheckBox chkP06;
    private javax.swing.JCheckBox chkP07;
    private javax.swing.JCheckBox chkP08;
    private javax.swing.JCheckBox chkP09;
    private javax.swing.JCheckBox chkP10;
    private javax.swing.JCheckBox chkP11;
    private javax.swing.JCheckBox chkP12;
    private javax.swing.JCheckBox chkP13;
    private javax.swing.JCheckBox chkP16;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JRadioButton rbP14;
    private javax.swing.JRadioButton rbP15;
    private javax.swing.JTextField txtCNPJLocal;
    private javax.swing.JTextField txtCNPJRemoto;
    private javax.swing.JTextField txtDBLocal;
    private javax.swing.JTextField txtDBRemoto;
    private javax.swing.JTextField txtExecucao;
    private javax.swing.JTextField txtIPRemoto;
    private javax.swing.JTextField txtIPlocal;
    private javax.swing.JPasswordField txtPasswdLocal;
    private javax.swing.JPasswordField txtPasswdRemoto;
    private javax.swing.JTextField txtPortaLocal;
    private javax.swing.JTextField txtPortaRemoto;
    private javax.swing.JTextField txtRzSocialLocal;
    private javax.swing.JTextField txtRzSocialRemoto;
    private javax.swing.JTextField txtUserLocal;
    private javax.swing.JTextField txtUserRemoto;
    // End of variables declaration//GEN-END:variables

    private void verificaCadastro() {

        txtExecucao.setText(String.valueOf(Sistema.configuracao.getMinutesToRun()));

        txtIPlocal.setText(Sistema.configuracao.getIpLocal());
        txtPortaLocal.setText(Sistema.configuracao.getPortaLocal());
        txtDBLocal.setText(Sistema.configuracao.getDatabaseLocal());
        txtUserLocal.setText(Sistema.configuracao.getUserLocal());
        txtPasswdLocal.setText(Sistema.configuracao.getPasswordLocal());

        txtIPRemoto.setText(Sistema.configuracao.getIpRemote());
        txtPortaRemoto.setText(Sistema.configuracao.getPortaRemote());
        txtDBRemoto.setText(Sistema.configuracao.getDatabaseRemote());
        txtUserRemoto.setText(Sistema.configuracao.getUserRemote());
        txtPasswdRemoto.setText(Sistema.configuracao.getPasswordRemote());

        chkP01.setSelected(Sistema.configuracao.getFuncoes().get("P01"));
        chkP02.setSelected(Sistema.configuracao.getFuncoes().get("P02"));
        chkP03.setSelected(Sistema.configuracao.getFuncoes().get("P03"));
        chkP04.setSelected(Sistema.configuracao.getFuncoes().get("P04"));
        chkP05.setSelected(Sistema.configuracao.getFuncoes().get("P05"));
        chkP06.setSelected(Sistema.configuracao.getFuncoes().get("P06"));
        chkP07.setSelected(Sistema.configuracao.getFuncoes().get("P07"));
        chkP08.setSelected(Sistema.configuracao.getFuncoes().get("P08"));
        chkP09.setSelected(Sistema.configuracao.getFuncoes().get("P09"));
        chkP10.setSelected(Sistema.configuracao.getFuncoes().get("P10"));
        chkP11.setSelected(Sistema.configuracao.getFuncoes().get("P11"));
        chkP12.setSelected(Sistema.configuracao.getFuncoes().get("P12"));
        chkP13.setSelected(Sistema.configuracao.getFuncoes().get("P13"));
        rbP14.setSelected(Sistema.configuracao.getFuncoes().get("P14"));
        rbP15.setSelected(Sistema.configuracao.getFuncoes().get("P15"));
        chkP16.setSelected(Sistema.configuracao.getFuncoes().get("P16"));

    }
}
