package br.com.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.service.jdbc.connections.spi.ConnectionProvider;

/**
 *
 * @author SAMUEL
 */
public class Migracao {

    public void rodaMigracoes() throws SQLException {

        File arquivo = new File("./src/main/resources/sql/shadowsync_scripts.sql");

        // roda migrações no banco local
        SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) HibernateUtil.getSessionFactory();
        ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();

        Connection conn = connectionProvider.getConnection();

        executaSqlScript(conn, arquivo);
        //End

        // roda migrações no banco remoto
        sessionFactoryImplementation = (SessionFactoryImplementor) HibernateUtil.getSessionFactoryRemote();
        connectionProvider = sessionFactoryImplementation.getConnectionProvider();

        conn = connectionProvider.getConnection();

        executaSqlScript(conn, arquivo);
        // end
    }

    private void executaSqlScript(Connection conn, File inputFile) {
        // Delimiter
        String delimiter = ";";
        // Create scanner
        Scanner scanner;
        try {
            scanner = new Scanner(inputFile).useDelimiter(delimiter);
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
            return;
        }
        // Loop through the SQL file statements 
        Statement currentStatement = null;
        while (scanner.hasNext()) {
            try {
                // Get statement 
                String rawStatement = scanner.next() + delimiter;
                
                // Execute statement
                currentStatement = conn.createStatement();
                currentStatement.execute(rawStatement);

            } catch (Exception e) {
//                e.printStackTrace();
                System.out.println(e.getMessage());
            } finally {
                // Release resources
                if (currentStatement != null) {
                    try {
                        conn.commit();
                        currentStatement.close();
                    } catch (SQLException e) {
//                        e.printStackTrace();
                        System.out.println(e.getMessage());
                    }
                }
                currentStatement = null;
            }
        }
    }
}
