package br.com.util;

import br.com.sisbrastools.encrypt.Decriptador;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.JDOMParseException;
import org.jdom.input.SAXBuilder;

/**
 *
 * @author Administrador
 */
public class Config {
    
    String pathConfig;
    private String ipLocal;
    private String portaLocal;
    private String databaseLocal;
    private String userLocal;
    private String passwordLocal;
    private String ipRemote;
    private String portaRemote;
    private String databaseRemote;
    private String userRemote;
    private String passwordRemote;
    private int minutesToRun;
    private Map<String, Boolean> funcoes = new HashMap();

    public Config() {
    }

    public Config(String pathConfig) throws Exception {
        this.pathConfig = pathConfig;
        this.inicializar();
    }
    
    private void inicializar() throws Exception{
        File f = new File(this.pathConfig);
        if (f.isFile()) {            
            //TRATAR ARQUIVO XML NO BANCO
            SAXBuilder sb = new SAXBuilder();
            Document d;

            try{
                d = sb.build(f);
                //Recuperamos o elemento root  
                Element raiz = d.getRootElement();                
                try{
                    this.minutesToRun = Integer.valueOf(raiz.getAttributeValue("execucao"));
                }catch(Exception e){}
                
                //Recuperamos os elementos filhos (children)  
                List elements = raiz.getChildren();
                Iterator i = elements.iterator();
                
                //Iteramos com os elementos filhos, e filhos do dos filhos  
                while (i.hasNext()) {
                    Element element = (Element) i.next();
                    switch (element.getName()) {
                        case "conexoes":
                            List elConnLocal = element.getChildren("connLocal");
                            Iterator itCxLocal = elConnLocal.iterator();
                            while (itCxLocal.hasNext()) {
                                Element elCxLocal = (Element) itCxLocal.next();
                                ipLocal = elCxLocal.getChildText("ip");
                                portaLocal = elCxLocal.getChildText("porta");
                                databaseLocal = elCxLocal.getChildText("database");
                                userLocal = elCxLocal.getChildText("user");
                                
                                passwordLocal = elCxLocal.getChildText("password");
                                Decriptador decript = new Decriptador(passwordLocal);
                                passwordLocal =  decript.doDecrypt(new File(".\\chaveL.key"));
                            }
                            
                            List elConnRemote = element.getChildren("connRemote");
                            Iterator itCxRemote = elConnRemote.iterator();
                            while (itCxRemote.hasNext()) {
                                Element elCxRemote = (Element) itCxRemote.next();
                                ipRemote = elCxRemote.getChildText("ip");
                                portaRemote = elCxRemote.getChildText("porta");
                                databaseRemote = elCxRemote.getChildText("database");
                                userRemote = elCxRemote.getChildText("user");
                                
                                passwordRemote = elCxRemote.getChildText("password");
                                Decriptador decript = new Decriptador(passwordRemote);
                                passwordRemote =  decript.doDecrypt(new File(".\\chaveR.key"));
                            }
                            break;
                        case "params":
                            funcoes.put("P01", Boolean.valueOf(element.getChildText("P01")));
                            funcoes.put("P02", Boolean.valueOf(element.getChildText("P02")));
                            funcoes.put("P03", Boolean.valueOf(element.getChildText("P03")));
                            funcoes.put("P04", Boolean.valueOf(element.getChildText("P04")));
                            funcoes.put("P05", Boolean.valueOf(element.getChildText("P05")));
                            funcoes.put("P06", Boolean.valueOf(element.getChildText("P06")));
                            funcoes.put("P07", Boolean.valueOf(element.getChildText("P07")));
                            funcoes.put("P08", Boolean.valueOf(element.getChildText("P08")));
                            funcoes.put("P09", Boolean.valueOf(element.getChildText("P09")));
                            funcoes.put("P10", Boolean.valueOf(element.getChildText("P10")));
                            funcoes.put("P11", Boolean.valueOf(element.getChildText("P11")));                            
                            funcoes.put("P12", Boolean.valueOf(element.getChildText("P12")));                            
                            funcoes.put("P13", Boolean.valueOf(element.getChildText("P13")));                            
                            funcoes.put("P14", Boolean.valueOf(element.getChildText("P14")));                            
                            funcoes.put("P15", Boolean.valueOf(element.getChildText("P15")));                            
                            funcoes.put("P16", Boolean.valueOf(element.getChildText("P16")));                            
                            break;                        
                    }
                }
                
            }catch (JDOMParseException e) {
                System.out.println("Erro de parse");                
            } catch(JDOMException e){
                e.printStackTrace();            
            } catch(IOException e){
                e.printStackTrace();
            }
        }
    }

    public String getPathConfig() {
        return pathConfig;
    }

    public void setPathConfig(String pathConfig) {
        this.pathConfig = pathConfig;
    }

    public int getMinutesToRun() {
        if (minutesToRun < 1) {
            return 1;
        } else {
            return minutesToRun;
        }
    }

    public void setMinutesToRun(int minutesToRun) {
        this.minutesToRun = minutesToRun;
    }

    public String getIpLocal() {
        return ipLocal;
    }

    public void setIpLocal(String ipLocal) {
        this.ipLocal = ipLocal;
    }

    public String getPortaLocal() {
        return portaLocal;
    }

    public void setPortaLocal(String portaLocal) {
        this.portaLocal = portaLocal;
    }

    public String getDatabaseLocal() {
        return databaseLocal;
    }

    public void setDatabaseLocal(String databaseLocal) {
        this.databaseLocal = databaseLocal;
    }

    public String getUserLocal() {
        return userLocal;
    }

    public void setUserLocal(String userLocal) {
        this.userLocal = userLocal;
    }

    public String getPasswordLocal() {
        return passwordLocal;
    }

    public void setPasswordLocal(String passwordLocal) {
        this.passwordLocal = passwordLocal;
    }

    public String getIpRemote() {
        return ipRemote;
    }

    public void setIpRemote(String ipRemote) {
        this.ipRemote = ipRemote;
    }

    public String getPortaRemote() {
        return portaRemote;
    }

    public void setPortaRemote(String portaRemote) {
        this.portaRemote = portaRemote;
    }

    public String getDatabaseRemote() {
        return databaseRemote;
    }

    public void setDatabaseRemote(String databaseRemote) {
        this.databaseRemote = databaseRemote;
    }

    public String getUserRemote() {
        return userRemote;
    }

    public void setUserRemote(String userRemote) {
        this.userRemote = userRemote;
    }

    public String getPasswordRemote() {
        return passwordRemote;
    }

    public void setPasswordRemote(String passwordRemote) {
        this.passwordRemote = passwordRemote;
    }

    public Map<String, Boolean> getFuncoes() {
        return funcoes;
    }

    public void setFuncoes(Map<String, Boolean> funcoes) {
        this.funcoes = funcoes;
    }
    
    public String getURLConnectionLocal(){
        return "jdbc:postgresql://"+this.ipLocal+":"+this.portaLocal+"/"+this.databaseLocal;        
    }
    
    public String getURLConnectionRemote(){
        return "jdbc:postgresql://"+this.ipRemote+":"+this.portaRemote+"/"+this.databaseRemote;        
    }
    
    public void imprime(){
        System.out.println("ip local: "+this.ipLocal);
        System.out.println("porta...: "+this.portaLocal);
        System.out.println("banco...: "+this.databaseLocal);
        System.out.println("user...l: "+this.userLocal);
        System.out.println("senha...: "+this.passwordLocal);
        
        System.out.println("ip web..: "+this.ipRemote);
        System.out.println("porta...: "+this.portaRemote);
        System.out.println("banco...: "+this.databaseRemote);
        System.out.println("user....: "+this.userRemote);
        System.out.println("senha...: "+this.passwordRemote);
        
        System.out.println("P01.....: "+funcoes.get("P01"));
        System.out.println("P02.....: "+funcoes.get("P02"));
        System.out.println("P03.....: "+funcoes.get("P03"));
        System.out.println("P04.....: "+funcoes.get("P04"));
        System.out.println("P05.....: "+funcoes.get("P05"));
        System.out.println("P06.....: "+funcoes.get("P06"));
        System.out.println("P07.....: "+funcoes.get("P07"));
        System.out.println("P08.....: "+funcoes.get("P08"));
        System.out.println("P09.....: "+funcoes.get("P09"));
        System.out.println("P10.....: "+funcoes.get("P10"));
        System.out.println("P11.....: "+funcoes.get("P11"));
        
    }
    
}
