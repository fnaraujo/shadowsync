package br.com.util;

import br.com.controle.Sistema;
import br.com.controle.exception.InfraEstruturaException;
import br.com.modelo.Bairro;
import br.com.modelo.Cargo;
import br.com.modelo.Categoria;
import br.com.modelo.Cep;
import br.com.modelo.Cliente;
import br.com.modelo.CodigoRelacao;
import br.com.modelo.CodigoTributario;
import br.com.modelo.ContatoCliente;
import br.com.modelo.CstIPI;
import br.com.modelo.CstPisCofins;
import br.com.modelo.Empresa;
import br.com.modelo.EnderecoCliente;
import br.com.modelo.EstoquePreco;
import br.com.modelo.EstoqueProduto;
import br.com.modelo.Fabricante;
import br.com.modelo.FormaPagamento;
import br.com.modelo.FormaPagamentoPrazo;
import br.com.modelo.Funcionario;
import br.com.modelo.Grupo;
import br.com.modelo.ItemVenda;
import br.com.modelo.LogStatusVenda;
import br.com.modelo.Loja;
import br.com.modelo.LojaDepartamento;
import br.com.modelo.Municipio;
import br.com.modelo.Ncm;
import br.com.modelo.PagamentoVenda;
import br.com.modelo.Produto;
import br.com.modelo.ShadowsyncInfo;
import br.com.modelo.StatusFunc;
import br.com.modelo.StatusVenda;
import br.com.modelo.TabelaPreco;
import br.com.modelo.UnidadeFederacao;
import br.com.modelo.UnidadeMedida;
import br.com.modelo.Venda;
import java.util.Properties;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * Hibernate Utility class with a convenient method to get Session Factory
 * object.
 *
 * @author flavio.nunes
 */
public class HibernateUtil {

    private static final SessionFactory sessionFactory;
    private static final SessionFactory sessionFactoryRemote;
    private static final ThreadLocal<Session> threadLocal = new ThreadLocal<>();
    private static final ThreadLocal<Session> threadLocalRemote = new ThreadLocal<>();
    public static final String URL;
    public static final String USER;
    public static final String SENHA;

    static {
        try {

            Sistema.getInstance();
            Configuration cfg = new Configuration().configure(Sistema.HIBERNATE_STREAM);
            Configuration cfgRemote;

            Properties p = new Properties();
            p.setProperty("hibernate.connection.url", Sistema.configuracao.getURLConnectionLocal());
            cfg.addProperties(p);

            cfg.addAnnotatedClass(Empresa.class);
            cfg.addAnnotatedClass(Loja.class);
            cfg.addAnnotatedClass(LojaDepartamento.class);

            cfg.addAnnotatedClass(Funcionario.class);
            cfg.addAnnotatedClass(StatusFunc.class);
            cfg.addAnnotatedClass(Cargo.class);
            cfg.addAnnotatedClass(Cep.class);
            cfg.addAnnotatedClass(Bairro.class);
            cfg.addAnnotatedClass(Municipio.class);
            cfg.addAnnotatedClass(UnidadeFederacao.class);

            cfg.addAnnotatedClass(FormaPagamento.class);
            cfg.addAnnotatedClass(FormaPagamentoPrazo.class);

            cfg.addAnnotatedClass(CstIPI.class);
            cfg.addAnnotatedClass(CstPisCofins.class);
            cfg.addAnnotatedClass(CodigoTributario.class);
            cfg.addAnnotatedClass(Ncm.class);
            cfg.addAnnotatedClass(Grupo.class);
            cfg.addAnnotatedClass(UnidadeMedida.class);
            cfg.addAnnotatedClass(Fabricante.class);
            cfg.addAnnotatedClass(Produto.class);
            cfg.addAnnotatedClass(CodigoRelacao.class);
            cfg.addAnnotatedClass(Cliente.class);
            cfg.addAnnotatedClass(Categoria.class);
            cfg.addAnnotatedClass(EstoqueProduto.class);
            cfg.addAnnotatedClass(EstoquePreco.class);
            cfg.addAnnotatedClass(TabelaPreco.class);

            cfg.addAnnotatedClass(Venda.class);
            cfg.addAnnotatedClass(ItemVenda.class);
            cfg.addAnnotatedClass(PagamentoVenda.class);
            
            cfg.addAnnotatedClass(LogStatusVenda.class);
            cfg.addAnnotatedClass(StatusVenda.class);
            
            cfg.addAnnotatedClass(EnderecoCliente.class);
            cfg.addAnnotatedClass(ContatoCliente.class);
            cfg.addAnnotatedClass(ShadowsyncInfo.class);

            ServiceRegistryBuilder registry = new ServiceRegistryBuilder();
            registry.applySettings(cfg.getProperties());
            ServiceRegistry serviceRegistry = registry.buildServiceRegistry();

            sessionFactory = cfg.buildSessionFactory(serviceRegistry);
            System.out.println("CONFIGURATION FACTORY LOCAL OK...");

            URL = cfg.getProperty("hibernate.connection.url");
            USER = cfg.getProperty("hibernate.connection.username");
            SENHA = cfg.getProperty("hibernate.connection.password");

            Properties pRemote = new Properties();
            pRemote.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
            pRemote.setProperty("hibernate.connection.provider_class", "org.hibernate.connection.C3P0ConnectionProvider");
            pRemote.setProperty("hibernate.connection.driver_class", "org.postgresql.Driver");
//            pRemote.setProperty("hibernate.connection.url", "jdbc:postgresql://54.69.162.119:5433/sispec");
//            pRemote.setProperty("hibernate.connection.url", "jdbc:postgresql://localhost:5432/MTIGRAOREMOTE");
//            pRemote.setProperty("hibernate.connection.url", "jdbc:postgresql://localhost:5432/DBSHADOWREMOTE");
            pRemote.setProperty("hibernate.connection.url", Sistema.configuracao.getURLConnectionRemote());
            pRemote.setProperty("hibernate.connection.charSet", "utf-8");
            pRemote.setProperty("hibernate.connection.username", Sistema.configuracao.getUserRemote());
            pRemote.setProperty("hibernate.connection.password", Sistema.configuracao.getPasswordRemote());
            pRemote.setProperty("hibernate.show_sql", "true");
            pRemote.setProperty("hibernate.current_session_context_class", "thread");
            pRemote.setProperty("hibernate.use_sql_comments", "false");
            pRemote.setProperty("hibernate.order_updates", "true");
            pRemote.setProperty("hibernate.c3p0.min_size", "1");
            pRemote.setProperty("hibernate.c3p0.max_size", "20");
            pRemote.setProperty("hibernate.c3p0.idle_test_period", "3000");
            pRemote.setProperty("hibernate.c3p0.acquire_increment", "2");
            pRemote.setProperty("hibernate.c3p0.timeout", "30");
            pRemote.setProperty("hibernate.jdbc.batch_size", "30");
            pRemote.setProperty("hibernate.temp.use_jdbc_metadata_defaults", "false");
            pRemote.setProperty("max_fetch_depth", "4");
            cfgRemote = new Configuration();
            cfgRemote.setProperties(pRemote);

            cfgRemote.addAnnotatedClass(Empresa.class);
            cfgRemote.addAnnotatedClass(Loja.class);
            cfgRemote.addAnnotatedClass(LojaDepartamento.class);

            cfgRemote.addAnnotatedClass(Funcionario.class);
            cfgRemote.addAnnotatedClass(StatusFunc.class);
            cfgRemote.addAnnotatedClass(Cargo.class);
            cfgRemote.addAnnotatedClass(Cep.class);
            cfgRemote.addAnnotatedClass(Bairro.class);
            cfgRemote.addAnnotatedClass(Municipio.class);
            cfgRemote.addAnnotatedClass(UnidadeFederacao.class);

            cfgRemote.addAnnotatedClass(FormaPagamento.class);
            cfgRemote.addAnnotatedClass(FormaPagamentoPrazo.class);

            cfgRemote.addAnnotatedClass(CstIPI.class);
            cfgRemote.addAnnotatedClass(CstPisCofins.class);
            cfgRemote.addAnnotatedClass(CodigoTributario.class);
            cfgRemote.addAnnotatedClass(Ncm.class);
            cfgRemote.addAnnotatedClass(Grupo.class);
            cfgRemote.addAnnotatedClass(UnidadeMedida.class);
            cfgRemote.addAnnotatedClass(Fabricante.class);
            cfgRemote.addAnnotatedClass(Produto.class);
            cfgRemote.addAnnotatedClass(CodigoRelacao.class);
            cfgRemote.addAnnotatedClass(Cliente.class);
            cfgRemote.addAnnotatedClass(Categoria.class);
            cfgRemote.addAnnotatedClass(EstoqueProduto.class);
            cfgRemote.addAnnotatedClass(TabelaPreco.class);
            cfgRemote.addAnnotatedClass(EstoquePreco.class);

            cfgRemote.addAnnotatedClass(Venda.class);
            cfgRemote.addAnnotatedClass(ItemVenda.class);
            cfgRemote.addAnnotatedClass(PagamentoVenda.class);
            
            cfgRemote.addAnnotatedClass(EnderecoCliente.class);
            cfgRemote.addAnnotatedClass(ContatoCliente.class);
            cfgRemote.addAnnotatedClass(ShadowsyncInfo.class);

            System.out.println("ADICIONOU AS CONFIGURACOES");

            ServiceRegistryBuilder registryRemote = new ServiceRegistryBuilder();
            registryRemote.applySettings(cfgRemote.getProperties());
            System.out.println("APPLY SETTINGS REMOTE");
            ServiceRegistry serviceRegistryRemote = registryRemote.buildServiceRegistry();
            System.out.println("SERVICES REGISTRY REMOTE");

            sessionFactoryRemote = cfgRemote.buildSessionFactory(serviceRegistryRemote);
            System.out.println("FACTORY WEB INICIADA...");

        } catch (Throwable ex) {
            // Log the exception.
            ex.printStackTrace();
            System.err.println("ERRO >>>> Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    private static Document getDocumentFile() throws Exception {
        return getDocumentFile(null);
    }

    private static Document getDocumentFile(String url) throws Exception {
        DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
        Document doc = docBuilder.parse(Sistema.HIBERNATE_STREAM.toString());
        Element document = doc.getDocumentElement();
        NodeList list = document.getElementsByTagName("property");
        for (int i = 0; i < list.getLength(); i++) {
            if (list.item(i).getAttributes().getNamedItem("name").getNodeValue().equals("hibernate.connection.url")) {
                if (url != null) {
                    list.item(i).getChildNodes().item(0).setNodeValue(url);
                }
            }
        }
        return doc;
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public static SessionFactory getSessionFactoryRemote() {
        return sessionFactoryRemote;
    }

    public static Session getSession() {
        Session session = null;
        try {
            session = threadLocal.get();
            session = sessionFactory.getCurrentSession();
            threadLocal.set(session);

            // Recupera ou cria uma nova sessÃ£o. Caso a
            // transacao tenha sofrido um commit ou rollback,
            // a sessao tera sido fechada. Neste caso uma
            // nova sessao sera criada. Caso contrario, a
            // sessao corrente sera recuperada.
        } catch (HibernateException e) {	//throw new InfraEstruturaException(e);
        }

        return session;
    }

    public static Session getSessionRemote() {
        Session session = null;
        try {
            session = threadLocalRemote.get();
            session = sessionFactoryRemote.getCurrentSession();
            threadLocalRemote.set(session);

            // Recupera ou cria uma nova sessao. Caso a
            // transacao tenha sofrido um commit ou rollback,
            // a sessao tera sido fechada. Neste caso uma
            // nova sessao sera criada. Caso contrario, a
            // sessao corrente sera recuperada.
        } catch (HibernateException e) {	//throw new InfraEstruturaException(e);
            e.printStackTrace();
        }

        return session;
    }

    public static void beginTransaction() {
        try {
            getSession().beginTransaction();

            // Cria ou recupera a transacao corrente,
            // caso ela nao tenha sofrido commit nem
            // rollback.
        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }
    }

    public static void beginTransactionRemote() {
        try {
            getSessionRemote().beginTransaction();

            // Cria ou recupera a transacao corrente,
            // caso ela nao tenha sofrido commit nem
            // rollback.
        } catch (HibernateException e) {
            throw new InfraEstruturaException(e);
        }
    }

    public static void commitTransaction() {
        try {
            getSession().getTransaction().commit();
            // Efetua o commit da transacao e fecha a sessÃ£o.
        } catch (HibernateException ex) {
            rollbackTransaction();
            throw new InfraEstruturaException(ex);
        }
    }

    public static void commitTransactionRemote() {
        try {
            getSessionRemote().getTransaction().commit();
            // Efetua o commit da transacao e fecha a sessÃ£o.
        } catch (HibernateException ex) {
            rollbackTransactionRemote();
            throw new InfraEstruturaException(ex);
        }
    }

    public static void rollbackTransaction() {
        try {
            getSession().getTransaction().rollback();
            // Efetua o rollback da transacao e fecha a sessÃ£o.
        } catch (HibernateException ex) {	//throw new InfraEstruturaException(ex);
        }
    }

    public static void rollbackTransactionRemote() {
        try {
            getSessionRemote().getTransaction().rollback();
            // Efetua o rollback da transacao e fecha a sessÃ£o.
        } catch (HibernateException ex) {	//throw new InfraEstruturaException(ex);
        }
    }

    public static void flushLocal() {
        try {
            getSession().flush();
            // Efetua o rollback da transacao e fecha a sessÃ£o.
        } catch (HibernateException ex) {	//throw new InfraEstruturaException(ex);
        }
    }

    public static void flushRemote() {
        try {
            getSessionRemote().flush();
            // Efetua o rollback da transacao e fecha a sessÃ£o.
        } catch (HibernateException ex) {	//throw new InfraEstruturaException(ex);
        }
    }
}
